import 'package:flutter/material.dart';
import 'package:hab_app/src/models/mealTypeDto.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/pages/add/addMealWidget.dart';
import 'package:hab_app/src/pages/add/addTaskWidget.dart';
import 'package:hab_app/src/pages/add/addWaterWidget.dart';
import 'package:hab_app/src/pages/add/addWorkoutWidget.dart';
import 'package:hab_app/src/pages/chat/chatRoomWidget.dart';
import 'package:hab_app/src/pages/classRoom/myClassDetail.dart';
import 'package:hab_app/src/pages/classRoom/myClassExercisesDetail.dart';
import 'package:hab_app/src/pages/classRoom/myClassSessionDetail.dart';
import 'package:hab_app/src/pages/classRoom/myClassWidget.dart';
import 'package:hab_app/src/pages/courses/courseDetail.dart';
import 'package:hab_app/src/pages/courses/courses.dart';
import 'package:hab_app/src/pages/debug.dart';
import 'package:hab_app/src/pages/healthIndexes/myHealthIndexEdit.dart';
import 'package:hab_app/src/pages/healthIndexes/myHealthIndexes.dart';
import 'package:hab_app/src/pages/home/HomeSetingWidget.dart';
import 'package:hab_app/src/pages/notification/NotificationsWidget.dart';
import 'package:hab_app/src/pages/page.dart';
import 'package:hab_app/src/pages/profile/change_password.dart';
import 'package:hab_app/src/pages/profile/f1_note.dart';
import 'package:hab_app/src/pages/profile/f1_search_user.dart';
import 'package:hab_app/src/pages/profile/f1_user.dart';
import 'package:hab_app/src/pages/profile/f1_user_view.dart';
import 'package:hab_app/src/pages/profile/parent_user.dart';
import 'package:hab_app/src/pages/profile/profile_edit.dart';
import 'package:hab_app/src/pages/profile/salesTarget.dart';
import 'package:hab_app/src/pages/profile/settings.dart';
import 'package:hab_app/src/pages/share/otp.dart';
import 'package:hab_app/src/pages/sign_in/forget_password.dart';
import 'package:hab_app/src/pages/sign_in/forget_password_change.dart';
import 'package:hab_app/src/pages/sign_in/login.dart';
import 'package:hab_app/src/pages/sign_in/register.dart';
import 'package:hab_app/src/pages/sign_in/register_set_password.dart';
import 'package:hab_app/src/pages/splash_screen.dart';
import 'package:hab_app/src/pages/zooom/zoomWidget.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    Widget page;
    switch (settings.name) {
      case '/Debug':
        page = DebugWidget(routeArgument: args as RouteArgument);
        break;
      case '/Splash':
        page = SplashScreenWidget();
        break;
      case '/Login':
        page = LoginWidget(phoneNumber: args as String);
        break;
      case '/Register':
        page = RegisterWidget();
        break;
      case '/Register/SetPassword':
        page = RegisterSetPassword(routeArgument: args as RouteArgument);
        break;
      case '/ForgetPassword':
        page = ForgetPasswordWidget(phoneNumber: args as String);
        break;
      case '/ForgetPassword/ChangePassword':
        page = ForgetPasswordChangeWidget(routeArgument: args as RouteArgument);
        break;
      case '/OTP':
        page = OTPWidget(argument: args as OTPArgument);
        break;
      case '/Page':
        page = PagesWidget(currentTab: args);
        break;
      case '/Work':
        page = PagesWidget(currentTab: 1);
        break;
      case '/Chat':
        page = PagesWidget(currentTab: 3);
        break;
      // case '/Home':
      //   page = PagesWidget(currentTab: 0);
      //   break;
      case '/Home/Seting':
        page = HomeSetingWidget();
        break;
      case '/Notifications':
        page = NotificationsWidget();
        break;
      case '/Profile/ChangePassword':
        page = ChangePasswordWidget();
        break;
      case '/ParentUser':
        page = ParentUserWidget();
        break;
      case '/F1/User':
        page = F1UserWidget();
        break;
      case '/F1/Search':
        page = F1SearchUserWidget();
        break;
      case '/F1/Note':
        page = F1NoteWidget(routeArgument: args as RouteUserArgument);
        break;
      case '/F1/User/View':
        page = F1UserViewWidget(routeArgument: args as RouteUserArgument);
        break;
      case '/Profile/Edit':
        page = EditProfileWidget(onLoad: args as Function);
        break;
      case '/Profile/Settings':
        page = SettingsWidget();
        break;

      /// mục tiêu doanh số
      case '/SalesTarget':
        page = SalesTargetWidget();
        break;

      /// danh sách khóa học
      case '/Courses':
        page = CoursesWidget();
        break;
      case '/Course/Detail':
        page = CourseDetailWidget(routeArgument: args as RouteArgument);
        break;

      ///  khóa học của tôi
      case '/MyClass':
        page = MyClassWidget();
        break;
      // lớp học
      case '/MyClass/Detail':
        page = MyClassDetailWidget(routeArgument: args as RouteClassArgument);
        break;
      // bài học
      case '/MyClass/Session/Detail':
        page = MyClassSessionDetailWidget(routeArgument: args as RouteClassArgument);
        break;
      // bài tập
      case '/MyClass/Exercise/Detail':
        page = MyClassExercisesDetailWidget(routeArgument: args as RouteClassArgument);
        break;
      // // chữa bài tập
      // case '/MyClass/Exercise/Post/Add':
      //   page = ExercisePostAddWidget(routeArgument: args as RouteClassArgument);
      //   break;
      // vào link zoom - vào học
      case '/MyClass/Zoom':
        page = ZoomWidget(routeArgument: args as RouteClassArgument);
        break;

      /// chat
      case '/Chat/Search':
        page = PagesWidget(currentTab: 30);
        break;
      case '/Chat/Room':
        page = ChatRoomWidget(roomId: args as int);
        break;

      /// add
      case '/Add/Meal':
        page = AddMealWidget(type: args as MealTypeDto);
        break;
      case '/Add/Water':
        page = AddWaterWidget();
        break;
      case '/Add/Workout':
        page = AddWorkoutWidget();
        break;
      case '/Add/Task':
        page = AddTaskWidget();
        break;

      case '/MyHealth':
        page = MyHealthIndexesWidget();
        break;
      case '/MyHealth/Edit':
        page = HealthIndexEditWidget(routeArgument: args as RouteArgument);
        break;
      default:
        page = SafeArea(child: Text('Route Error'));
        break;
    }
    print("settings.name ${settings.toString()}");
    print("settings.args ${(args.toString())}");

    return MaterialPageRoute(
      settings: settings,
      builder: (_) => MediaQuery(data: MediaQuery.of(_).copyWith(textScaleFactor: 1), child: page),
    );
  }
}
