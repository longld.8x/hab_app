import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hab_app/route_gen.dart';
import 'package:hab_app/src/models/setting.dart';
import 'package:loader_overlay/loader_overlay.dart';

import 'src/helpers/app_config.dart' as config;
import 'src/repositories/firebase_repository.dart' as firebaseRepo;
import 'src/repositories/setting_repository.dart' as settingRepo;
import 'src/repositories/socket_repository.dart' as socketRepo;
import 'src/repositories/user_repository.dart' as userRepo;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GlobalConfiguration().loadFromAsset("configurations");
  runApp(MyApp());
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    systemNavigationBarColor: Colors.transparent, // navigation bar color
    statusBarColor: Colors.transparent, //config.Colors.mainColor(), // status bar color
    statusBarIconBrightness: Brightness.dark,
    statusBarBrightness: Brightness.dark,
  ));
  await FlutterDownloader.initialize(debug: true);
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    settingRepo.initSettings();
    userRepo.getCurrentUser().then((value) {
      if (value != null && value.id != null) socketRepo.connect();
    });
    firebaseRepo.initFcm();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // return LifeCycleManager(
    //   child:
    final botToastBuilder = BotToastInit();
    return ValueListenableBuilder(
      valueListenable: settingRepo.setting,
      builder: (context, Setting _setting, _) {
        // print(CustomTrace(StackTrace.current, message: _setting.toMap().toString()));
        // return new MaterialApp(

        return GlobalLoaderOverlay(
          useDefaultLoading: true,
          overlayOpacity: 0.7,
          overlayColor: config.Colors.customeColor("#000000"),
          child: new MaterialApp(
            localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: [
              const Locale('vi'), // English
            ],
            builder: (BuildContext context, Widget child) {
              child = botToastBuilder(context, child);
              return child;
            },
            navigatorObservers: [BotToastNavigatorObserver()],
            navigatorKey: settingRepo.navigatorKey,
            scaffoldMessengerKey: settingRepo.scaffoldMessengerKey,
            title: _setting.appName,
            initialRoute: '/Splash',
            onGenerateRoute: RouteGenerator.generateRoute,
            debugShowCheckedModeBanner: false,
            locale: _setting.mobileLanguage.value,
            theme: ThemeData(
              fontFamily: 'Roboto',
              floatingActionButtonTheme: FloatingActionButtonThemeData(elevation: 0, foregroundColor: Colors.white),
              brightness: _setting.brightness.value,
              backgroundColor: config.Colors.backgroundColor(),
              primaryColor: config.Colors.primaryColor(),
              accentColor: config.Colors.accentColor(),
              focusColor: config.Colors.secondColor(),
              hintColor: config.Colors.hintColor(),
              accentIconTheme: IconThemeData(size: 18, color: config.Colors.accentColor(), opacity: 1),
              primaryIconTheme: IconThemeData(size: 18, color: Color(0xFF363636), opacity: 1),
              iconTheme: IconThemeData(size: 18),
              inputDecorationTheme: InputDecorationTheme(
                labelStyle: Theme.of(context).textTheme.caption,
                hintStyle: Theme.of(context).textTheme.caption, //TextStyle(fontStyle: Theme.of(context).textTheme.caption, color: Theme.of(context).textTheme.caption.color.withOpacity(0.5)),
                errorStyle: TextStyle(fontWeight: FontWeight.w500, color: Color(0xFFF5222D)),
                border: UnderlineInputBorder(borderSide: BorderSide(color: config.Colors.inputBgColor()), borderRadius: BorderRadius.circular(12.0)),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: config.Colors.inputBgColor()), borderRadius: BorderRadius.circular(12.0)),
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: config.Colors.inputBgColor()), borderRadius: BorderRadius.circular(12.0)),
                errorBorder: UnderlineInputBorder(borderSide: BorderSide(color: config.Colors.inputBgColor()), borderRadius: BorderRadius.circular(12.0)),
                focusedErrorBorder: InputBorder.none,
                contentPadding: EdgeInsets.all(18),
              ),
              buttonTheme: ButtonThemeData(
                height: 54,
                buttonColor: config.Colors.primaryColor(),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(14.0),
                ),
              ),
              textTheme: TextTheme(
                headline6: TextStyle(fontSize: 14, fontWeight: FontWeight.w600, color: config.Colors.secondColor(), height: 1.35),
                headline5: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: config.Colors.secondColor(), height: 1.35),
                headline4: TextStyle(fontSize: 18, fontWeight: FontWeight.w600, color: config.Colors.secondColor(), height: 1.35),
                headline3: TextStyle(fontSize: 20, fontWeight: FontWeight.w600, color: config.Colors.secondColor(), height: 1.35),
                headline2: TextStyle(fontSize: 22, fontWeight: FontWeight.w600, color: config.Colors.secondColor(), height: 1.35),
                headline1: TextStyle(fontSize: 24, fontWeight: FontWeight.w600, color: config.Colors.secondColor(), height: 1.35),
                bodyText2: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: config.Colors.secondColor(), height: 1.35),
                bodyText1: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: config.Colors.secondColor(), height: 1.35),
                caption: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: config.Colors.hintColor(), height: 1.35),
                subtitle1: TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: config.Colors.secondColor(), height: 1),
                subtitle2: TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: config.Colors.secondColor(), height: 1),
              ).apply(),
              scaffoldBackgroundColor: config.Colors.backgroundColor(),
              appBarTheme: AppBarTheme(
                color: config.Colors.backgroundColor(),
                iconTheme: IconThemeData(color: config.Colors.customeColor("000000")),
                elevation: 0,
                centerTitle: true,
                brightness: _setting.brightness.value,
              ),
            ),
          ),
        );
      },
    );
  }
}
