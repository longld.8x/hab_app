import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/import.dart';
import 'package:hab_app/src/models/chat/roomDataDto.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/models/target.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:intl/intl.dart';

class ListViewItem extends StatelessWidget {
  final List<dynamic> data;
  final Function onView;
  final double height;
  final bool isNeverScroll;
  final ScrollController controller;
  const ListViewItem(this.data, this.onView, {Key key, this.controller, this.height, this.isNeverScroll = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget _listView() {
      return new ListView.builder(
        shrinkWrap: true,
        controller: controller != null ? controller : new ScrollController(),
        physics: isNeverScroll == false ? BouncingScrollPhysics() : NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) => onView(data[index], index),
        itemCount: data != null ? data.length : 0,
        padding: EdgeInsets.all(0),
      );
    }

    return Container(
      width: config.App.appWidth(100),
      height: height,
      child: _listView(),
    );
  }
}

class UserViewItem extends StatelessWidget {
  final User data;
  final BoxDecoration deco;
  final Function onTap;

  const UserViewItem(this.data, {Key key, this.onTap, this.deco}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomeViewItem(
      data.fullName,
      data.profilePicture,
      0,
      sub: SubText(
        data != null && data.phoneNumber != null ? data.phoneNumber : "09xx xxx xxx",
        style: TextStyle(color: config.Colors.hintColor()),
      ),
      deco: deco,
      status: data.isOnline,
      icon: Icon(
        Icons.arrow_forward_ios,
        size: 16,
      ),
      onTap: (_, __) {
        if (onTap != null)
          onTap(data);
        else {
          Navigator.of(context).pushNamed('/F1/User/View', arguments: RouteUserArgument(id: data.id.toString(), level: "1", param: data));
        }
      },
    );
  }
}

class TargetViewItem extends StatelessWidget {
  final Target data;
  final Function onTap;
  const TargetViewItem(this.data, {Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GlobalKey<AnimatedCircularChartState> chartKey = new GlobalKey<AnimatedCircularChartState>();
    var _dateView = "";
    if (_dateView != null && data.startDate != null) {
      _dateView += (DateFormat('dd/MM/yyyy').format(data.startDate));
    }
    if (_dateView != null && data.endDate != null) {
      if (_dateView.isNotEmpty) _dateView += " - ";
      _dateView += (DateFormat('dd/MM/yyyy').format(data.endDate));
    }
    var _boxWidth = 80;
    var _chartWidget = new AnimatedCircularChart(
      key: chartKey,
      size: new Size(_boxWidth * 0.99, _boxWidth * 0.99),
      initialChartData: <CircularStackEntry>[
        new CircularStackEntry(
          <CircularSegmentEntry>[
            new CircularSegmentEntry(
              data.currentPoint,
              config.Colors.primaryColor(),
              rankKey: 'Calo',
            ),
            new CircularSegmentEntry(
              data.totalPoint,
              config.Colors.inputBgColor(),
              rankKey: 'Còn thiếu',
            ),
          ],
          rankKey: 'points',
        ),
      ],
      holeLabel: "50%",
      labelStyle: TextStyle(color: config.Colors.accentColor(), fontSize: 12, fontWeight: FontWeight.w500),
      chartType: CircularChartType.Radial,
      duration: Duration(milliseconds: 600),
      edgeStyle: SegmentEdgeStyle.round,
      percentageValues: true,
      holeRadius: _boxWidth * 0.22,
    );

    return Container(
      width: config.App.appWidth(100),
      height: 80,
      decoration: BoxDecoration(
        color: config.Colors.backgroundColor(),
        borderRadius: BorderRadius.all(const Radius.circular(10.0)),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 2),
            color: Color(0xFF000000).withOpacity(0.16),
            spreadRadius: 1,
            blurRadius: 4,
          ),
        ],
      ),
      margin: EdgeInsets.only(bottom: 16, left: 16, right: 16),
      padding: EdgeInsets.only(top: 10, left: 16, right: 16, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(width: 50, height: 50, child: _chartWidget),
          Container(
            padding: EdgeInsets.only(left: 15, right: 15),
            width: config.App.appWidth(100) - (50 + 32) - 32 - 32,
            child: ColumnStart(
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: AccentText(data != null && data.name != null ? data.name : "", style: TextStyle(fontWeight: FontWeight.w500)),
                ),
                SubText(_dateView, style: TextStyle(color: config.Colors.customeColor("#979797"))),
              ],
            ),
          ),
          Container(
            width: 32,
            height: 32,
            child: Icon(
              Icons.arrow_forward_ios,
              size: 16,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(const Radius.circular(50.0)),
            ),
          ),
        ],
      ),
    ).onTap(() {
      if (onTap != null)
        onTap(data);
      else {
        // Navigator.of(context).pushNamed('/UserChild', arguments: RouteArgument(id: data.id.toString(), tag: data.phoneNumber, param: data));
      }
    });
  }
}

class SearchViewItem extends StatelessWidget {
  final String data;
  final Function onTap;
  final Function onTapIcon;
  const SearchViewItem(this.data, {Key key, this.onTap, this.onTapIcon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: config.App.appWidth(100),
      height: 50,
      decoration: BoxDecoration(
        color: config.Colors.backgroundColor(),
      ),
      margin: EdgeInsets.only(bottom: 1),
      padding: EdgeInsets.only(top: 0, left: 30, right: 0, bottom: 0),
      child: RowWidget(
        Container(
          child: AccentText(data),
        ).onTap(() {
          if (onTap != null) onTap(data);
        }),
        Container(
          // padding: EdgeInsets.only(top: 0, left: 16, right: 18, bottom: 0),
          child: IconButton(
            icon: Icon(Icons.close, size: 16, color: config.Colors.customeColor("#979797")),
            padding: EdgeInsets.all(12),
            onPressed: () {
              if (onTapIcon != null) onTapIcon(data);
            },
          ),
        ),
        flex: 82,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
      ),
    );
  }
}

class CustomeViewItem extends StatelessWidget {
  final String fullName;
  final String profilePicture;
  final Widget sub;
  final Widget icon;
  final int index;
  final bool status;
  final Function onTap;
  final BoxDecoration deco;

  const CustomeViewItem(this.fullName, this.profilePicture, this.index, {Key key, this.sub, this.icon, this.deco, this.status, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: config.App.appWidth(100),
      height: 70,
      decoration: deco != null
          ? deco
          : BoxDecoration(
              color: config.Colors.backgroundColor(),
            ),
      margin: EdgeInsets.only(bottom: 1),
      padding: EdgeInsets.only(top: 10, left: 16, right: 16, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          UserAvatar(profilePicture, status: status),
          Container(
            padding: EdgeInsets.only(top: 4, left: 15, right: 15),
            width: config.App.appWidth(100) - 100 - 32,
            child: ColumnStart(
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: AccentText(fullName != null ? fullName : "", style: TextStyle(fontWeight: FontWeight.w500)),
                ),
                sub
              ],
            ),
          ),
          icon != null
              ? Container(
                  width: 50,
                  height: 50,
                  child: icon,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(const Radius.circular(50.0)),
                  ),
                )
              : Container(),
        ],
      ),
    ).onTap(() {
      if (onTap != null) this.onTap(fullName, profilePicture);
    });
  }
}

class RoomChatViewItem extends StatelessWidget {
  final RoomDataDto data;
  final Function onTap;
  final User user;
  final int index;
  const RoomChatViewItem(this.data, this.user, this.index, {Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isGroup = !(data.user1 != null && data.user2 != null);
    Widget _avatar;
    String _name;
    if (isGroup) {
      _avatar = UserAvatar(data.room.photo);
      _name = data.room.name;
    } else {
      _avatar = UserAvatar(data.user1?.ref_user_id == user.id ? data.user2?.avatar : data.user1?.avatar);
      _name = data.user1.ref_user_id == user.id ? data.user2.name : data.user1.name;
    }
    var _isSeen = data.lastMessage?.seen == true || data.lastMessage?.user?.ref_user_id == user.id;
    var _msgImage = data.lastMessage.message_type == 'IMAGE';
    return Container(
      width: config.App.appWidth(100),
      decoration: BoxDecoration(
        color: config.Colors.backgroundColor(),
      ),
      margin: EdgeInsets.only(bottom: 1),
      padding: EdgeInsets.only(top: 10, left: 16, right: 16, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          _avatar,
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 16, right: 0),
              child: ColumnStart(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.only(bottom: 5),
                          child: AccentText(
                            _name,
                            style: TextStyle(fontSize: 16, fontWeight: (_isSeen) ? FontWeight.w400 : FontWeight.w500),
                          ),
                        ),
                      ),
                      Container(
                        child: SubText(
                          Helper.showTimeText(data.lastMessage.createdAt),
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: config.Colors.customeColor("#979797"),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SubText(
                    _msgImage ? "[Hình ảnh]" : data.lastMessage.message,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: (_isSeen) ? FontWeight.w400 : FontWeight.w500,
                      color: config.Colors.customeColor(_isSeen ? "#979797" : "#2A2A2A"),
                    ),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ).onTap(() => Navigator.of(context).pushNamed('/Chat/Room', arguments: data.room.id));
  }
}
