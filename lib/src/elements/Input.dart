import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../src/helpers/app_config.dart' as config;

class InputField extends StatelessWidget {
  const InputField({
    Key key,
    this.labelText,
    this.hintText,
    this.prefixIcon,
    this.suffixIcon,
    this.controller,
    this.onSaved,
    this.onChanged,
    this.onEditingComplete,
    this.validator,
    this.keyboardType = TextInputType.text,
    this.autovalidate = false,
    this.obscureText = false,
    this.readOnly = false,
    this.enableInteractiveSelection = true,
    this.maxLength = -1,
    this.onTap,
    this.decoration,
    this.style,
    this.mask,
    this.toUpperCase = false,
    this.textAlign = TextAlign.left,
  }) : super(key: key);

  final TextInputType keyboardType;
  final String labelText;
  final String hintText;
  final bool autovalidate;
  final bool obscureText;
  final bool readOnly;
  final bool enableInteractiveSelection;
  final Function onEditingComplete;
  final TextEditingController controller;
  final ValueChanged<String> onChanged;
  final FormFieldValidator<String> validator;
  final FormFieldSetter<String> onSaved;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final int maxLength;
  final Function onTap;
  final InputDecoration decoration;
  final TextStyle style;
  final TextInputFormatter mask;
  final bool toUpperCase;
  final TextAlign textAlign;

  @override
  Widget build(BuildContext context) {
    List<TextInputFormatter> _inputFormatters() {
      List<TextInputFormatter> arr = [];
      if (maxLength > -1) arr.add(new LengthLimitingTextInputFormatter(maxLength));
      if (mask != null) arr.add(mask);
      if (toUpperCase == true) arr.add(new AutoCapWordsInputFormatter());

      return arr;
    }

    InputDecoration _decoration = decoration != null
        ? decoration
        : InputDecoration(
            filled: true,
            fillColor: config.Colors.inputBgColor(),
            labelText: labelText,
            hintText: hintText,
            prefixIcon: prefixIcon,
            suffixIcon: suffixIcon,
            errorStyle: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.errorColor()),
          );

    TextStyle _textStyle = Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor());
    if (style != null) _textStyle = _textStyle.merge(style);

    return TextFormField(
      controller: controller != null ? controller : null,
      enableInteractiveSelection: enableInteractiveSelection,
      readOnly: readOnly,
      autovalidate: autovalidate,
      keyboardType: keyboardType,
      obscureText: obscureText,
      validator: validator,
      onSaved: onSaved,
      onChanged: onChanged,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(FocusNode());
        if (onEditingComplete != null) onEditingComplete();
      },
      decoration: _decoration,
      style: _textStyle,
      inputFormatters: _inputFormatters(),
      onTap: onTap,
      textAlign: textAlign,
    );
  }
}

class AutoCapWordsInputFormatter extends TextInputFormatter {
  final RegExp capWordsPattern = new RegExp(r'(\w)(\w*\s*)');

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    //String newText = capWordsPattern.allMatches(newValue.text).map((match) => match.group(1).toUpperCase() + match.group(2)).join();
    String newText = newValue.text.toUpperCase();
    return new TextEditingValue(
      text: newText,
      selection: newValue.selection ?? const TextSelection.collapsed(offset: -1),
      composing: newText == newValue.text ? newValue.composing : TextRange.empty,
    );
  }
}
