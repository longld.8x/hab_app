import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hab_app/src/elements/Body.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';

class LayoutManager extends StatelessWidget {
  GlobalKey<ScaffoldState> scaffoldKey;
  PreferredSizeWidget appBar;
  Widget body;
  Widget main;
  Function onWillPop;
  final bool isContentPadding;
  final bool isSingleChildScrollView;
  LayoutManager({
    Key key,
    this.scaffoldKey,
    this.appBar,
    this.body,
    this.main,
    this.onWillPop,
    this.isContentPadding = false,
    this.isSingleChildScrollView = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return KeyboardVisibilityBuilder(builder: (context, visible) {
      // print("KeyboardVisibilityBuilder $visible");
      // print("scaffoldKey $scaffoldKey");
      return WillPopScope(
        onWillPop: () {
          Helper.of(context).onWillBack(visible, onAction: onWillPop);
        },
        child: Scaffold(
          key: scaffoldKey,
          // resizeToAvoidBottomPadding: false,
          appBar: appBar,
          body: main != null
              ? main
              : BodyManager(
                  child: body,
                  isContentPadding: isContentPadding,
                  isSingleChildScrollView: isSingleChildScrollView,
                ),
        ),
      );
    });
  }
}

class LayoutAppbarBgManager extends StatelessWidget {
  GlobalKey<ScaffoldState> scaffoldKey;
  Widget title;
  double titleHeight;
  Widget titleRight;
  Widget main;
  String bgImage;
  //Color mainColor;
  Function onWillPop;
  bool titleFull;
  final bool isContentPadding;
  final bool isSingleChildScrollView;
  //final bool bodyBorder;
  final double bottom;
  LayoutAppbarBgManager({
    Key key,
    this.scaffoldKey,
    this.main,
    this.title,
    this.titleFull = false,
    this.titleRight,
    this.onWillPop,
    // this.mainColor = Colors.white,
    this.bottom = 0,
    this.titleHeight = 56.0,
    this.bgImage = 'assets/icon/profile.png',
    //this.bodyBorder = false,
    this.isContentPadding = false,
    this.isSingleChildScrollView = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double statusbarHeight = MediaQuery.of(context).padding.top;
    final _viewInsetsBottom = MediaQuery.of(context).viewInsets.bottom;
    Widget _appBar() {
      return Container(
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
          color: (bgImage != null && bgImage.isNotEmpty && bgImage.indexOf('#') > -1) ? config.Colors.customeColor(bgImage) : config.Colors.inputBgColor(),
          image: (bgImage != null && bgImage.isNotEmpty && bgImage.indexOf('assets') > -1)
              ? DecorationImage(
                  image: AssetImage(bgImage),
                  fit: BoxFit.fitWidth,
                  alignment: Alignment.topLeft,
                )
              : null,
        ),
        child: Container(
          margin: EdgeInsets.only(top: statusbarHeight, bottom: bottom),
          height: titleHeight,
          width: config.App.appWidth(100),
          alignment: Alignment.center,
          child: titleFull == true
              ? title
              : Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 10,
                        child: Container(
                          padding: EdgeInsets.only(left: 8),
                          child: IconButton(
                            icon: Icon(
                              Icons.arrow_back_ios,
                              size: 18,
                              color: config.Colors.customeColor("#ffffff"),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 80,
                        child: Container(
                          padding: EdgeInsets.only(right: 0),
                          child: title,
                        ),
                      ),
                      Expanded(flex: 10, child: titleRight != null ? titleRight : Container()),
                    ],
                  ),
                ),
        ),
      );
    }

    Widget _bodyView() {
      return Positioned(
        top: statusbarHeight + titleHeight + bottom,
        child: Container(
          // decoration: BoxDecoration(
          //   color: mainColor,
          //   // borderRadius: bodyBorder ? const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)) : null,
          // ),
          width: config.App.appWidth(100),
          height: config.App.appHeight(100) - (statusbarHeight + 2 + 40 + 24 + (bottom - 10)) - _viewInsetsBottom,
          child: main,
        ),
      );
    }

    return LayoutManager(
      key: key,
      appBar: null,
      scaffoldKey: scaffoldKey,
      onWillPop: onWillPop,
      isContentPadding: isContentPadding,
      isSingleChildScrollView: isSingleChildScrollView,
      main: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[
          _appBar(),
          _bodyView(),
        ],
      ),
    );
  }
}
