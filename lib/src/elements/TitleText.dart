import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;

/// TitleText
class TitleText extends StatelessWidget {
  const TitleText(this.text, {Key key, this.style, this.maxLines}) : super(key: key);
  final String text;
  final TextStyle style;
  final int maxLines;

  @override
  Widget build(BuildContext context) {
    var _style = Theme.of(context).textTheme.headline4.copyWith(color: config.Colors.accentColor());
    if (style != null) _style = _style.merge(style);
    return Text(
      text == null ? "" : text,
      style: _style,
      maxLines: maxLines,
      overflow: maxLines != null && maxLines > 0 ? TextOverflow.ellipsis : TextOverflow.visible,
    );
  }
}
