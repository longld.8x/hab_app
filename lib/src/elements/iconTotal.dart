import 'package:flutter/material.dart';

class IconTotalWidget extends StatelessWidget {
  final int total;
  final Widget icon;
  final Function action;
  IconTotalWidget({Key key, this.total, this.icon, this.action}) : super(key: key);

  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 50,
      child: InkWell(
        onTap: () => action(),
        child: Stack(
          children: [
            Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.only(bottom: 16, right: 20),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(40),
                  ),
                ),
                child: icon,
              ),
            ),
            Positioned(
              bottom: 25,
              right: 10,
              child: total > 0
                  ? Container(
                      padding: EdgeInsets.only(top: 2.0, bottom: 2.0, right: 2.0, left: 2.0),
                      width: 18,
                      child: Text(
                        total < 10 ? total.toString() : ("9+"),
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.caption.merge(
                              TextStyle(color: Colors.white, fontSize: 10), //Theme.of(context).primaryColor
                            ),
                      ),
                      decoration: BoxDecoration(
                        color: Color(0xFFCF1322),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    )
                  : Container(),
              //constraints: BoxConstraints(minWidth: 15, maxWidth: 15, minHeight: 15, maxHeight: 15),
            )
          ],
        ),
      ),
    );
  }
}
