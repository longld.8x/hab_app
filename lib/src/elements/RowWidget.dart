import 'package:flutter/material.dart';

class IconWidget extends StatelessWidget {
  final Widget icon;
  final Widget text;
  final double padding;
  final MainAxisAlignment mainAxisAlignment;
  //final Function onTap;
  const IconWidget(
    this.icon,
    this.text, {
    Key key,
    this.padding = -1,
    this.mainAxisAlignment = MainAxisAlignment.start,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _padding = (padding > -1) ? padding : 10;
    return Container(
      child: RichText(
        text: TextSpan(
          children: [
            WidgetSpan(
              child: Container(
                margin: EdgeInsets.only(bottom: 2),
                child: icon,
              ),
            ),
            WidgetSpan(
              child: Container(
                padding: EdgeInsets.only(left: _padding),
                //margin: EdgeInsets.only(top: 1),
                child: text,
              ),
            ),
          ],
        ),
      ),
    );
    // return Container(
    //   child: Row(
    //     mainAxisAlignment: mainAxisAlignment,
    //     children: [
    //       Container(
    //         padding: EdgeInsets.only(right: 0),
    //         child: icon,
    //       ),
    //       Container(
    //         padding: EdgeInsets.only(left: _padding),
    //         child: text,
    //       ),
    //     ],
    //   ),
    // );
  }
}

class TextIcon extends StatelessWidget {
  final Widget icon;
  final Widget text;
  final double padding;
  const TextIcon(this.text, this.icon, {Key key, this.padding = -1}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _padding = (padding > -1) ? padding : 10;
    return Container(
      child: RichText(
        text: TextSpan(
          children: [
            WidgetSpan(
              child: text,
            ),
            WidgetSpan(
              child: Container(
                padding: EdgeInsets.only(left: _padding),
                child: icon,
              ),
            ),
          ],
        ),
      ),
    );
    // Row(
    //   mainAxisAlignment: MainAxisAlignment.start,
    //   children: [
    //     Container(
    //       padding: EdgeInsets.only(right: 0),
    //       child: text,
    //     ),
    //     Container(
    //       padding: EdgeInsets.only(left: _padding),
    //       child: icon,
    //     ),
    //   ],
    // ),
    //);
  }
}

class RowWidget extends StatelessWidget {
  final Widget widget1;
  final Widget widget2;
  final double padding;
  final MainAxisAlignment mainAxisAlignment;
  final int flex;
  const RowWidget(this.widget1, this.widget2, {Key key, this.padding = -1, this.flex, this.mainAxisAlignment = MainAxisAlignment.start}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _padding = (padding > -1) ? padding : 10;
    if (flex == null)
      return Container(
        child: Row(
          mainAxisAlignment: mainAxisAlignment,
          children: [
            Container(
              padding: EdgeInsets.only(right: 0),
              child: widget1,
            ),
            Container(
              padding: EdgeInsets.only(left: _padding),
              child: widget2,
            ),
          ],
        ),
      );
    return Container(
      child: Row(
        mainAxisAlignment: mainAxisAlignment,
        children: [
          Expanded(
            flex: flex,
            child: Container(
              padding: EdgeInsets.only(right: 0),
              child: widget1,
            ),
          ),
          Expanded(
            flex: 100 - flex,
            child: Container(
              padding: EdgeInsets.only(right: 0),
              child: widget2,
            ),
          ),
        ],
      ),
    );
  }
}
