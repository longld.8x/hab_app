import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:percent_indicator/linear_percent_indicator.dart';

class ProgressBar extends StatelessWidget {
  final Color activeColor;
  final double percent;
  final double width;
  final double horizontal;
  const ProgressBar(this.percent, {Key key, this.activeColor = null, this.width = null, this.horizontal = 6.0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _activeColor = (activeColor != null) ? activeColor : config.Colors.primaryColor();
    var _bgColor = config.Colors.customeColor("#E0E9F8");
    var _percent = percent;
    var _horizontal = horizontal;
    if (percent == null || percent.isNaN) _percent = 0;
    if (percent > 1) {
      _percent = percent - 1;
      _bgColor = _activeColor;
      _activeColor = config.Colors.errorColor();
    }
    if (_percent > 1) _percent = 1;
    if (_horizontal == null || horizontal.isNaN) _horizontal = 0;

    return Container(
      child: LinearPercentIndicator(
        width: width != null ? width : null,
        animation: true,
        animationDuration: 600,
        lineHeight: 10.0,
        percent: _percent,
        linearStrokeCap: LinearStrokeCap.roundAll,
        progressColor: _activeColor,
        backgroundColor: _bgColor,
        padding: EdgeInsets.symmetric(horizontal: _horizontal),
      ),
    );
  }
}
