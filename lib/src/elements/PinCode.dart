import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:pin_code_fields/pin_code_fields.dart';

class PinCodeField extends StatefulWidget {
  final TextEditingController controller;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onCompleted;
  final FormFieldValidator<String> validator;
  final FormFieldSetter<String> onSaved;
  final int length;
  final bool hasError;
  const PinCodeField({
    Key key,
    this.controller,
    this.onSaved,
    this.onChanged,
    this.onCompleted,
    this.validator,
    this.length,
    this.hasError,
  }) : super(key: key);

  _PinCodeState createState() => _PinCodeState();
}

class _PinCodeState extends State<PinCodeField> {
  TextEditingController textEditingController;
  StreamController<ErrorAnimationType> errorController;
  @override
  void initState() {
    super.initState();
    if (widget.controller == null)
      textEditingController = TextEditingController();
    else
      textEditingController = widget.controller;
    errorController = StreamController<ErrorAnimationType>();
  }

  @override
  void dispose() {
    errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PinCodeTextField(
      appContext: context,
      pastedTextStyle: TextStyle(
        color: config.Colors.primaryColor(),
        fontWeight: FontWeight.bold,
      ),
      length: widget.length,
      obscureText: false,
      blinkWhenObscuring: true,
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(5),
        fieldHeight: 55,
        fieldWidth: 60,
        inactiveFillColor: config.Colors.inputBgColor(),
        inactiveColor: widget.hasError ? config.Colors.inputBgColor() : config.Colors.errorColor(),
        activeFillColor: widget.hasError ? config.Colors.primaryColor() : config.Colors.errorColor(),
        activeColor: widget.hasError ? config.Colors.primaryColor() : config.Colors.errorColor(),
        selectedFillColor: config.Colors.primaryColor(),
        selectedColor: config.Colors.primaryColor(),
        borderWidth: 1,
      ),
      cursorColor: config.Colors.primaryColor(),
      animationDuration: Duration(milliseconds: 100),
      backgroundColor: config.Colors.backgroundColor(), // widget.hasError ? config.Colors.backgroundColor() : config.Colors.customeColor("#EE6E70", opacity: 0.15),
      //enableActiveFill: true,
      errorAnimationController: errorController,
      controller: textEditingController,
      keyboardType: TextInputType.number,
      boxShadows: [
        BoxShadow(
          offset: Offset(0, 0),
          color: widget.hasError ? config.Colors.inputBgColor() : config.Colors.customeColor("#EE6E70", opacity: 0.15), //config.Colors.inputBgColor(),
          blurRadius: 0,
        ),
      ],
      textStyle: Theme.of(context).textTheme.headline3.copyWith(color: config.Colors.accentColor()),
      validator: widget.validator,
      onCompleted: widget.onCompleted,
      onChanged: widget.onChanged,
      beforeTextPaste: (text) {
        print("Allowing to paste $text");
        //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
        //but you can show anything you want here, like your pop up saying wrong paste format or etc
        return true;
      },
    );
  }
}
