import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;

class UrlImage extends StatelessWidget {
  final String imageUrl;
  final String defaultImg;
  final double width;
  final double height;
  final BoxFit fit;
  final bool border;
  final double radius;
  UrlImage(this.imageUrl, this.defaultImg, {Key key, this.width, this.height, this.fit = BoxFit.fill, this.radius = 8, this.border = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    boxBorder(Widget child) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(radius),
        child: Container(
          decoration: BoxDecoration(
            color: config.Colors.backgroundColor(),
            borderRadius: BorderRadius.all(Radius.circular(radius)),
            border: border ? new Border.all(width: 1.0, color: config.Colors.customeColor("#F4F9FE")) : null,
          ),
          child: child,
        ),
      );
    }

    var _imageUrl = imageUrl;
    if (_imageUrl == null || _imageUrl.isEmpty) {
      _imageUrl = defaultImg;
    }

    if (_imageUrl == null || _imageUrl.isEmpty) {
      _imageUrl = 'assets/icon/logo.png';
    }

    // lỗi
    try {
      if (_imageUrl.contains('[')) {
        print(_imageUrl);
        var __a = json.decode(imageUrl);
        _imageUrl = __a[0];
      }
    } catch (e) {
      print("e $e");
    }
    // print("imageUrlimageUrlimageUrlimageUrl $imageUrl");

    if (_imageUrl.startsWith('http')) {
      // print('_imageUrl.startsWith(\'http\')');
      if (_imageUrl.endsWith('.svg')) {
        //  print('_imageUrl.endsWith(\'.svg\')');
        return boxBorder(SvgPicture.network(
          _imageUrl.replaceAll('https', 'http'),
          fit: fit,
          width: width,
          height: height,
        ));
      } else {
        // print('_imageUrl. url');
        return boxBorder(
          CachedNetworkImage(
            fit: fit,
            imageUrl: _imageUrl,
            width: width,
            height: height,
            errorWidget: (context, url, error) {
              return Image.asset(
                _imageUrl ?? 'assets/icon/default-course.png',
                fit: fit,
                width: width,
                height: height,
              );
            },
          ),
        );
      }
    } else if (_imageUrl.endsWith('.svg')) {
      //print('_imageUrl.endsWith(\'.svg\')');
      return boxBorder(SvgPicture.asset(
        _imageUrl,
        fit: fit,
        width: width,
        height: height,
      ));
    } else if (_imageUrl.indexOf('assets/') > -1) {
      //print('_imageUrl.indexOf(\'assets\')');
      return boxBorder(Image.asset(
        _imageUrl,
        fit: fit,
        width: width,
        height: height,
      ));
    } else
      return boxBorder(Image.asset(
        defaultImg ?? 'assets/icon/default-course.png',
        fit: fit,
        width: width,
        height: height,
      ));
  }
}
