import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/ImagePick.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/models/chat/messageDto.dart';
import 'package:intl/intl.dart';

Widget MessageWidget(context, {MessageDto message, bool isMe, bool isGroup, MessageDto messageNext, MessageDto messagePrev}) {
  var _width = config.App.appWidth(100);
  var textColor = isMe ? config.Colors.customeColor("#ffffff") : config.Colors.accentColor();
  var textSubColor = isMe ? config.Colors.customeColor("#ffffff") : config.Colors.customeColor("#000000", opacity: 0.5);
  var bgColor = isMe ? config.Colors.primaryColor() : config.Colors.customeColor("#F7F7F9");

  _inBlock() {
    if (messageNext != null && messageNext.user.ref_user_id == message.user.ref_user_id) return true;
    return false;
  }

  _newBlock() {
    if (messagePrev != null && messagePrev.user.ref_user_id == message.user.ref_user_id) return false;
    return true;
  }

  _msgTime() {
    if (_inBlock() == true) return Container();
    return Container(
      child: Column(
        children: [
          SizedBox(height: 2.0),
          AccentText(
            message.createdAt != null ? DateFormat('HH:mm').format(message.createdAt) : '',
            style: TextStyle(
              fontSize: 12,
              color: textSubColor,
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }

  _msgName() {
    if (isMe) return Container();
    if (!_newBlock()) return Container();
    return Container(
      child: AccentText(
        message.user.name,
        style: TextStyle(
          fontSize: 12,
          color: textSubColor,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }

  _msgContent() {
    if (message.message_type == "TEXT" || message.message_type == "TMP") {
      Widget _txt = AccentText(
        message.message,
        style: TextStyle(
          fontSize: 16,
          color: textColor,
        ),
      );
      return Container(
        constraints: BoxConstraints(minWidth: 50, maxWidth: _width * 0.75),
        child: _txt,
      );
    } else if (message.message_type == "IMAGE" && message.message.isNotEmpty) {
      var imgs = message.message.split('|');
      var _w = (_width * 0.75) / 3;
      var _length = imgs.length;
      var _rows = (_length / 3).ceil();
      var _crossAxis = _length >= 3 ? 3 : _length;
      return Container(
        constraints: BoxConstraints(minWidth: _w, maxWidth: _width * 0.75),
        height: _rows * (_w),
        width: _crossAxis * (_w),
        child: GridView.count(
          crossAxisCount: _crossAxis,
          physics: NeverScrollableScrollPhysics(),
          children: List.generate(_length, (i) {
            var _img = imgs[i].toString();
            return Container(
              width: _w,
              height: _w,
              margin: EdgeInsets.only(right: ((i + 1) % _crossAxis == 0) ? 0 : 5, top: (i >= 3) ? 5 : 0),
              child: ImageUrlView(_img, _w),
            ).onTap(() async {
              FocusScope.of(context).requestFocus(FocusNode());
              await showDialog(
                context: context,
                builder: (_) => ImageDialogUrl(_img),
              );
            });
          }),
        ),
      );
    }
    return Container();
  }

  _avatar() {
    if (isMe) return Container();
    if (_inBlock() == true)
      return Container(
        alignment: Alignment.bottomLeft,
        width: (25 + 4.0),
      );
    return Container(
      alignment: Alignment.bottomLeft,
      padding: EdgeInsets.only(right: 4),
      child: UserAvatar(
        message.user.avatar,
        width: 25,
      ),
    );
  }

  _body() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      margin: EdgeInsets.only(top: _newBlock() ? 6 : 0),
      decoration: BoxDecoration(
        color: bgColor,
        borderRadius: isMe
            ? BorderRadius.only(
                topLeft: Radius.circular(8.0),
                bottomLeft: Radius.circular(8.0),
                bottomRight: Radius.circular(8.0),
              )
            : BorderRadius.only(
                topRight: Radius.circular(8.0),
                bottomRight: Radius.circular(8.0),
                bottomLeft: Radius.circular(8.0),
              ),
      ),
      child: Column(
        crossAxisAlignment: isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          _msgName(),
          _msgContent(),
          _msgTime(),
        ],
      ),
    );
  }

  return Container(
    child: Container(
      margin: EdgeInsets.only(bottom: 2.0, right: 16, left: 16),
      child: Row(
        mainAxisAlignment: isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          _avatar(),
          _body(),
        ],
      ),
    ),
  );
}

class FakeMessage extends StatelessWidget {
  const FakeMessage(
    this.isBig, {
    Key key,
  }) : super(key: key);

  final bool isBig;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
      height: isBig ? 128.0 : 36.0,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(8.0)),
        color: Colors.grey.shade300,
      ),
    );
  }
}
