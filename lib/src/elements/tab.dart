import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;

class SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  SliverAppBarDelegate(this._tabBar);
  final TabBar _tabBar;
  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      margin: EdgeInsets.only(left: 10, right: 10, top: 0),
      color: Colors.white,
      child: Container(
        decoration: new BoxDecoration(
          border: new Border(bottom: BorderSide(color: config.Colors.inputBgColor())),
        ),
        child: Container(padding: EdgeInsets.only(left: 0, right: 0), child: _tabBar),
      ),
    );
  }

  @override
  bool shouldRebuild(SliverAppBarDelegate oldDelegate) {
    return false;
  }
}

class TabTitle extends Tab {
  final String text;
  const TabTitle(this.text, {Key key}) : super(key: key, text: text);

  @override
  Widget build(BuildContext context) {
    return Tab(
      child: Container(
        alignment: Alignment.center,
        child: Text(text, style: TextStyle(fontWeight: FontWeight.w500)),
      ),
    );
  }
}

class TabViewBox extends StatelessWidget {
  final Widget child;
  final bool isButton;
  const TabViewBox(
    this.child,
    this.isButton, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: config.App.appWidth(100),
        margin: EdgeInsets.only(bottom: (isButton != null && isButton == true) ? 90 : 0),
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
        ),
        child: child,
      ),
    );
  }
}
