import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;

class LineText extends StatelessWidget {
  final String text;
  final TextStyle style;
  final int maxLines;

  const LineText(this.text, {Key key, this.style, this.maxLines}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _style = Theme.of(context).textTheme.bodyText1;
    if (style != null) _style = _style.merge(style);
    return Text(
      text == null ? "" : text,
      style: _style,
      maxLines: maxLines,
      overflow: maxLines != null && maxLines > 0 ? TextOverflow.ellipsis : TextOverflow.visible,
    );
  }
}

class PrimaryText extends StatelessWidget {
  const PrimaryText(this.text, {Key key, this.style}) : super(key: key);
  final String text;
  final TextStyle style;

  @override
  Widget build(BuildContext context) {
    var _style = Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.primaryColor());
    if (style != null) _style = _style.merge(style);
    return Text(text == null ? "" : text, style: _style);
  }
}

class AccentText extends StatelessWidget {
  const AccentText(this.text, {Key key, this.style, this.textAlign = TextAlign.left, this.maxLines = 99, this.overflow = TextOverflow.visible}) : super(key: key);
  final String text;
  final TextStyle style;
  final TextAlign textAlign;
  final int maxLines;
  final TextOverflow overflow;

  @override
  Widget build(BuildContext context) {
    var _style = Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor());
    if (style != null) _style = _style.merge(style);
    return Text(
      text == null ? "" : text,
      style: _style,
      textAlign: textAlign,
      maxLines: maxLines,
      overflow: maxLines != null && maxLines > 0 ? (overflow != null ? overflow : TextOverflow.ellipsis) : TextOverflow.visible,
    );
  }
}

class SecondText extends StatelessWidget {
  const SecondText(this.text, {Key key, this.style}) : super(key: key);
  final String text;
  final TextStyle style;

  @override
  Widget build(BuildContext context) {
    var _style = Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.secondColor());
    if (style != null) _style = _style.merge(style);
    return Text(text == null ? "" : text, style: _style);
  }
}

class SubText extends StatelessWidget {
  const SubText(this.text, {Key key, this.style, this.maxLines}) : super(key: key);
  final String text;
  final TextStyle style;
  final int maxLines;

  @override
  Widget build(BuildContext context) {
    var _style = Theme.of(context).textTheme.subtitle1.copyWith(color: config.Colors.subColor(), fontWeight: FontWeight.w400);
    if (style != null) _style = _style.merge(style);
    return Text(
      text == null ? "" : text,
      style: _style,
      maxLines: maxLines,
      overflow: maxLines != null && maxLines > 0 ? TextOverflow.ellipsis : TextOverflow.visible,
    );
  }
}

class ErrorText extends StatelessWidget {
  const ErrorText(this.text, {Key key, this.style}) : super(key: key);
  final String text;
  final TextStyle style;

  @override
  Widget build(BuildContext context) {
    var _style = Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.errorColor());
    if (style != null) _style = _style.merge(style);
    return Text(text == null ? "" : text, style: _style);
  }
}

class LineSelectableText extends StatelessWidget {
  const LineSelectableText(this.text, {Key key, this.style}) : super(key: key);
  final String text;
  final TextStyle style;

  @override
  Widget build(BuildContext context) {
    var _style = Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.secondColor());
    if (style != null) _style = _style.merge(style);
    return SelectableText(text == null ? "" : text, style: _style);
  }
}
