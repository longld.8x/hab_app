import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/UrlImage.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;

class CourseHorizontalBox extends StatelessWidget {
  final String image;
  final Widget info;
  final double width;
  final double height;
  const CourseHorizontalBox({Key key, this.image, this.info, this.width, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        shape: BoxShape.rectangle,
        border: Border.all(width: 1, color: config.Colors.customeColor("#E4E4E4")),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
            child: UrlImage(
              image,
              'assets/icon/default-course.png',
              width: width,
              height: height != null ? height * 0.56 : null,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            padding: EdgeInsets.all(0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
            child: info,
          ),
        ],
      ),
    );
  }
}

class CourseVerticalBox extends StatelessWidget {
  final String image;
  final Widget info;
  const CourseVerticalBox({Key key, this.image, this.info}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _width = config.App.appWidth(47) - 16;
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        //borderRadius: BorderRadius.all(Radius.circular(8)),
        // shape: BoxShape.rectangle,
        // boxShadow: [
        //   BoxShadow(
        //     offset: Offset(0, 2),
        //     color: Color(0xFF000000).withOpacity(0.16),
        //     spreadRadius: 1,
        //     blurRadius: 4,
        //   ),
        //],
      ),
      child: Container(
        padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 47,
              child: Container(
                padding: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                alignment: Alignment.center,
                child: UrlImage(
                  image,
                  'assets/icon/default-course.png',
                  width: _width,
                  height: _width * (90 / 160),
                  fit: BoxFit.cover,
                ),
                // child: Image.asset(image, fit: BoxFit.fill),
              ),
            ),
            Expanded(
              flex: 53,
              child: Container(
                padding: EdgeInsets.all(0),
                child: info,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CourseNameRankBox extends StatelessWidget {
  final String name;
  final double rank;
  final type;
  const CourseNameRankBox(this.name, this.rank, this.type, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //print(type);
    var _rank = Container(
      width: 50,
      alignment: Alignment.center,
      padding: EdgeInsets.only(top: type == "detail" ? 4 : 2, bottom: type == "detail" ? 4 : 2),
      decoration: new BoxDecoration(
        color: config.Colors.customeColor("#F3C41D"),
        borderRadius: BorderRadius.all(Radius.circular(4)),
      ),
      child: TextIcon(
        SubText(
          rank.toString(),
          style: TextStyle(color: config.Colors.customeColor("#ffffff")),
        ),
        Icon(Icons.star, size: 14, color: config.Colors.customeColor("#ffffff")),
        padding: 0,
      ),
      margin: EdgeInsets.fromLTRB(type == "detail" ? 4 : 0, 0, 0, type == "detail" ? 6 : 0),
    );
    if (type == "vertical") {
      var _boxWidth = config.App.appWidth(53);
      return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              constraints: BoxConstraints(maxWidth: (_boxWidth - 10 * 2)),
              child: Text(
                name == null ? "" : name,
                style: Theme.of(context).textTheme.headline5.copyWith(color: config.Colors.accentColor(), fontWeight: FontWeight.w500, height: 1),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            // _rank
          ],
        ),
      );
    } else if (type == "horizontal") {
      var _boxWidth = config.App.appWidth(60);
      return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              constraints: BoxConstraints(maxWidth: (_boxWidth - 2 - 10 * 2)),
              //constraints: BoxConstraints(maxWidth: (_boxWidth - 2 - 50 - 10 * 2)),
              child: Text(
                name == null ? "" : name,
                style: Theme.of(context).textTheme.headline5.copyWith(color: config.Colors.accentColor(), fontWeight: FontWeight.w500),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            // _rank
          ],
        ),
      );
    } else if (type == "detail") {
      return Container(
        child: RichText(
          text: TextSpan(
            text: name == null ? "" : name,
            style: Theme.of(context).textTheme.headline1.copyWith(color: config.Colors.accentColor(), fontWeight: FontWeight.w500),
            // children: [
            //   WidgetSpan(
            //     child: _rank,
            //   ),
            // ],
          ),
        ),
      );
    } else {
      return Container();
    }
  }
}

class ClassHorizontalBox extends StatelessWidget {
  final String image;
  final Widget info;
  final double width;
  final double height;
  const ClassHorizontalBox({Key key, this.image, this.info, this.width, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        shape: BoxShape.rectangle,
        border: Border.all(width: 1, color: config.Colors.customeColor("#E4E4E4")),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
            child: UrlImage(
              image,
              'assets/icon/default-course.png',
              width: width,
              height: height != null ? height * 0.56 : null,
              border: true,
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
              ),
              child: info,
            ),
          ),
        ],
      ),
    );
  }
}

class ClassVerticalBox extends StatelessWidget {
  final String image;
  final Widget info;
  const ClassVerticalBox({Key key, this.image, this.info}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _width = config.App.appWidth(47) - 16;
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        //borderRadius: BorderRadius.all(Radius.circular(8)),
        // shape: BoxShape.rectangle,
        // boxShadow: [
        //   BoxShadow(
        //     offset: Offset(0, 2),
        //     color: Color(0xFF000000).withOpacity(0.16),
        //     spreadRadius: 1,
        //     blurRadius: 4,
        //   ),
        //],
      ),
      child: Container(
        padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 47,
              child: Container(
                padding: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                alignment: Alignment.center,
                child: UrlImage(
                  image,
                  'assets/icon/default-course.png',
                  border: true,
                  width: _width,
                  height: _width * (90 / 160),
                  fit: BoxFit.cover,
                ),

                // child: Image.asset(image, fit: BoxFit.fill),
              ),
            ),
            Expanded(
              flex: 53,
              child: Container(
                padding: EdgeInsets.all(0),
                child: info,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TextTagBox extends StatelessWidget {
  final String title;
  final TextStyle style;
  final Widget box;
  const TextTagBox(this.title, this.box, {Key key, this.style}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _style = Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor(), fontSize: 14, fontWeight: FontWeight.w500);
    if (style != null) _style = _style.merge(style);
    return Container(
      child: RichText(
        text: TextSpan(
          style: _style,
          text: title,
          children: [WidgetSpan(child: box)],
        ),
      ),
    );
  }
}

class LabelBox extends StatelessWidget {
  final int status;
  final String title;
  const LabelBox(this.title, this.status, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _color = ([1, 2].indexOf(status) == -1)
        ? config.Colors.hintColor()
        : status == 2
            ? config.Colors.primaryColor()
            : status == 1
                ? config.Colors.errorColor()
                : config.Colors.hintColor();
    return Container(
      decoration: new BoxDecoration(
        color: _color.withOpacity(0.1),
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
      padding: EdgeInsets.fromLTRB(12, 2, 12, 4),
      margin: EdgeInsets.only(left: 8, top: 0),
      child: AccentText(
        title,
        style: TextStyle(fontSize: 12, color: _color),
      ),
    );
  }
}

class LabelBoxEx extends StatelessWidget {
  final int status;
  final String title;
  const LabelBoxEx(this.title, this.status, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _color = ([1, 2].indexOf(status) == -1)
        ? config.Colors.hintColor()
        : status == 2
            ? config.Colors.primaryColor()
            : status == 1
                ? config.Colors.errorColor()
                : config.Colors.hintColor();
    return Container(
      decoration: new BoxDecoration(
        color: _color.withOpacity(0.1),
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
      padding: EdgeInsets.fromLTRB(12, 2, 12, 4),
      margin: EdgeInsets.only(left: 8, top: 0),
      child: AccentText(
        title,
        style: TextStyle(fontSize: 12, color: _color),
      ),
    );
  }
}

class LabelBoxSS extends StatelessWidget {
  final int status;
  final String title;
  const LabelBoxSS(this.title, this.status, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _color = ([1, 3, 4].indexOf(status) == -1)
        ? config.Colors.hintColor()
        : (status == 1)
            ? config.Colors.primaryColor()
            : status == 3
                ? config.Colors.errorColor()
                : status == 4
                    ? config.Colors.customeColor('#FF900A')
                    : config.Colors.backgroundColor();
    return Container(
      decoration: new BoxDecoration(
        color: _color.withOpacity(0.1),
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
      padding: EdgeInsets.fromLTRB(12, 2, 12, 4),
      margin: EdgeInsets.only(left: 8, top: 0),
      child: AccentText(
        title,
        style: TextStyle(fontSize: 12, color: _color),
      ),
    );
  }
}
