import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:path/path.dart' as p;

class ImagePick extends StatelessWidget {
  final Asset image;
  final double width;
  final Function remove;
  ImagePick(this.image, this.width, {Key key, this.remove}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: width,
      margin: EdgeInsets.only(left: 8, top: 8, right: 8, bottom: 8),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            child: AssetThumb(
              asset: image,
              width: width.toInt(),
              height: width.toInt(),
              spinner: const Center(
                child: SizedBox(
                  width: 10,
                  height: 10,
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
          ),
          Positioned(
              right: 2,
              top: 2,
              child: (remove != null)
                  ? Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(18)),
                        color: config.Colors.customeColor('#000000', opacity: 0.5),
                      ),
                      width: 18,
                      height: 18,
                      alignment: Alignment.center,
                      child: IconButton(
                        icon: Icon(Icons.close, size: 14, color: config.Colors.customeColor('#ffffff')),
                        padding: EdgeInsets.all(2),
                        onPressed: () {
                          remove(image);
                        },
                      ),
                    )
                  : Container())
        ],
      ),
    );
  }
}

class ImageUrlView extends StatelessWidget {
  final String imageUrl;
  final double width;
  final EdgeInsets margin;
  final Function remove;
  ImageUrlView(this.imageUrl, this.width, {Key key, this.remove, this.margin}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      margin: margin != null ? margin : EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 0),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              alignment: Alignment.center,
              imageUrl: imageUrl,
              width: width,
              height: width,
            ),
          ),
          Positioned(
              right: 2,
              top: 2,
              child: (remove != null)
                  ? Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(18)),
                        color: config.Colors.customeColor('#000000', opacity: 0.5),
                      ),
                      width: 18,
                      height: 18,
                      alignment: Alignment.center,
                      child: IconButton(
                        icon: Icon(Icons.close, size: 14, color: config.Colors.customeColor('#ffffff')),
                        padding: EdgeInsets.all(2),
                        onPressed: () {
                          remove(imageUrl);
                        },
                      ),
                    )
                  : Container())
        ],
      ),
    );
  }
}

class ImageDialogUrl extends StatelessWidget {
  final String imageUrl;
  ImageDialogUrl(this.imageUrl, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.all(10),
      child: Container(
        width: config.App.appWidth(100),
        child: ClipRRect(
          // borderRadius: BorderRadius.all(Radius.circular(8)),
          child: CachedNetworkImage(
            fit: BoxFit.cover,
            alignment: Alignment.center,
            imageUrl: imageUrl,
          ),
        ),
      ),
    );
  }
}

class FilePick extends StatelessWidget {
  final File file;
  final double width;
  final Function remove;
  FilePick(this.file, this.width, {Key key, this.remove}) : super(key: key);
  List<String> fileVideo = ['.mp4', '.mp3'];
  List<String> fileImage = ['.jpg', '.png'];
  @override
  Widget build(BuildContext context) {
    var _extension = p.extension(file.path);
    print("_extension _extension $_extension");
    Widget _wgFile;
    if (fileImage.contains(_extension)) {
      print("fileImage ${file.path}");
      _wgFile = Image.file(file);
    } else if (fileVideo.contains(_extension)) {
      _wgFile = Container(
        alignment: Alignment.center,
        child: Text(" Video "),
        color: Colors.white,
      );
    }
    // else if (fileVideo.contains(_extension)) {
    //   print("fileVideo ${file.path}");
    //   _wgFile = VideoPlayer(VideoPlayerController.file(file));
    // }
    else
      _wgFile = Container(
        alignment: Alignment.center,
        child: Text("file: $_extension"),
        color: Colors.white,
      );
    return Container(
      height: width,
      margin: EdgeInsets.only(left: 8, top: 8, right: 8, bottom: 8),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            child: _wgFile,
          ),
          Positioned(
              right: 2,
              top: 2,
              child: (remove != null)
                  ? Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(18)),
                        color: config.Colors.customeColor('#000000', opacity: 0.5),
                      ),
                      width: 18,
                      height: 18,
                      alignment: Alignment.center,
                      child: IconButton(
                        icon: Icon(Icons.close, size: 14, color: config.Colors.customeColor('#ffffff')),
                        padding: EdgeInsets.all(2),
                        onPressed: () {
                          remove(file);
                        },
                      ),
                    )
                  : Container())
        ],
      ),
    );
  }
}
