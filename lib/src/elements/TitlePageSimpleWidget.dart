import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/TitleText.dart';

class TitlePageSimpleWidget extends StatelessWidget {
  final String text;
  const TitlePageSimpleWidget(
    this.text, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 58,
      padding: EdgeInsets.only(left: 15, right: 15),
      alignment: Alignment.bottomLeft,
      child: TitleText(this.text),
    );
  }
}
