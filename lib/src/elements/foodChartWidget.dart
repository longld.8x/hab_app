import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/foodDto.dart';

class FoodChartWidget extends StatefulWidget {
  final List<FoodDto> dataItem;
  final bool isExpan;
  final Function onAdd;
  final Function actionEvent;
  final List<String> actionType;
  FoodChartWidget({Key key, this.dataItem, this.isExpan = false, this.onAdd = null, this.actionType, this.actionEvent}) : super(key: key);
  @override
  _FoodChartWidgetState createState() => _FoodChartWidgetState();
}

class _FoodChartWidgetState extends State<FoodChartWidget> {
  bool _isExpanData = false;

  @override
  void initState() {
    super.initState();
    if (widget.isExpan == false) _isExpanData = true;
  }

  var _border = BorderSide(color: config.Colors.customeColor('#E0E9F8'), width: 1);
  @override
  Widget build(BuildContext context) {
    FoodDto totalFood = new FoodDto(
      calories: 0,
      carb: 0,
      fat: 0,
      protein: 0,
    );
    if (widget.dataItem != null && widget.dataItem.length > 0) {
      totalFood = widget.dataItem.reduce((value, element) => new FoodDto(
            calories: (value.calories ?? 0) + (element.calories ?? 0),
            carb: (value.carb ?? 0) + (element.carb ?? 0),
            fat: (value.fat ?? 0) + (element.fat ?? 0),
            protein: (value.protein ?? 0) + (element.protein ?? 0),
          ));
    }
    var _width = config.App.appWidth(100);
    Widget _chart() {
      var _chartWidth = 80.0;
      var _circularChart = new AnimatedCircularChart(
        size: new Size(_chartWidth, _chartWidth),
        initialChartData: <CircularStackEntry>[
          new CircularStackEntry(
            <CircularSegmentEntry>[
              new CircularSegmentEntry((totalFood.carb ?? 1), config.Colors.customeColor("#1A60FF")),
              new CircularSegmentEntry(1, config.Colors.backgroundColor()),
              new CircularSegmentEntry((totalFood.fat ?? 1), config.Colors.customeColor("#EB001B")),
              new CircularSegmentEntry(1, config.Colors.backgroundColor()),
              new CircularSegmentEntry((totalFood.protein ?? 1), config.Colors.customeColor("#BE52F2")),
              new CircularSegmentEntry(1, config.Colors.backgroundColor()),
            ],
          ),
        ],
        chartType: CircularChartType.Radial,
        duration: Duration(milliseconds: 1000),
        percentageValues: false,
        // edgeStyle: SegmentEdgeStyle.round,
        // holeRadius: _chartWidth / 2.9,
      );
      return Container(
        width: _chartWidth + 20,
        height: _chartWidth,
        padding: EdgeInsets.only(left: 10, right: 10),
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              child: Container(
                width: _chartWidth,
                height: _chartWidth,
                alignment: Alignment.center,
                child: _circularChart,
              ),
            ),
            Positioned(
              top: 0,
              width: _chartWidth,
              height: _chartWidth,
              child: Container(
                width: _chartWidth,
                height: _chartWidth,
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Center(
                      child: AccentText(
                        Helper.formatNumber(totalFood.calories ?? 0),
                        style: TextStyle(fontWeight: FontWeight.w500),
                      ),
                    ),
                    Center(
                      child: SubText(
                        "Calo",
                        style: TextStyle(color: config.Colors.hintColor()),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget _nutritionItem(String line1, String line2, Widget line3) {
      return Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 3),
            child: Text(
              line1,
              style: TextStyle(color: config.Colors.customeColor("#3D639D", opacity: 0.75), fontWeight: FontWeight.w400),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5, bottom: 5),
            child: Text(
              line2,
              style: TextStyle(color: config.Colors.customeColor("#151522"), fontWeight: FontWeight.w500),
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 2),
            child: line3,
          ),
        ],
      );
    }

    Widget _expanWidget() {
      if (widget.isExpan == true)
        return Container(
          alignment: Alignment.center,
          child: IconButton(
            padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
            icon: _isExpanData == true ? Icon(Icons.keyboard_arrow_up) : Icon(Icons.keyboard_arrow_down),
            onPressed: () {
              setState(() => _isExpanData = !_isExpanData);
            },
          ),
        );
      else
        return Container();
    }

    Widget _nutritionExpan() {
      if (_isExpanData == false) return Container();
      Widget _title = Container(
        width: _width,
        padding: EdgeInsets.only(top: 12, bottom: 16, left: 16, right: 16),
        child: TitleText("Món ăn"),
      );

      if (widget.onAdd != null) {
        _title = Row(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.only(top: 12, bottom: 16, left: 16, right: 16),
                child: TitleText("Món ăn"),
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              padding: EdgeInsets.only(right: 16),
              child: PrimaryText('Thêm món'),
            ).onTap(() {
              widget.onAdd();
            }),
          ],
        );
      }
      return Container(
        child: Column(
          children: [
            Container(
              decoration: new BoxDecoration(
                border: new Border(
                  bottom: (widget.dataItem != null && widget.dataItem.length > 0) ? _border.copyWith(color: config.Colors.customeColor('#E4E4E4', opacity: 0.3)) : BorderSide.none,
                ),
              ),
              child: _title,
            ),
            Container(
              child: Column(
                  children: widget.dataItem.map((e) {
                var _item = e;
                return Container(
                  child: FoodItemWidget(
                    data: _item,
                    actionType: widget.actionType,
                    action: (String type) {
                      widget.actionEvent(_item, type);
                    },
                  ),
                );
              }).toList()),
            ),
          ],
        ),
      );
    }

    var _t = (totalFood.carb ?? 0) + (totalFood.fat ?? 0) + (totalFood.protein ?? 0);
    var _t1 = (totalFood.carb ?? 0);
    var _t2 = (totalFood.fat ?? 0);
    var _t3 = (totalFood.protein ?? 0);

    return Container(
      width: _width,
      child: Column(
        children: [
          Container(
            height: 90,
            decoration: new BoxDecoration(
              border: new Border(
                bottom: _border.copyWith(width: 3, color: config.Colors.customeColor('#F4F9FE')),
              ),
            ),
            padding: EdgeInsets.only(top: 5, bottom: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  child: _chart(),
                ),
                Expanded(
                  child: _nutritionItem("${_t == 0 ? '0' : ((_t1 / _t) * 100).toStringAsFixed(1)}%", Helper.formatNumber(_t1, prefix: 'g'), Text("Carbs", style: TextStyle(color: config.Colors.customeColor("#1A60FF"), fontWeight: FontWeight.w400))),
                ),
                Expanded(
                  child: _nutritionItem("${_t == 0 ? '0' : ((_t2 / _t) * 100).toStringAsFixed(1)}%", Helper.formatNumber(_t2, prefix: 'g'), Text("Chất béo", style: TextStyle(color: config.Colors.customeColor("#EB001B"), fontWeight: FontWeight.w400))),
                ),
                Expanded(
                  child: _nutritionItem("${_t == 0 ? '0' : ((_t3 / _t) * 100).toStringAsFixed(1)}%", Helper.formatNumber(_t3, prefix: 'g'), Text("Protein", style: TextStyle(color: config.Colors.customeColor("#BE52F2"), fontWeight: FontWeight.w400))),
                ),
                _expanWidget(),
              ],
            ),
          ),
          _nutritionExpan()
        ],
      ),
    );
  }
}

class FoodItemWidget extends StatelessWidget {
  final FoodDto data;
  final Function action;
  final List<String> actionType;

  FoodItemWidget({Key key, this.data, this.actionType, this.action}) : super(key: key);

  var _border = BorderSide(color: config.Colors.customeColor('#E0E9F8'), width: 1);
  @override
  Widget build(BuildContext context) {
    var _width = config.App.appWidth(100);
    Widget _nutritionExpanCol(int index, String t1, String t2, Color color) {
      /// 1
      if (index == 1) {
        List<Widget> _titleFood = [
          Container(
            padding: EdgeInsets.only(bottom: 0),
            alignment: Alignment.topCenter,
            child: SubText(
              t2,
              style: TextStyle(color: config.Colors.customeColor('#3D639D', opacity: 0.75)),
            ),
          )
        ];
        if (action == null)
          _titleFood.insert(
              0,
              Container(
                padding: EdgeInsets.only(bottom: 10),
                child: AccentText(
                  t1,
                  style: TextStyle(color: color, fontWeight: FontWeight.w500),
                ),
              ));
        else
          _titleFood.add(Container(
            padding: EdgeInsets.only(bottom: 10),
            child: AccentText(
              '',
              style: TextStyle(color: color, fontWeight: FontWeight.w500),
            ),
          ));
        return Expanded(
          child: Container(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Column(
              children: _titleFood,
            ),
          ),
        );
      }

      /// != 1
      return Expanded(
        child: Container(
          padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 6),
                child: AccentText(
                  t1,
                  style: TextStyle(color: color),
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 0),
                child: AccentText(
                  t2,
                  style: TextStyle(color: color),
                ),
              ),
            ],
          ),
        ),
      );
    }

    Widget _nutritionTitleAction() {
      List<Widget> wg = [];
      if (action != null)
        wg.add(Expanded(
          child: Container(
            padding: EdgeInsets.only(left: 16),
            alignment: Alignment.bottomLeft,
            child: AccentText(
              data.foodName,
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
          ),
        ));
      else
        wg.add(Expanded(child: Container()));
      if (actionType != null && actionType.indexOf('edit') > -1) {
        wg.add(Container(
          height: 35,
          width: 35,
          child: IconButton(
            iconSize: 16,
            icon: SvgPicture.asset('assets/icon/edit2.svg'),
            onPressed: () {
              action("edit");
            },
          ),
        ));
      }
      if (actionType != null && actionType.indexOf('add') > -1) {
        wg.add(Container(
          height: 35,
          width: 35,
          child: IconButton(
            icon: SvgPicture.asset('assets/icon/plus2.svg'),
            onPressed: () {
              action("add");
            },
          ),
        ));
      }
      if (actionType != null && actionType.indexOf('remove') > -1) {
        wg.add(Container(
          height: 35,
          width: 35,
          child: IconButton(
            icon: SvgPicture.asset('assets/icon/close_red.svg'),
            onPressed: () {
              action("remove");
            },
          ),
        ));
      }
      return Row(crossAxisAlignment: CrossAxisAlignment.end, children: wg);
    }

    Widget _nutritionExpanDash() {
      var w = Container(
        decoration: BoxDecoration(color: Colors.white),
        width: 10,
        height: 50,
        child: Flex(
          children: List.generate(10, (_) {
            return SizedBox(
              width: 1,
              height: 2,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: config.Colors.accentColor(),
                ),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.vertical,
        ),
      );
      return w;
    }

    return Container(
      width: _width,
      decoration: new BoxDecoration(
        border: new Border(
          bottom: _border,
        ),
      ),
      child: Column(
        children: [
          Container(child: _nutritionTitleAction()),
          Container(
            child: Row(
              children: [
                _nutritionExpanCol(1, data.foodName, "${data.quantity} ${data.foodUnitUnitName}", config.Colors.accentColor()),
                _nutritionExpanDash(),
                _nutritionExpanCol(2, Helper.formatNumber(data.carb ?? 0, prefix: 'g'), "Carbs", config.Colors.customeColor('#1A60FF')),
                _nutritionExpanCol(2, Helper.formatNumber(data.fat ?? 0, prefix: 'g'), "Chất béo", config.Colors.customeColor('#EB001B')),
                _nutritionExpanCol(2, Helper.formatNumber(data.protein ?? 0, prefix: 'g'), "Protein", config.Colors.customeColor('#BE52F2')),
                _nutritionExpanCol(2, Helper.formatNumber(data.calories ?? 0, prefix: 'g'), "Calo", config.Colors.customeColor('#171051')),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
