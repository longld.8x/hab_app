import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;

class BodyManager extends StatelessWidget {
  final Widget child;
  final bool isContentPadding;
  final bool isSingleChildScrollView;

  BodyManager({
    Key key,
    this.child,
    this.isContentPadding = false,
    this.isSingleChildScrollView = true,
  }) : super(key: key);

  Widget build(BuildContext context) {
    // print("MediaQuery.of(context).viewInsets.bottom: ${MediaQuery.of(context).viewInsets.bottom}");
    final _bottom = MediaQuery.of(context).viewInsets.bottom;
    double _padding = isContentPadding ? 15.0 : 0;

    return Container(
      width: config.App.appWidth(100),
      // height: config.App.appHeight(100),
      margin: EdgeInsets.only(bottom: _bottom),
      child: isSingleChildScrollView
          ? SingleChildScrollView(
              padding: EdgeInsets.only(left: _padding, right: _padding),
              child: child,
            )
          : Container(
              padding: EdgeInsets.only(left: _padding, right: _padding),
              child: child,
            ),
    );
  }
}
