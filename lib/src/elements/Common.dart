import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;

class ColumnStart extends StatelessWidget {
  final List<Widget> children;
  final MainAxisAlignment mainAxisAlignment;
  final CrossAxisAlignment crossAxisAlignment;
  const ColumnStart({Key key, this.children, this.mainAxisAlignment = MainAxisAlignment.start, this.crossAxisAlignment = CrossAxisAlignment.stretch}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: mainAxisAlignment,
      crossAxisAlignment: crossAxisAlignment,
      mainAxisSize: MainAxisSize.max,
      children: children,
    );
  }
}

class UserAvatar extends StatelessWidget {
  final String profilePicture;
  final double width;
  final bool status;
  final Color border;
  const UserAvatar(this.profilePicture, {Key key, this.width = 50, this.status, this.border = null}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    ImageProvider bg = AssetImage("assets/icon/avatar.png");
    try {
      if ((profilePicture != null && profilePicture.isNotEmpty)) {
        bg = CachedNetworkImageProvider(profilePicture);
      }
    } catch (e) {}

    var wStatus = Container();
    if (status != null) {
      wStatus = Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        alignment: Alignment.center,
        padding: EdgeInsets.all(1),
        child: Container(
          width: 9,
          height: 9,
          decoration: BoxDecoration(
            color: status == true ? config.Colors.customeColor('#41CD2A') : config.Colors.customeColor('#A7A7A7', opacity: 0.9),
            borderRadius: BorderRadius.all(Radius.circular(9)),
          ),
        ),
      );
    }
    return Container(
      width: width,
      height: width,
      child: Stack(
        children: <Widget>[
          CircleAvatar(
            backgroundImage: bg,
            radius: width / 2,
          ),
          Positioned(
            right: 0,
            bottom: 6,
            child: wStatus,
          )
        ],
      ),
    );
  }
}

class CourseRowInfo extends StatelessWidget {
  final String icon;
  final String text;
  final EdgeInsets padding;
  final double fontSize;
  const CourseRowInfo(this.icon, this.text, {Key key, this.padding, this.fontSize = 16}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _padding = padding;
    if (padding == null) _padding = EdgeInsets.fromLTRB(20, 0, 20, 16);
    return Container(
      padding: _padding,
      child: RichText(
        text: TextSpan(
          children: [
            WidgetSpan(
              child: Container(
                child: SvgPicture.asset(
                  icon,
                  width: fontSize,
                  height: fontSize,
                ),
                padding: EdgeInsets.only(right: 6, bottom: 3),
              ),
            ),
            TextSpan(
              text: text,
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    fontSize: fontSize,
                    fontWeight: FontWeight.w400,
                    color: config.Colors.accentColor(),
                  ),
            ),
          ],
        ),
      ),
    );
  }
}
