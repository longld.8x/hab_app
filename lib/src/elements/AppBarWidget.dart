import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hab_app/src/elements/Body.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;

class AppbarProfileManager extends StatelessWidget {
  final String title;
  final double barHeight = 56.0;
  Function onPressed;
  AppbarProfileManager(this.title, {Key key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double statusbarHeight = MediaQuery.of(context).padding.top;

    return new Container(
      padding: EdgeInsets.only(top: statusbarHeight),
      height: statusbarHeight + barHeight,
      child: Container(
        child: Container(
          height: barHeight,
          decoration: BoxDecoration(
            color: config.Colors.backgroundColor(),
            borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                flex: 15,
                child: Container(
                  padding: EdgeInsets.only(right: 0),
                  child: IconButton(
                    icon: Icon(Icons.arrow_back_ios, size: 18),
                    color: Colors.black,
                    onPressed: () {
                      if (onPressed != null) onPressed();
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ),
              Expanded(
                flex: 85,
                child: Container(
                  padding: EdgeInsets.only(right: 0),
                  child: TitleText(title),
                ),
              ),
            ],
          ),
        ),
      ),
      decoration: BoxDecoration(
        color: config.Colors.inputBgColor(),
        image: DecorationImage(
          image: AssetImage("assets/icon/profile.png"),
          fit: BoxFit.fitWidth,
          alignment: Alignment.topLeft,
        ),
      ),
    );
  }
}

class AppbarIcon extends StatelessWidget {
  Function onPressed;
  Widget icon;
  AppbarIcon({Key key, this.onPressed, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: icon != null ? icon : const Icon(Icons.arrow_back_ios, size: 18, color: Colors.black),
      onPressed: () {
        if (onPressed != null)
          onPressed();
        else
          Navigator.of(context).pop();
      },
    );
  }
}
