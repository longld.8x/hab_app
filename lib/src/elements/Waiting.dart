import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;

class Waiting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.delayed(Duration(seconds: 5)),
      builder: (c, s) => Text(
        "",
        style: TextStyle(color: Colors.white),
      ),
    );
  }
}

class LoadContent extends StatelessWidget {
  final bool warp;
  final bool border;
  const LoadContent({Key key, this.warp = false, this.border = false}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (warp == null || warp == false)
      return Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(16),
              child: AccentText("Đang tải dữ liệu..."),
            ),
          ],
        ),
      );
    else
      return Container(
        width: config.App.appWidth(100),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
          borderRadius: border ? const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)) : null,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(16),
              child: AccentText("Đang tải dữ liệu..."),
            ),
          ],
        ),
      );
  }
}

class NoContent extends StatelessWidget {
  final bool warp;
  final bool border;
  final double height;
  final String text;
  const NoContent({Key key, this.warp = true, this.height, this.border = false, this.text = "Không có dữ liệu."}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (warp == null || warp == false)
      return Container(
        width: config.App.appWidth(100),
        height: height,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Container(
                padding: EdgeInsets.all(16),
                child: AccentText(text),
              ),
            ),
          ],
        ),
      );
    else
      return Container(
        width: config.App.appWidth(100),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
          borderRadius: border ? const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)) : null,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(16),
              child: AccentText(text),
            ),
          ],
        ),
      );
  }
}
