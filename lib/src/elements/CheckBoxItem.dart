import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:hab_app/src/elements/LineText.dart';

import 'package:hab_app/src/helpers/app_config.dart' as config;

class CheckBoxItem extends StatelessWidget {
  final bool isSelected;
  final String title;
  final VoidCallback onTap;

  CheckBoxItem(this.isSelected, this.title, this.onTap);
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: InkWell(
        onTap: onTap,
        child: Container(
          //padding: const EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                width: 24,
                height: 24,
                child: new Center(
                  child: Icon(
                    Icons.check,
                    color: (isSelected ? Colors.white : Colors.transparent),
                    size: 15.0,
                  ),
                ),
                decoration: new BoxDecoration(
                  color: (isSelected ? config.Colors.primaryColor() : Colors.transparent),
                  border: new Border.all(width: 1.0, color: (isSelected ? config.Colors.primaryColor() : config.Colors.customeColor("#DADADA"))),
                  borderRadius: const BorderRadius.all(const Radius.circular(4.0)),
                ),
              ),
              Container(
                margin: new EdgeInsets.only(left: 10.0),
                child: LineText(title),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class RadioItem extends StatelessWidget {
  final bool isSelected;
  final String title;
  final VoidCallback onTap;

  RadioItem(this.isSelected, this.title, this.onTap);
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: InkWell(
        onTap: onTap,
        child: Container(
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                width: 16,
                height: 16,
                child: new Center(
                  child: Icon(
                    Icons.check,
                    color: (isSelected ? Colors.white : Colors.transparent),
                    size: 15.0,
                  ),
                ),
                decoration: new BoxDecoration(
                  color: (isSelected ? config.Colors.primaryColor() : Colors.transparent),
                  border: new Border.all(width: 1.0, color: (isSelected ? config.Colors.primaryColor() : config.Colors.customeColor("#DADADA"))),
                  borderRadius: const BorderRadius.all(const Radius.circular(24.0)),
                ),
              ),
              Container(
                margin: new EdgeInsets.only(left: 10.0),
                child: LineText(title),
              )
            ],
          ),
        ),
      ),
    );
  }
}
