import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;

class Button extends StatelessWidget {
  const Button({
    Key key,
    this.text,
    this.child,
    this.textStyle,
    this.color,
    this.onPressed,
    this.isActive = true,
  }) : super(key: key);

  final Widget child;
  final String text;
  final VoidCallback onPressed;
  final TextStyle textStyle;
  final Color color;
  final bool isActive;

  @override
  Widget build(BuildContext context) {
    // text
    Widget _child = this.child;
    if (this.text != null && this.text.isNotEmpty) {
      TextStyle _textStyle = Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.buttonColor());
      if (this.textStyle != null) _textStyle = _textStyle.merge(this.textStyle);
      _child = Text(
        this.text,
        style: _textStyle,
      );
    }
    // Color
    Color _color = this.color;
    if (_color == null) {
      _color = config.Colors.primaryColor();
    }
    return FlatButton(
      child: _child,
      onPressed: () {
        if (this.isActive)
          this.onPressed();
        else
          print("button not active");
      },
      color: isActive ? _color : _color.withOpacity(0.35),
    );
  }
}

class Button2 extends StatelessWidget {
  const Button2({
    Key key,
    this.text,
    this.child,
    this.textStyle,
    this.color,
    this.onPressed,
    this.isActive = true,
  }) : super(key: key);

  final Widget child;
  final String text;
  final VoidCallback onPressed;
  final TextStyle textStyle;
  final Color color;
  final bool isActive;

  @override
  Widget build(BuildContext context) {
    // text
    Widget _child = this.child;
    if (this.text != null && text.isNotEmpty) {
      TextStyle _textStyle = Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.primaryColor(), fontSize: 16);
      if (this.textStyle != null) _textStyle = _textStyle.merge(this.textStyle);
      _child = Text(
        this.text,
        style: _textStyle,
      );
    }
    // Color
    Color _color = this.color;
    if (_color == null) {
      _color = config.Colors.inputBgColor();
    }
    return Button(
      text: null,
      child: _child,
      onPressed: this.onPressed,
      color: _color,
      isActive: isActive,
    );
  }
}

class CircularButton extends StatelessWidget {
  final Color color;
  final Widget icon;
  final Function onClick;
  final String text;

  CircularButton({this.color, this.icon, this.text, this.onClick});

  @override
  Widget build(BuildContext context) {
    var _color = color != null ? color : config.Colors.customeColor("#1A60FF");
    return Container(
      alignment: Alignment.center,
      width: 150,
      child: Container(
        decoration: BoxDecoration(color: _color, shape: BoxShape.circle),
        width: 67,
        height: 67,
        child: IconButton(icon: icon, onPressed: onClick),
      ),
    );
  }
}

class CircularButton2 extends StatelessWidget {
  final Color color;
  final Widget icon;
  final Function onClick;
  final String text;

  CircularButton2({this.color, this.icon, this.text, this.onClick});

  @override
  Widget build(BuildContext context) {
    var _color = color != null ? color : config.Colors.customeColor("#ffffff");
    return Container(
      width: 150,
      height: 150,
      alignment: Alignment.center,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(color: _color, shape: BoxShape.circle),
            width: 67,
            height: 67,
            child: IconButton(icon: icon, onPressed: onClick),
          ),
          SizedBox(height: 5),
          SubText(text, style: TextStyle(color: Colors.white))
        ],
      ),
    );
  }
}
