class RoomDto {
  int id = null;
  String name = null;
  int user_id = null;
  String photo = null;
  int type = null;
  DateTime createdAt = null;
  DateTime updatedAt = null;
  RoomDto({
    this.id,
    this.name,
    this.user_id,
    this.photo,
    this.type,
  });

  @override
  String toString() {
    return 'RoomDto';
  }

  RoomDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'] ?? 0;
    name = json['name'] ?? '';
    type = json['type'] != null ? int.parse(json['type'].toString()) : 0;
    user_id = json['user_id'] != null ? int.parse(json['user_id'].toString()) : 0;
    photo = json['photo'] ?? '';
    try {
      createdAt = json['createdAt'] == null ? null : DateTime.parse(json['createdAt'].toString()).toLocal();
      updatedAt = json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'].toString()).toLocal();
    } catch (e) {
      print(e);
    }
  }
}
