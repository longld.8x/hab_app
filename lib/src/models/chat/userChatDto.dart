class UserChatDto {
  int id = null;
  int ref_user_id = null;
  String name = null;
  String phoneNumber = null;
  String avatar = null;
  DateTime createdAt = null;
  DateTime updatedAt = null;
  UserChatDto({
    this.id,
    this.ref_user_id,
    this.name,
    this.avatar,
    this.phoneNumber,
    this.createdAt,
  });

  @override
  String toString() {
    return 'RoomDto';
  }

  UserChatDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'] ?? 0;
    ref_user_id = json['ref_user_id'] ?? 0;
    name = json['name'] ?? '';
    avatar = json['avatar'] ?? '';
    phoneNumber = json['phoneNumber'] ?? '';
    try {
      createdAt = json['createdAt'] == null ? null : DateTime.parse(json['createdAt'].toString()).toLocal();
      updatedAt = json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'].toString()).toLocal();
    } catch (e) {
      print(e);
    }
  }
}
