import 'messageDto.dart';
import 'roomDto.dart';
import 'userChatDto.dart';

class RoomDataDto {
  RoomDto room = null;
  MessageDto lastMessage = null;
  UserChatDto user1 = null;
  UserChatDto user2 = null;
  int members = null;
  int media = null;
  RoomDataDto({this.room, this.lastMessage, this.user1, this.user2, this.media, this.members});

  @override
  String toString() {
    return 'RoomDto';
  }
}
