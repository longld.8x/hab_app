import 'package:hab_app/src/models/chat/userChatDto.dart';

class MessageDto {
  int id = null;
  int user_id = null;
  String message = null;
  String message_type = null;
  String message_code = null;
  int room_id = null;
  bool seen = null;
  UserChatDto user = null;
  DateTime createdAt = null;
  DateTime updatedAt = null;
  MessageDto({
    this.id,
    this.user_id,
    this.message,
    this.message_type,
    this.message_code,
    this.room_id,
    this.seen = false,
    this.user,
    this.createdAt,
    this.updatedAt,
  });

  @override
  String toString() {
    return 'id: ${this.id},user_id: ${this.user_id},message: ${this.message},message_code: ${this.message_code},room_id: ${this.room_id}';
  }

  MessageDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'] ?? 0;
    user_id = json['user_id'] ?? 0;
    message = json['message'].toString() ?? '';
    message_type = json['message_type'].toString() ?? '';
    message_code = json['message_code'].toString() ?? '';
    room_id = json['room_id'] ?? 0;
    seen = json['seen'] ?? false;
    try {
      createdAt = json['createdAt'] == null ? null : DateTime.parse(json['createdAt'].toString()).toLocal();
      //print("createdAt createdAt $createdAt ${json}");
      updatedAt = json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'].toString()).toLocal();
    } catch (e) {
      print(e);
    }
    user = json['user'] != null ? UserChatDto.fromJSON(json['user']) : null;
  }
}
