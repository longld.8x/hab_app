import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DocumentDto {
  String documentCode = null;

  String documentName = null;

  String contentUrl = null;

  int status = null;

  DateTime creationTime = null;

  int creatorUserId = null;

  int id = null;

  int documentType = null;
  //      Text = 1,
  //      File = 2,
  //      Video = 3,
  //      Image = 4,
  //      Sound = 5,
  //      Doc = 6,
  //      Excel = 7,
  //      PowerPoint = 8
  Widget get docTypeView => Container(
        child: SvgPicture.asset(
          documentType == 6
              ? "assets/icon/file_word.svg"
              : documentType == 7
                  ? "assets/icon/file_excel.svg"
                  : documentType == 8
                      ? "assets/icon/file_powerpoint.svg"
                      : 'assets/icon/icon_file.svg',
          width: 23,
          height: 23,
        ),
      );

  DocumentDto();

  @override
  String toString() {
    return 'DocumentDto[documentCode=$documentCode, documentName=$documentName, contentUrl=$contentUrl, documentType=$documentType, status=$status, creationTime=$creationTime, creatorUserId=$creatorUserId, id=$id, ]';
  }

  DocumentDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    documentCode = json['documentCode'];
    documentName = json['documentName'];
    contentUrl = json['contentUrl'];
    documentType = (json['documentType']);
    status = (json['status']);
    creationTime = json['creationTime'] == null ? null : DateTime.parse(json['creationTime']);
    creatorUserId = json['creatorUserId'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    return {'documentCode': documentCode, 'documentName': documentName, 'contentUrl': contentUrl, 'documentType': documentType, 'status': status, 'creationTime': creationTime == null ? '' : creationTime.toUtc().toIso8601String(), 'creatorUserId': creatorUserId, 'id': id};
  }

  static List<DocumentDto> listfromJSON(List<dynamic> json) {
    return json == null ? new List<DocumentDto>() : json.map((value) => new DocumentDto.fromJSON(value)).toList();
  }

  static Map<String, DocumentDto> mapfromJSON(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, DocumentDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new DocumentDto.fromJSON(value));
    }
    return map;
  }
}
