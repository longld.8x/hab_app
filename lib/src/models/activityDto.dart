import 'dart:convert';

import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/accountClassDto.dart';

class ActivityDto {
  String id = null;

  String activityType = null;

  String accountCode = null;

  List<String> mediaUrls = null;

  String content = null;

  String activityDetail = null;

  DateTime createdDate = null;

  int approvedUserId = null;

  DateTime approvedDate = null;

  DateTime modifiedDate = null;

  int score = null;

  String objectType = null;

  String objectIdentify = null;

  String username = null;

  int userId = null;

  int status = null;
  AccountLeaderClassDto user = null;

  ActivityDto();

  @override
  String toString() {
    return 'ActivityDto[id=$id, activityType=$activityType, accountCode=$accountCode, mediaUrls=$mediaUrls, content=$content, activityDetail=$activityDetail, createdDate=$createdDate, approvedUserId=$approvedUserId, approvedDate=$approvedDate, modifiedDate=$modifiedDate, score=$score, objectType=$objectType, objectIdentify=$objectIdentify, username=$username, userId=$userId, status=$status, ]';
  }

  ActivityDto.fromJSON(Map<String, dynamic> jsonMap) {
    if (json == null) return;
    id = jsonMap['id'];
    activityType = jsonMap['activityType'];
    accountCode = jsonMap['accountCode'];
    print("mediaUrls ${jsonMap['mediaUrls']}");
    if (jsonMap['mediaUrls'] != null && jsonMap['mediaUrls'].toString().isNotEmpty && jsonMap['mediaUrls'].toString().contains("|")) {
      mediaUrls = jsonMap['mediaUrls'].toString().split('|');
    } else if (jsonMap['mediaUrls'] != null && jsonMap['mediaUrls'].toString().isNotEmpty && jsonMap['mediaUrls'].toString().contains("[")) {
      mediaUrls = json.decode(jsonMap['mediaUrls']).map<String>((x) => x.toString()).toList();
    }
    content = jsonMap['content'];
    activityDetail = jsonMap['activityDetail'];

    approvedUserId = jsonMap['approvedUserId'];
    try {
      createdDate = jsonMap['createdDate'] == null ? null : Helper.parseDate(jsonMap['createdDate']);
      approvedDate = jsonMap['approvedDate'] == null ? null : Helper.parseDate(jsonMap['approvedDate']);
      modifiedDate = jsonMap['modifiedDate'] == null ? null : Helper.parseDate(jsonMap['modifiedDate']);
    } catch (e) {}

    score = jsonMap['score'];
    objectType = jsonMap['objectType'];
    objectIdentify = jsonMap['objectIdentify'];
    username = jsonMap['username'];
    userId = jsonMap['userId'];
    status = jsonMap['status'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'activityType': activityType,
      'accountCode': accountCode,
      'mediaUrls': mediaUrls,
      'content': content,
      'activityDetail': activityDetail,
      'createdDate': createdDate == null ? '' : createdDate.toUtc().toIso8601String(),
      'approvedUserId': approvedUserId,
      'approvedDate': approvedDate == null ? '' : approvedDate.toUtc().toIso8601String(),
      'modifiedDate': modifiedDate == null ? '' : modifiedDate.toUtc().toIso8601String(),
      'score': score,
      'objectType': objectType,
      'objectIdentify': objectIdentify,
      'username': username,
      'userId': userId,
      'status': status
    };
  }

  static List<ActivityDto> listfromJSON(List<dynamic> json) {
    return json == null ? new List<ActivityDto>() : json.map((value) => new ActivityDto.fromJSON(value)).toList();
  }

  static Map<String, ActivityDto> mapfromJSON(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ActivityDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new ActivityDto.fromJSON(value));
    }
    return map;
  }
}
