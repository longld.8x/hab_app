import 'dart:convert';

import '../helpers/custom_trace.dart';

class Course {
  int id;
  double productValue;
  String productCode;
  String productName;
  double discountValue;
  double discountAmount;
  double paymentAmount;
  double fixAmount;
  String description;
  int productType;
  int refId;
  bool isDiscount;
  bool isBestSallers;
  bool isNew;
  bool isTop;
  bool isSale;
  List<String> images;
  String role;
  String thumbnail;
  String tag;
  double rank;
  int totalBuy;

  Course({
    this.id,
    this.productValue,
    this.productCode,
    this.productName,
    this.discountValue,
    this.discountAmount,
    this.paymentAmount,
    this.fixAmount,
    this.description,
    this.productType,
    this.isDiscount,
    this.isNew,
    this.isTop,
    this.isSale,
    this.images,
    this.rank,
    this.totalBuy,
    this.thumbnail,
    this.tag,
  });

  Course.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'];
      productName = jsonMap['productName'] != null ? jsonMap['productName'].toString() : '';
      productCode = jsonMap['productCode'] != null ? jsonMap['productCode'].toString() : '';
      productValue = jsonMap['productValue'] != null ? double.parse(jsonMap['productValue'].toString()) : 0.0;
      discountValue = jsonMap['discountValue'] != null ? double.parse(jsonMap['discountValue'].toString()) : 0.0;
      discountAmount = jsonMap['discountAmount'] != null ? double.parse(jsonMap['discountAmount'].toString()) : 0.0;
      paymentAmount = jsonMap['paymentAmount'] != null ? double.parse(jsonMap['paymentAmount'].toString()) : 0.0;
      fixAmount = jsonMap['fixAmount'] != null ? double.parse(jsonMap['fixAmount'].toString()) : 0.0;
      fixAmount = jsonMap['fixAmount'] != null ? double.parse(jsonMap['fixAmount'].toString()) : 0.0;
      description = jsonMap['description'] != null ? (jsonMap['description'].toString()) : '';
      productType = jsonMap['productType'] != null ? int.parse(jsonMap['productType'].toString()) : 0;
      refId = jsonMap['refId'] != null ? int.parse(jsonMap['refId'].toString()) : 0;
      isDiscount = jsonMap['isDiscount'] != null ? (jsonMap['isDiscount']) : false;
      isNew = jsonMap['isNew'] != null ? (jsonMap['isNew']) : false;
      isTop = jsonMap['isTop'] != null ? (jsonMap['isTop']) : false;
      if (isNew) tag = "Mới";
      isSale = jsonMap['isSale'] != null ? (jsonMap['isSale']) : false;

      rank = jsonMap['rank'] != null ? double.parse(jsonMap['rank'].toString()) : 5.0;
      totalBuy = jsonMap['totalBuy'] != null ? int.parse(jsonMap['totalBuy'].toString()) : 0;

      if (jsonMap['images'] != null && jsonMap['images'].toString().isNotEmpty) {
        images = json.decode(jsonMap["images"]).map<String>((x) => x.toString()).toList();
        if (images.length > 0) thumbnail = images[0];
      }
    } catch (e) {
      id = 0;
      productValue = 0.0;
      productCode = "";
      productName = "";
      discountValue = 0.0;
      discountAmount = 0.0;
      paymentAmount = 0.0;
      fixAmount = 0.0;
      description = "";
      productType = 1;
      refId = 1;
      isDiscount = false;
      isBestSallers = false;
      isNew = false;
      isTop = false;
      isSale = false;
      images = <String>[];
      print(CustomTrace(StackTrace.current, message: e));
    }
  }
}

class CoursePreview {
  int id;
  double productValue;
  String productCode;
  String productName;
  double discountValue;
  double discountAmount;
  double paymentAmount;
  double fixAmount;
  String courseDescription;
  int productType;
  int refId;
  bool isDiscount;
  bool isBestSallers;
  bool isNew;
  bool isTop;
  bool isSale;
  List<String> images;
  String role;
  String thumbnail;
  String tag;
  double rank;
  int buyer;

  // details
  int productId;
  List<dynamic> courseResults;
  String courseCode;
  String courseName;

  CoursePreview({
    this.id,
    this.productValue,
    this.productCode,
    this.productName,
    this.discountValue,
    this.discountAmount,
    this.paymentAmount,
    this.fixAmount,
    this.courseDescription,
    this.productType,
    this.isDiscount,
    this.isNew,
    this.isTop,
    this.isSale,
    this.images,
    this.rank,
    this.buyer,
    this.thumbnail,
    this.tag,
  });

  CoursePreview.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'];
      productId = jsonMap['productId'] ?? 0;
      productName = jsonMap['productName'] != null ? jsonMap['productName'].toString() : '';
      productCode = jsonMap['productCode'] != null ? jsonMap['productCode'].toString() : '';
      productValue = jsonMap['productValue'] != null ? double.parse(jsonMap['productValue'].toString()) : 0.0;
      discountValue = jsonMap['discountValue'] != null ? double.parse(jsonMap['discountValue'].toString()) : 0.0;
      discountAmount = jsonMap['discountAmount'] != null ? double.parse(jsonMap['discountAmount'].toString()) : 0.0;
      paymentAmount = jsonMap['paymentAmount'] != null ? double.parse(jsonMap['paymentAmount'].toString()) : 0.0;
      fixAmount = jsonMap['fixAmount'] != null ? double.parse(jsonMap['fixAmount'].toString()) : 0.0;
      fixAmount = jsonMap['fixAmount'] != null ? double.parse(jsonMap['fixAmount'].toString()) : 0.0;
      courseDescription = jsonMap['courseDescription'] != null ? (jsonMap['courseDescription'].toString()) : '';
      productType = jsonMap['productType'] != null ? int.parse(jsonMap['productType'].toString()) : 0;
      refId = jsonMap['refId'] != null ? int.parse(jsonMap['refId'].toString()) : 0;
      isDiscount = jsonMap['isDiscount'] != null ? (jsonMap['isDiscount']) : false;
      isNew = jsonMap['isNew'] != null ? (jsonMap['isNew']) : false;
      isTop = jsonMap['isTop'] != null ? (jsonMap['isTop']) : false;
      if (isNew) tag = "Mới";
      isSale = jsonMap['isSale'] != null ? (jsonMap['isSale']) : false;

      rank = jsonMap['rank'] != null ? double.parse(jsonMap['rank'].toString()) : 0.0;
      buyer = jsonMap['buyer'] != null ? int.parse(jsonMap['buyer'].toString()) : 11000;

      if (jsonMap['images'] != null && jsonMap['images'].toString().isNotEmpty) {
        images = json.decode(jsonMap["images"]).map<String>((x) => x.toString()).toList();
        if (images.length > 0) thumbnail = images[0];
      } else {
        images = <String>[];
      }
      if (jsonMap['courseResults'] != null && jsonMap['courseResults'].toString().isNotEmpty) {
        try {
          courseResults = (jsonMap["courseResults"]);
        } catch (e) {
          print("courseResults $e");
        }
      } else {
        courseResults = <String>[];
      }
      courseCode = jsonMap['courseCode'] != null ? jsonMap['courseCode'].toString() : '';
      courseName = jsonMap['courseName'] != null ? jsonMap['courseName'].toString() : '';
    } catch (e) {
      id = 0;
      productValue = 0.0;
      productCode = "";
      productName = "";
      discountValue = 0.0;
      discountAmount = 0.0;
      paymentAmount = 0.0;
      fixAmount = 0.0;
      courseDescription = "";
      productType = 1;
      refId = 1;
      isDiscount = false;
      isBestSallers = false;
      isNew = false;
      isTop = false;
      isSale = false;
      images = <String>[];
      courseResults = <String>[];
      print(CustomTrace(StackTrace.current, message: e));
    }
  }
}
