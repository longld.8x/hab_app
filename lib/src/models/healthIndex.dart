import 'package:hab_app/src/helpers/helper.dart';

class HealthIndexDto {
  String id = null;
  String accountCode = null;
  DateTime createdDate = null;
  String description = null;
  int status = null;
  double height = null; // cao
  double weight = null; // nang
  double bust = null; // v1
  double waist = null; // v2
  double hip = null; // v3
  int gender = null; // gioi tinh
  int workoutIntensity = null; // cuong do luyen tap

  HealthIndexDto({
    this.id,
    this.accountCode,
    this.createdDate,
    this.description,
    this.status,
    this.height,
    this.weight,
    this.bust,
    this.waist,
    this.hip,
    this.gender,
    this.workoutIntensity,
  });
  @override
  String toString() {
    return 'HealthIndexDto[id=$id,accountCode=$accountCode, createdDate=$createdDate, description=$description, height=$height'
        ', weight=$weight, bust=$bust, waist=$waist, hip=$hip, gender=$gender, workoutIntensity=$workoutIntensity]';
  }

  HealthIndexDto.fromJSON(Map<String, dynamic> jsonMap) {
    if (jsonMap == null) return;
    id = jsonMap['id'];
    accountCode = jsonMap['accountCode'];
    description = jsonMap['description'];
    status = jsonMap['status'];
    createdDate = jsonMap['createdDate'] == null ? null : Helper.parseDate(jsonMap['createdDate']);
    // print("jsonMap['indexes']");
    // print(jsonMap['indexes']);
    // print(jsonMap['indexes'][0]['indexType']);
    if (jsonMap['indexes'] != null) {
      (jsonMap['indexes']).map((e) {
        if (e['indexType'].toString().toLowerCase() == 'height') height = double.parse(e['value'] != null ? e['value'].toString() : "0");
        if (e['indexType'].toString().toLowerCase() == 'weight') weight = double.parse(e['value'] != null ? e['value'].toString() : "0");
        if (e['indexType'].toString().toLowerCase() == 'bust') bust = double.parse(e['value'] != null ? e['value'].toString() : "0");
        if (e['indexType'].toString().toLowerCase() == 'waist') waist = double.parse(e['value'] != null ? e['value'].toString() : "0");
        if (e['indexType'].toString().toLowerCase() == 'hip') hip = double.parse(e['value'] != null ? e['value'].toString() : "0");
        if (e['indexType'].toString().toLowerCase() == 'gender') gender = int.parse(e['value'] != null ? e['value'].toString() : "0");
        if (e['indexType'].toString().toLowerCase() == 'workoutintensity') workoutIntensity = int.parse(e['value'] != null ? e['value'].toString() : "0");
      }).toList();
    } else {
      height = 0;
      weight = 0;
      bust = 0;
      waist = 0;
      hip = 0;
      gender = 0;
      workoutIntensity = 0;
    }
  }

  static List<HealthIndexDto> listFromJson(List<dynamic> json) {
    return json == null ? new List<HealthIndexDto>() : json.map((value) => new HealthIndexDto.fromJSON(value)).toList();
  }
}

class IndexDto {
  String description = null;
  String indexType = null; // Height, Weight, Bust, Waist, Hip, TextInfo, Gender, WorkoutIntensity, RMR, AMR, TMR, Y
  String text = null;
  String value = null;

  IndexDto();
  @override
  String toString() {
    return 'IndexDto[indexType=$indexType, text=$text, value=$value, description=$description]';
  }

  IndexDto.fromJSON(Map<String, dynamic> jsonMap) {
    if (jsonMap == null) return;
    indexType = jsonMap['indexType'];
    text = jsonMap['text'];
    value = jsonMap['value'];
    description = jsonMap['description'];
  }

  static List<IndexDto> listFromJson(List<dynamic> json) {
    return json == null ? new List<IndexDto>() : json.map((value) => new IndexDto.fromJSON(value)).toList();
  }
}
