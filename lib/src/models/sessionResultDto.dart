class SessionResultDto {
  int sessionId = null;
  int classSessionId = null;
  int order = null;
  String sessionName = null;
  String description = null;
  String fromTime = null;
  String toTime = null;
  DateTime startDate = null;
  DateTime endDate = null;
  String linkRoom = null;
  // 1 = online
  // 2 - offline
  int sessionType = null;
  // public enum SessionStatus : byte
  //   {
  //       Pending = 0,
  //       InPorcess = 1,
  //       Finished = 2,
  //       Cancelled = 3,
  //       ChangedDate = 4
  //   }
  int sessionStatus = null;

  bool isEnable = null;
  bool joined = false;

  String get sessionStatusView => sessionStatus == 0
      ? "Chưa học"
      : sessionStatus == 1
          ? "Đang học"
          : sessionStatus == 2
              ? "Đã học"
              : sessionStatus == 3
                  ? "Đã hủy"
                  : sessionStatus == 4
                      ? "Đổi ngày học"
                      : "";

  String address = '';
  SessionResultDto({this.sessionName, this.startDate});

  @override
  String toString() {
    return 'SessionResultDto[sessionId=$sessionId,sessionName=$sessionName, order=$order,isEnable=$isEnable, description=$description, fromTime=$fromTime, toTime=$toTime, startDate=$startDate, linkRoom=$linkRoom, sessionType=$sessionType, ]';
  }

  SessionResultDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    sessionId = json['sessionId'] ?? 0;
    classSessionId = json['classSessionId'] ?? 0;
    sessionName = json['sessionName'] ?? '';
    order = json['order'];
    description = json['description'] ?? '';
    fromTime = json['fromTime'];
    toTime = json['toTime'];
    startDate = json['startDate'] == null ? null : DateTime.parse(json['startDate']);
    endDate = json['endDate'] == null ? null : DateTime.parse(json['endDate']);
    linkRoom = json['linkRoom'];
    sessionType = (json['sessionType']);
    isEnable = (json['isEnable']);
    sessionStatus = (json['sessionStatus']);
    address = (json['address']) ?? '';
    joined = (json['joined']) ?? false;
  }

  Map<String, dynamic> toJson() {
    return {'sessionId': sessionId, 'sessionName': sessionName, 'order': order, 'description': description, 'fromTime': fromTime, 'toTime': toTime, 'startDate': startDate == null ? '' : startDate.toUtc().toIso8601String(), 'linkRoom': linkRoom, 'sessionType': sessionType};
  }

  static List<SessionResultDto> listfromJSON(List<dynamic> json) {
    return json == null ? new List<SessionResultDto>() : json.map((value) => new SessionResultDto.fromJSON(value)).toList();
  }

  static Map<String, SessionResultDto> mapfromJSON(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, SessionResultDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new SessionResultDto.fromJSON(value));
    }
    return map;
  }
}
