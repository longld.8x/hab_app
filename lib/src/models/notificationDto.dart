import 'package:hab_app/src/helpers/helper.dart';

class NotificationDto {
  int tenantId = null;
  DateTime createdDate = null;
  DateTime modifiedDate = null;
  String description = null;
  String body = null;
  dynamic data = null;
  String notifitionId = null;
  String title = null;
  int userId = null;
  String icon = null;
  int state = null;
  int creatorUserId = null;
  String notificationType = null;
  String id = null;
  String appNotificationName = null;
  NotificationDto({
    this.appNotificationName,
    this.data,
  });
  @override
  String toString() {
    return 'NotificationDto[tenantId=$tenantId, createdDate=$createdDate, modifiedDate=$modifiedDate, description=$description, body=$body, data=$data, notifitionId=$notifitionId, title=$title, userId=$userId, icon=$icon, state=$state, creatorUserId=$creatorUserId, notificationType=$notificationType, id=$id, appNotificationName=$appNotificationName, ]';
  }

  NotificationDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    tenantId = json['tenantId'];
    createdDate = Helper.parseDate(json['createdDate']);
    modifiedDate = Helper.parseDate(json['modifiedDate']);

    description = json['description'];
    body = json['body'];
    data = json['data'];
    notifitionId = json['notifitionId'];
    title = json['title'];
    userId = json['userId'];
    icon = json['icon'];
    state = json['state'] == null ? 0 : json['state'];
    creatorUserId = json['creatorUserId'];
    notificationType = json['notificationType'];
    id = json['id'];
    appNotificationName = json['appNotificationName'];
  }

  Map<String, dynamic> toJson() {
    return {
      'tenantId': tenantId,
      'createdDate': createdDate == null ? '' : createdDate.toUtc().toIso8601String(),
      'modifiedDate': modifiedDate == null ? '' : modifiedDate.toUtc().toIso8601String(),
      'description': description,
      'body': body,
      'data': data,
      'notifitionId': notifitionId,
      'title': title,
      'userId': userId,
      'icon': icon,
      'state': state,
      'creatorUserId': creatorUserId,
      'notificationType': notificationType,
      'id': id,
      'appNotificationName': appNotificationName
    };
  }

  static List<NotificationDto> listFromJson(List<dynamic> json) {
    return json == null ? new List<NotificationDto>() : json.map((value) => new NotificationDto.fromJSON(value)).toList();
  }

  static Map<String, NotificationDto> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, NotificationDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new NotificationDto.fromJSON(value));
    }
    return map;
  }
}

class FirebaseNotificationDto {
  String title = null;
  String body = null;
  FirebaseNotificationDto({this.title, this.body});
  FirebaseNotificationDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    title = json['title'];
    body = json['body'];
  }
}

class FirebaseDataDto {
  String appNotificationName = null;
  String type = null;
  String click_action = null;
  dynamic message = null;
  dynamic user = null;
  dynamic room = null;
  FirebaseDataDto({
    this.appNotificationName,
    this.type,
    this.click_action,
    this.user,
    this.room,
  });
  FirebaseDataDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    appNotificationName = (json['appNotificationName'] ?? '').toString();
    type = (json['type'] ?? '').toString();
    click_action = (json['click_action'] ?? '').toString();
    message = json['message'] ?? null;
    user = json['user'] ?? null;
    room = json['room'] ?? null;
  }
}

class FirebaseMessagingDto {
  FirebaseNotificationDto notification = null;
  FirebaseDataDto data = null;
  FirebaseMessagingDto({
    this.notification,
    this.data,
  });
}
