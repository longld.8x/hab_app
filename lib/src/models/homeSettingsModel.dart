import '../helpers/custom_trace.dart';

class HomeSettingsModel {
  ///calo,follow_eat,follow_water,follow_gym,work,my_courses,courses
  String id;
  String name;
  //int position;
  bool active;

  HomeSettingsModel({
    this.id,
    this.name,
    //this.position,
    this.active,
  });

  HomeSettingsModel.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'] != null ? jsonMap['name'].toString() : '';
      //position = int.parse(jsonMap['position'] != null ? jsonMap['position'].toString() : '0');
      active = jsonMap['active'] != null ? jsonMap['active'] : false;
    } catch (e) {
      id = '';
      name = '';
      //position = 9999;
      active = false;
    }
  }

  Map toJson() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["name"] = name;
    map["active"] = active;
    return map;
  }
}
