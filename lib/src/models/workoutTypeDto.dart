class WorkoutTypeDto {
  // [{"workoutCategoryName":"Đạp xe","workoutCategoryCode":"001","workoutUnit":1,"workoutCalo":1000.00,"isActive":true,"id":1}]
  String workoutCategoryName = null;
  String workoutCategoryCode = null;
  int workoutUnit = null;
  double workoutCalo = null;
  bool isActive = null;
  int id = null;
  WorkoutTypeDto({
    this.workoutCategoryName,
    this.workoutCategoryCode,
    this.workoutUnit,
    this.workoutCalo,
    this.isActive,
    this.id,
  });

  @override
  String toString() {
    return 'WorkoutTypeDto[ workoutCategoryName=$workoutCategoryName, workoutCategoryCode=$workoutCategoryCode, id=$id, workoutCalo=$workoutCalo, ]';
  }

  WorkoutTypeDto.fromJSON(Map<String, dynamic> jsonMap) {
    if (jsonMap == null) return;
    try {
      workoutCategoryName = jsonMap['workoutCategoryName'] ?? '';
      workoutCategoryCode = jsonMap['workoutCategoryCode'] ?? '';
      workoutUnit = int.parse(jsonMap['workoutUnit'] != null ? jsonMap['workoutUnit'].toString() : '1');
      workoutCalo = double.parse(jsonMap['workoutCalo'] != null ? jsonMap['workoutCalo'].toString() : '0');
      isActive = jsonMap['isActive'];
      id = jsonMap['id'] ?? 0;
    } catch (e) {
      print("WorkoutTypeDto.fromJSON ${jsonMap}");
      print(e);
    }
  }

  static Map<String, WorkoutTypeDto> mapfromJSON(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, WorkoutTypeDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new WorkoutTypeDto.fromJSON(value));
    }
    return map;
  }
}
