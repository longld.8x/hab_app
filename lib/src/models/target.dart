import '../helpers/custom_trace.dart';

class Target {
  int id;
  int userId;
  String name;
  DateTime startDate;
  DateTime endDate;
  double totalPoint;
  double currentPoint;

  Target({
    this.id,
    this.userId,
    this.name,
    this.startDate,
    this.endDate,
    this.totalPoint,
    this.currentPoint,
  });

  Target.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'];
      userId = jsonMap['userId'];
      name = jsonMap['name'] != null ? jsonMap['name'] : '';
      startDate = jsonMap['startDate'] != null ? DateTime.parse(jsonMap['startDate']) : null;
      endDate = jsonMap['endDate'] != null ? DateTime.parse(jsonMap['endDate']) : null;
      totalPoint = double.parse(jsonMap['totalPoint'] != null ? jsonMap['totalPoint'].toString() : '');
      currentPoint = double.parse(jsonMap['currentPoint'] != null ? jsonMap['currentPoint'].toString() : '');
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
    }
  }
}
