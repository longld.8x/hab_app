class CaloInfoDto {
  double caloLimit = null;
  double caloInput = null;
  double caloExchange = null;
  double eat = null;
  double eatLimit = null;
  double drink = null;
  double drinkLimit = null;
  double exercise = null;
  double exerciseLimit = null;
  CaloInfoDto({
    this.caloLimit,
    this.caloInput,
    this.caloExchange,
    this.eat,
    this.eatLimit,
    this.drink,
    this.drinkLimit,
    this.exercise,
    this.exerciseLimit,
  });

  @override
  String toString() {
    return 'CaloInfoDto[ caloLimit=$caloLimit, caloInput=$caloInput, caloExchange=$caloExchange,'
        'eat=$eat,eatLimit=$eatLimit,'
        'drink=$drink,drinkLimit=$drinkLimit,'
        'exercise=$exercise,exerciseLimit=$exerciseLimit, ]';
  }

  CaloInfoDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    try {
      caloLimit = json['caloLimit'] != null ? double.parse(json['caloLimit'].toString()) : 0;
      caloInput = json['caloInput'] != null ? double.parse(json['caloInput'].toString()) : 0;
      caloExchange = json['caloExchange'] != null ? double.parse(json['caloExchange'].toString()) : 0;
      eat = json['eat'] != null ? double.parse(json['eat'].toString()) : 0;
      eatLimit = json['eatLimit'] != null ? double.parse(json['eatLimit'].toString()) : 0;
      drink = json['drink'] != null ? double.parse(json['drink'].toString()) : 0;
      drinkLimit = json['drinkLimit'] != null ? double.parse(json['drinkLimit'].toString()) : 0;
      exercise = json['exercise'] != null ? double.parse(json['exercise'].toString()) : 0;
      exerciseLimit = json['exerciseLimit'] != null ? double.parse(json['exerciseLimit'].toString()) : 0;
    } catch (e) {
      print("CaloInfoDto.fromJSON ${json}");
      print(e);
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'caloLimit': caloLimit,
      'caloInput': caloInput,
      'caloExchange': caloExchange,
      'eat': eat,
      'eatLimit': eatLimit,
      'drink': drink,
      'drinkLimit': drinkLimit,
      'exercise': exercise,
      'exerciseLimit': exerciseLimit,
    };
  }
}
