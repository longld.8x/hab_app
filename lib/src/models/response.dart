import 'package:hab_app/src/models/accountClassDto.dart';
import 'package:hab_app/src/models/activityDto.dart';
import 'package:hab_app/src/models/commentDto.dart';
import 'package:hab_app/src/models/course.dart';
import 'package:hab_app/src/models/documentDto.dart';
import 'package:hab_app/src/models/exercisesResultDto.dart';
import 'package:hab_app/src/models/foodDto.dart';
import 'package:hab_app/src/models/friendUser.dart';
import 'package:hab_app/src/models/healthIndex.dart';
import 'package:hab_app/src/models/myClassDetailDto.dart';
import 'package:hab_app/src/models/notificationDto.dart';
import 'package:hab_app/src/models/sessionResultDto.dart';
import 'package:hab_app/src/models/unitDto.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/models/workDto.dart';

class ResponseMessage {
  String errorCode;
  String message;
  dynamic results;

  bool get success => errorCode == "00";

  ResponseMessage(this.errorCode, this.message);

  ResponseMessage.fromJSON(Map<dynamic, dynamic> jsonMap) {
    try {
      errorCode = jsonMap['responseStatus']['errorCode'] != null ? jsonMap['responseStatus']['errorCode'] : "01";
      message = jsonMap['responseStatus']['message'] != null ? jsonMap['responseStatus']['message'] : "";
      results = jsonMap['responseStatus']['results'] != null ? jsonMap['responseStatus']['results'] : (jsonMap['results'] != null ? jsonMap['results'] : "");
    } catch (e) {
      print(e);
    }
  }
  ResponseMessage.success({dynamic result}) {
    this.errorCode = "00";
    this.message = "";
    this.results = result;
  }
  ResponseMessage.error({String message}) {
    this.errorCode = "01";
    this.message = message != null ? message : "Có lỗi trong quá trình xử lý";
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["success"] = success;
    map["errorCode"] = errorCode;
    map["message"] = message;
    map["results"] = results;
    return map;
  }
}

class PageResult<T> {
  int totalCount;
  int totalPage;
  List<T> items;

  PageResult({this.totalCount, this.totalPage, this.items});

  PageResult.fromJSON(Map<dynamic, dynamic> jsonMap) {
    // print("jsonMap['totalCount'] ${jsonMap['totalCount']}");
    // print("jsonMap['items'] ${jsonMap['items']}");
    if (jsonMap == null) {
      this.totalCount = 0;
      this.totalPage = 0;
      this.items = [];
    } else {
      try {
        this.totalCount = jsonMap['totalCount'] != null ? jsonMap['totalCount'] : 0;
        this.totalPage = jsonMap['totalPages'] != null ? jsonMap['totalPages'] : 0;
        this.items = jsonMap['items'] != null
            ? jsonMap["items"].map<T>((json) => fromJsonObject<T>(json))?.toList()
            : jsonMap['result'] != null
                ? jsonMap["result"].map<T>((json) => fromJsonObject<T>(json))?.toList()
                : <T>[];
      } catch (e) {
        print("PageResult.fromJSON $e");
      }
    }
  }

  T fromJsonObject<T>(dynamic json) {
    // print("fromJsonObject $json");
    if (T == User) {
      return User.fromJSON(json) as T;
    } else if (T == Course) {
      return Course.fromJSON(json) as T;
    } else if (T == MyClassDetailDto) {
      return MyClassDetailDto.fromJSON(json) as T;
    } else if (T == SessionResultDto) {
      return SessionResultDto.fromJSON(json) as T;
    } else if (T == AccountClassDto) {
      return AccountClassDto.fromJSON(json) as T;
    } else if (T == ExercisesResultDto) {
      return ExercisesResultDto.fromJSON(json) as T;
    } else if (T == DocumentDto) {
      return DocumentDto.fromJSON(json) as T;
    } else if (T == CommentDto) {
      return CommentDto.fromJSON(json) as T;
    } else if (T == FoodDto) {
      return FoodDto.fromDtoJSON(json) as T;
    } else if (T == UnitDto) {
      return UnitDto.fromJSON(json) as T;
    } else if (T == ActivityDto) {
      return ActivityDto.fromJSON(json) as T;
    } else if (T == NotificationDto) {
      return NotificationDto.fromJSON(json) as T;
    } else if (T == FriendUser) {
      return FriendUser.fromJSON(json) as T;
    } else if (T == WorkDto) {
      return WorkDto.fromJSON(json) as T;
    } else if (T == HealthIndexDto) {
      return HealthIndexDto.fromJSON(json) as T;
    } else {
      throw Exception("Unknown class");
    }
  }

  PageResult.noData() {
    this.totalCount = 0;
    this.items = <T>[];
  }
}
