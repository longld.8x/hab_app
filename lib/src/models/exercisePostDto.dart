import 'package:hab_app/src/models/accountClassDto.dart';
import 'package:hab_app/src/models/activityDto.dart';

class ExercisePostDto {
  AccountLeaderClassDto user;
  ActivityDto post;
  List<ActivityDto> comments;
  int totalComment;
  int viewTotalComment;
  ExercisePostDto({this.user, this.post, this.comments, this.totalComment, this.viewTotalComment});
}
