class ZoomDto {
  String userId = null;
  String meetingId = null;
  String meetingPassword = null;
  String zak = null;
  ZoomDto({this.userId, this.meetingId, this.meetingPassword, this.zak});

  @override
  String toString() {
    return 'ZoomDto[ userId=$userId,meetingId=$meetingId, meetingPassword=$meetingPassword, zak=$zak,]';
  }

  ZoomDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    try {
      userId = json['userId'];
      meetingId = json['meetingId'];
      meetingPassword = json['meetingPassword'];
      zak = json['zak'];
    } catch (e) {
      print("ZoomDto.fromJSON ${json}");
      print(e);
    }
  }
}
