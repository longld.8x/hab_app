import 'dart:convert';

class ExercisesResultDto {
  int exerciseId = null;
  int classId = null;
  int moduleId = null;
  int exerciseClassModuleId = null;
  int classUserModuleExercisesId = null;
  String exerciseCode = null;
  String exerciseName = null;
  DateTime deadlineTime = null;
  bool createTask = null;

  int repeatTime = null;
  // public enum SessionExerciseStatus : byte
  //   {
  //       Pending = 0, //CHưa làm
  //       Expire = 1, //Quá hạn
  //       Finished = 2, //Đã làm = Chưa sửa
  //       Edited = 3 //Đã sửa
  //   }
  int exerciseStatus = null;
  String get exerciseStatusView => exerciseStatus == 0
      ? "Chưa làm"
      : exerciseStatus == 1
          ? "Quá hạn - chưa làm"
          : exerciseStatus == 2
              ? "Chưa chữa"
              : exerciseStatus == 3
                  ? "Đã chữa"
                  : "";

  String description = null;
  List<String> content = [];

  ExercisesResultDto();

  @override
  String toString() {
    return 'ExercisesResultDto[exerciseClassModuleId=$exerciseClassModuleId,classId=$classId,moduleId=$moduleId,exerciseId=$exerciseId, exerciseCode=$exerciseCode, exerciseName=$exerciseName, exerciseStatus=$exerciseStatus, deadlineTime=$deadlineTime, createTask=$createTask, ]';
  }

  ExercisesResultDto.fromJSON(Map<String, dynamic> jsonMap) {
    if (jsonMap == null) return;
    exerciseClassModuleId = jsonMap['exerciseClassModuleId'] ?? 0;
    classId = jsonMap['classId'] ?? 0;
    moduleId = jsonMap['moduleId'] ?? 0;
    exerciseId = jsonMap['exerciseId'] ?? 0;
    classUserModuleExercisesId = jsonMap['classUserModuleExercisesId'] ?? 0;
    exerciseCode = jsonMap['exerciseCode'] ?? '';
    exerciseName = jsonMap['exerciseName'] ?? '';
    exerciseStatus = (jsonMap['exerciseStatus'] ?? 0);
    deadlineTime = jsonMap['deadlineTime'] == null ? null : DateTime.parse(jsonMap['deadlineTime']);
    createTask = jsonMap['createTask'] ?? 0;
    description = jsonMap['description'] ?? '';
    if (jsonMap['content'] != null && jsonMap['content'].toString().isNotEmpty) {
      content = json.decode(jsonMap["content"]).map<String>((x) => x.toString()).toList();
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'exerciseClassModuleId': exerciseClassModuleId,
      'classId': classId,
      'moduleId': moduleId,
      'exerciseId': exerciseId,
      'exerciseCode': exerciseCode,
      'exerciseName': exerciseName,
      'exerciseStatus': exerciseStatus,
      'deadlineTime': deadlineTime == null ? '' : deadlineTime.toUtc().toIso8601String(),
      'createTask': createTask,
      'description': description,
      'content': content,
    };
  }

  static List<ExercisesResultDto> listfromJSON(List<dynamic> json) {
    return json == null ? new List<ExercisesResultDto>() : json.map((value) => new ExercisesResultDto.fromJSON(value)).toList();
  }

  static Map<String, ExercisesResultDto> mapfromJSON(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ExercisesResultDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new ExercisesResultDto.fromJSON(value));
    }
    return map;
  }
}
