class AccountClassDto {
  int userId = null;
  String userName = null;
  String fullName = null;
  String phoneNumber = null;
  String profilePicture = null;
  bool isOnline = null;
  AccountClassDto();

  @override
  String toString() {
    return 'AccountClassDto[userId=$userId, userName=$userName, fullName=$fullName, phoneNumber=$phoneNumber,profilePicture=$profilePicture,isOnline=$isOnline, ]';
  }

  AccountClassDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    userId = json['userId'];
    userName = json['userName'];
    fullName = json['fullName'];
    phoneNumber = json['phoneNumber'];
    profilePicture = json['profilePicture'];
    isOnline = json['isOnline'] ?? false;
  }

  Map<String, dynamic> toJson() {
    return {'userId': userId, 'userName': userName, 'fullName': fullName, 'phoneNumber': phoneNumber, 'profilePicture': profilePicture, 'isOnline': isOnline};
  }

  static List<AccountClassDto> listfromJSON(List<dynamic> json) {
    return json == null ? new List<AccountClassDto>() : json.map((value) => new AccountClassDto.fromJSON(value)).toList();
  }

  static Map<String, AccountClassDto> mapfromJSON(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, AccountClassDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new AccountClassDto.fromJSON(value));
    }
    return map;
  }
}

class AccountLeaderClassDto {
  int userId = null;
  String userName = null;
  String fullName = null;
  String phoneNumber = null;
  String emailAddress = null;
  String profilePicture = null;
  String accountCode = null;
  AccountLeaderClassDto leader = null;
  bool isOnline = null;
  AccountLeaderClassDto({
    this.userId,
    this.userName,
    this.fullName,
    this.phoneNumber,
    this.emailAddress,
    this.profilePicture,
    this.accountCode,
    this.leader,
    this.isOnline,
  });

  AccountLeaderClassDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    userId = json['userId'];
    userName = json['userName'];
    fullName = json['fullName'];
    phoneNumber = json['phoneNumber'];
    emailAddress = json['emailAddress'];
    profilePicture = json['profilePicture'];
    accountCode = json['accountCode'];
    isOnline = json['isOnline'] ?? false;
    if (json['leader'] != null) {
      leader = AccountLeaderClassDto.fromJSON({
        'userId': json['leader']['userId'],
        'userName': json['leader']['userName'],
        'fullName': json['leader']['fullName'],
        'phoneNumber': json['leader']['phoneNumber'],
        'emailAddress': json['leader']['emailAddress'],
        'profilePicture': json['leader']['profilePicture'],
        'accountCode': json['leader']['accountCode'],
        'isOnline': json['leader']['isOnline'] ?? false,
      });
    } else {
      leader = null;
    }
  }
}
