class FoodDto {
  String foodCode = null;
  String foodName = null;
  int quantity = null;
  double calories = null;
  double protein = null;
  double carb = null;
  double fat = null;
  double clycemicIndex = null;
  double clycemicLoad = null;
  double fiber = null;
  int status = null;
  int foodGroupId = null;
  int foodUnitId = null;
  int userId = null;
  int id = null;
  String foodGroupFoodGroupName = null;
  String foodUnitUnitName = null;
  String userName = null;

  FoodDto({
    this.id,
    this.foodCode,
    this.foodName,
    this.quantity,
    this.calories,
    this.protein,
    this.carb,
    this.fat,
    this.clycemicIndex,
    this.clycemicLoad,
    this.fiber,
    this.status,
    this.foodGroupId,
    this.foodUnitId,
    this.userId,
    this.foodGroupFoodGroupName,
    this.foodUnitUnitName,
    this.userName,
  });

  @override
  String toString() {
    return 'FoodDto[foodCode=$foodCode, foodName=$foodName, quantity=$quantity, calories=$calories, protein=$protein, carb=$carb, fat=$fat, clycemicIndex=$clycemicIndex, clycemicLoad=$clycemicLoad, fiber=$fiber, status=$status, foodGroupId=$foodGroupId, foodUnitId=$foodUnitId, userId=$userId, id=$id, ]';
  }

  FoodDto.fromDtoJSON(Map<String, dynamic> jsonFood) {
    if (jsonFood == null) return;
    var json = jsonFood['food'];
    if (json == null) return;
    try {
      foodCode = json['foodCode'];
      foodName = json['foodName'];
      quantity = json['quantity'];
      calories = json['calories'];
      protein = json['protein'];
      carb = json['carb'];
      fat = json['fat'];
      clycemicIndex = json['clycemicIndex'];
      clycemicLoad = json['clycemicLoad'];
      fiber = json['fiber'];
      status = (json['status']);
      foodGroupId = json['foodGroupId'];
      foodUnitId = json['foodUnitId'];
      userId = json['userId'];
      id = json['id'];
      foodGroupFoodGroupName = jsonFood['foodGroupFoodGroupName'];
      foodUnitUnitName = jsonFood['foodUnitUnitName'];
      userName = jsonFood['userName'];
    } catch (e) {
      print("FoodDto.fromJSON ${json}");
      print(e);
    }
  }
  FoodDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    try {
      foodCode = json['foodCode'];
      foodName = json['foodName'];
      quantity = json['quantity'];
      calories = json['calories'];
      protein = json['protein'];
      carb = json['carb'];
      fat = json['fat'];
      clycemicIndex = json['clycemicIndex'];
      clycemicLoad = json['clycemicLoad'];
      fiber = json['fiber'];
      status = (json['status']);
      foodGroupId = json['foodGroupId'];
      foodUnitId = json['foodUnitId'];
      userId = json['userId'];
      id = json['id'];
      foodGroupFoodGroupName = json['foodGroupFoodGroupName'];
      foodUnitUnitName = json['foodUnitUnitName'];
      userName = json['userName'];
    } catch (e) {
      print("FoodDto.fromJSON ${json}");
      print(e);
    }
  }

  Map<String, dynamic> toJson() {
    return {'foodCode': foodCode, 'foodName': foodName, 'quantity': quantity, 'calories': calories, 'protein': protein, 'carb': carb, 'fat': fat, 'clycemicIndex': clycemicIndex, 'clycemicLoad': clycemicLoad, 'fiber': fiber, 'status': status, 'foodGroupId': foodGroupId, 'foodUnitId': foodUnitId, 'userId': userId, 'id': id};
  }

  static List<FoodDto> listfromJSON(List<dynamic> json) {
    return json == null ? new List<FoodDto>() : json.map((value) => new FoodDto.fromJSON(value)).toList();
  }

  static Map<String, FoodDto> mapfromJSON(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, FoodDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new FoodDto.fromJSON(value));
    }
    return map;
  }
}
