import 'package:hab_app/src/models/accountClassDto.dart';
import 'package:hab_app/src/models/sessionResultDto.dart';

class MyClassDetailDto {
  int classId = null;

  String classCode = null;

  String className = null;

  int courseId = null;

  String courseCode = null;

  String courseName = null;

  String courseDescription = null;

  int numberOffDay = null;

  int sessionTotal = null;

  int sessionPassTotal = null;

  SessionResultDto nextSession = null;

  List<SessionResultDto> sessions = [];

  List<AccountClassDto> coaches = [];

  List<AccountClassDto> students = [];

  List<String> images = null;

  DateTime fromDate = null;

  DateTime toDate = null;

  String classOwner = null;

  List<String> labels = [];

  int chatRoomId = null;

  // public enum ClassUserRoleType : byte
  // {
  //        Student = 1,
  //        Coach = 2,
  //        Leader = 3,
  //        Checker  = 4,
  //        Owner = 5
  //    }
  List<int> classRoles = [];

  // public enum ClassStatus : byte
  //   {
  //       Pending = 0,
  //       Active = 1,
  //       Lock = 3,
  //       Canceled = 4,
  //       InProcessing = 5,
  //       Finished = 6
  //   }
  int status = null;

  MyClassDetailDto({
    this.classId,
    this.classCode,
    this.courseName,
    this.courseDescription,
    this.numberOffDay,
    this.sessionTotal,
    this.sessionPassTotal,
    this.nextSession,
    this.images,
    this.labels,
    this.chatRoomId,
  });

  @override
  String toString() {
    return 'MyClassDetailDto[classId=$classId, classCode=$classCode, className=$className, courseId=$courseId, courseCode=$courseCode, courseName=$courseName, courseDescription=$courseDescription, numberOffDay=$numberOffDay, sessionTotal=$sessionTotal, sessionPassTotal=$sessionPassTotal, nextSession=$nextSession, sessions=$sessions, coaches=$coaches, students=$students, images=$images, '
        'fromDate=$fromDate, toDate=$toDate, classOwner=$classOwner, labels=$labels, chatRoomId=$chatRoomId, ]';
  }

  MyClassDetailDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    classId = json['classId'];
    classCode = json['classCode'];
    className = json['className'];
    courseId = json['courseId'];
    courseCode = json['courseCode'];
    courseName = json['courseName'];
    courseDescription = json['courseDescription'];
    numberOffDay = json['numberOffDay'];
    sessionTotal = json['sessionTotal'];
    sessionPassTotal = json['sessionPassTotal'];
    if (json['nextSession'] != null) {
      nextSession = new SessionResultDto.fromJSON(json['nextSession']);
    }
    //courseResults = (json['courseResults'] as List).map((item) => item as String).toList();
    sessions = SessionResultDto.listfromJSON(json['sessions']);
    coaches = AccountClassDto.listfromJSON(json['coaches']);
    students = AccountClassDto.listfromJSON(json['students']);
    if (json['images'] != null && json['images'].toString().isNotEmpty) {
      images = [json["images"], json["images"]]; //.map<String>((x) => x.toString()).toList();
      //images = (json["images"]).map<String>((x) => x.toString()).toList();
    } else {
      images = <String>[];
    }

    fromDate = json['fromDate'] == null ? null : DateTime.parse(json['fromDate']);
    toDate = json['toDate'] == null ? null : DateTime.parse(json['toDate']);
    classOwner = json['classOwner'];
    labels = (json['labels'] as List).map((item) => item as String).toList();
    classRoles = (json['classRoles'] as List).map((item) => item as int).toList();
    status = json['status'] ?? 0;
    chatRoomId = json['chatRoomId'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    return {
      'classId': classId,
      'classCode': classCode,
      'className': className,
      'courseId': courseId,
      'courseCode': courseCode,
      'courseName': courseName,
      'courseDescription': courseDescription,
      'numberOffDay': numberOffDay,
      'sessionTotal': sessionTotal,
      'sessionPassTotal': sessionPassTotal,
      'nextSession': nextSession,
      'sessions': sessions,
      'coaches': coaches,
      'students': students,
      'images': images,
      'fromDate': fromDate == null ? '' : fromDate.toUtc().toIso8601String(),
      'toDate': toDate == null ? '' : toDate.toUtc().toIso8601String(),
      'classOwner': classOwner,
      'labels': labels,
      'classRoles': classRoles,
      'status': status,
      'chatRoomId': chatRoomId
    };
  }

  static List<MyClassDetailDto> listfromJSON(List<dynamic> json) {
    return json == null ? new List<MyClassDetailDto>() : json.map((value) => new MyClassDetailDto.fromJSON(value)).toList();
  }

  static Map<String, MyClassDetailDto> mapfromJSON(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, MyClassDetailDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new MyClassDetailDto.fromJSON(value));
    }
    return map;
  }
}
