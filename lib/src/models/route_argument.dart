import 'package:hab_app/src/models/user.dart';

class RouteArgument {
  int id;
  String code;
  String tag;
  dynamic param;
  Function event;

  RouteArgument({this.id, this.code, this.tag, this.param, this.event});

  @override
  String toString() {
    return '{id: $id, code:${code.toString()}, tag:${tag.toString()}, param:${param.toString()}, event:${event.toString()}}';
  }
}

class OTPArgument {
  String type;
  String phone;
  bool isVerify;
  Function success;
  Function error;

  OTPArgument({this.type, this.phone, this.success, this.error, this.isVerify});
}

class RouteUserArgument {
  String id;
  String level;
  String content;
  Function event;
  User param;

  RouteUserArgument({this.id, this.level, this.content, this.param, this.event});

  @override
  String toString() {
    return '{id: $id, tag:${level.toString()}}';
  }
}

class RouteClassArgument {
  int classId;
  int sessionId;
  int classSessionId;
  int exerciseClassModuleId;
  int documentId;
  List<int> role;
  dynamic param;
  Function event;

  RouteClassArgument({this.classId, this.sessionId, this.classSessionId, this.exerciseClassModuleId, this.documentId, this.role, this.param, this.event});

  @override
  String toString() {
    return '{classId: $classId, sessionId:${sessionId}'
        ', classSessionId:${classSessionId}'
        ', documentId:${documentId}'
        ', exerciseClassModuleId:${exerciseClassModuleId}'
        ', role:${role}'
        ', param:${param}'
        ', event:${event}'
        ', 1:${1}}';
  }
}

class RouteChatArgument {
  int roomId;
  String roomName;
  String roomAvatar;
  List<int> userIds;
  dynamic param;
  Function event;

  RouteChatArgument({this.roomId, this.roomName, this.roomAvatar, this.userIds, this.param, this.event});
}
