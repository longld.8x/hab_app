import '../helpers/custom_trace.dart';

class FriendUser {
  int id;
  int friendUserId;
  String friendAcountCode;
  String friendUserName;
  String friendFullName;
  String friendTenancyName;
  String friendAvatar;
  int state;
  int friendTenantId;

  FriendUser({
    this.id,
    // this.name,
    this.friendUserId,
    this.friendAcountCode,
    this.friendUserName,
    this.friendFullName,
    this.friendTenancyName,
    this.friendAvatar,
    this.state,
    this.friendTenantId,
  });

  @override
  FriendUser.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'];
      friendUserId = jsonMap['friendUserId'];
      friendUserId = jsonMap['friendUserId'];
      friendTenantId = jsonMap['friendTenantId'];
      state = jsonMap['state'];
      friendUserName = jsonMap['friendUserName'] != null ? jsonMap['friendUserName'] : '';
      friendAcountCode = jsonMap['friendAcountCode'] != null ? jsonMap['friendAcountCode'] : '';
      friendFullName = jsonMap['friendFullName'] != null ? jsonMap['friendFullName'] : '';
      friendTenancyName = jsonMap['friendTenancyName'] != null ? jsonMap['friendTenancyName'] : '';
      friendAvatar = jsonMap['friendAvatar'] != null ? jsonMap['friendAvatar'] : '';
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
    }
  }
}
