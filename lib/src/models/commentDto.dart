class CommentDto {
  String id = null;

  String content = null;

  String mediaLinks = null;

  int score = null;

  String objectType = null;

  String objectIdentify = null;

  int commentType = null;

  String commentUsername = null;

  int commentUserId = null;

  int status = null;

  int approvedUserId = null;

  DateTime createdDate = null;

  DateTime modifiedDate = null;

  DateTime approvedDate = null;

  CommentDto();

  @override
  String toString() {
    return 'CommentDto[id=$id, content=$content, mediaLinks=$mediaLinks, score=$score, objectType=$objectType, objectIdentify=$objectIdentify, commentType=$commentType, commentUsername=$commentUsername, commentUserId=$commentUserId, status=$status, approvedUserId=$approvedUserId, createdDate=$createdDate, modifiedDate=$modifiedDate, approvedDate=$approvedDate, ]';
  }

  CommentDto.fromJSON(Map<String, dynamic> json) {
    try {
      if (json == null) return;
      id = json['id'];
      content = json['content'];
      mediaLinks = json['mediaLinks'];
      score = json['score'];
      objectType = json['objectType'];
      objectIdentify = json['objectIdentify'];
      commentType = json['commentType'];
      commentUsername = json['commentUsername'];
      commentUserId = json['commentUserId'];
      status = json['status'];
      approvedUserId = json['approvedUserId'];
      try {
        createdDate = json['createdDate'] == null ? null : DateTime.parse(json['createdDate']);
      } catch (e) {
        print(e);
      }
      try {
        modifiedDate = json['modifiedDate'] == null ? null : DateTime.parse(json['modifiedDate']);
      } catch (e) {
        print(e);
      }
      try {
        approvedDate = json['approvedDate'] == null ? null : DateTime.parse(json['approvedDate']);
      } catch (e) {
        print(e);
      }
    } catch (ex) {
      print("CommentDto.fromJSON catch  $ex");
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'Id': id,
      'Content': content,
      'MediaLinks': mediaLinks,
      'Score': score,
      'ObjectType': objectType,
      'ObjectIdentify': objectIdentify,
      'CommentType': commentType,
      'CommentUsername': commentUsername,
      'CommentUserId': commentUserId,
      'Status': status,
      'ApprovedUserId': approvedUserId,
      'CreatedDate': createdDate == null ? '' : createdDate.toUtc().toIso8601String(),
      'ModifiedDate': modifiedDate == null ? '' : modifiedDate.toUtc().toIso8601String(),
      'ApprovedDate': approvedDate == null ? '' : approvedDate.toUtc().toIso8601String()
    };
  }

  static List<CommentDto> listfromJSON(List<dynamic> json) {
    return json == null ? new List<CommentDto>() : json.map((value) => new CommentDto.fromJSON(value)).toList();
  }

  static Map<String, CommentDto> mapfromJSON(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, CommentDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new CommentDto.fromJSON(value));
    }
    return map;
  }
}
