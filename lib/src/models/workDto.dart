import 'package:hab_app/src/helpers/helper.dart';

import '../helpers/custom_trace.dart';

class WorkDto {
  String id;
  String taskName;
  String description;
  DateTime startDate;
  DateTime dueDate;
  DateTime createdDate;
  String status;
  String taskType;
  int relationIdentify;
  String taskDetail;
  int classId;
  int sessionId;
  int classSessionId;
  int exerciseId;
  int exerciseClassModuleId;

  // public enum TaskType
  //   {
  //       Calo = 1, //Bữa ăn
  //       Workout = 2, //Tập luyện
  //       Water = 3, //Nước uống
  //       Exercise = 4, //Làm bài tập
  //       Sales = 5, //Doanh số bán hàng
  //       CheckingExercise = 6, //Chữa bài tập
  //       Health = 7, //Sức khỏe
  //       Life = 8, //Cuộc sống
  //       Relation = 9, //Mối quan hệ
  //       Personal = 10, //Cá nhân
  //       Study = 11,
  //       Coaching = 12,
  //       Other = 99
  //   }

  WorkDto({
    this.id,
    this.taskName,
    this.description,
    this.startDate,
    this.dueDate,
    this.createdDate,
    this.status,
    this.taskType,
    this.relationIdentify,
    this.taskDetail,
    this.classId,
    this.sessionId,
    this.classSessionId,
    this.exerciseId,
    this.exerciseClassModuleId,
    // this.totalPoint,
    // this.currentPoint,
  });

  WorkDto.fromJSON(Map<String, dynamic> jsonMap) {
    print("jsonMap ${jsonMap}");
    try {
      id = jsonMap['id'];
      taskName = jsonMap['taskName'] != null ? jsonMap['taskName'] : '';
      description = jsonMap['description'] != null ? jsonMap['description'] : '';
      try {
        startDate = jsonMap['startDate'] == null ? null : Helper.parseDate(jsonMap['startDate']);
        dueDate = jsonMap['dueDate'] == null ? null : Helper.parseDate(jsonMap['dueDate']);
        createdDate = jsonMap['createdDate'] == null ? null : Helper.parseDate(jsonMap['createdDate']);
      } catch (e) {
        print("lỗi date $e");
      }
      try {
        status = jsonMap['status'].toString();
        taskType = jsonMap['taskType'].toString();
        taskDetail = jsonMap['taskDetail'].toString();
      } catch (e) {
        print("lỗi date $e");
      }
      try {
        if (taskDetail.isNotEmpty && taskDetail.contains('|')) {
          var _taskDetail = taskDetail.split("|");
          if (_taskDetail[0].toString().isNotEmpty) classId = int.parse(_taskDetail[0]);
          if (_taskDetail[1].toString().isNotEmpty) sessionId = int.parse(_taskDetail[1]);
          if (_taskDetail[2].toString().isNotEmpty) classSessionId = int.parse(_taskDetail[2]);
          if (_taskDetail[3].toString().isNotEmpty) exerciseId = int.parse(_taskDetail[3]);
          if (_taskDetail[4].toString().isNotEmpty) exerciseClassModuleId = int.parse(_taskDetail[4]);
        }
      } catch (e) {
        print("lỗi taskDetail $e");
      }
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
    }
  }
  static List<WorkDto> listfromJSON(List<dynamic> json) {
    return json == null ? new List<WorkDto>() : json.map((value) => new WorkDto.fromJSON(value)).toList();
  }
}

class DayWorkDto {
  DateTime day;
  List<WorkDto> works;
  DayWorkDto({
    this.day,
    this.works,
  });
}
