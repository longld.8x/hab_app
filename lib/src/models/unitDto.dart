import 'dart:io';

class UnitDto {
  String unitName = null;
  int order = null;
  int id = null;
  UnitDto();

  @override
  String toString() {
    return 'UnitDto[ unitName=$unitName, order=$order, id=$id, ]';
  }

  UnitDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    try {
      unitName = json['unitName'];
      order = json['order'];
      id = json['id'];
    } catch (e) {
      print("UnitDto.fromJSON ${json}");
      print(e);
    }
  }

  Map<String, dynamic> toJson() {
    return {'unitName': unitName, 'order': order, 'id': id};
  }

  static List<UnitDto> listfromJSON(List<dynamic> json) {
    return json == null ? new List<UnitDto>() : json.map((value) => new UnitDto.fromJSON(value)).toList();
  }

  static Map<String, UnitDto> mapfromJSON(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, UnitDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new UnitDto.fromJSON(value));
    }
    return map;
  }
}

class MealFileDto {
  File file;
  String type;
  String source;

  MealFileDto({this.file, this.type, this.source});
}
