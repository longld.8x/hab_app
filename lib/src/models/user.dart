import 'dart:io' show File;

import '../helpers/custom_trace.dart';

class User {
  int id;
  // String name;
  // String surname;
  String fullName;
  String emailAddress;
  int gender;
  String password;
  String apiToken;
  String deviceToken;
  String phoneNumber;
  String address;
  String accountCode;
  int accountType;
  bool isActive;
  String profilePicture;
  DateTime birthday;
  DateTime creationTime;
  bool isOnline = null;
  int totalNetwork;

  bool auth;
  double infoHeight;
  double infoWeight;
  // edit
  File image;

  User({
    this.id,
    // this.name,
    this.accountCode,
    this.accountType,
    this.fullName,
    this.emailAddress,
    this.gender,
    this.phoneNumber,
    this.profilePicture,
    this.birthday,
    this.infoHeight,
    this.infoWeight,
    this.totalNetwork,
    this.isOnline,
  });

  @override
  User.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'];
      if (id == 0) id = jsonMap['userId'];
      print("User.fromJSON $id $jsonMap");
      // name = jsonMap['name'] != null ? jsonMap['name'] : '';
      // surname = jsonMap['surname'] != null ? jsonMap['surname'] : '';
      //fullName = jsonMap['fullName'] != null ? jsonMap['fullName'] : '';
      // hab chỉ có 1 trường họ và tên
      fullName = jsonMap['fullName'] != null ? jsonMap['fullName'] : '';
      emailAddress = jsonMap['emailAddress'] != null ? jsonMap['emailAddress'] : '';
      gender = jsonMap['gender'] != null ? jsonMap['gender'] : 0;
      password = jsonMap['password'] != null ? jsonMap['password'] : '';
      apiToken = jsonMap['api_token'] != null ? jsonMap['api_token'] : null;
      deviceToken = jsonMap['device_token'] != null ? jsonMap['device_token'] : null;
      phoneNumber = jsonMap['phoneNumber'] != null ? jsonMap['phoneNumber'] : '';
      address = jsonMap['address'] ?? '';
      accountCode = jsonMap['accountCode'] != null ? jsonMap['accountCode'] : '';
      accountType = jsonMap['accountType'] != null ? jsonMap['accountType'] : '';
      isActive = jsonMap['isActive'] != null ? jsonMap['isActive'] : false;

      infoHeight = jsonMap['height'] != null ? double.parse(jsonMap['height'].toString()) : null;
      infoWeight = jsonMap['weight'] != null ? double.parse(jsonMap['weight'].toString()) : null;
      totalNetwork = jsonMap['totalNetwork'] != null ? int.parse(jsonMap['totalNetwork'].toString()) : 0;

      birthday = jsonMap['doB'] != null ? DateTime.parse(jsonMap['doB'].toString()) : null;
      creationTime = jsonMap['creationTime'] != null ? DateTime.parse(jsonMap['creationTime'].toString()) : null;
      profilePicture = jsonMap['profilePicture'] ?? '';
      isOnline = jsonMap['isOnline'] ?? '';
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    // map["name"] = name;
    // map["surname"] = surname;
    map["fullName"] = fullName;
    map["emailAddress"] = emailAddress;
    map["gender"] = gender;
    map["phoneNumber"] = phoneNumber;
    map["password"] = password;
    map["infoHeight"] = infoHeight;
    map["infoWeight"] = infoWeight;
    map["totalNetwork"] = totalNetwork;
    map["isOnline"] = isOnline;
    return map;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    // data['name'] = this.name;
    // data['surname'] = this.surname;
    data['fullName'] = this.fullName;
    data['emailAddress'] = this.emailAddress;
    data['gender'] = this.gender;
    data['phoneNumber'] = this.phoneNumber;
    data['password'] = this.password;
    data['accountType'] = this.accountType;
    data['isActive'] = this.isActive;
    data['address'] = this.address;
    data['profilePicture'] = this.profilePicture;
    data['totalNetwork'] = this.totalNetwork;
    data['isOnline'] = this.isOnline;
    return data;
  }
}

class AuthenUser {
  String userName = '';
  String password = '';
  String fullName = '';
  String phoneNumber = '';
  String accountCode = '';
  String profilePicture = '';
  int id;
  AuthenUser(this.id, this.userName, this.fullName, this.password, this.phoneNumber, this.accountCode, this.profilePicture);

  AuthenUser.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'] ?? '';
      userName = jsonMap['userName'] ?? '';
      fullName = jsonMap['fullName'] ?? '';
      password = jsonMap['password'] ?? '';
      phoneNumber = jsonMap['phoneNumber'] ?? '';
      accountCode = jsonMap['accountCode'] ?? '';
      profilePicture = jsonMap['profilePicture'] ?? '';
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["userName"] = userName;
    map["fullName"] = fullName;
    map["password"] = password;
    map["phoneNumber"] = phoneNumber;
    map["accountCode"] = accountCode;
    map["profilePicture"] = profilePicture;
    return map;
  }
}
