import 'dart:io';

class MealTypeDto {
  String mealName = null;
  int order = null;
  int id = null;
  MealTypeDto();

  @override
  String toString() {
    return 'MealTypeDto[ unitName=$mealName, order=$order, id=$id, ]';
  }

  MealTypeDto.fromJSON(Map<String, dynamic> json) {
    if (json == null) return;
    try {
      mealName = json['mealName'];
      order = json['order'];
      id = json['id'];
    } catch (e) {
      print("MealTypeDto.fromJSON ${json}");
      print(e);
    }
  }

  Map<String, dynamic> toJson() {
    return {'mealName': mealName, 'order': order, 'id': id};
  }

  static List<MealTypeDto> listfromJSON(List<dynamic> json) {
    return json == null ? new List<MealTypeDto>() : json.map((value) => new MealTypeDto.fromJSON(value)).toList();
  }

  static Map<String, MealTypeDto> mapfromJSON(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, MealTypeDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new MealTypeDto.fromJSON(value));
    }
    return map;
  }
}

class MealFileDto {
  File file;
  String type;
  String source;

  MealFileDto({this.file, this.type, this.source});
}
