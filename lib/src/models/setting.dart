import 'package:flutter/material.dart';
import 'package:hab_app/src/models/homeSettingsModel.dart';

import '../helpers/custom_trace.dart';

class Setting {
  String appName = 'HAB';
  String backgroundColor = '#ffffff'; // màn hình nền
  String primaryColor = '#1A60FF'; // màu chữ 1 => màu xanh HAB
  String accentColor = '#171051'; // màu chữ 2 => màu title
  String secondColor = '#151522'; // màu chữ 3 => màu đen
  String hintColor = '#A7A7A7';
  String errorColor = '#EB001B';
  String inputBgColor = '#F4F9FE';
  String buttonColor = '#ffffff';
  String subColor = '#2A2A2A';
  ValueNotifier<Locale> mobileLanguage = new ValueNotifier(Locale('vi', ''));
  ValueNotifier<Brightness> brightness = new ValueNotifier(Brightness.light);
  String appVersion;
  String buildNumber;
  String otpType = "OTP";
  String hotline = '';
  String termsOfUse = '';
  String privacyPolicy = '';
  Map<String, dynamic> deviceInfo = null;
  String uuid = '';
  String authenType = '';
  String loginSecurity = 'on';
  //int totalNoti = 0;
  ValueNotifier<int> totalNotifications = new ValueNotifier(0);
  ValueNotifier<List<HomeSettingsModel>> homeSettings = new ValueNotifier(List<HomeSettingsModel>());
  Setting();

  Setting.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      appName = jsonMap['appName'] ?? 'HAB';
      backgroundColor = jsonMap['backgroundColor'] ?? '#ffffff';
      primaryColor = jsonMap['primaryColor'] ?? '#1A60FF';
      accentColor = jsonMap['accentColor'] ?? '#171051';
      secondColor = jsonMap['secondColor'] ?? '#151522';
      hintColor = jsonMap['hintColor'] ?? '#A7A7A7';
      errorColor = jsonMap['errorColor'] ?? '#EB001B';
      inputBgColor = jsonMap['inputBgColor'] ?? '#F4F9FE';
      buttonColor = jsonMap['buttonColor'] ?? '#ffffff';
      subColor = jsonMap['subColor'] ?? '#2A2A2A';

      mobileLanguage.value = Locale(jsonMap['mobileLanguage'] ?? "en", '');
      appVersion = jsonMap['appVersion'] ?? '';
      buildNumber = jsonMap['buildNumber'] ?? '';
      otpType = jsonMap['otpType'] ?? 'OTP';
      hotline = jsonMap['hotline'] ?? '';
      privacyPolicy = jsonMap['privacyPolicy'] ?? '';
      termsOfUse = jsonMap['termsOfUse'] ?? '';
      uuid = jsonMap['uuid'] ?? '';
      loginSecurity = jsonMap['loginSecurity'] ?? 'on';
      authenType = '';
      totalNotifications.value = 0;
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["appName"] = appName;
    map["mobileLanguage"] = mobileLanguage.value.languageCode;
    map["appVersion"] = appVersion;
    map["buildNumber"] = buildNumber;
    map["otpType"] = otpType;
    map["hotline"] = hotline;
    map["termsOfUse"] = termsOfUse;
    map["privacyPolicy"] = privacyPolicy;
    map["deviceInfo"] = deviceInfo;
    map["uuid"] = uuid;
    map["authenType"] = authenType;
    return map;
  }
}
