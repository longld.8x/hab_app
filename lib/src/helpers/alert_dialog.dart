import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/Button.dart';

import '../../src/helpers/app_config.dart' as config;

class DialogHLS {
  static Widget actionBtn({BuildContext context, String text, Function action, Color color, TextStyle textStyle}) {
    return Button(
      text: text ?? "Đóng",
      color: color,
      textStyle: textStyle,
      onPressed: () {
        if (action != null) {
          Navigator.of(context).pop();
          action();
        } else {
          Navigator.of(context).pop();
        }
      },
    );
  }

  static void DialogBase({BuildContext context, String title, String subtitle, Widget actions}) {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(14.0))),
        contentPadding: EdgeInsets.all(25.0),
        titleTextStyle: Theme.of(context).textTheme.headline5.copyWith(color: config.Colors.accentColor()),
        content: Container(
          width: config.App.appWidth(100),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              (title != null && title.isNotEmpty)
                  ? Container(
                      child: Text(
                        title,
                        style: Theme.of(context).textTheme.headline5.copyWith(color: config.Colors.accentColor()),
                      ),
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(bottom: 15),
                    )
                  : Container(),
              Container(
                child: Text(
                  subtitle,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.subColor()),
                ),
                padding: EdgeInsets.only(bottom: 20),
              ),
              Container(child: actions),
            ],
          ),
        ),
      ),
    );
  }

  static void confirmDialog(BuildContext context, String title, String subtitle, Function action, {String textAction = "Xác nhận"}) {
    var _actions = Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Expanded(
        child: Container(
          child: actionBtn(
            context: context,
            text: "Đóng",
            color: config.Colors.customeColor('#E0E9F8'),
            textStyle: TextStyle(color: config.Colors.primaryColor()),
          ),
          padding: EdgeInsets.only(right: 5),
        ),
      ),
      Expanded(
        child: Container(
          child: actionBtn(
            context: context,
            text: textAction,
            action: action,
          ),
          padding: EdgeInsets.only(left: 5),
        ),
      )
    ]);
    DialogBase(context: context, title: title, subtitle: subtitle, actions: _actions);
  }
}
