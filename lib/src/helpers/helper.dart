import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:bot_toast/bot_toast.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/docso.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:intl/intl.dart' as intl;
import 'package:loader_overlay/loader_overlay.dart';

class Helper {
  BuildContext context;
  DateTime currentBackPressTime;

  Helper.of(BuildContext _context) {
    this.context = _context;
  }

  static void socketNoti(bool status, String text) {
    BotToast.showText(
      text: text,
      contentColor: status == false ? config.Colors.customeColor('#000000', opacity: 0.2) : config.Colors.primaryColor(opacity: 0.85),
      contentPadding: EdgeInsets.symmetric(vertical: 4, horizontal: 16),
      borderRadius: BorderRadius.all(Radius.circular(20.0)),
      align: Alignment(0, 0.85),
      textStyle: TextStyle(fontSize: 12, color: config.Colors.customeColor('#ffffff')),
      enableKeyboardSafeArea: true,
    );
  }

  static void successMessenge(String text) {
    BotToast.showText(
      text: text,
      contentColor: config.Colors.primaryColor(opacity: 0.85),
      contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
      borderRadius: BorderRadius.all(Radius.circular(20.0)),
      align: Alignment(0, 0.8),
      textStyle: TextStyle(fontSize: 14, color: config.Colors.customeColor('#ffffff')),
    );
    // Fluttertoast.showToast(
    //   msg: text,
    //   backgroundColor: config.Colors.primaryColor(opacity: 0.85),
    // );
    //ScaffoldMessenger.of(settingRepo.scaffoldMessengerKey.currentContext).showSnackBar(SnackBar(content: Text(text)));
  }

  static void errorMessenge(String text) {
    BotToast.showText(
      text: text,
      contentColor: config.Colors.errorColor(opacity: 0.85),
      contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
      borderRadius: BorderRadius.all(Radius.circular(20.0)),
      align: Alignment(0, 0.8),
      textStyle: TextStyle(fontSize: 14, color: config.Colors.customeColor('#ffffff')),
    );
    // Fluttertoast.showToast(
    //   msg: text,
    //   backgroundColor: config.Colors.errorColor(opacity: 0.85),
    // );
    //ScaffoldMessenger.of(settingRepo.scaffoldMessengerKey.currentContext).showSnackBar(SnackBar(content: Text(text)));
  }

  static void infoMessenge(String text) {
    BotToast.showText(
      text: text,
      contentColor: config.Colors.customeColor('#000000', opacity: 0.5),
      contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 24),
      borderRadius: BorderRadius.all(Radius.circular(40.0)),
      align: Alignment(0, 0.8),
      textStyle: TextStyle(fontSize: 14, color: config.Colors.customeColor('#ffffff')),
    );
    // Fluttertoast.showToast(
    //   msg: text,
    //   backgroundColor: config.Colors.errorColor(opacity: 0.85),
    // );
    //ScaffoldMessenger.of(settingRepo.scaffoldMessengerKey.currentContext).showSnackBar(SnackBar(content: Text(text)));
  }

  static void showLoaderOverlay() {
    FocusScope.of(settingRepo.navigatorKey.currentContext).requestFocus(FocusNode());
    settingRepo.navigatorKey.currentContext.showLoaderOverlay();
  }

  static void hideLoaderOverlay() {
    settingRepo.navigatorKey.currentContext.hideLoaderOverlay();
  }

  static String dateString(int value) {
    if (value == null) return "00";
    if (value < 0) return "00";
    if (value >= 10)
      return value.toString();
    else
      return "0${value.toString()}";
  }

  static String getData(Map<String, dynamic> data) {
    return data['result'] ?? [];
  }

  static int getIntData(Map<String, dynamic> data) {
    return (data['data'] as int) ?? 0;
  }

  static double getDoubleData(Map<String, dynamic> data) {
    return (data['data'] as double) ?? 0;
  }

  static bool getBoolData(Map<String, dynamic> data) {
    return (data['data'] as bool) ?? false;
  }

  static getObjectData(Map<String, dynamic> data) {
    return data['data'] ?? new Map<String, dynamic>();
  }

  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }

  static Uri getUri(String path) {
    String _path = Uri.parse(GlobalConfiguration().getString('web_url')).path;
    if (!_path.endsWith('/')) {
      _path += '/';
    }
    Uri uri = Uri(scheme: Uri.parse(GlobalConfiguration().getString('web_url')).scheme, host: Uri.parse(GlobalConfiguration().getString('web_url')).host, port: Uri.parse(GlobalConfiguration().getString('web_url')).port, path: _path + path);
    return uri;
  }

  static Uri getApiUri(String path) {
    String _path = Uri.parse(GlobalConfiguration().getString('api_url')).path;
    if (!_path.endsWith('/')) {
      _path += '/';
    }
    Uri uri = Uri(scheme: Uri.parse(GlobalConfiguration().getString('api_url')).scheme, host: Uri.parse(GlobalConfiguration().getString('api_url')).host, port: Uri.parse(GlobalConfiguration().getString('api_url')).port, path: _path + path);
    return uri;
  }

  Future<bool> onWillPop() async {
    FocusScope.of(context).requestFocus(FocusNode());
    Helper.hideLoaderOverlay();
    DateTime now = DateTime.now();
    if (currentBackPressTime == null || now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Helper.infoMessenge("Chạm lần nữa để thoát");
      return Future.value(false);
    }
    SystemNavigator.pop(animated: true);
    return Future.value(true);
  }

  Future<bool> onWillBack(bool isKeyboardVisible, {Function onAction}) {
    print("isKeyboardVisible $isKeyboardVisible");
    if (isKeyboardVisible) {
      FocusScope.of(settingRepo.navigatorKey.currentContext).requestFocus(FocusNode());
      return Future.value(false);
    }
    if (onAction == null) {
      Helper.hideLoaderOverlay();
      Navigator.of(settingRepo.navigatorKey.currentContext).pop();
    } else {
      onAction();
    }
    return Future.value(true);
  }

  static String getDateString(DateTime date, String format) {
    var formatter = new intl.DateFormat(format);
    String formatted = formatter.format(date);
    return formatted;
  }

  static String formatNumber(double number, {String prefix = null}) {
    final oCcy = new intl.NumberFormat("#,##0", "vi_VN");
    String formatted = oCcy.format(number);
    return formatted + (prefix != null ? prefix : "");
  }

  static String formatNumber2(double number) {
    final oCcy = new intl.NumberFormat("#,##0.00", "vi_VN");
    String formatted = oCcy.format(number);
    return formatted;
  }

  static String convertLatin(String s) {
    Map<String, String> char_map = {
      // Latin
      'À': 'A', 'Á': 'A', 'Ả': 'A', 'Ã': 'A', 'Ạ': 'A',
      'Â': 'A', 'Ầ': 'A', 'Ấ': 'A', 'Ẩ': 'A', 'Ẫ': 'A', 'Ậ': 'A',
      'Ă': 'A', 'Ằ': 'A', 'Ắ': 'A', 'Ẳ': 'A', 'Ẵ': 'A', 'Ặ': 'A',
      'Ì': 'I', 'Í': 'I', 'Ỉ': 'I', 'Ĩ': 'I', 'Ị': 'I', 'Î': 'I', 'Ï': 'I',
      'Ù': 'U', 'Ú': 'U', 'Ủ': 'U', 'Ũ': 'U', 'Ụ': 'U',
      'Ư': 'U', 'Ừ': 'U', 'Ứ': 'U', 'Ử': 'U', 'Ữ': 'U', 'Ự': 'U',
      'È': 'E', 'É': 'E', 'Ẻ': 'E', 'Ẽ': 'E', 'Ẹ': 'E',
      'Ê': 'E', 'Ề': 'E', 'Ế': 'E', 'Ể': 'E', 'Ễ': 'E', 'Ệ': 'E',
      'Ỳ': 'Y', 'Ý': 'Y', 'Ỷ': 'Y', 'Ỹ': 'Y', 'Ỵ': 'Y',
      'Ò': 'O', 'Ó': 'O', 'Ỏ': 'O', 'Õ': 'O', 'Ọ': 'O',
      'Ô': 'O', 'Ồ': 'O', 'Ố': 'O', 'Ổ': 'O', 'Ỗ': 'O', 'Ộ': 'O',
      'Ơ': 'O', 'Ờ': 'O', 'Ớ': 'O', 'Ở': 'O', 'Ỡ': 'O', 'Ợ': 'O',

      'à': 'a', 'á': 'a', 'ả': 'a', 'ã': 'a', 'ạ': 'a',
      'â': 'a', 'ầ': 'a', 'ấ': 'a', 'ẩ': 'a', 'ẫ': 'a', 'ậ': 'a',
      'ă': 'a', 'ằ': 'a', 'ắ': 'a', 'ẳ': 'a', 'ẵ': 'a', 'ặ': 'a',
      'ì': 'i', 'í': 'i', 'ỉ': 'i', 'ĩ': 'i', 'ị': 'i', 'î': 'i', 'ï': 'i',
      'ù': 'u', 'ú': 'u', 'ủ': 'u', 'ũ': 'u', 'ụ': 'u',
      'ư': 'u', 'ừ': 'u', 'ứ': 'u', 'ử': 'u', 'ữ': 'u', 'ự': 'u',
      'è': 'e', 'é': 'e', 'ẻ': 'e', 'ẽ': 'e', 'ẹ': 'e',
      'ê': 'e', 'ề': 'e', 'ế': 'e', 'ể': 'e', 'ễ': 'e', 'ệ': 'e',
      'ỳ': 'y', 'ý': 'y', 'ỷ': 'y', 'ỹ': 'y', 'ỵ': 'y',
      'ò': 'o', 'ó': 'o', 'ỏ': 'o', 'õ': 'o', 'ọ': 'o',
      'ô': 'o', 'ồ': 'o', 'ố': 'o', 'ổ': 'o', 'ỗ': 'o', 'ộ': 'o',
      'ơ': 'o', 'ờ': 'o', 'ớ': 'o', 'ở': 'o', 'ỡ': 'o', 'ợ': 'o',
      'đ': 'd',

      'Ä': 'A', 'Å': 'A', 'Æ': 'AE', 'Ç': 'C',

      'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E',
      'Đ': 'D', 'Ñ': 'N', 'Ô': 'O', 'Õ': 'O', 'Ö': 'O', 'Ő': 'O',
      'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U', 'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH',
      'ß': 'ss',
      'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c',
      'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',
      'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ő': 'o',
      'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ű': 'u', 'ý': 'y', 'þ': 'th',
      'ÿ': 'y',

      // Latin symbols
      '©': '(c)',

      // Greek
      'Α': 'A', 'Β': 'B', 'Γ': 'G', 'Δ': 'D', 'Ε': 'E', 'Ζ': 'Z', 'Η': 'H', 'Θ': '8',
      'Ι': 'I', 'Κ': 'K', 'Λ': 'L', 'Μ': 'M', 'Ν': 'N', 'Ξ': '3', 'Ο': 'O', 'Π': 'P',
      'Ρ': 'R', 'Σ': 'S', 'Τ': 'T', 'Υ': 'Y', 'Φ': 'F', 'Χ': 'X', 'Ψ': 'PS', 'Ω': 'W',
      'Ά': 'A', 'Έ': 'E', 'Ί': 'I', 'Ό': 'O', 'Ύ': 'Y', 'Ή': 'H', 'Ώ': 'W', 'Ϊ': 'I',
      'Ϋ': 'Y',
      'α': 'a', 'β': 'b', 'γ': 'g', 'δ': 'd', 'ε': 'e', 'ζ': 'z', 'η': 'h', 'θ': '8',
      'ι': 'i', 'κ': 'k', 'λ': 'l', 'μ': 'm', 'ν': 'n', 'ξ': '3', 'ο': 'o', 'π': 'p',
      'ρ': 'r', 'σ': 's', 'τ': 't', 'υ': 'y', 'φ': 'f', 'χ': 'x', 'ψ': 'ps', 'ω': 'w',
      'ά': 'a', 'έ': 'e', 'ί': 'i', 'ό': 'o', 'ύ': 'y', 'ή': 'h', 'ώ': 'w', 'ς': 's',
      'ϊ': 'i', 'ΰ': 'y', 'ϋ': 'y', 'ΐ': 'i',

      // Turkish
      'Ş': 'S', 'İ': 'I', 'Ç': 'C', 'Ü': 'U', 'Ö': 'O', 'Ğ': 'G',
      'ş': 's', 'ı': 'i', 'ç': 'c', 'ü': 'u', 'ö': 'o', 'ğ': 'g',

      // Russian
      'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
      'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
      'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
      'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
      'Я': 'Ya',
      'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
      'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
      'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
      'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
      'я': 'ya',

      // Ukrainian
      'Є': 'Ye', 'І': 'I', 'Ї': 'Yi', 'Ґ': 'G',
      'є': 'ye', 'і': 'i', 'ї': 'yi', 'ґ': 'g',

      // Czech
      'Č': 'C', 'Ď': 'D', 'Ě': 'E', 'Ň': 'N', 'Ř': 'R', 'Š': 'S', 'Ť': 'T', 'Ů': 'U',
      'Ž': 'Z',
      'č': 'c', 'ď': 'd', 'ě': 'e', 'ň': 'n', 'ř': 'r', 'š': 's', 'ť': 't', 'ů': 'u',
      'ž': 'z',

      // Polish
      'Ą': 'A', 'Ć': 'C', 'Ę': 'e', 'Ł': 'L', 'Ń': 'N', 'Ó': 'o', 'Ś': 'S', 'Ź': 'Z',
      'Ż': 'Z',
      'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ó': 'o', 'ś': 's', 'ź': 'z',
      'ż': 'z',

      // Latvian
      'Ā': 'A', 'Č': 'C', 'Ē': 'E', 'Ģ': 'G', 'Ī': 'i', 'Ķ': 'k', 'Ļ': 'L', 'Ņ': 'N',
      'Š': 'S', 'Ū': 'u', 'Ž': 'Z',
      'ā': 'a', 'č': 'c', 'ē': 'e', 'ģ': 'g', 'ī': 'i', 'ķ': 'k', 'ļ': 'l', 'ņ': 'n',
      'š': 's', 'ū': 'u', 'ž': 'z'
    };
    char_map.forEach((k, v) {
      s = s.replaceAll(new RegExp(k), v.toString());
    });
    return s;
  }

  static String toWordNumber(double number) {
    if (number == null || number == 0) return "";
    return new NumberToWord().convert(number, "đồng");
  }

  ///  telco theo phonenumber
  static Map<String, List<String>> prefixTelco = {
    "VTE": ["086", "096", "097", "098", "032", "033", "034", "035", "036", "037", "038", "039"],
    "VNA": ["091", "094", "083", "084", "085", "081", "082", "087", "088"],
    "VMS": ["090", "093", "070", "076", "077", "078", "079", "089"],
    "GMOBILE": ["099", "059"],
    "VNM": ["092", "052", "056", "058"]
  };

  /// lấy telco theo phonenumber
  static String getTelco(String phoneNumber) {
    RegExp regPhone = new RegExp(
      r"^0[0-9]{9}",
      caseSensitive: false,
      multiLine: false,
    );
    if (!regPhone.hasMatch(phoneNumber)) {
      return "";
    }
    var p = phoneNumber.substring(0, 3);
    String key = "";
    prefixTelco.forEach((k, v) {
      if (v.indexOf(p) > -1) {
        key = k;
      }
    });
    return key;
  }

  /// valid số điện thoại
  static String validPhonumber(String phoneNumber) {
    if (phoneNumber.isEmpty) {
      return 'Vui lòng nhập số điện thoại';
    }
    if (phoneNumber.length != 10) {
      return 'Số điện thoại không hợp lệ';
    }
    RegExp regPhone = new RegExp(
      r"^0[0-9]{9}",
      caseSensitive: false,
      multiLine: false,
    );
    if (!regPhone.hasMatch(phoneNumber)) {
      return 'Số điện thoại không đúng định dạng';
    }
    var telco = getTelco(phoneNumber);
    if (telco == "") return 'Nhà mạng không được hỗ trợ';
    return "";
  }

  static String securityTypeName(String paymentMethod) {
    var _paymentMethod = paymentMethod != null ? paymentMethod.toLowerCase() : "";
    switch (_paymentMethod) {
      case '1':
        return "Mật khẩu cấp 2";
        break;
      case '2':
        return "Odp";
        break;
      case '3':
        return "Otp";
        break;
      case 'biometrictype.face':
        return "Khuôn mặt";
        break;
      case 'biometrictype.fingerprint':
        return "Vân tay";
        break;
      case 'biometrictype.iris':
        return "Mống mắt";
        break;
      default:
        return "";
        break;
    }
    return "";
  }

  static String showDate(DateTime start, DateTime end) {
    if (start == null || end == null) {
      if (start != null) return "${new intl.DateFormat('dd/MM/yyyy').format(start)}";
      if (end != null) return "${new intl.DateFormat('dd/MM/yyyy').format(end)}";
      return "";
    }
    return "${new intl.DateFormat('dd/MM/yyyy').format(start)} - ${new intl.DateFormat('dd/MM/yyyy').format(end)}";
  }

  static String showTime(DateTime start, DateTime end, {bool isTime = true}) {
    if (isTime == false) showDate(start, end);
    if (start == null || end == null) {
      if (start != null) return "${new intl.DateFormat('HH:mm dd/MM/yyyy').format(start)}";
      if (end != null) return "${new intl.DateFormat('HH:mm dd/MM/yyyy').format(end)}";
      return "";
    }
    if (start.day == end.day && start.month == end.month && start.year == end.year) {
      return "${new intl.DateFormat('HH:mm').format(start)} - ${new intl.DateFormat('HH:mm dd/MM/yyyy').format(end)}";
    }
    return "${new intl.DateFormat('HH:mm dd/MM/yyyy').format(start)} - ${new intl.DateFormat('HH:mm dd/MM/yyyy').format(end)}";
  }

  /// string 'vừa xong, 1 giờ trước,...
  static String showTimeText(DateTime date) {
    if (date == null) return "";
    DateTime now = DateTime.now();
    // print("showTimeText $date showTimeText $now");
    var d = now.difference(date).inDays;
    var h = now.difference(date).inHours;
    var m = now.difference(date).inMinutes;
    // print("showTimeText $d  $h $m");
    if (d > 7) return "${new intl.DateFormat('dd/MM/yyyy').format(date)}";
    if (d > 0) return "${d} ngày";
    if (h > 0) return "${h} giờ";
    if (m > 0) return "${m} phút";
    // print("showTimeText d $d h $h m $m");
    return "Vừa xong";
  }

  static double getTextLineHeight(String value, TextStyle style, double width) {
    final _tp = TextPainter(text: TextSpan(text: value, style: style), textDirection: TextDirection.ltr);
    _tp.layout(maxWidth: width);
    var _lines = _tp.computeLineMetrics();
    var _h = _lines.map((e) => e.height);
    var _hh = _h.reduce((a1, a2) => a1 + a2);
    //print("value $value: _hh $_hh");
    return _hh;
  }

  static String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

  static double getWidgetHeight(TextSpan value, double width) {
    final _tp = TextPainter(text: value, textDirection: TextDirection.ltr);
    _tp.layout(maxWidth: width);
    var _lines = _tp.computeLineMetrics();
    var _h = _lines.map((e) => e.height);
    var _hh = _h.reduce((a1, a2) => a1 + a2);
    // print("value $value: _hh $_hh");
    return _hh;
  }

  static DateTime parseDate(String value) {
    if (value == null) return null;
    if (value.indexOf('/Date(') > -1) {
      var numeric = value.split('(')[1].split(')')[0];
      var negative = numeric.contains('-');
      var parts = numeric.split(negative ? '-' : '+');
      var date = DateTime.fromMillisecondsSinceEpoch(int.parse(parts[0]), isUtc: false);
      var offset = Duration(seconds: 0);
      if (parts.length > 1) {
        var _local = int.parse(parts[1].substring(0, 2));
        if (_local != 7) {
          final multiplier = negative ? 1 : -1;
          offset = Duration(
            hours: _local * multiplier,
            minutes: _local * multiplier,
          );
        }
      }
      var _date = date.add(offset);
      //print("parseDate $_date");
      return _date;
    }
    try {
      return DateTime.parse(value);
    } catch (e) {
      print("parseDate catch $e");
      return null;
    }
  }

  static double getPercent(double current, double total) {
    if (total == null || total == 0) return 0;
    if (current > total) current = total;
    return current / total;
  }
}

// public enum IndexType
//     {
//         Height,
//         Weight,
//         Bust, //Ngực
//         Waist, //Eo
//         Hip, //Mông
//         Gender, // 0 - 1
//         WorkoutIntensity, // 5
//     }
