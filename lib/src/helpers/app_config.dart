import 'package:flutter/material.dart';

import '../repositories/setting_repository.dart' as settingRepo;

class App {
  App();

  static double appBar() {
    var _context = settingRepo.navigatorKey.currentContext;
    var _queryData = MediaQuery.of(_context);
    //var _height = _queryData.size.height / 100.0;
    // var _width = _queryData.size.width / 100.0;
    final double statusbarHeight = _queryData.padding.top;
    return 56.0 + statusbarHeight;
  }

  static double bottomNavigationBar() {
    return 68;
  }

  static double appWidth(double v) {
    var _context = settingRepo.navigatorKey.currentContext;
    var _queryData = MediaQuery.of(_context);
    //var _height = _queryData.size.height / 100.0;
    var _width = _queryData.size.width / 100.0;
    return _width * v;
  }

  static double appHeight(double v) {
    var _context = settingRepo.navigatorKey.currentContext;
    var _queryData = MediaQuery.of(_context);
    var _height = _queryData.size.height / 100.0;
    // var _width = _queryData.size.width / 100.0;
    return _height * v;
  }
}

class Colors {
  static Color backgroundColor({double opacity = 1}) {
    try {
      return Color(int.parse(settingRepo.setting.value.backgroundColor.replaceAll("#", "0xFF"))).withOpacity(opacity);
    } catch (e) {
      return Color(0xFFCCCCCC).withOpacity(opacity);
    }
  }

  static Color primaryColor({double opacity = 1}) {
    try {
      return Color(int.parse(settingRepo.setting.value.primaryColor.replaceAll("#", "0xFF"))).withOpacity(opacity);
    } catch (e) {
      return Color(0xFFCCCCCC).withOpacity(opacity);
    }
  }

  static Color accentColor({double opacity = 1}) {
    try {
      return Color(int.parse(settingRepo.setting.value.accentColor.replaceAll("#", "0xFF"))).withOpacity(opacity);
    } catch (e) {
      return Color(0xFFCCCCCC).withOpacity(opacity);
    }
  }

  static Color secondColor({double opacity = 1}) {
    try {
      return Color(int.parse(settingRepo.setting.value.secondColor.replaceAll("#", "0xFF"))).withOpacity(opacity);
    } catch (e) {
      return Color(0xFFCCCCCC).withOpacity(opacity);
    }
  }

  static Color hintColor({double opacity = 1}) {
    try {
      return Color(int.parse(settingRepo.setting.value.hintColor.replaceAll("#", "0xFF"))).withOpacity(opacity);
    } catch (e) {
      return Color(0xFFCCCCCC).withOpacity(opacity);
    }
  }

  static Color errorColor({double opacity = 1}) {
    try {
      return Color(int.parse(settingRepo.setting.value.errorColor.replaceAll("#", "0xFF"))).withOpacity(opacity);
    } catch (e) {
      return Color(0xFFCCCCCC).withOpacity(opacity);
    }
  }

  static Color inputBgColor({double opacity = 1}) {
    try {
      return Color(int.parse(settingRepo.setting.value.inputBgColor.replaceAll("#", "0xFF"))).withOpacity(opacity);
    } catch (e) {
      return Color(0xFFCCCCCC).withOpacity(opacity);
    }
  }

  static Color buttonColor({double opacity = 1}) {
    try {
      return Color(int.parse(settingRepo.setting.value.buttonColor.replaceAll("#", "0xFF"))).withOpacity(opacity);
    } catch (e) {
      return Color(0xFFCCCCCC).withOpacity(opacity);
    }
  }

  static Color subColor({double opacity = 1}) {
    try {
      return Color(int.parse(settingRepo.setting.value.subColor.replaceAll("#", "0xFF"))).withOpacity(opacity);
    } catch (e) {
      return Color(0xFFCCCCCC).withOpacity(opacity);
    }
  }
  // static Color dividerColor({double opacity = 1}) {
  //   try {
  //     return Color(int.parse(settingRepo.setting.value.subColor.replaceAll("#", "0xFF"))).withOpacity(opacity);
  //   } catch (e) {
  //     return Color(0xFFCCCCCC).withOpacity(opacity);
  //   }
  // }

  static Color customeColor(String color, {double opacity = 1}) {
    if (opacity == null) opacity = 1;
    try {
      return Color(int.parse(color.replaceAll("#", "0xFF"))).withOpacity(opacity);
    } catch (e) {
      return Color(0xFFCCCCCC).withOpacity(opacity);
    }
  }
}

class Style {
  static TextStyle subStyle() {
    return TextStyle(color: Colors.accentColor(), fontSize: 12, fontWeight: FontWeight.w400);
  }
}
