import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/notificationDto.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/repositories/firebase_repository.dart' as firebaseRepo;
import 'package:hab_app/src/repositories/notification_repository.dart' as notiRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class NotificationController extends ControllerMVC {
  NotificationController() {}
  GlobalKey<ScaffoldState> scaffoldKey;
  int serverTotalCount = 0;
  int currentPage = -1;
  int itemPage = 15;
  bool isLoading = false;
  List<NotificationDto> dataList = [];

  void pageState() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    loadData(0);
  }

  /// search - get data f1
  void loadData(int page) async {
    Helper.showLoaderOverlay();
    isLoading = true;
    if (page == 0) dataList = [];
    notiRepo.getNotifications(page, itemPage).then((PageResult<NotificationDto> result) {
      if (result != null) {
        print("result.totalCount ${result.totalCount}");
        setState(() {
          dataList.addAll(result.items);
          serverTotalCount = result.totalCount;
          currentPage = page;
        });
      } else {
        setState(() {
          dataList = dataList;
          serverTotalCount = result.totalCount;
          currentPage = page;
        });
      }
    }).whenComplete(() {
      Helper.hideLoaderOverlay();
      isLoading = false;
    });
  }

  Future<ResponseMessage> read(String id) async {
    return notiRepo.read(id).then((value) {
      if (value.success) firebaseRepo.getTotalNotifications();
      return value;
    }).whenComplete(() => Helper.hideLoaderOverlay());
  }

  Future<ResponseMessage> readAll() async {
    Helper.showLoaderOverlay();
    return notiRepo.readAll().then((value) {
      if (value.success) firebaseRepo.getTotalNotifications();
      return value;
    }).whenComplete(() => Helper.hideLoaderOverlay());
  }

  Future<ResponseMessage> delete(String id) async {
    Helper.showLoaderOverlay();
    return notiRepo.deletetNotification(id).then((value) {
      if (value.success) firebaseRepo.getTotalNotifications();
      return value;
    }).whenComplete(() => Helper.hideLoaderOverlay());
  }

  /// chuyển page
  notificationToPage(NotificationDto item) async {
    await notiRepo.notificationToPage(item);
  }
}
