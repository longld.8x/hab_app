import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/foodDto.dart';
import 'package:hab_app/src/models/mealTypeDto.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/unitDto.dart';
import 'package:hab_app/src/models/workoutTypeDto.dart';
import 'package:hab_app/src/repositories/activities_repository.dart' as actRepo;
import 'package:hab_app/src/repositories/file_repository.dart' as fileRepo;
import 'package:intl/intl.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class AddController extends ControllerMVC {
  AddController() {}

  GlobalKey<ScaffoldState> scaffoldKeyAddMeal;

  void pageAddMealState() {
    this.scaffoldKeyAddMeal = new GlobalKey<ScaffoldState>();
  }

  /// theem moi mon - chon mon
  GlobalKey<ScaffoldState> scaffoldKeySelectedFood;
  int serverTotalCount = 0;
  int currentPage = -1;
  String currentKeyword = '';
  int itemPage = 15;
  List<FoodDto> dataFoods = [];
  bool isLoadingFoods = false;

  void pageSelectedFoodState() {
    this.scaffoldKeySelectedFood = new GlobalKey<ScaffoldState>();
    loadFoods(0, '');
  }

  void loadFoods(int page, String keyword) {
    if (currentPage == page && currentKeyword == keyword) {
      return;
    }
    Helper.showLoaderOverlay();
    isLoadingFoods = true;
    if (page == 0) dataFoods = [];
    actRepo.getFoods(keyword, page, 15).then((result) {
      setState(() {
        dataFoods.addAll(result.items);
        serverTotalCount = result.totalCount;
        currentPage = page;
        currentKeyword = keyword;
      });
    }).whenComplete(() {
      Helper.hideLoaderOverlay();
      isLoadingFoods = false;
    });
  }

  Future<ResponseMessage> addMeal(MealTypeDto type, List<Asset> images, List<FoodDto> foods) async {
    if ((images == null || images.length == 0) && (foods == null || foods.length == 0)) {
      return ResponseMessage.error(message: 'Vui lòng chọn hình ảnh hoặc chọn món ăn.');
    }
    Helper.showLoaderOverlay();
    return fileRepo.uploadAssetFiles(images).then((files) {
      Helper.showLoaderOverlay();
      var fakeId = "${new DateFormat('yyyyMMddHHmmss').format(DateTime.now())}_${'meal'}_${type.id}";
      return actRepo.addMeal(fakeId, type.mealName, files, json.encode(foods)).whenComplete(() => Helper.hideLoaderOverlay());
    }).whenComplete(() => Helper.hideLoaderOverlay());
  }

  /// edit

  GlobalKey<ScaffoldState> scaffoldKeyAddFood;
  GlobalKey<FormState> formKeyEdit;
  List<UnitDto> dataUnits;
  void pageAddFoodState() {
    this.formKeyEdit = new GlobalKey<FormState>();
    this.scaffoldKeyAddFood = new GlobalKey<ScaffoldState>();
    getUnits();
  }

  Future<ResponseMessage> getUnits() {
    Helper.showLoaderOverlay();
    return actRepo.getUnits().then((result) {
      if (result.success)
        setState(() {
          dataUnits = result.results;
        });
    }).whenComplete(() {
      Helper.hideLoaderOverlay();
    });
  }

  Future<ResponseMessage> addFood(FoodDto model) {
    Helper.showLoaderOverlay();
    return actRepo.addFood(model).whenComplete(() {
      Helper.hideLoaderOverlay();
    });
  }

  /// them nuoc uong
  GlobalKey<ScaffoldState> scaffoldKeyWater;
  GlobalKey<FormState> formKeyWater;
  void pageWaterState() {
    this.formKeyWater = new GlobalKey<FormState>();
    this.scaffoldKeyWater = new GlobalKey<ScaffoldState>();
  }

  Future<ResponseMessage> addWater(double water) async {
    if ((water == null || water <= 0)) {
      return ResponseMessage.error(message: 'Vui lòng nhập nước uống.');
    }
    Helper.showLoaderOverlay();
    var fakeId = "${new DateFormat('yyyyMMddHHmmss').format(DateTime.now())}_${'water'}";
    return actRepo.addWater(fakeId, water).whenComplete(() => Helper.hideLoaderOverlay());
  }

  /// them van dong
  GlobalKey<ScaffoldState> scaffoldKeyWorkout;
  GlobalKey<FormState> formKeyWorkout;
  List<WorkoutTypeDto> dataWorkoutType;
  WorkoutTypeDto workoutType;

  void pageWorkoutState() {
    this.formKeyWorkout = new GlobalKey<FormState>();
    this.scaffoldKeyWorkout = new GlobalKey<ScaffoldState>();
    getWorkoutType();
  }

  void getWorkoutType() {
    Helper.showLoaderOverlay();
    actRepo
        .getWorkoutType()
        .then((value) => setState(() {
              dataWorkoutType = value.results;
              workoutType = dataWorkoutType.first;
            }))
        .whenComplete(() => Helper.hideLoaderOverlay());
  }

  Future<ResponseMessage> addWorkout(WorkoutTypeDto type, DateTime tmpTime, String totalTime, String caloText, List<Asset> images) async {
    if (type == null) {
      return ResponseMessage.error(message: 'Vui lòng chọn loại vận động.');
    }
    if (tmpTime == null || totalTime == null || totalTime.isEmpty) {
      return ResponseMessage.error(message: 'Vui lòng chọn thời gian vận động.');
    }
    if (images.length == 0) {
      return ResponseMessage.error(message: 'Vui lòng chọn hình ảnh.');
    }

    Helper.showLoaderOverlay();
    return fileRepo.uploadAssetFiles(images).then((files) {
      Helper.showLoaderOverlay();
      var fakeId = "${new DateFormat('yyyyMMddHHmmss').format(DateTime.now())}_${'workout'}";
      var dataMap = Map<String, dynamic>();
      dataMap['workoutTypeId'] = type.id;
      dataMap['workoutTypeName'] = type.workoutCategoryName;

      dataMap['time'] = tmpTime.toString();
      dataMap['totalTime'] = totalTime;
      dataMap['hour'] = tmpTime.hour;
      dataMap['minute'] = tmpTime.minute;

      dataMap['calo'] = caloText;

      return actRepo.addWorkout(fakeId, files, dataMap.toString()).whenComplete(() => Helper.hideLoaderOverlay());
    }).whenComplete(() => Helper.hideLoaderOverlay());
  }
}
