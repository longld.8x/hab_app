import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class ProfileController extends ControllerMVC {
  ProfileController() {}

  GlobalKey<FormState> formKey;
  GlobalKey<ScaffoldState> scaffoldKey;
  User user;
  void pageState() {
    this.formKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.user = userRepo.currentUser.value;
    Helper.hideLoaderOverlay();
  }

  // thay doi mk - đã dang nhap
  GlobalKey<FormState> formKeyChangePassword;
  GlobalKey<ScaffoldState> scaffoldKeyChangePassword;
  void changePasswordState() {
    this.formKeyChangePassword = new GlobalKey<FormState>();
    this.scaffoldKeyChangePassword = new GlobalKey<ScaffoldState>();
  }

  Future<ResponseMessage> changePassword(String password, String password1, String password2) async {
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();
    return userRepo.changePassword(password, password2).whenComplete(() => Helper.hideLoaderOverlay());
  }

  // chỉnh sửa thông tin
  GlobalKey<FormState> formKeyEdit;
  GlobalKey<ScaffoldState> scaffoldKeyEdit;
  User userEdit;
  void editState() {
    this.formKeyEdit = new GlobalKey<FormState>();
    this.scaffoldKeyEdit = new GlobalKey<ScaffoldState>();
    this.userEdit = (userRepo.currentUser.value);
    Helper.hideLoaderOverlay();

    userRepo.getUserByIds([1, 2, 3, 4]);
  }

  Future<ResponseMessage> editProfile(User _user) async {
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();
    ResponseMessage result = await userRepo.editProfile(_user).whenComplete(() => Helper.hideLoaderOverlay());
    if (result.success) {
      Helper.showLoaderOverlay();
      await userRepo.userClientUpdate(_user.apiToken).whenComplete(() => Helper.hideLoaderOverlay());
      result.results = userRepo.currentUser.value;
    }
    return result;
  }

  // mục tiêu doanh số
  GlobalKey<FormState> formKeySales;
  GlobalKey<ScaffoldState> scaffoldKeySales;
  void salesState() {
    this.formKeySales = new GlobalKey<FormState>();
    this.scaffoldKeySales = new GlobalKey<ScaffoldState>();
  }

  logout() async {
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();
    return userRepo.logout().then((value) {
      settingRepo.navigatorKey.currentState.pushNamedAndRemoveUntil('/Login', (Route<dynamic> route) => false);
    }).whenComplete(() => Helper.hideLoaderOverlay());
  }
}
