import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/workDto.dart';
import 'package:hab_app/src/repositories/work_repository.dart' as workRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class WorkController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;

  /// index - danh sách lớp học của tôi
  int serverTotalCount = 0;
  int currentPage = -1;
  int itemPage = 15;
  bool isLoading = false;
  List<DayWorkDto> dayWorks;
  List<WorkDto> homeWorks;

  WorkController() {}
  DateTime fromDate;
  DateTime toDate;

  //home
  void homeWidgetState() {
    isLoading = true;
    setDate('now');
    homeWorks = [];
    workRepo.getWorks(fromDate, toDate, 0, 3).then((result) {
      setState(() {
        homeWorks = result.items;
      });
    }).whenComplete(() => isLoading = false);
  }

  // index
  void pageState() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    dayWorks = [];
    setDate('month');
    loadData(0);
  }

  void setDate(String type) {
    var _now = DateTime.now();
    fromDate = _now;
    if (type == 'month') fromDate = DateTime(_now.year, _now.month, 1);
    toDate = DateTime(_now.year, _now.month + 1, 1).add(Duration(seconds: -1));
  }

  void loadData(int page) {
    if (currentPage == page) {
      return;
    }
    Helper.showLoaderOverlay();
    isLoading = true;
    if (page == 0 || dayWorks == null) dayWorks = [];
    if (fromDate == null || toDate == null) setDate('month');
    workRepo.getWorks(fromDate, toDate, page, itemPage).then((result) {
      if (result != null && result.items != null) {
        var _tmp = dayWorks;
        for (var _item in result.items) {
          var _d = DateTime(_item.startDate.year, _item.startDate.month, _item.startDate.day);
          var _index = _tmp.indexWhere((e) => e.day == _d);
          if (_index > -1) {
            _tmp[_index].works.add(_item);
          } else {
            _tmp.add(new DayWorkDto(day: _d, works: [_item]));
          }
        }
        setState(() {
          dayWorks = _tmp;
          serverTotalCount = result.totalCount;
          currentPage = page;
        });
      }
    }).whenComplete(() {
      Helper.hideLoaderOverlay();
      isLoading = false;
    });
  }
}
