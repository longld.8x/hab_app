import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/chat/messageDto.dart';
import 'package:hab_app/src/models/chat/roomDataDto.dart';
import 'package:hab_app/src/models/chat/roomDto.dart';
import 'package:hab_app/src/models/chat/userChatDto.dart';
import 'package:hab_app/src/models/friendUser.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/repositories/chat_repository.dart' as chatRepo;
import 'package:hab_app/src/repositories/firebase_repository.dart' as firebaseRepo;
import 'package:hab_app/src/repositories/socket_repository.dart' as socketRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class ChatController extends ControllerMVC {
  ChatController() {}
  GlobalKey<FormState> formKey;
  GlobalKey<ScaffoldState> scaffoldKey;
  User user;
  List<User> usersSearch;
  bool isConnect = true;
  List<StreamSubscription> socketListeners = <StreamSubscription>[];

  void pageState() {
    this.formKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    user = userRepo.currentUser.value;
    socketRepo.connect();
    getRooms(1);
    new Future.delayed(Duration(microseconds: 600), () {
      getFriends(0);
    });
    socketListener();
    firebaseRepo.firebaseEvent.addListener(firebaseListener);
  }

  void socketListener() {
    socketListeners.add(socketRepo.onConnect(() => setState(() => isConnect = true)));
    socketListeners.add(socketRepo.onDisconnect(() => setState(() => isConnect = false)));
    //socketListeners.add(socketRepo.onMessage(onNewMessage));
    socketListeners.add(socketRepo.onNewRoom((int rId) {
      print("ChatController socketRepo.onNewRoom");
      chatRepo.getRoom(rId).then((result) {
        if (result != null) setState(() => dataRooms.insert(0, result));
      });
    }));
    socketListeners.add(socketRepo.onSeen((int roomId) {
      print("ChatRoomController socketRepo.onSeen");
      setState(() {
        for (var x in dataRooms) {
          if (x.room.id == roomId) {
            x.lastMessage.seen = true;
          }
        }
      });
    }));
  }

  void firebaseListener() {
    var _type = firebaseRepo.firebaseEvent.value.event;
    var _data = firebaseRepo.firebaseEvent.value.data;
    if (_type == 'chat_new_message') {
      var _room = RoomDto.fromJSON(json.decode(_data['data']['room']));
      var _roomsDto = dataRooms.where((x) => x.room.id == _room.id).toList();
      if (_roomsDto.length == 0) {
        chatRepo.getRoom(_room.id).then((result) {
          if (result != null) setState(() => dataRooms.insert(0, result));
        });
        return;
      }
      RoomDataDto _roomDto = _roomsDto.first;
      var _message = MessageDto.fromJSON(json.decode(_data['data']['message']));
      _message.user = UserChatDto.fromJSON(json.decode(_data['data']['user']));
      _roomDto.lastMessage = _message;
      setState(() {
        dataRooms.removeWhere((e) => e.room.id == _room.id);
        dataRooms.insert(0, _roomDto);
      });
    }
  }

  /// ========================================= room chat =========================================
  List<RoomDataDto> dataRooms = <RoomDataDto>[];
  int currentPageRooms = 0;
  int totalPageRooms = 0;
  int itemPage = 15;
  bool isLoadingRooms = false;

  void getRooms(int page) {
    var userId = userRepo.currentUser.value.id;
    if (userId == 0) return;
    //Helper.showLoaderOverlay();

    isLoadingRooms = true;
    if (page == 1) dataRooms = [];
    chatRepo.getRooms(page).then((result) {
      if (result != null) {
        setState(() {
          dataRooms.addAll(result.items);
          totalPageRooms = result.totalPage;
          currentPageRooms = page;
        });
      }
    }).whenComplete(() {
      // Helper.hideLoaderOverlay();
      isLoadingRooms = false;
    });
  }

  /// ========================================= friends chat =========================================
  List<FriendUser> dataFriends = <FriendUser>[];
  int currentPageFriends = -1;
  bool isLoadingFriends = false;
  getFriends(int page, {bool isLoading = true}) async {
    if (currentPageFriends == page) {
      return;
    }
    Helper.showLoaderOverlay();
    if (isLoading) isLoadingFriends = true;
    if (page == 0) dataFriends = [];
    await chatRepo.getUsersChat('', page, itemPage).then((result) {
      if (result != null && result.items != null) {
        setState(() {
          dataFriends.addAll(result.items);
          currentPageFriends = page;
        });
      }
    }).whenComplete(() {
      Helper.hideLoaderOverlay();
      isLoadingFriends = false;
    });
  }

  Future<ResponseMessage> findOrCreateRoom(int userId) async {
    Helper.showLoaderOverlay();
    return chatRepo.findOrCreateRoom(user.id, userId).whenComplete(() => Helper.hideLoaderOverlay());
  }

  onExitPage() {
    //socketRepo.disconnect();
    for (var x in socketListeners) x.cancel();
    firebaseRepo.firebaseEvent.removeListener(firebaseListener);
  }
}
