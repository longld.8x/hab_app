import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/documentDto.dart';
import 'package:hab_app/src/models/exercisesResultDto.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/models/sessionResultDto.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/models/zoomDto.dart';
import 'package:hab_app/src/repositories/activities_repository.dart' as activitiesRepo;
import 'package:hab_app/src/repositories/class_room_repository.dart' as classRepo;
import 'package:hab_app/src/repositories/socket_repository.dart' as socketRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class MyClassSessionController extends ControllerMVC {
  MyClassSessionController() {}

  bool isLoading = false;

  /// chi tiết
  GlobalKey<ScaffoldState> scaffoldKeyDetail;
  SessionResultDto dataDetail;
  int classId;
  int classSessionId = 0;
  int sessionId;
  List<int> role = <int>[];
  User user;
  ZoomDto zoomDto;

  void pageState(RouteClassArgument routeArgument) {
    this.scaffoldKeyDetail = new GlobalKey<ScaffoldState>();
    classId = routeArgument.classId;
    sessionId = routeArgument.sessionId;
    classSessionId = routeArgument.classSessionId;
    role = routeArgument.role;

    user = userRepo.currentUser.value;
    getClassSessionDetai(classSessionId).then((result) {
      setSessionResult(result);
      if (dataDetail != null) {
        getStateExcercies(dataDetail.classSessionId, false);
        getStateDocument(dataDetail.classSessionId, false);
        // socket group
        if (dataDetail != null && dataDetail.classSessionId != null && dataDetail.classSessionId > 0) {
          socketJoinGroup(dataDetail.classSessionId);
          socketListener();
        }
      }
    });

    //firebaseRepo.firebaseEvent.addListener(firebaseListener);
  }

  void setSessionResult(SessionResultDto dto) {
    if (dto != null) {
      setState(() {
        dataDetail = dto;
        if (dataDetail.sessionType == 1 && dataDetail.sessionStatus == 1 && dataDetail.linkRoom != null && dataDetail.linkRoom.isNotEmpty && dataDetail.linkRoom.indexOf('|') > -1) {
          var zoomOption = (dataDetail.linkRoom.toString()).split('|');
          zoomDto = ZoomDto(
            userId: '${user.fullName} - ${user.accountCode}',
            meetingId: zoomOption[0].toString(),
            meetingPassword: zoomOption[1].toString(),
            zak: zoomOption.length >= 3 ? zoomOption[3].toString() : "",
          );
        } else {
          zoomDto = null;
        }
      });
    } else {
      setState(() => dataDetail = null);
    }
  }

  /// dispose
  void onExitSocket() {
    if (dataDetail != null && dataDetail.classSessionId != null && dataDetail.classSessionId > 0) {
      socketRepo.exitGroup('${dataDetail.classSessionId.toString()}');
      for (var x in socketListeners) x.cancel();
    }
  }

  /// socket Listeners socketListeners
  List<StreamSubscription> socketListeners = <StreamSubscription>[];
  void socketJoinGroup(int classSessionId) async {
    await socketRepo.joinGroup('join_class_session', classSessionId.toString());
  }

  void socketListener() {
    socketListeners.add(socketRepo.onServerEvent((data) {
      var _event = data[0]['event'];
      if (_event == 'ClassSessionStarted' || _event == 'ClassSessionEnded') getClassSessionDetai(classSessionId, loading: false).then((result) => setSessionResult(result));
    }));
  }

  /// firebase Listener firebaseListener
  // void firebaseListener() {
  //   var _type = firebaseRepo.firebaseEvent.value.event;
  //   if (_type == 'JoinZoom') {
  //     getClassSessionDetai(classSessionId).then((result) => setSessionResult(result));
  //   }
  //   firebaseRepo.showNotification(firebaseRepo.firebaseEvent.value.data);
  // }

  bool isLoadingExcercies = false;
  List<ExercisesResultDto> dataExcercies;

  getClassSessionDetai(int _classSessionId, {bool loading: true}) async {
    if (loading) {
      isLoading = true;
      Helper.showLoaderOverlay();
    }
    return classRepo.getMyClassSessionDetai(_classSessionId).whenComplete(() {
      if (loading) {
        Helper.hideLoaderOverlay();
        setState(() => isLoading = false);
      }
    });
  }

  void getStateExcercies(int classSessionId, bool loading) {
    if (dataDetail.sessionStatus != 2) {
      setState(() {
        dataExcercies = [];
      });
      return;
    }
    if (dataExcercies == null) getExcercies(classSessionId, 0, 9999, loading);
  }

  void getExcercies(int classSessionId, int page, int itemInPage, bool loading) {
    if (classId <= 0 || sessionId <= 0) {
      setState(() {
        dataExcercies = [];
      });
      return;
    }
    if ([2].indexOf(dataDetail.sessionStatus) < 0) {
      setState(() {
        dataExcercies = [];
      });
      return;
    }
    if (loading) Helper.showLoaderOverlay();
    isLoadingExcercies = true;
    classRepo.getMyClassExcercies(classSessionId, page, itemInPage).then((result) {
      setState(() => dataExcercies = result.items);
    }).whenComplete(() {
      if (loading) Helper.hideLoaderOverlay();
      setState(() => isLoadingExcercies = false);
    });
  }

  bool isLoadingDocument = false;
  List<DocumentDto> dataDocuments;
  void getStateDocument(int classSessionId, bool loading) {
    if (dataDocuments == null) getDocument(classSessionId, 0, 9999, loading);
  }

  void getDocument(int classSessionId, int page, int itemInPage, bool loading) {
    if (classId <= 0 || sessionId <= 0) {
      setState(() {
        dataDocuments = [];
      });
      return;
    }
    if (loading) Helper.showLoaderOverlay();
    isLoadingDocument = true;
    classRepo.getMyClassDocument(classSessionId, page, itemInPage).then((result) {
      setState(() => dataDocuments = result.items);
    }).whenComplete(() {
      if (loading) Helper.hideLoaderOverlay();
      setState(() => isLoadingDocument = false);
    });
  }

  /// hoc offline
  checkInOffline() {
    if (dataDetail == null) return ResponseMessage.error(message: "Không xác định thông tin buổi học!");
    Helper.showLoaderOverlay();
    return activitiesRepo.checkInOffline(dataDetail.classSessionId.toString()).whenComplete(() {
      Helper.hideLoaderOverlay();
    });
  }
}
