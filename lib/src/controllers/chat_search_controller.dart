import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/friendUser.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/repositories/chat_repository.dart' as chatRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class ChatSearchController extends ControllerMVC {
  ChatSearchController();
  User user;

  void pageState() {
    user = userRepo.currentUser.value;
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  GlobalKey<FormState> formKey;
  GlobalKey<ScaffoldState> scaffoldKey;
  // tìm kiếm user chat
  List<FriendUser> dataFriends = <FriendUser>[];
  int serverTotalCount = 0;
  int currentPage = -1;
  int itemPage = 15;
  bool isLoading = false;
  String currentKeyword = "";

  void addSearchChatUser(User _user) {}

  void searchUsers(int userId, int page, String keyword) {
    if (currentPage == page && currentKeyword == keyword) {
      return;
    }
    if (userId == 0) userId = userRepo.currentUser.value.id;
    Helper.showLoaderOverlay();
    isLoading = true;
    if (page == 0) dataFriends = [];
    chatRepo.getUsersChat(keyword, page, itemPage).then((result) {
      if (result != null && result.items != null) {
        setState(() {
          dataFriends.addAll(result.items);
          serverTotalCount = result.totalCount;
          currentPage = page;
          currentKeyword = keyword;
        });
      }
    }).whenComplete(() {
      Helper.hideLoaderOverlay();
      isLoading = false;
    });
  }

  Future<ResponseMessage> findOrCreateRoom(int userId) async {
    //Helper.showLoaderOverlay();
    return chatRepo.findOrCreateRoom(user.id, userId).whenComplete(() => Helper.hideLoaderOverlay());
  }
}
