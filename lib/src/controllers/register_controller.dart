import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class RegisterController extends ControllerMVC {
  GlobalKey<FormState> formKeyRegister;
  GlobalKey<ScaffoldState> scaffoldKeyRegister;
  // dang ky nhập mk
  GlobalKey<FormState> formKeyRegisterSetPassword;
  GlobalKey<ScaffoldState> scaffoldKeyRegisterSetPassword;

  RegisterController() {}

  void registerState() {
    this.formKeyRegister = new GlobalKey<FormState>();
    this.scaffoldKeyRegister = new GlobalKey<ScaffoldState>();
  }

  void registerSetPasswordState() {
    this.formKeyRegisterSetPassword = new GlobalKey<FormState>();
    this.scaffoldKeyRegisterSetPassword = new GlobalKey<ScaffoldState>();
  }

  ///đăng ký
  Future<ResponseMessage> checkPhoneNumber(String phoneNumber) async {
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();
    return userRepo.checkPhoneNumber(phoneNumber).whenComplete(() => Helper.hideLoaderOverlay());
  }

  ///đăng ký
  Future<ResponseMessage> registerAccount(String phoneNumber, String password) async {
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();
    return userRepo.registerAccount(phoneNumber, password).whenComplete(() => Helper.hideLoaderOverlay());
  }
}
