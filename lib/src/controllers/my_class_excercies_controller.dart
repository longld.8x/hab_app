import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/activityDto.dart';
import 'package:hab_app/src/models/exercisePostDto.dart';
import 'package:hab_app/src/models/exercisesResultDto.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/repositories/activities_repository.dart' as actRepo;
import 'package:hab_app/src/repositories/class_room_repository.dart' as classRepo;
import 'package:hab_app/src/repositories/file_repository.dart' as fileRepo;
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:hab_app/src/repositories/socket_repository.dart' as socketRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MyClassExcerciesController extends ControllerMVC {
  MyClassExcerciesController() {}

  bool isLoading = false;

  /// chi tiết
  GlobalKey<ScaffoldState> scaffoldKey;
  ExercisesResultDto dataDetail;
  List<ExercisePostDto> dataPostExercises;
  String objectIdentify = '';
  int classId = 0;
  //int sessionId = 0;
  int classSessionId = 0;
  int exerciseClassModuleId = 0;
  List<int> role = <int>[];
  // all ex
  int itemPage = 5;
  int currentPage = -1;
  int serverTotalCount = 0;
  // my ex
  List<ExercisePostDto> dataMyExercises;
  int itemPage2 = 5;
  int currentPage2 = -1;
  int serverTotalCount2 = 0;

  Future<void> pageState(RouteClassArgument routeArgument) {
    if (routeArgument.exerciseClassModuleId == null) {
      Navigator.of(settingRepo.navigatorKey.currentContext).pop();
      Helper.errorMessenge("Có lỗi trong quá trình xử lý!");
    }
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    classId = routeArgument.classId;
    // sessionId = routeArgument.sessionId;
    classSessionId = routeArgument.classSessionId;
    exerciseClassModuleId = routeArgument.exerciseClassModuleId;
    role = routeArgument.role;
    dataDetail = routeArgument.param as ExercisesResultDto;
    getMyClassExcercieDetail(exerciseClassModuleId);
    objectIdentify = actRepo.newObjectIdentify(classId, classSessionId, exerciseClassModuleId);
    // join
    socketJoinGroup(classSessionId, exerciseClassModuleId);
    // listen
    socketListener(classSessionId, exerciseClassModuleId);
  }

  Future<void> getMyClassExcercieDetail(int id) async {
    Helper.showLoaderOverlay();
    isLoading = true;
    classRepo.getMyClassExcercieDetail(id).then((result) async {
      if (result.success)
        setState(() {
          dataDetail = result.results as ExercisesResultDto;
        });
    }).whenComplete(() {
      Helper.hideLoaderOverlay();
      setState(() => isLoading = false);
    });
  }

  Future<void> getPostExercises(String keyWord, int page, {bool loading: true}) async {
    if (classId <= 0) return;
    if (loading) Helper.showLoaderOverlay();
    if (page == 0) setState(() => dataPostExercises = []);
    if (dataPostExercises == null) setState(() => dataPostExercises = []);
    actRepo.getActivity(objectIdentify, 'Exercise', keyWord, page, itemPage).then((result) async {
      setState(() {
        serverTotalCount = result.totalCount;
        currentPage = page;
      });
      if (result.items != null && result.items.length > 0) {
        var _postIds = result.items.map<String>((e) => e.id).toList();
        var _userIds = result.items.map<int>((e) => e.userId).toList();
        var _details = await actRepo.getActivityMulti(objectIdentify, 'Comment', _postIds.join(','), '', 0, 2);

        for (var x in _details) {
          _userIds.addAll(x.items.map((e2) => e2.userId).toList());
        }
        var _userInfos = await classRepo.getUsersInLeaderClass(classId, _userIds);

        result.items.forEach((post) {
          var _search = _details.where((e) => e.items.first.objectIdentify == post.id);
          PageResult<ActivityDto> _itemComments = _search != null && _search.length > 0 ? _search.first : null;
          if (_itemComments != null && _itemComments.items.length > 0) {
            for (var x in _itemComments.items) {
              x.user = _userInfos.where((e) => e.userId == x.userId).first;
            }
          }

          List<ActivityDto> _cms = _itemComments != null ? _itemComments.items : [];
          int _tot = _itemComments != null ? _itemComments.totalCount : 0;
          var _user = _userInfos.where((e) => e.userId == post.userId).first;
          setState(() {
            dataPostExercises.add(new ExercisePostDto(
              post: post,
              user: _user,
              comments: _cms,
              totalComment: _tot,
              viewTotalComment: _cms.length,
            ));
          });
        });
      }
    }).whenComplete(() {
      if (loading) Helper.hideLoaderOverlay();
    });
  }

  Future<void> getMyExercises(String keyWord, int page, {bool loading: true}) async {
    if (classId <= 0) return;
    if (loading) Helper.showLoaderOverlay();
    if (page == 0) setState(() => dataMyExercises = []);
    if (dataMyExercises == null) setState(() => dataMyExercises = []);
    PageResult<ActivityDto> result;
    var _lstUsers = [];
    if (role.indexOf(3) > -1) {
      _lstUsers = await classRepo.getSubUserOnLeaderClass(classId);
    } else if (role.indexOf(1) > -1) {
      _lstUsers = [userRepo.currentUser.value.id];
    } else {
      _lstUsers = [];
    }
    if (_lstUsers.length == 0) return;
    result = await actRepo.getActivityByLeader(objectIdentify, 'Exercise', _lstUsers.join(','), keyWord, page, itemPage).whenComplete(() {
      if (loading) Helper.hideLoaderOverlay();
    });
    if (result == null) return;
    setState(() {
      serverTotalCount2 = result.totalCount;
      currentPage2 = page;
    });
    if (result.items != null && result.items.length > 0) {
      var _postIds = result.items.map<String>((e) => e.id).toList();
      var _userIds = result.items.map<int>((e) => e.userId).toList();

      var _details = await actRepo.getActivityMulti(objectIdentify, 'Comment', _postIds.join(','), '', 0, 2);

      for (var x in _details) {
        _userIds.addAll(x.items.map((e2) => e2.userId).toList());
      }
      var _userInfos = await classRepo.getUsersInLeaderClass(classId, _userIds);

      result.items.forEach((post) {
        var _search = _details.where((e) => e.items.first.objectIdentify == post.id);
        PageResult<ActivityDto> _itemComments = _search != null && _search.length > 0 ? _search.first : null;
        if (_itemComments != null && _itemComments.items.length > 0) {
          for (var x in _itemComments.items) {
            x.user = _userInfos.where((e) => e.userId == x.userId).first;
          }
        }

        List<ActivityDto> _cms = _itemComments != null ? _itemComments.items : [];
        int _tot = _itemComments != null ? _itemComments.totalCount : 0;
        var _user = _userInfos.where((e) => e.userId == post.userId).first;
        setState(() {
          dataMyExercises.add(new ExercisePostDto(
            post: post,
            user: _user,
            comments: _cms,
            totalComment: _tot,
            viewTotalComment: _cms.length,
          ));
        });
      });
    }
  }

  // build lại block
  Future<void> getPostExercisesById(String postId, String type, {bool loading: true}) async {
    var _index = dataPostExercises.indexWhere((element) => element.post.id == postId);
    var _old = dataPostExercises[_index];
    var loadItem = 0;
    if (type == 'add_comment') loadItem = 1;
    if (type == 'load_comment' && _old.totalComment > _old.viewTotalComment) {
      loadItem = 10;
    }
    if (loadItem == 0) return;
    if (loading) Helper.showLoaderOverlay();
    var result = await actRepo.getActivityMulti(objectIdentify, 'Comment', postId, '', 0, (_old.viewTotalComment + loadItem)).whenComplete(() => Helper.hideLoaderOverlay());

    var _itemComments = result[0];
    if (_itemComments != null && _itemComments.items.length > 0) {
      var _userIds = _itemComments.items.map<int>((e) => e.userId).toList();
      if (loading) Helper.showLoaderOverlay();
      var _userInfos = await classRepo.getUsersInLeaderClass(classId, _userIds).whenComplete(() => Helper.hideLoaderOverlay());
      for (var x in _itemComments.items) {
        x.user = _userInfos.where((e) => e.userId == x.userId).first;
      }
    }

    setState(() {
      dataPostExercises[_index].totalComment = _itemComments != null ? _itemComments.totalCount : 0;
      dataPostExercises[_index].comments = _itemComments != null ? (_itemComments.items) : [];
      dataPostExercises[_index].viewTotalComment += 1;
    });

    var _indexMyEx = dataMyExercises.indexWhere((element) => element.post.id == postId);
    if (_indexMyEx >= 0) {
      dataMyExercises[_indexMyEx].totalComment = _itemComments != null ? _itemComments.totalCount : 0;
      dataMyExercises[_indexMyEx].comments = _itemComments != null ? (_itemComments.items) : [];
      dataMyExercises[_indexMyEx].viewTotalComment += 1;
    }
  }

  /// add -- view
  GlobalKey<ScaffoldState> scaffoldKeyAdd;
  List<int> roleUser = [];
  void pageAddState(RouteClassArgument routeArgument) {
    this.scaffoldKeyAdd = new GlobalKey<ScaffoldState>();
    if (routeArgument.param != null) {
      dataDetail = routeArgument.param as ExercisesResultDto;
    }
    exerciseClassModuleId = routeArgument.exerciseClassModuleId;
    classId = routeArgument.classId;
    //sessionId = routeArgument.sessionId;
    classSessionId = routeArgument.classSessionId;
    roleUser = routeArgument.role;
  }

  Future<ResponseMessage> addExercise(List<Asset> images, String content) async {
    if (images.length == 0 && (content.isEmpty || content.length == 0)) {
      return ResponseMessage.error(message: 'Vui lòng chọn hình ảnh hoặc nhập nội dung.');
    }
    Helper.showLoaderOverlay();
    return fileRepo.uploadAssetFiles(images).then((files) {
      Helper.showLoaderOverlay();
      var _id = actRepo.newObjectIdentify(classId, classSessionId, exerciseClassModuleId);
      return actRepo.addExercise(_id, content, files, roleUser, '').whenComplete(() => Helper.hideLoaderOverlay());
    }).whenComplete(() => Helper.hideLoaderOverlay());
  }

  ///  -- addComment  post
  Future<ResponseMessage> addComment(String postExerciseId, String content, List<Asset> images, int point) async {
    if (images.length == 0 && (content.isEmpty || content.length == 0)) {
      return ResponseMessage.error(message: 'Vui lòng chọn hình ảnh hoặc nhập nội dung.');
    }
    Helper.showLoaderOverlay();
    return fileRepo.uploadAssetFiles(images).then((files) {
      Helper.showLoaderOverlay();
      return actRepo.addComment(postExerciseId, content, files, role, point).whenComplete(() => Helper.hideLoaderOverlay());
    }).whenComplete(() => Helper.hideLoaderOverlay());
  }

  /// details

  GlobalKey<ScaffoldState> scaffoldKeyPost;
  void pagePostDetailState(ExercisePostDto _post, ExercisesResultDto _exercise, List<int> _role, int _classSessionId) {
    this.scaffoldKeyPost = new GlobalKey<ScaffoldState>();
    role = _role;
    // join
    socketJoinGroupDetail(_classSessionId, _exercise.exerciseClassModuleId);
    // listen
    socketListenerDetail(_post, _exercise.classId);
  }

  getNewPost(String postId) async {
    var result = await actRepo.getActivityById(postId);
    var _post = result.results as ActivityDto;
    var _userIds = [_post.userId];
    var _userInfos = await classRepo.getUsersInLeaderClass(classId, _userIds);
    var _user = _userInfos.where((e) => e.userId == _post.userId).first;
    setState(() {
      dataPostExercises.insert(
          0,
          new ExercisePostDto(
            post: _post,
            user: _user,
            comments: [],
            totalComment: 0,
            viewTotalComment: 0,
          ));
    });
  }

  Future<ExercisePostDto> getDetailsPostExercise(ExercisePostDto post, int _classId, String type, {bool loading: true}) async {
    var loadItem = 0;
    if (type == 'add_comment') loadItem = 1;
    if (type == 'load_comment' && post.totalComment > post.viewTotalComment) {
      loadItem = 10;
    }
    if (loadItem == 0) return post;
    if (loading) Helper.showLoaderOverlay();

    var result = await actRepo.getActivityMulti(objectIdentify, 'Comment', post.post.id, '', 0, (post.viewTotalComment + loadItem)).whenComplete(() => Helper.hideLoaderOverlay());
    var _itemComments = result[0];
    if (_itemComments != null && _itemComments.items.length > 0) {
      if (loading) Helper.showLoaderOverlay();
      var _userIds = _itemComments.items.map<int>((e) => e.userId).toList();
      var _userInfos = await classRepo.getUsersInLeaderClass(_classId, _userIds).whenComplete(() => Helper.hideLoaderOverlay());
      for (var x in _itemComments.items) {
        x.user = _userInfos.where((e) => e.userId == x.userId).first;
      }
    }
    post.totalComment = _itemComments != null ? _itemComments.totalCount : 0;
    post.comments = _itemComments != null ? _itemComments.items : [];
    post.viewTotalComment += 1;
    return post;
  }

  /// socket Listeners socketListeners
  List<StreamSubscription> socketListeners = <StreamSubscription>[];
  void socketJoinGroup(int classSessionId, int exerciseClassModuleId) async {
    await socketRepo.joinGroup('join_group', '${classSessionId}_${exerciseClassModuleId}');
  }

  void socketListener(int classSessionId, int exerciseClassModuleId) {
    socketListeners.add(socketRepo.onServerEvent((data) {
      var _event = data[0]['event'];
      print("socketListener_event $_event");
      if (_event == 'ActivityExercise') {
        var _map = json.decode(data[0]['data']);
        var _postId = _map['id'].toString();
        print("socketListener_event ActivityExercise $_postId ");
        getNewPost(_postId);
      }
      if (_event == 'ActivityComment') {
        var _map = json.decode(data[0]['data']);
        var _postId = _map['id'].toString();
        print("socketListener_event ActivityComment $_postId ");
        getPostExercisesById(_postId, 'add_comment', loading: false);
      }
    }));
  }

  void onExitSocket(int classSessionId, int exerciseClassModuleId) {
    print("onExitSocket classSessionId $classSessionId");
    if (classSessionId > 0) socketRepo.exitGroup('${classSessionId}_${exerciseClassModuleId}');
    for (var x in socketListeners) x.cancel();
  }

  /// socket Listeners socketListeners
  List<StreamSubscription> socketListenersDetail = <StreamSubscription>[];
  void socketJoinGroupDetail(int _classSessionId, int _exerciseClassModuleId) async {
    await socketRepo.joinGroup('join_group', '${_classSessionId}_${_exerciseClassModuleId}');
  }

  void socketListenerDetail(ExercisePostDto _post, int _classId) {
    socketListenersDetail.add(socketRepo.onServerEvent((data) {
      var _event = data[0]['event'];
      if (_event == 'ActivityComment') {
        var _map = json.decode(data[0]['data']);
        var _postId = _map['id'].toString();
        print("socketListenerDetail ActivityComment $_postId ");
        print("_postId == _post.post.id $_postId ${_post.post.id} ");
        if (_postId == _post.post.id) {
          getDetailsPostExercise(_post, _classId, 'add_comment', loading: false).then((value) => setState(() => _post = value));
        }
      }
    }));
  }

  void onExitSocketDetail(int _classSessionId, int _exerciseClassModuleId) {
    if (_classSessionId > 0) socketRepo.exitGroup('${_classSessionId}_${_exerciseClassModuleId}');
    for (var x in socketListenersDetail) x.cancel();
  }
}
