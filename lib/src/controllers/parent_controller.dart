import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;

class ParentController extends ControllerMVC {
  ParentController() {}

  GlobalKey<FormState> formKey;
  GlobalKey<ScaffoldState> scaffoldKey;
  User user;
  User parent;
  ResponseMessage checkParent;
  void pageState() async {
    this.formKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.user = userRepo.currentUser.value;
    Helper.showLoaderOverlay();
    userRepo.getParent().then((value) => setState(() => checkParent = value)).whenComplete(() => Helper.hideLoaderOverlay());
  }

  Future<ResponseMessage> searchParent(String phoneNumber) async {
    Helper.showLoaderOverlay();
    return userRepo.searchUser(phoneNumber).whenComplete(() => Helper.hideLoaderOverlay());
  }

  Future<ResponseMessage> setParent(String phoneNumber) async {
    Helper.showLoaderOverlay();
    return userRepo.setParent(phoneNumber).whenComplete(() => Helper.hideLoaderOverlay());
  }
}
