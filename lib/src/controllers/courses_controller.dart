import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/course.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/repositories/course_repository.dart' as courseRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class CoursesController extends ControllerMVC {
  CoursesController() {}
  bool isLoading = false;

  // index
  List<Course> dataCourses = [];

  /// index
  GlobalKey<ScaffoldState> scaffoldKey;
  void pageState() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    Helper.showLoaderOverlay();
    courseRepo.getCourses().then((result) {
      if (result.success) setState(() => dataCourses = result.results);
    }).whenComplete(() {
      Helper.hideLoaderOverlay();
      isLoading = false;
    });
  }

  /// xem thông tin khóa học - khi mua ( chưa đc gán lơp)
  GlobalKey<ScaffoldState> scaffoldKeyPreview;
  CoursePreview coursePreview;
  void pagePreviewState(int productId) {
    this.scaffoldKeyPreview = new GlobalKey<ScaffoldState>();
    getCourseData(productId);
  }

  void getCourseData(int productId) {
    isLoading = true;
    Helper.showLoaderOverlay();

    courseRepo.getCoursePreview(productId).then((ResponseMessage result) {
      if (result.success) setState(() => coursePreview = result.results);
    }).whenComplete(() {
      isLoading = false;
      Helper.hideLoaderOverlay();
    });
  }

  /// xem thông tin mua khóa học
  GlobalKey<ScaffoldState> scaffoldKeyOrder;
  void pageOrderState() {
    this.scaffoldKeyOrder = new GlobalKey<ScaffoldState>();
  }

  /// xem thông tin mua khóa học
  GlobalKey<ScaffoldState> scaffoldKeyCoupon;
  void pageCouponState() {
    this.scaffoldKeyCoupon = new GlobalKey<ScaffoldState>();
  }

  /// xem thông tin mua khóa học
  GlobalKey<ScaffoldState> scaffoldKeyPickTime;
  void pagePickTimeState() {
    this.scaffoldKeyPickTime = new GlobalKey<ScaffoldState>();
  }

  /// kiểm tra mã giảm giá
  Future<ResponseMessage> getCoupon(String code) async {
    Helper.showLoaderOverlay();
    return courseRepo.getCoupon(code).whenComplete(() => Helper.hideLoaderOverlay());
  }

  /// mua khóa học - đăng ký
  Future<ResponseMessage> buyCourse(int productId, String coupon, double couponAmount, DateTime dateJoin) async {
    print("productId $productId, String coupon $coupon, DateTime dateJoin $dateJoin");
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();

    return courseRepo.buyCourse(productId, couponAmount > 0 && coupon != null && coupon.isNotEmpty ? coupon : '', dateJoin).whenComplete(() => Helper.hideLoaderOverlay());
  }

  /// chi tiết
  GlobalKey<FormState> formKeyDetail;
  GlobalKey<ScaffoldState> scaffoldKeyDetail;
  void pageDetailState(int classId) {
    this.formKeyDetail = new GlobalKey<FormState>();
    this.scaffoldKeyDetail = new GlobalKey<ScaffoldState>();
  }

  /// home page
  void homeWidgetState() {
    isLoading = true;
    courseRepo.getCourses().then((result) {
      if (result.success) setState(() => dataCourses = result.results);
    }).whenComplete(() => isLoading = false);
  }

  /// home page
  Future<ResponseMessage> checkParent() {
    Helper.showLoaderOverlay();
    return userRepo.getParent().whenComplete(() => Helper.hideLoaderOverlay());
  }
}
