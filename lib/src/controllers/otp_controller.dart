import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hab_app/src/repositories/otp_repository.dart' as otpRepo;

class OTPController extends ControllerMVC {
  GlobalKey<FormState> formKey;
  GlobalKey<ScaffoldState> scaffoldKey;
  OTPController() {}

  void OTPState() {
    this.formKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  int getType(String type) {
    // public enum OtpType
    //  {
    //      Transfer = 1,
    //      Payment = 2,
    //      ResetPass = 3,
    //      ChangePassLevel2 = 4,
    //      Register = 5,
    //      Login = 6,
    //      ChangePaymentMethod = 7
    //  }
    if (type == "register") return 5;
    if (type == "forget_password") return 3;
    return 0;
  }

  bool getAuth(String type) {
    return !(type == "register" || type == "forget_password");
  }

  /// gửi OTP
  Future<ResponseMessage> sendOtp(String type, String phoneNumber) async {
    int _type = getType(type);
    bool _auth = getAuth(type);
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();
    if (_auth) return otpRepo.sendOtpAuth(_type).whenComplete(() => Helper.hideLoaderOverlay());
    return otpRepo.sendOtp(_type, phoneNumber).whenComplete(() => Helper.hideLoaderOverlay());
  }

  /// gửi lại OTP
  Future<ResponseMessage> reSendOtp(String type, String phoneNumber) async {
    int _type = getType(type);
    bool _auth = getAuth(type);
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();
    if (_auth) return otpRepo.sendOtpAuth(_type, isResend: true).whenComplete(() => Helper.hideLoaderOverlay());
    return otpRepo.sendOtp(_type, phoneNumber, isResend: true).whenComplete(() => Helper.hideLoaderOverlay());
  }

  /// gửi lại OTP
  Future<ResponseMessage> verify(String type, String otp, String phoneNumber) async {
    int _type = getType(type);
    bool _auth = getAuth(type);
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();
    if (_auth) return otpRepo.verifyOtpAuth(_type, otp).whenComplete(() => Helper.hideLoaderOverlay());
    return otpRepo.verifyOtp(_type, phoneNumber, otp).whenComplete(() => Helper.hideLoaderOverlay());
  }
}
