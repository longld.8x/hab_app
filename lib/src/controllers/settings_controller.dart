import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;

class SettingsController extends ControllerMVC {
  SettingsController() {}

  GlobalKey<FormState> formKey;
  GlobalKey<ScaffoldState> scaffoldKey;
  User user;
  void pageState() {
    this.formKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.user = userRepo.currentUser.value;
  }
}
