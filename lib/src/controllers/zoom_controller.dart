import 'package:flutter/material.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:permission_handler/permission_handler.dart';

class ZoomController extends ControllerMVC {
  GlobalKey<FormState> formKey;
  GlobalKey<ScaffoldState> scaffoldKey;

  ZoomController() {}
  int classId = 0;
  int sessionId = 0;
  bool isLoading = false;
  bool isPermission = true;

  // index
  void pageState(RouteClassArgument routeArgument) {
    this.formKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    classId = routeArgument.classId;
    sessionId = routeArgument.sessionId;
    initPermission();
  }

  void initPermission() async {
    isPermission = true;
    Map<Permission, PermissionStatus> statuses = await [
      Permission.microphone,
      Permission.camera,
    ].request();

    if (!(statuses[Permission.microphone].isGranted) || !(statuses[Permission.camera].isGranted)) {
      setState(() => isPermission = false);
    }
  }

  // void initZomm() async {
  //   Helper.showLoaderOverlay();
  //   isLoading = true;
  //   classRoomRepo.getZoomData(classId, sessionId).then((result) {
  //     if (result.success) {
  //       print("classRoomRepo.getZoomData ${result.results['id'].toString()} - ${result.results['password'].toString()}");
  //       setState(() {
  //         meetingId = result.results['id'].toString();
  //         meetingPassword = result.results['password'].toString();
  //       });
  //     } else {
  //       Navigator.of(settingRepo.navigatorKey.currentContext).pop();
  //       Helper.errorMessenge(result.message);
  //     }
  //   }).whenComplete(() {
  //     //Helper.hideLoaderOverlay();
  //     isLoading = false;
  //   });
  // }
}
