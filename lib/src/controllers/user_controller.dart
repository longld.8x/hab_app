import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class UserController extends ControllerMVC {
  User user = new User();
  // dang nhap
  GlobalKey<FormState> formKeyLogin;
  GlobalKey<ScaffoldState> scaffoldKeyLogin;
  // doi mk
  GlobalKey<FormState> formKeyForgetPassword;
  GlobalKey<ScaffoldState> scaffoldKeyForgetPassword;
  // thay doi mk - chua dang nhap
  GlobalKey<FormState> formKeyForgetPasswordChange;
  GlobalKey<ScaffoldState> scaffoldKeyForgetPasswordChange;

  UserController() {}

  void loginState() {
    this.formKeyLogin = new GlobalKey<FormState>();
    this.scaffoldKeyLogin = new GlobalKey<ScaffoldState>();
  }

  void forgetPasswordState() {
    this.formKeyForgetPassword = new GlobalKey<FormState>();
    this.scaffoldKeyForgetPassword = new GlobalKey<ScaffoldState>();
  }

  void forgetPasswordChangeState() {
    this.formKeyForgetPasswordChange = new GlobalKey<FormState>();
    this.scaffoldKeyForgetPasswordChange = new GlobalKey<ScaffoldState>();
  }

  ///đăng nhập
  Future<ResponseMessage> login(String userName, String password) async {
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();
    return userRepo.login(userName, password).whenComplete(() => Helper.hideLoaderOverlay());
  }

  logoutClear() async {
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();
    return userRepo.logoutClear().then((value) {
      settingRepo.navigatorKey.currentState.pushNamedAndRemoveUntil('/Login', (Route<dynamic> route) => false);
    }).whenComplete(() => Helper.hideLoaderOverlay());
  }
}
