import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/healthIndex.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/repositories/health_repository.dart' as healthRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class HealthIndexesController extends ControllerMVC {
  HealthIndexesController() {}
  GlobalKey<ScaffoldState> scaffoldKey;
  int pageTotalCount = 0;
  int pageCurrent = -1;
  int pageItem = 15;
  bool isLoading = false;
  DateTime fromDate;
  DateTime toDate;
  List<HealthIndexDto> dataList = [];

  void pageState() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    setDate();
    loadData(0);
  }

  void setDate() {
    var _now = DateTime.now();
    fromDate = DateTime(2020, 1, 1);
    toDate = _now.add(Duration(days: 100));
  }

  /// search - get data f1
  void loadData(int page) async {
    Helper.showLoaderOverlay();
    isLoading = true;
    if (page == 0) dataList = [];
    healthRepo.getMyIndexes(fromDate, toDate, page, pageItem).then((PageResult<HealthIndexDto> result) {
      if (result != null) {
        setState(() {
          dataList.addAll(result.items);
          pageTotalCount = result.totalCount;
          pageTotalCount = page;
        });
      } else {
        setState(() {
          dataList = dataList;
          pageTotalCount = result.totalCount;
          pageTotalCount = page;
        });
      }
    }).whenComplete(() {
      Helper.hideLoaderOverlay();
      isLoading = false;
    });
  }

  /// add and edit
  GlobalKey<FormState> formKeyEdit;
  GlobalKey<ScaffoldState> scaffoldKeyEdit;
  HealthIndexDto editModel;
  void editState(String code) {
    this.formKeyEdit = new GlobalKey<FormState>();
    this.scaffoldKeyEdit = new GlobalKey<ScaffoldState>();
  }

  // getEditData(String id) async {
  //   print("id $id");
  //   Helper.showLoaderOverlay();
  //   return await Future.delayed(Duration(microseconds: 1), () {
  //     if (id == null || id.isEmpty) {
  //       setState(() {
  //         editModel = new HealthIndexDto();
  //       });
  //       return (editModel);
  //     } else {
  //       setState(() {
  //         editModel = new HealthIndexDto();
  //       });
  //       return (editModel);
  //     }
  //   }).whenComplete(() => Helper.hideLoaderOverlay());
  // }

  Future<ResponseMessage> onUpdateModel(String action, HealthIndexDto model) async {
    Helper.showLoaderOverlay();
    if (action == "edit" && editModel != null && editModel.id.isNotEmpty) {
      model.id = editModel.id;
      return await healthRepo.editMyIndexes(model).whenComplete(() {
        Helper.hideLoaderOverlay();
      });
    } else
      return await healthRepo.addMyIndexes(model).whenComplete(() {
        Helper.hideLoaderOverlay();
      });
  }
}
