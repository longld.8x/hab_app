import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class ForgetController extends ControllerMVC {
  GlobalKey<FormState> formKey;
  GlobalKey<ScaffoldState> scaffoldKey;
  // dang ky nhập mk
  GlobalKey<FormState> formKeySetPassword;
  GlobalKey<ScaffoldState> scaffoldKeySetPassword;

  ForgetController() {}

  void pageState() {
    this.formKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void pagePasswordState() {
    this.formKeySetPassword = new GlobalKey<FormState>();
    this.scaffoldKeySetPassword = new GlobalKey<ScaffoldState>();
  }

  ///đăng ký
  Future<ResponseMessage> checkPhoneNumber(String phoneNumber) async {
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();
    return userRepo.checkPhoneNumber(phoneNumber).whenComplete(() => Helper.hideLoaderOverlay());
  }

  ///đăng ký
  Future<ResponseMessage> forgetPassword(String phoneNumber, String password) async {
    FocusScope.of(state?.context).requestFocus(FocusNode());
    Helper.showLoaderOverlay();
    return userRepo.forgetPassword(phoneNumber, password).whenComplete(() => Helper.hideLoaderOverlay());
  }
}
