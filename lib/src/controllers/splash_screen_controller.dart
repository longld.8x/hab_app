import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/repositories/setting_repository.dart'
    as settingRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class SplashScreenController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  ValueNotifier<Map<String, double>> progress = new ValueNotifier(new Map());

  SplashScreenController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    progress.value = {"Setting": 0, "User": 0};
  }

  @override
  void initState() {
    super.initState();
    settingRepo.setting.addListener(() {
      if (settingRepo.setting.value.appName != null &&
          settingRepo.setting.value.appName != '' &&
          settingRepo.setting.value.backgroundColor != null) {
        progress.value["Setting"] = 41;
        progress?.notifyListeners();
      }
    });
    userRepo.currentUser.addListener(() {
      if (userRepo.currentUser.value.auth != null) {
        progress.value["User"] = 59;
        progress?.notifyListeners();
      }
    });
    Timer(Duration(seconds: 30), () {
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text("Vui lòng kiểm tra lại kết nối internet của bạn"),
      ));
    });
  }

  void reopenApp() {
    try {
      if (userRepo.currentUser.value.apiToken == null) {
        userRepo.logout().then((value) => settingRepo.navigatorKey.currentState
            .pushNamedAndRemoveUntil(
                '/Login', (Route<dynamic> route) => false));
      } else {
        settingRepo.navigatorKey.currentState.pushNamedAndRemoveUntil(
            '/Page', (Route<dynamic> route) => false,
            arguments: 0);
      }
    } catch (e) {
      userRepo.logout().then((value) => settingRepo.navigatorKey.currentState
          .pushNamedAndRemoveUntil('/Login', (Route<dynamic> route) => false));
    }

    // new Future.delayed(Duration(microseconds: 200), () {
    //   Navigator.of(context).pushNamedAndRemoveUntil('/Login', (Route<dynamic> route) => false);
    // });
  }
}
