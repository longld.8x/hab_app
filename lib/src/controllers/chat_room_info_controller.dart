import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/chat/messageDto.dart';
import 'package:hab_app/src/models/chat/roomDataDto.dart';
import 'package:hab_app/src/models/chat/userChatDto.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/repositories/chat_repository.dart' as chatRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class ChatRoomInfoController extends ControllerMVC {
  ChatRoomInfoController();

  /// ====================================================================================================================================

  GlobalKey<ScaffoldState> scaffoldKeyChatRoomInfo;
  bool isLoadingInfo = false;
  bool isLoadingFile = true;
  bool isLoadingMembers = true;
  bool isRoomGroup = false;
  User user;
  int roomId;
  RoomDataDto roomDataDto;
  List<MessageDto> dataFile = <MessageDto>[];
  List<UserChatDto> dataMembers = <UserChatDto>[];

  void pageInfoState(int rId) {
    roomId = rId;
    this.scaffoldKeyChatRoomInfo = new GlobalKey<ScaffoldState>();
    // user
    user = userRepo.currentUser.value;
    //
    getRoomInfoData(rId);
  }

  void getRoomInfoData(int roomId) async {
    isLoadingInfo = true;
    Helper.showLoaderOverlay();
    getFile(roomId, 1);
    return chatRepo.getRoom(roomId).then((result) {
      if (result != null) {
        if (result.user1 != null && result.user2 != null) {
          setState(() {
            roomDataDto = result;
            dataMembers.add(result.user1);
            dataMembers.add(result.user2);
            isRoomGroup = false;
          });
        } else {
          setState(() {
            roomDataDto = result;
            isRoomGroup = true;
          });
        }
      }
    }).whenComplete(() {
      isLoadingInfo = false;
      Helper.hideLoaderOverlay();
    });
  }

  void getFile(int roomId, int page) {
    isLoadingFile = true;
    chatRepo.getFileMessages(roomId, page).then((result) {
      if (result != null) {
        setState(() {
          dataFile.addAll(result.items);
        });
      }
    }).whenComplete(() {
      setState(() => isLoadingFile = false);
    });
  }

  void getMembers(int roomId, int page) {
    isLoadingMembers = true;
    chatRepo.getMembers(roomId, page).then((result) {
      if (result != null) {
        setState(() {
          dataMembers.addAll(result.items);
        });
      }
    }).whenComplete(() {
      setState(() => isLoadingMembers = false);
      if (isLoadingInfo == false && isLoadingMembers == false) Helper.hideLoaderOverlay();
    });
  }

  /// ============ member
  GlobalKey<ScaffoldState> scaffoldKeyInfoMembers;
  int currentPage = 0;
  int itemPage = 20;

  void pageInfoMembersState(RoomDataDto dto) {
    this.scaffoldKeyInfoMembers = new GlobalKey<ScaffoldState>();
    // user
    user = userRepo.currentUser.value;
    getMembers(dto.room.id, 1);
  }

  /// ============ details medias

  List<MessageDto> dataImgMessages = <MessageDto>[];
  List<MessageDto> dataFileMessages = <MessageDto>[];
  bool loadingDataImgMessages = false;
  bool loadingDataFileMessages = false;
  GlobalKey<ScaffoldState> scaffoldKeyInfoMedias;

  void pageInfoMediasState(RoomDataDto dto) {
    this.scaffoldKeyInfoMedias = new GlobalKey<ScaffoldState>();
    // user
    user = userRepo.currentUser.value;
    getMessagesImage(dto.room.id, 1);
    getMessagesFile(dto.room.id, 1);
  }

  void getMessagesImage(int roomId, int page) {
    setState(() => loadingDataImgMessages = true);
    chatRepo.getMessages(roomId, page, type: 'IMAGE').then((result) {
      if (result != null) {
        setState(() {
          dataImgMessages.addAll(result.items);
        });
      }
    }).whenComplete(() {
      setState(() => loadingDataImgMessages = false);
    });
  }

  void getMessagesFile(int roomId, int page) {
    setState(() => loadingDataFileMessages = true);
    chatRepo.getMessages(roomId, page, type: 'FILE').then((result) {
      if (result != null) {
        setState(() {
          dataFileMessages.addAll(result.items);
        });
      }
    }).whenComplete(() {
      setState(() => loadingDataFileMessages = false);
    });
  }
}
