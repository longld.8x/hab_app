import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/accountClassDto.dart';
import 'package:hab_app/src/models/myClassDetailDto.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/sessionResultDto.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/models/zoomDto.dart';
import 'package:hab_app/src/repositories/activities_repository.dart' as activitiesRepo;
import 'package:hab_app/src/repositories/class_room_repository.dart' as classRepo;
import 'package:hab_app/src/repositories/socket_repository.dart' as socketRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class MyClassController extends ControllerMVC {
  MyClassController() {}
  List<MyClassDetailDto> dataMyClass = [];

  /// home page
  void homeWidgetState() {
    isLoading = true;
    classRepo.getMyClass(0, 3).then((result) {
      setState(() => dataMyClass = result.items);
    }).whenComplete(() => isLoading = false);
  }

  /// index - danh sách lớp học của tôi
  int serverTotalCount = 0;
  int currentPage = -1;
  int itemPage = 15;
  bool isLoading = false;

  /// index - danh sách lớp học của tôi
  GlobalKey<ScaffoldState> scaffoldKey;
  void pageState() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    loadData(0);
  }

  void loadData(int page) {
    if (currentPage == page) {
      return;
    }
    Helper.showLoaderOverlay();
    isLoading = true;
    if (page == 0) dataMyClass = [];
    classRepo.getMyClass(page, 15 * 15).then((result) {
      setState(() {
        dataMyClass.addAll(result.items);
        serverTotalCount = result.totalCount;
        currentPage = page;
      });
    }).whenComplete(() {
      Helper.hideLoaderOverlay();
      isLoading = false;
    });
  }

  /// chi tiết  lớp học
  GlobalKey<ScaffoldState> scaffoldKeyDetail;
  MyClassDetailDto dataDetail;
  SessionResultDto nextSession;
  User user;
  ZoomDto zoomDto;
  void pageDetailState(int classId) {
    if (classId <= 0) return;
    Helper.showLoaderOverlay();
    user = userRepo.currentUser.value;
    isLoading = true;
    this.scaffoldKeyDetail = new GlobalKey<ScaffoldState>();
    classRepo.getMyClassDetail(classId).then((result) {
      if (result.success) {
        setState(() {
          dataDetail = (result.results);
        });
        setNextSessionResult(dataDetail.nextSession);
        initNoti(classId, dataDetail.chatRoomId);
        print("nextSessionnssion $nextSession");
        // socket group
        if (nextSession != null && nextSession.classSessionId != null && nextSession.classSessionId > 0) {
          socketJoinGroup(nextSession.classSessionId);
          socketListener(nextSession.classSessionId);
        }
      }
    }).whenComplete(() {
      Helper.hideLoaderOverlay();
      setState(() => isLoading = false);
    });
    initStateHlv(classId);

    //firebaseRepo.firebaseEvent.addListener(firebaseListener);

    Future.delayed(Duration(microseconds: 500), () {
      getSession(classId, 0, 9999, isLoading: false);
    });
    Future.delayed(Duration(microseconds: 1000), () {
      getMembers(classId, 0, 9999, isLoading: false);
    });
  }

  /// dispose
  void onExitSocket() {
    if (nextSession != null && nextSession.classSessionId != null && nextSession.classSessionId > 0) {
      socketRepo.exitGroup('${nextSession.classSessionId.toString()}');
      for (var x in socketListeners) x.cancel();
    }
  }

  /// socket Listeners socketListeners
  List<StreamSubscription> socketListeners = <StreamSubscription>[];
  void socketJoinGroup(int classSessionId) async {
    await socketRepo.joinGroup('join_class_session', classSessionId.toString());
  }

  void socketListener(classSessionId) {
    socketListeners.add(socketRepo.onServerEvent((data) {
      var _event = data[0]['event'];
      if (_event == 'ClassSessionStarted' || _event == 'ClassSessionEnded') getNextSessionDetai(classSessionId).then((result) => setNextSessionResult(result));
    }));
  }

  /// firebase Listener firebaseListener
  // void firebaseListener() {
  //   var _type = firebaseRepo.firebaseEvent.value.event;
  //   if (_type == 'JoinZoom' && nextSession != null) {
  //     Helper.showLoaderOverlay();
  //     classRepo.getMyClassSessionDetai(nextSession.classSessionId).then((result) {
  //       if (result != null) {
  //         setState(() => nextSession = result);
  //         if (nextSession.linkRoom != null && nextSession.linkRoom.isNotEmpty && nextSession.linkRoom.indexOf('|') > -1) {
  //           var zoomOption = (nextSession.linkRoom.toString()).split('|');
  //           zoomDto = ZoomDto(
  //             userId: '${user.fullName} - ${user.accountCode}',
  //             meetingId: zoomOption[0].toString(),
  //             meetingPassword: zoomOption[1].toString(),
  //           );
  //         }
  //       } else {
  //         setState(() => nextSession = null);
  //       }
  //     }).whenComplete(() {
  //       Helper.hideLoaderOverlay();
  //       setState(() => isLoading = false);
  //     });
  //   }
  //   firebaseRepo.showNotification(firebaseRepo.firebaseEvent.value.data);
  // }

  void setNextSessionResult(SessionResultDto dto) {
    if (dto != null) {
      setState(() {
        nextSession = dto;
        if (dto != null && dto.sessionStatus == 1 && dto.sessionType == 1 && dto.linkRoom != null && dto.linkRoom.isNotEmpty && nextSession.linkRoom.indexOf('|') > -1) {
          var zoomOption = (dto.linkRoom.toString()).split('|');
          zoomDto = ZoomDto(
            userId: '${user.fullName} - ${user.accountCode}',
            meetingId: zoomOption[0].toString(),
            meetingPassword: zoomOption[1].toString(),
            zak: zoomOption.length >= 3 ? zoomOption[3].toString() : "",
          );
        } else {
          zoomDto = null;
        }
      });
    } else {
      setState(() => nextSession = null);
    }
  }

  getNextSessionDetai(int _classSessionId) {
    return classRepo.getMyClassSessionDetai(_classSessionId);
  }

  void refreshDetailPage(int classId) {
    print("RefreshIndicator(child:");
    pageDetailState(classId);
  }

  /// thong bao
  int myClassNotiTotal = 10;
  void initNoti(int classId, int roomId) {
    if (roomId == 0) setState(() => myClassNotiTotal = 0);
    // classRepo.getMyClassNotiTotal(classId).then((result) {
    //   if (result.success) setState(() => myClassNotiTotal = result.results as int);
    // });
  }

  /// thong tin bai hoc
  bool isLoadingHlv = false;
  List<AccountClassDto> dataHlv;
  void initStateHlv(int classId) {
    if (dataHlv == null) getHLV(classId, 0, 9999);
  }

  void getHLV(int classId, int page, int itemInPage) {
    if (classId <= 0) return;
    Helper.showLoaderOverlay();
    isLoadingHlv = true;
    classRepo.getMyClassHLV(classId, page, itemInPage).then((result) {
      setState(() => dataHlv = result.items);
    }).whenComplete(() {
      setState(() => isLoadingHlv = false);
    });
  }

  /// danh sách bai học
  List<SessionResultDto> dataSession;
  bool isLoadingSession = false;
  void initStateSession(int classId) {
    if (dataSession == null) getSession(classId, 0, 9999);
  }

  void getSession(int classId, int page, int itemInPage, {bool isLoading = true}) {
    if (classId <= 0) return;
    if (isLoading) Helper.showLoaderOverlay();
    isLoadingSession = true;
    classRepo.getMyClassSession(classId, page, itemInPage).then((result) {
      var _data = result.items ?? [];
      if (_data.length > 1) _data.sort((a, b) => a.startDate.compareTo(b.startDate));
      setState(() => dataSession = _data);
    }).whenComplete(() {
      if (isLoading) Helper.hideLoaderOverlay();
      setState(() => isLoadingSession = false);
    });
  }

  /// danh sách học viên
  List<AccountClassDto> dataMembers;
  bool isLoadingMembers = false;
  void initStateMembers(int classId) {
    if (dataMembers == null) getMembers(classId, 0, 9999);
  }

  void getMembers(int classId, int page, int itemInPage, {bool isLoading = true}) {
    if (classId <= 0) return;
    if (isLoading) Helper.showLoaderOverlay();
    isLoadingMembers = true;
    classRepo.getMyClassMembers(classId, page, itemInPage).then((result) {
      setState(() => dataMembers = result.items);
    }).whenComplete(() {
      if (isLoading) Helper.hideLoaderOverlay();
      setState(() => isLoadingMembers = false);
    });
  }

  /// hoc offline
  checkInOffline() {
    if (nextSession == null) return ResponseMessage.error(message: "Không xác định thông tin buổi học!");
    print(nextSession.classSessionId);
    Helper.showLoaderOverlay();
    return activitiesRepo.checkInOffline(nextSession.classSessionId.toString()).whenComplete(() {
      Helper.hideLoaderOverlay();
    });
  }
}
