import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/caloInfoDto.dart';
import 'package:hab_app/src/models/homeSettingsModel.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/repositories/activities_repository.dart' as actRepo;
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class HomeController extends ControllerMVC {
  HomeController() {}

  GlobalKey<ScaffoldState> scaffoldKeyMain;
  void mainState() {
    this.scaffoldKeyMain = new GlobalKey<ScaffoldState>();
  }

  GlobalKey<FormState> formKey;
  GlobalKey<ScaffoldState> scaffoldKey;

  GlobalKey<ScaffoldState> scaffoldKeySettings;
  CaloInfoDto caloInfo;

  void pageState() {
    this.formKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void pageSettingState() {
    this.scaffoldKeySettings = new GlobalKey<ScaffoldState>();
    // setState(() => count = 0);
  }

  void saveSetting(List<HomeSettingsModel> data) {
    settingRepo.setHomeSettings(data);
  }

  Future<ResponseMessage> getMealType() {
    Helper.showLoaderOverlay();
    return actRepo.getMealType().whenComplete(() {
      Helper.hideLoaderOverlay();
    });
  }

  bool loadingCalo = false;
  getCalo() {
    Helper.showLoaderOverlay();
    loadingCalo = true;
    actRepo.getCaloInfo().then((result) {
      if (result.success) {
        setState(() {
          caloInfo = CaloInfoDto.fromJSON(result.results);
        });

        print("caloInfo ${caloInfo.toString()}");
      }
    }).whenComplete(() {
      loadingCalo = false;
      Helper.hideLoaderOverlay();
    });
  }
}
