import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/commentDto.dart';
import 'package:hab_app/src/models/myClassDetailDto.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/repositories/class_room_repository.dart' as classRepo;
import 'package:hab_app/src/repositories/comment_repository.dart' as cmRepo;
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class F1Controller extends ControllerMVC {
  F1Controller() {}

  List<User> dataList = <User>[];
  int serverTotalCount = 0;
  int currentPage = -1;
  int itemPage = 15;
  bool isLoading = false;
  String currentKeyword = "";

  /// cộng sự của tôi - search
  GlobalKey<ScaffoldState> scaffoldKeyF1Search;
  List<String> keywords;
  void f1SearchState() {
    this.scaffoldKeyF1Search = new GlobalKey<ScaffoldState>();
    //controller = new StreamController<List<User>>.broadcast();
  }

  void getSearchF1Keyword() {
    settingRepo.getSearchF1Keyword().then((value) {
      if (value == null) return;
      if (value.length <= 15) {
        setState(() => keywords = value);
      } else {
        setState(() => keywords = value.sublist(0, 15));
      }
    });
  }

  void addSearchF1Keyword(String v) {
    if (v == null || v.isEmpty) return;
    settingRepo.getSearchF1Keyword().then((value) {
      if (value == null) value = [];
      value.insert(0, v);
      settingRepo.setSearchF1Keyword(value);
    });
  }

  void removeSearchF1Keyword(String v) {
    //if (v == null || v.isEmpty) return;
    settingRepo.getSearchF1Keyword().then((value) {
      if (value == null) return;
      value.remove(v);
      settingRepo.setSearchF1Keyword(value).then((value) {
        getSearchF1Keyword();
      });
    });
  }

  /// cộng sự của tôi
  GlobalKey<ScaffoldState> scaffoldKey;
  void pageState() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    //controller = new StreamController<List<User>>.broadcast();
    loadF1Data(0, 0, "");
  }

  /// search - get data f1
  void loadF1Data(int userId, int page, String keyword) {
    if (currentPage == page && currentKeyword == keyword) {
      //controller.sink.add(dataList);
      return;
    }
    if (userId == 0) userId = userRepo.currentUser.value.id;
    Helper.showLoaderOverlay();
    isLoading = true;
    if (page == 0) dataList = [];
    userRepo.getNetworks(userId, keyword, page, itemPage).then((PageResult<User> result) {
      if (result != null) {
        setState(() {
          dataList.addAll(result.items);
          serverTotalCount = result.totalCount;
          currentPage = page;
          currentKeyword = keyword;
        });
        //controller.sink.add(dataList);
      }
    }).whenComplete(() {
      Helper.hideLoaderOverlay();
      isLoading = false;
    });
  }

  CommentDto f1Note = null;
  List<MyClassDetailDto> f1ViewClass = [];
  // view f1 - user child
  GlobalKey<ScaffoldState> scaffoldKeyUserView;
  void f1ChildState(int userViewId) {
    Helper.showLoaderOverlay();
    this.scaffoldKeyUserView = new GlobalKey<ScaffoldState>();
    loadF1Data(userViewId, 0, "");
    loadNote(userViewId);
    loadClassRoom(userViewId);
  }

  // ghi chú - công sự của tôi
  GlobalKey<ScaffoldState> scaffoldKeyF1Note;
  void f1NoteState() {
    this.scaffoldKeyF1Note = new GlobalKey<ScaffoldState>();
  }

  ///  -- addNoteComment  post
  Future<ResponseMessage> addNoteComment(String content, int userViewId, String id) async {
    if ((content.isEmpty || content.length == 0)) {
      return ResponseMessage.error(message: 'Vui lòng nhập nội dung.');
    }
    Helper.showLoaderOverlay();
    if (id != null && id.isNotEmpty)
      return cmRepo.addNoteComment(content, userViewId).whenComplete(() => Helper.hideLoaderOverlay());
    else
      return cmRepo.editNoteComment(id, content, userViewId).whenComplete(() => Helper.hideLoaderOverlay());
  }

  Future<ResponseMessage> loadNote(int userViewId) async {
    Helper.showLoaderOverlay();
    return cmRepo.getNoteComments(userViewId).then((rs) {
      if (rs.items != null && rs.items.length > 0) {
        setState(() => f1Note = rs.items[0]);
      }
    }).whenComplete(() => Helper.hideLoaderOverlay());
  }

  Future<ResponseMessage> loadClassRoom(userViewId) async {
    Helper.showLoaderOverlay();
    f1ViewClass = [];
    return classRepo.getMyClass(0, 15 * 15, userId: userViewId).then((result) {
      setState(() {
        f1ViewClass.addAll(result.items);
      });
    }).whenComplete(() => Helper.hideLoaderOverlay());
  }
}
