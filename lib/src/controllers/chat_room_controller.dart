import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/chat/messageDto.dart';
import 'package:hab_app/src/models/chat/roomDataDto.dart';
import 'package:hab_app/src/models/chat/roomDto.dart';
import 'package:hab_app/src/models/chat/userChatDto.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/repositories/chat_repository.dart' as chatRepo;
import 'package:hab_app/src/repositories/file_repository.dart' as fileRepo;
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:hab_app/src/repositories/socket_repository.dart' as socketRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ChatRoomController extends ControllerMVC {
  ChatRoomController();

  /// chat /// =================================================================================================== chat

  GlobalKey<ScaffoldState> scaffoldKeyChatRoom;
  User user;
  int roomId;
  bool isRoomGroup = false;
  bool isTyping = false;
  RoomDto roomChat;
  RoomDataDto roomData;
  List<StreamSubscription> socketListeners = <StreamSubscription>[];

  List<MessageDto> dataMessages = <MessageDto>[];
  List<UserChatDto> dataMembers = <UserChatDto>[];
  int serverMessageTotalPage = 0;
  int currentMessagePage = 0;
  bool isLoadingMembers = false;
  bool isLoadingMessage = false;
  bool isConnect = true;
  void pageChatRoomState(int rId) {
    if (rId == null || rId <= 0) Navigator.of(settingRepo.navigatorKey.currentContext).pop();
    socketRepo.connect();
    roomId = rId;
    this.scaffoldKeyChatRoom = new GlobalKey<ScaffoldState>();
    // tao rooom
    initRoom(roomId);
    // user
    user = userRepo.currentUser.value;
    // socketInit
    socketListener();
  }

  void socketListener() {
    socketListeners.add(socketRepo.onConnect(() => setState(() => isConnect = true)));
    socketListeners.add(socketRepo.onDisconnect(() => setState(() => isConnect = false)));
    socketListeners.add(socketRepo.onJoinRoom((UserChatDto u, int rId) {
      print("ChatRoomController socketRepo.onJoinRoom");
      if (u.ref_user_id == user.id) getRoomData(rId);
    }));
    socketListeners.add(socketRepo.onTyping((UserChatDto u, int rId) {
      print("ChatRoomController socketRepo.onTyping");
      if (u.ref_user_id != user.id) setState(() => isTyping = true);
    }));
    socketListeners.add(socketRepo.onEndTyping((UserChatDto u, int rId) {
      print("ChatRoomController socketRepo.onEndTyping");
      if (u.ref_user_id != user.id) setState(() => isTyping = false);
    }));
    socketListeners.add(socketRepo.onMessage((MessageDto mes) {
      print("ChatRoomController socketRepo.onMessage");
      if (mes.room_id == roomChat.id && mes.user.ref_user_id != user.id) {
        setState(() => dataMessages.add(mes));
      }
    }));
  }

  void initRoom(int roomId) async {
    isLoadingMessage = true;
    isLoadingMembers = true;
    Helper.showLoaderOverlay();
    await onJoinRoom(roomId);
  }

  void getRoomData(int roomId) async {
    return chatRepo.getRoom(roomId).then((result) {
      if (result != null) {
        setState(() {
          roomData = result;
          roomChat = result.room;
        });
        getMessages(result.room.id, 1, false);
        if (result.user1 != null && result.user2 != null) {
          setState(() {
            isLoadingMembers = false;
            dataMembers.add(result.user1);
            dataMembers.add(result.user2);
            isRoomGroup = false;
          });
          chatRepo.addFriend(result.user1.ref_user_id == user.id ? result.user2.ref_user_id : result.user1.ref_user_id);
        } else {
          setState(() {
            isRoomGroup = true;
          });
          getMembers(result.room.id);
        }
        seenMessages(result.room.id);
      }
    });
  }

  void getMembers(int roomId) {
    isLoadingMembers = true;
    chatRepo.getMembers(roomId, 1).then((result) {
      if (result != null) {
        setState(() => dataMembers = result.items.toList());
      }
    }).whenComplete(() {
      setState(() => isLoadingMembers = false);
      if (isLoadingMessage == false && isLoadingMembers == false) Helper.hideLoaderOverlay();
    });
  }

  void getMessages(int roomId, int page, bool loading) {
    isLoadingMessage = true;
    if (page == 1) dataMessages = [];
    //if (loading) Helper.showLoaderOverlay();
    chatRepo.getMessages(roomId, page).then((result) {
      if (result != null) {
        setState(() {
          dataMessages.insertAll(0, result.items.toList());
          serverMessageTotalPage = result.totalPage;
          currentMessagePage = page;
        });
      }
    }).whenComplete(() {
      setState(() => isLoadingMessage = false);
      //if (loading) Helper.hideLoaderOverlay();
      if (isLoadingMessage == false && isLoadingMembers == false) Helper.hideLoaderOverlay();
    });
  }

  /// socket
  void createRoom(int userId) async {
    setState(() => roomId = 0);
    await socketRepo.createRoom(userId);
  }

  void seenMessages(int roomId) async {
    await socketRepo.seenMessages(roomId);
  }

  void onJoinRoom(int rId) async {
    setState(() => roomId = rId);
    await socketRepo.joinRoom(roomId);
  }

  Future<bool> onSendFiles(List<File> images) async {
    if (images != null && images.length > 0) {
      var message_code = "${user.id}_${new DateFormat('yyyyMMddHHmmss').format(DateTime.now())}";
      var dto = new MessageDto(
        room_id: roomChat.id,
        message: "Đang gửi",
        user: UserChatDto(
          ref_user_id: user.id,
        ),
        createdAt: DateTime.now(),
        message_code: message_code,
        message_type: "TMP",
        seen: false,
      );
      setState(() {
        dataMessages.add(dto);
      });
      return await fileRepo.uploadFiles(images).then((files) {
        if (files.length > 0) {
          var dto = new MessageDto(
            room_id: roomChat.id,
            message: files.join('|'),
            user: UserChatDto(
              ref_user_id: user.id,
            ),
            createdAt: DateTime.now(),
            message_code: "${user.id}_${new DateFormat('yyyyMMddHHmmss').format(DateTime.now())}",
            message_type: 'IMAGE',
            seen: false,
          );
          setState(() {
            dataMessages.removeWhere((x) => x.message_code == message_code && x.message_type == 'TMP');
            dataMessages.add(dto);
          });
          socketRepo.sendMessage(dto);
        }
      });
    }
    return false;
  }

  void onSendText(String e) {
    if (e == null || e.isEmpty) return;
    var dto = new MessageDto(
      room_id: roomChat.id,
      message: e,
      user: UserChatDto(
        ref_user_id: user.id,
      ),
      createdAt: DateTime.now(),
      message_code: "${user.id}_${new DateFormat('yyyyMMddHHmmss').format(DateTime.now())}",
      message_type: 'TEXT',
      seen: false,
    );
    setState(() {
      dataMessages.add(dto);
    });
    socketRepo.sendMessage(dto);
  }

  void onSendMessage(String e, List<File> images) {
    onSendFiles(images).then((x) => onSendText(e));
  }

  void onTyping() => socketRepo.typing(roomId);
  void onEndTyping() => socketRepo.endTyping(roomId);

  void onSeen(String e, String type) {
    var dto = new MessageDto(
      room_id: roomChat.id,
      message: e,
      user: UserChatDto(
        ref_user_id: user.id,
      ),
      createdAt: DateTime.now(),
      message_code: "${user.id}_${new DateFormat('yyyyMMddHHmmss').format(DateTime.now())}",
      message_type: type,
      seen: false,
    );
    setState(() {
      dataMessages.add(dto);
    });
    socketRepo.sendMessage(dto);
  }

  void onExitRoom() {
    if (roomId > 0) socketRepo.exitRoom(roomId);
    for (var x in socketListeners) x.cancel();
    // socketRepo.disconnect();
  }

  /// ====================================================================================================================================

  GlobalKey<ScaffoldState> scaffoldKeyChatRoomInfo;
  bool isLoadingInfo = false;

  List<MessageDto> dataMessagesFile = <MessageDto>[];
  int totalMessageFile = 0;

  void pageChatRoomInfoState(int rId) {
    roomId = rId;
    this.scaffoldKeyChatRoomInfo = new GlobalKey<ScaffoldState>();
    // user
    user = userRepo.currentUser.value;
    //
    getRoomInfoData(rId);
  }

  void getRoomInfoData(int roomId) async {
    isLoadingInfo = true;
    Helper.showLoaderOverlay();
    return chatRepo.getRoom(roomId).then((result) {
      if (result != null) {
        setState(() => roomChat = result.room);
        if (result.user1 != null && result.user2 != null) {
          setState(() {
            isLoadingMembers = false;
            dataMembers.add(result.user1);
            dataMembers.add(result.user2);
          });
        } else {
          getMembers(result.room.id);
        }
        // chatRepo.getMessages(roomId, 1, type: "IMAGE").then((resultFile) {
        //   setState(() {
        //     dataMessagesFile.addAll(resultFile.items);
        //     totalMessageFile = resultFile.totalCount;
        //   });
        // });
      }
    }).whenComplete(() {
      isLoadingInfo = false;
      Helper.hideLoaderOverlay();
    });
  }
}
