import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/health_indexes_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/healthIndex.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MyHealthIndexesWidget extends StatefulWidget {
  @override
  _MyHealthIndexesWidgetState createState() => _MyHealthIndexesWidgetState();
}

class _MyHealthIndexesWidgetState extends StateMVC<MyHealthIndexesWidget> with SingleTickerProviderStateMixin {
  HealthIndexesController _con;
  ScrollController _scrollController;
  _MyHealthIndexesWidgetState() : super(HealthIndexesController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.pageState();
    initScroll();
  }

  initScroll() {
    _scrollController = ScrollController();
    _scrollController.addListener(() async {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset && (_con.pageTotalCount > ((_con.pageCurrent + 1) * _con.pageItem))) {
        _con.loadData(_con.pageCurrent + 1);
      }
    });
  }

  reloaData() {
    print("reload data");
    _con.loadData(0);
  }

  @override
  Widget build(BuildContext context) {
    Widget _textVal(String label, String value, String cm) {
      return RichText(
        text: TextSpan(
          text: label,
          style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.w400, fontSize: 14, color: config.Colors.customeColor('#171051')),
          children: [
            WidgetSpan(
              child: Container(
                padding: EdgeInsets.only(left: 2, right: 1),
                child: Text(
                  value,
                  style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.w500, fontSize: 14, color: config.Colors.customeColor('#171051')),
                ),
              ),
            ),
            WidgetSpan(
              child: Container(
                padding: EdgeInsets.only(left: 0, right: 4),
                child: Text(
                  cm,
                  style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.w400, fontSize: 14, color: config.Colors.customeColor('#171051')),
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget _itemView(int index, HealthIndexDto item) {
      var width = config.App.appWidth(100) - 32;
      var _boxImg = width * 0.15;
      var _boxWidth = width * 0.85;
      return Slidable(
        actionPane: SlidableDrawerActionPane(),
        actionExtentRatio: 0.15,
        child: Container(
          color: config.Colors.backgroundColor(),
          padding: EdgeInsets.only(top: 5, left: 16, right: 16),
          child: ColumnStart(
            children: [
              index == 0
                  ? Container(
                      padding: EdgeInsets.only(bottom: 8),
                      child: Divider(
                        height: 1,
                        color: config.Colors.customeColor("#E0E9F8"),
                      ),
                    )
                  : Container(),
              Container(
                padding: EdgeInsets.only(bottom: 5),
                child: RowWidget(
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 0, 6, 0),
                    child: Icon(Icons.circle, size: 9, color: index == 0 ? config.Colors.primaryColor() : config.Colors.customeColor("#41CD2A")),
                  ),
                  LineText(
                    "Chỉ số cơ thể ngày: ${new DateFormat('dd/MM/yyyy').format(item.createdDate)}",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: config.Colors.customeColor('#171051')),
                    maxLines: 1,
                  ),
                  flex: 0,
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 5),
                child: Row(
                  children: [
                    _textVal("Chiều cao: ", item.height.toString(), "cm,"),
                    _textVal("Cân nặng:", item.weight.toString(), "kg"),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 5),
                child: Row(
                  children: [
                    _textVal("Vòng 1: ", item.bust.toString(), "cm,"),
                    _textVal("Vòng 2:", item.waist.toString(), "cm,"),
                    _textVal("Vòng 3:", item.hip.toString(), "cm"),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 10),
                child: SubText(
                  new DateFormat('HH:mm dd/MM/yyyy').format(item.createdDate),
                  style: Theme.of(context).textTheme.subtitle1.copyWith(
                        color: config.Colors.customeColor("#979797"),
                        fontWeight: FontWeight.w400,
                      ),
                ),
              ),
              Divider(
                height: 1,
                color: config.Colors.customeColor("#E0E9F8"),
              )
            ],
          ),
        ),
        actions: <Widget>[],
        secondaryActions: <Widget>[
          index == 0
              ? new IconSlideAction(
                  color: config.Colors.primaryColor(),
                  iconWidget: Container(
                    child: SvgPicture.asset('assets/icon/edit.svg'),
                    alignment: Alignment.center,
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/MyHealth/Edit', arguments: RouteArgument(code: item.id, param: item, event: reloaData));
                  },
                )
              : new IconSlideAction(
                  color: config.Colors.customeColor('#f7e848'),
                  iconWidget: Container(
                    child: Icon(
                      Icons.warning_sharp,
                      color: config.Colors.customeColor('#ffffff'),
                    ),
                    alignment: Alignment.center,
                  ),
                  onTap: () {
                    Helper.infoMessenge("Không được phép chỉnh sửa");
                  },
                ),
          // new IconSlideAction(
          //   color: config.Colors.customeColor("#FF0B27"),
          //   iconWidget: Container(
          //     child: SvgPicture.asset('assets/icon/trash.svg'),
          //     alignment: Alignment.center,
          //   ),
          //   onTap: () {
          //     print("delête");
          //   },
          // ),
        ],
      );
    }

    Widget _bodyView() {
      if (_con.isLoading) return LoadContent(warp: true);
      if (_con.dataList == null || _con.dataList.length == 0)
        return NoContent(
          warp: true,
          text: "Không có dữ liệu",
        );
      return Column(
        children: [
          Container(
            width: config.App.appWidth(100),
            padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
            decoration: BoxDecoration(
              border: Border(top: BorderSide(width: 5, color: config.Colors.customeColor("#E0E9F8"))),
            ),
            child: Container(
              child: AccentText(
                "Lưu ý: Vuốt sang trái để chỉnh sửa chỉ số đã nhập, chỉ được phép chỉnh sửa chỉ số gần nhất.",
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: new ListView.builder(
                shrinkWrap: true,
                controller: _scrollController,
                scrollDirection: Axis.vertical,
                itemCount: _con.dataList.length,
                itemBuilder: (BuildContext context, int i) {
                  var _item = _con.dataList[i];
                  return _itemView(i, _item);
                },
              ),
            ),
          ),
        ],
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKey,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Chỉ số cơ thể"),
        actions: [
          IconButton(
            icon: Icon(Icons.add, size: 20, color: config.Colors.primaryColor()),
            padding: EdgeInsets.only(right: 16),
            onPressed: () {
              Navigator.of(context).pushNamed('/MyHealth/Edit', arguments: RouteArgument(event: reloaData));
            },
          ),
        ],
      ),
      main: _bodyView(),
    );
  }
}
