import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/health_indexes_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/healthIndex.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class HealthIndexEditWidget extends StatefulWidget {
  RouteArgument routeArgument;
  HealthIndexEditWidget({Key key, this.routeArgument}) : super();
  @override
  _HealthIndexEditWidgetState createState() => new _HealthIndexEditWidgetState();
}

class _HealthIndexEditWidgetState extends StateMVC<HealthIndexEditWidget> {
  HealthIndexesController _con;
  _HealthIndexEditWidgetState() : super(HealthIndexesController()) {
    _con = controller;
  }

  bool _activeBtn = false;
  String _action = "add";

  @override
  void initState() {
    super.initState();
    _con.editState(widget.routeArgument.code);
    _con.editModel = (widget.routeArgument.param);
    _action = widget.routeArgument != null && widget.routeArgument.code != null && widget.routeArgument.code.isNotEmpty ? 'edit' : 'add';
    //_con.getEditData(widget.routeArgument.code).then((_) {
    initCtrl();
    //});
  }

  @override
  void dispose() {
    super.dispose();
  }

  TextEditingController _ctrlHeight;
  TextEditingController _ctrlWeight;
  TextEditingController _ctrlWaist;
  TextEditingController _ctrlBust;
  TextEditingController _ctrlHip;
  initCtrl() {
    print("long initCtrl");
    _ctrlHeight = fieldCtrl(_con.editModel?.height);
    _ctrlWeight = fieldCtrl(_con.editModel?.weight);
    _ctrlWaist = fieldCtrl(_con.editModel?.waist);
    _ctrlBust = fieldCtrl(_con.editModel?.bust);
    _ctrlHip = fieldCtrl(_con.editModel?.hip);
  }

  fieldCtrl(double val) {
    return new TextEditingController(text: (val == null || val == 0) ? "" : val.toString());
  }

  getValue(String val) {
    var _v = double.parse(val == null || val.isEmpty ? '0' : val);
    return _v;
  }

  onActiveBtn() {
    var _v = _ctrlHeight.text.isNotEmpty && _ctrlWeight.text.isNotEmpty && _ctrlWaist.text.isNotEmpty && _ctrlBust.text.isNotEmpty && _ctrlHip.text.isNotEmpty;
    setState(() => _activeBtn = _v);
  }

  fieldChange(String type, String val) {
    onActiveBtn();
  }

  fieldValid(String type, String val) {
    if (val.isEmpty) return "Vui lòng nhập chỉ số.";
    var _v = double.parse(val.isEmpty ? '0' : val);
    if (type == 'kg') {
      if (_v > 1000) return "Chỉ số nhập không chính xác.";
      return null;
    }
    if (type == 'cm') {
      if (_v > 1000) return "Chỉ số nhập không chính xác.";
      return null;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    var _appWidth = config.App.appWidth(100);

    Widget _formWidget() {
      return Form(
        key: _con.formKeyEdit,
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15, bottom: 0, top: 0),
          decoration: BoxDecoration(color: config.Colors.backgroundColor()),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 16),
              InputField(
                controller: _ctrlHeight,
                labelText: "Chiều cao(cm)",
                hintText: "178",
                keyboardType: TextInputType.number,
                maxLength: 6,
                onChanged: (String value) => fieldChange('height', value),
                validator: (String value) => fieldValid('cm', value),
                // onEditingComplete: () {
                //   onActiveBtn();
                // },
              ),
              SizedBox(height: 24),
              InputField(
                controller: _ctrlWeight,
                labelText: "Cân nặng(kg)",
                hintText: '70',
                keyboardType: TextInputType.number,
                maxLength: 6,
                onChanged: (String value) => fieldChange('width', value),
                validator: (String value) => fieldValid('kg', value),
                // onEditingComplete: () {
                //   onActiveBtn();
                // },
              ),
              SizedBox(height: 24),
              InputField(
                controller: _ctrlBust,
                labelText: "Vòng 1(cm)",
                hintText: '90',
                keyboardType: TextInputType.number,
                maxLength: 6,
                onChanged: (String value) => fieldChange('bust', value),
                validator: (String value) => fieldValid('cm', value),
                // onEditingComplete: () {
                //   onActiveBtn();
                // },
              ),
              SizedBox(height: 24),
              InputField(
                controller: _ctrlWaist,
                labelText: "Vòng 2(cm)",
                hintText: '60',
                keyboardType: TextInputType.number,
                maxLength: 6,
                onChanged: (String value) => fieldChange('waist', value),
                validator: (String value) => fieldValid('cm', value),
                // onEditingComplete: () {
                //   onActiveBtn();
                // },
              ),
              SizedBox(height: 24),
              InputField(
                controller: _ctrlHip,
                labelText: "Vòng 3(cm)",
                hintText: '90',
                keyboardType: TextInputType.number,
                maxLength: 6,
                onChanged: (String value) => fieldChange('hipt', value),
                validator: (String value) => fieldValid('cm', value),
                // onEditingComplete: () {
                //   onActiveBtn();
                // },
              ),
              SizedBox(height: 24),
              Button(
                isActive: _activeBtn,
                text: _action == 'add' ? 'Thêm mới' : "Cập nhật",
                onPressed: () {
                  if (_con.formKeyEdit.currentState.validate()) {
                    var _model = new HealthIndexDto(
                      height: getValue(_ctrlHeight.text),
                      weight: getValue(_ctrlWeight.text),
                      waist: getValue(_ctrlWaist.text),
                      bust: getValue(_ctrlBust.text),
                      hip: getValue(_ctrlHip.text),
                    );
                    _con.onUpdateModel(_action, _model).then((ResponseMessage result) {
                      if (result.success) {
                        Navigator.of(context).pop();
                        Helper.successMessenge('${_action == 'add' ? 'Thêm mới' : "Cập nhật"} chỉ số cơ thể thành công');
                        if (widget.routeArgument != null && widget.routeArgument.event != null) widget.routeArgument.event();
                      } else
                        Helper.errorMessenge(result.message);
                    });
                  }
                },
              ),
            ],
          ),
        ),
      );
    }

    Widget _bodyView() {
      return Container(
        width: _appWidth,
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
        ),
        child: _formWidget(),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyEdit,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText(_action == 'add' ? "Thêm mới chỉ số cơ thể" : "Chỉnh sửa chỉ số cơ thể"),
      ),
      body: _bodyView(),
    );
  }
}
