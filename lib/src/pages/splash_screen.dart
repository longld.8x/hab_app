import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/splash_screen_controller.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class SplashScreenWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends StateMVC<SplashScreenWidget> {
  SplashScreenController _con;
  bool progressNext = false;

  SplashScreenState() : super(SplashScreenController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    loadData();
  }

  void loadData() {
    _con.progress.addListener(() async {
      double progress = 0;
      _con.progress.value.values.forEach((_progress) => progress += _progress);
      await Future.delayed(Duration(microseconds: 200));
      if (progress == 100 && progressNext == false) {
        progressNext = true;
        _con.reopenApp();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
        ),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/icon/logo.png',
                width: 150,
              ),
              SizedBox(height: 50),
              Waiting(),
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).accentColor),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
