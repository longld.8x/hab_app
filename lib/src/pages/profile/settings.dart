import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hab_app/src/controllers/settings_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitlePageSimpleWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/alert_dialog.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/setting.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class SettingsWidget extends StatefulWidget {
  @override
  _SettingsWidgetState createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends StateMVC<SettingsWidget> {
  SettingsController _con;
  String _authenType = "";
  Setting _setting = settingRepo.setting.value;
  final LocalAuthentication _auth = LocalAuthentication();
  List<BiometricType> _availableBiometrics;
  bool _canCheckBiometrics = false;
  _SettingsWidgetState() : super(SettingsController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.pageState();
    if (_setting.loginSecurity == "on") {
      _checkBiometrics();
      _getAvailableBiometrics();
    }
  }

  @override
  void dispose() {
    super.dispose();
    _auth.stopAuthentication();
  }

  @override
  Widget build(BuildContext context) {
    /// đăng nhập vân tay
    Widget loginSecurityItemWidget(BiometricType type) {
      var _pName = Helper.securityTypeName(type.toString());
      var _text = "Đăng nhập bằng $_pName";
      var _icon = type == BiometricType.fingerprint.toString()
          ? Icon(
              Icons.fingerprint,
              color: Colors.redAccent,
            )
          : type == BiometricType.face.toString()
              ? SvgPicture.asset(
                  'assets/icon/face_id.svg',
                  height: 20,
                )
              : type == BiometricType.iris.toString()
                  ? SvgPicture.asset(
                      'assets/icon/face_id.svg',
                      height: 20,
                    )
                  : SvgPicture.asset(
                      'assets/icon/face_id.svg',
                      height: 20,
                    );
      return ListTile(
        leading: _icon,
        trailing: CupertinoSwitch(
          activeColor: config.Colors.customeColor("#41CD2A"),
          value: _authenType == type.toString(),
          onChanged: (bool v) async {
            if (v == true) {
              await _authenticateBiometrics(type);
            } else if (v == false) {
              await _cancelAuthenticateBiometrics(type);
            }
            print("onChanged: $v");
          },
        ),
        title: AccentText(_text),
        onTap: () async {
          if (_authenType == type.toString()) {
            await _cancelAuthenticateBiometrics(type);
          } else {
            await _authenticateBiometrics(type);
          }
        },
      );
    }

    /// đăng nhập vân tay
    Widget loginSecurityWidget() {
      if (_setting.loginSecurity != "on") return null;
      List<Widget> lsWidget = <Widget>[];
      if (_canCheckBiometrics && _availableBiometrics != null) {
        if (_availableBiometrics.contains(BiometricType.fingerprint)) {
          return (loginSecurityItemWidget(BiometricType.fingerprint));
        }
        if (_availableBiometrics.contains(BiometricType.face)) {
          return (loginSecurityItemWidget(BiometricType.face));
        }
        if (_availableBiometrics.contains(BiometricType.iris)) {
          return (loginSecurityItemWidget(BiometricType.iris));
        }
      }

      return Container();
    }

    Widget _listWidget() {
      return Container(
        padding: EdgeInsets.only(left: 0, right: 0),
        child: new ListView(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          padding: EdgeInsets.all(0),
          children: [
            loginSecurityWidget(),
          ],
        ),
      );
    }

    Widget _body() {
      return Column(
        children: <Widget>[
          TitlePageSimpleWidget("Cài đặt đăng nhập "),
          SizedBox(height: 80),
          _listWidget(),
        ],
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKey,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText(""),
      ),
      body: Container(child: _body()),
    );
  }

  // cancel authen
  Future<void> _cancelAuthenticateBiometrics(BiometricType type) async {
    var _pName = Helper.securityTypeName(type.toString());
    DialogHLS.confirmDialog(context, "Xác nhận", 'Hủy sử dụng $_pName để đăng nhập app HAB!', () async {
      await _auth.stopAuthentication();
      await settingRepo.setAuthenType(null);
      setState(() => _authenType = "");
      Fluttertoast.showToast(msg: "Hủy sử dụng $_pName để Đăng nhập thành công!");
    });
  }

  Future<void> _authenticateBiometrics(BiometricType type) async {
    print("_authenticateBiometrics ${_setting.loginSecurity}, BiometricType type ${type}");
    if (_setting.loginSecurity != "on") return;
    if (!_canCheckBiometrics) return;
    var _pName = Helper.securityTypeName(type.toString());
    bool authenticated = await _authenticateWithBiometrics('Sử dụng $_pName để Đăng nhập app HAB!', 'Đăng ký dùng $_pName để đăng nhập.');
    var _typeBiometric = authenticated ? BiometricType.fingerprint.toString() : "";
    await settingRepo.setAuthenType(_typeBiometric);
    setState(() => _authenType = _typeBiometric);
    if (authenticated) {
      Fluttertoast.showToast(msg: "Đăng ký sử dụng $_pName để Đăng nhập thành công.");
    }
  }

  Future<bool> _authenticateWithBiometrics(String localizedReason, String signInTitle) async {
    if (!_canCheckBiometrics) return false;
    bool authenticated = false;
    try {
      authenticated = await _auth.authenticateWithBiometrics(
        localizedReason: localizedReason,
        useErrorDialogs: true,
        stickyAuth: true,
        androidAuthStrings: new AndroidAuthMessages(
          //fingerprintHint: "",
          signInTitle: signInTitle,
          cancelButton: "Hủy bỏ",
        ),
      );
    } on PlatformException catch (e) {
      print(e);
    }
    print("mounted $mounted");
    if (!mounted) return false;
    return authenticated;
  }

  Future<void> _checkBiometrics() async {
    if (_setting.loginSecurity != "on") return;
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await _auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }
    print("canCheckBiometrics $canCheckBiometrics");

    if (!mounted) return;
    setState(() {
      _canCheckBiometrics = canCheckBiometrics;
    });
    if (!_canCheckBiometrics) return;
    settingRepo.getAuthenType().then((value) => setState(() => _authenType = value));
  }

  Future<void> _getAvailableBiometrics() async {
    if (_setting.loginSecurity != "on") return;
    List<BiometricType> availableBiometrics;
    try {
      availableBiometrics = await _auth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      print(e);
    }
    print("availableBiometrics $availableBiometrics");
    if (!mounted) return;
    setState(() {
      _availableBiometrics = availableBiometrics;
    });
  }
}
