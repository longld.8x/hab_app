import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/f1_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/ListViewItem.dart';
import 'package:hab_app/src/elements/ReadMore.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/models/target.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/pages/classRoom/classWidget.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class F1UserViewWidget extends StatefulWidget {
  final RouteUserArgument routeArgument;
  F1UserViewWidget({Key key, this.routeArgument}) : super(key: key);
  @override
  _F1UserViewWidgetState createState() => _F1UserViewWidgetState();
}

class _F1UserViewWidgetState extends StateMVC<F1UserViewWidget> {
  F1Controller _con;
  User data;
  _F1UserViewWidgetState() : super(F1Controller()) {
    _con = controller;
  }
  int _f1Level = 0;
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    print("widget.routeArgument.param ${widget.routeArgument.param}");
    data = widget.routeArgument.param as User;
    _f1Level = int.parse(widget.routeArgument.level.toString());
    _scrollController = ScrollController();
    _con.f1ChildState(data.id);
  }

  void changeNote(_) {
    _con.loadNote(data.id);
  }

  @override
  Widget build(BuildContext context) {
    Widget _infoUserView() {
      return RowWidget(
        Container(
          margin: EdgeInsets.only(top: 20, right: 5, left: 30, bottom: 15),
          width: 80,
          height: 80,
          child: UserAvatar(
            data.profilePicture,
            width: 80,
          ),
        ),
        Container(
          width: config.App.appWidth(100) - 80 - 45,
          padding: EdgeInsets.only(top: 0, right: 10, left: 0, bottom: 0),
          alignment: Alignment.centerRight,
          child: ColumnStart(
            children: [
              Container(
                child: IconWidget(
                  Container(
                    child: TitleText(data.fullName),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 5),
                    padding: EdgeInsets.only(top: 4, left: 10, right: 10, bottom: 2),
                    decoration: BoxDecoration(
                      color: config.Colors.primaryColor(),
                      borderRadius: BorderRadius.all(const Radius.circular(4.0)),
                    ),
                    child: SubText(
                      "F" + _f1Level.toString(),
                      style: TextStyle(color: config.Colors.backgroundColor()),
                    ),
                  ),
                  padding: 5,
                ),
                padding: EdgeInsets.only(bottom: 5),
              ),
              data.birthday != null
                  ? Container(
                      child: IconWidget(
                        Icon(Icons.calendar_today, size: 16),
                        SubText(new DateFormat('dd/MM/yyyy').format(data.birthday), style: TextStyle(color: config.Colors.accentColor())),
                      ),
                      padding: EdgeInsets.only(bottom: 3),
                    )
                  : Container(),
              IconWidget(
                Icon(Icons.phone, size: 16),
                SubText(data.phoneNumber, style: TextStyle(color: config.Colors.accentColor())),
              ),
            ],
          ),
        ),
        mainAxisAlignment: MainAxisAlignment.start,
        //flex: 60,
      );
    }

    Widget _noteUserView() {
      if (_con.f1Note != null && _con.f1Note.content != null && _con.f1Note.content.isNotEmpty)
        return Container(
          padding: EdgeInsets.only(top: 0, right: 24, left: 24, bottom: 24),
          child: Container(
            padding: EdgeInsets.all(8),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: config.Colors.inputBgColor(),
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  child: AccentText("Ghi chú: ", style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16)),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: ReadMoreText(
                    _con.f1Note.content,
                    trimLines: 4,
                    colorClickableText: config.Colors.primaryColor(),
                    trimMode: TrimMode.Line,
                    trimCollapsedText: 'Xem thêm',
                    trimExpandedText: 'Đóng',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal, color: config.Colors.hintColor(), fontStyle: FontStyle.italic),
                    moreStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.normal, color: config.Colors.primaryColor()),
                  ),
                ),
              ],
            ),
          ).onTap(() {
            Navigator.of(context).pushNamed('/F1/Note',
                arguments: RouteUserArgument(
                  param: data,
                  content: (_con.f1Note != null && _con.f1Note.content != null && _con.f1Note.content.isNotEmpty) ? _con.f1Note.content : "",
                  id: (_con.f1Note != null && _con.f1Note.content != null && _con.f1Note.content.isNotEmpty) ? _con.f1Note.id : "",
                  event: changeNote,
                ));
          }),
        );
      else
        return Container(
          padding: EdgeInsets.only(top: 0, right: 24, left: 24, bottom: 24),
          alignment: Alignment.center,
          child: Button(
            child: Container(
              padding: EdgeInsets.only(top: 20, bottom: 20),
              child: IconWidget(
                Icon(
                  Icons.add,
                  color: config.Colors.primaryColor(),
                ),
                PrimaryText("Thêm ghi chú"),
                mainAxisAlignment: MainAxisAlignment.center,
              ),
            ),
            onPressed: () {
              Navigator.of(context).pushNamed('/F1/Note', arguments: RouteUserArgument(param: data, event: changeNote));
            },
            color: config.Colors.customeColor("#F7F7F9"),
          ),
        );
    }

    Widget _myClassRoomView() {
      if (_con.f1ViewClass == null || _con.f1ViewClass.length == 0) {
        return Container();
      }
      return Container(
        padding: EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 0),
        alignment: Alignment.center,
        child: ColumnStart(
          children: [
            Container(
              padding: EdgeInsets.all(16),
              child: TitleText("Khoá học đang tham gia"),
              decoration: BoxDecoration(
                color: config.Colors.inputBgColor(),
              ),
              margin: EdgeInsets.only(bottom: 24),
            ),
            Container(
              height: (config.App.appWidth(100)) * 0.6 * (237 / 225), // (237 / 217),
              child: new ListView.builder(
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) => ClassWidget(model: _con.f1ViewClass[index], horizontal: true),
                itemCount: _con.f1ViewClass.length,
              ),
              margin: EdgeInsets.only(bottom: 8),
            ),
          ],
        ),
      );
    }

    var _dataTarget = [
      new Target(
        name: "Lê Đăng Long",
        startDate: DateTime.now(),
        endDate: DateTime.now(),
        totalPoint: 100,
        currentPoint: 26,
      ),
      new Target(
        name: "Lê Đăng Long",
        startDate: DateTime.now(),
        endDate: DateTime.now(),
        totalPoint: 100,
        currentPoint: 26,
      ),
      new Target(
        name: "Lê Đăng Long",
        startDate: DateTime.now(),
        endDate: DateTime.now(),
        totalPoint: 100,
        currentPoint: 26,
      ),
      new Target(
        name: "Lê Đăng Long",
        startDate: DateTime.now(),
        endDate: DateTime.now(),
        totalPoint: 100,
        currentPoint: 26,
      ),
    ];
    Widget _myTargetView() {
      return Container(
        padding: EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 0),
        alignment: Alignment.center,
        child: ColumnStart(
          children: [
            Container(
              padding: EdgeInsets.only(top: 16, bottom: 16, left: 24, right: 24),
              child: TitleText("Mục tiêu cam kết"),
              decoration: BoxDecoration(
                color: config.Colors.inputBgColor(),
              ),
              margin: EdgeInsets.only(bottom: 24),
            ),
            Container(
              child: ListViewItem(
                _dataTarget,
                (Target item, _) {
                  return TargetViewItem(item);
                },
                height: (_dataTarget.length * (80.0 + 16)),
                isNeverScroll: true,
              ),
              margin: EdgeInsets.only(bottom: 8),
            ),
          ],
        ),
      );
    }

    Widget listDataBuilder() {
      if (_con.dataList == null || _con.dataList.length == 0)
        return Container(
          alignment: Alignment.center,
          padding: EdgeInsets.fromLTRB(0, 40, 0, 40),
          child: Center(
            child: Text(_con.isLoading ? 'Đang tải dữ liệu...' : 'Không có cộng sự F1'),
          ),
        );
      return ListViewItem(
        _con.dataList,
        (User item, _) {
          return UserViewItem(
            item,
            onTap: (d) {
              print("RouteUserArgument ${d.id} ${d.fullName}");
              Navigator.of(context).pushNamed('/F1/User/View', arguments: RouteUserArgument(id: d.id.toString(), level: (_f1Level + 1).toString(), param: d));
            },
          );
        },
        // height: _dataF1Child.length * (70.0 + 1),
        controller: _scrollController,
      );
    }

    Widget _myF1View() {
      return Container(
        width: config.App.appWidth(100),
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
        ),
        child: ColumnStart(
          children: [
            RowWidget(
              Container(
                padding: EdgeInsets.only(top: 16, bottom: 16, left: 24, right: 24),
                child: AccentText("Danh sách cộng sự F1", style: TextStyle(fontWeight: FontWeight.w500)),
              ),
              Container(
                padding: EdgeInsets.only(top: 15, right: 24, left: 0, bottom: 15),
                alignment: Alignment.centerRight,
                child: RowWidget(
                  AccentText("Tổng số: "),
                  PrimaryText(Helper.formatNumber((_con.serverTotalCount ?? 0).toDouble())),
                  padding: 0,
                  mainAxisAlignment: MainAxisAlignment.end,
                ),
              ),
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //flex: 60,
            ),
            Container(
              decoration: BoxDecoration(
                color: config.Colors.backgroundColor(),
              ),
              child: listDataBuilder(),
            ),
          ],
        ),
      );
    }

    Widget _bodyView() {
      if (data == null) return Container();
      return Container(
        width: config.App.appWidth(100),
        child: ColumnStart(children: [
          _infoUserView(),
          _noteUserView(),
          _myClassRoomView(),
          // _myTargetView(),
          _myF1View(),
        ]),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyUserView,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Cộng sự của tôi"),
        actions: [
          IconButton(
            icon: SvgPicture.asset('assets/icon/home_alt_outline.svg'),
            onPressed: () {
              Navigator.of(context).pushNamedAndRemoveUntil('/Page', (Route<dynamic> route) => false, arguments: 4);
            },
          )
        ],
      ),
      body: Container(child: _bodyView()),
    );
  }
}
