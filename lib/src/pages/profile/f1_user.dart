import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/f1_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/ListViewItem.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class F1UserWidget extends StatefulWidget {
  @override
  _F1UserWidgetState createState() => _F1UserWidgetState();
}

class _F1UserWidgetState extends StateMVC<F1UserWidget> {
  F1Controller _con;
  ScrollController _scrollController;
  _F1UserWidgetState() : super(F1Controller()) {
    _con = controller;
  }
  @override
  void initState() {
    super.initState();
    _con.pageState();
    initScroll();
  }

  initScroll() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset && (_con.serverTotalCount > ((_con.currentPage + 1) * _con.itemPage))) {
        _con.loadF1Data(0, _con.currentPage + 1, "");
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget listDataBuilder() {
      if (_con.dataList == null || _con.dataList.length == 0)
        return Container(
          alignment: Alignment.center,
          padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
          child: Center(
            child: Text(_con.isLoading ? 'Đang tải dữ liệu...' : 'Không có dữ liệu.'),
          ),
        );
      return ListView.builder(
        controller: _scrollController,
        itemCount: _con.dataList.length,
        itemBuilder: (_context, index) => UserViewItem(_con.dataList[index]),
      );
    }

    Widget _bodyView() {
      return Container(
        width: config.App.appWidth(100),
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
        ),
        child: ColumnStart(
          children: [
            RowWidget(
              Container(
                padding: EdgeInsets.only(top: 15, left: 24, right: 0, bottom: 15),
                child: AccentText("Danh sách cộng sự F1", style: TextStyle(fontWeight: FontWeight.w500)),
              ),
              Container(
                padding: EdgeInsets.only(top: 15, right: 24, left: 0, bottom: 15),
                alignment: Alignment.centerRight,
                child: RowWidget(
                  AccentText("Tổng số: "),
                  PrimaryText(Helper.formatNumber((_con.serverTotalCount ?? 0).toDouble())),
                  padding: 0,
                  mainAxisAlignment: MainAxisAlignment.end,
                ),
              ),
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //flex: 60,
            ),
            Expanded(
              child: Container(
                  decoration: BoxDecoration(
                    color: config.Colors.backgroundColor(),
                  ),
                  child: listDataBuilder()),
            ),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKey,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Cộng sự của tôi"),
        actions: [
          IconButton(
            icon: SvgPicture.asset('assets/icon/search.svg'),
            onPressed: () {
              Navigator.of(context).pushNamed('/F1/Search');
            },
          )
        ],
      ),
      main: Container(child: _bodyView()),
    );
  }
}
