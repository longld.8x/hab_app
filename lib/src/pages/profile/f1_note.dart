import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/f1_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class F1NoteWidget extends StatefulWidget {
  final RouteUserArgument routeArgument;
  F1NoteWidget({Key key, this.routeArgument}) : super(key: key);

  @override
  _F1NoteWidgetState createState() => _F1NoteWidgetState();
}

class _F1NoteWidgetState extends StateMVC<F1NoteWidget> {
  F1Controller _con;
  TextEditingController _ctrlText = TextEditingController();
  bool isResult = false;
  User _userView;
  String _id;
  _F1NoteWidgetState() : super(F1Controller()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.f1NoteState();
    _userView = widget.routeArgument.param;
    _ctrlText.text = widget.routeArgument.content;
    _id = (widget.routeArgument.id);
  }

  void search() {
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() => isResult = true);
  }

  @override
  Widget build(BuildContext context) {
    Widget _bodyView() {
      return Container(
        height: config.App.appHeight(100), //- config.App.appBar(),
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
          //borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
        ),
        child: TextField(
          controller: _ctrlText,
          autofocus: true,
          keyboardType: TextInputType.multiline,
          maxLines: null,
          style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor()),
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyF1Note,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Cộng sự của tôi"),
        actions: [
          IconButton(
            icon: PrimaryText("Lưu"),
            onPressed: () {
              if (_ctrlText.text.isNotEmpty && _ctrlText.text.length > 0) {
                _con.addNoteComment(_ctrlText.text, _userView.id, _id).then((result) {
                  if (result.success) {
                    if (widget.routeArgument.event != null) widget.routeArgument.event(_ctrlText.text);
                    Navigator.of(context).pop();
                    Helper.successMessenge('Ghi chú thành công');
                  } else
                    Helper.errorMessenge(result.message);
                });
              } else {
                Helper.errorMessenge('Vui lòng nhập ghi chú');
              }
            },
          )
        ],
      ),
      body: Container(child: _bodyView()),
    );
  }
}
