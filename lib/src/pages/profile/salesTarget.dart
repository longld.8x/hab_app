import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/profile_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitlePageSimpleWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class SalesTargetWidget extends StatefulWidget {
  @override
  _SalesTargetWidgetState createState() => _SalesTargetWidgetState();
}

class _SalesTargetWidgetState extends StateMVC<SalesTargetWidget> {
  ProfileController _con;
  // TextEditingController _ctrlText = new MoneyMaskedTextController(
  //   precision: 0,
  //   initialValue: 0,
  //   decimalSeparator: ',',
  //   thousandSeparator: '.',
  //   rightSymbol: ' đ',
  // );
  TextEditingController _ctrlText = new TextEditingController();

  String _msg;
  bool _activeBtn = false;
  _SalesTargetWidgetState() : super(ProfileController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.salesState();
  }

  @override
  Widget build(BuildContext context) {
    Widget _validateWidget() {
      if (_msg == null || _msg.isEmpty) return Container();
      return Container(
        padding: EdgeInsets.only(left: 15, right: 15),
        alignment: Alignment.topLeft,
        child: ErrorText(_msg),
      );
    }

    String ctrlValid(String newValue) {
      if (newValue == null || newValue.isEmpty) return "Vui lòng nhập mục tiêu doanh số";
      return null;
    }

    Widget _formWidget() {
      return Form(
        key: _con.formKeySales,
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InputField(
                labelText: "Nhập mục tiêu doanh số",
                controller: _ctrlText,
                hintText: '1.000.000.000',
                keyboardType: TextInputType.number,
                maxLength: 16,
                validator: (newValue) {
                  return ctrlValid(newValue);
                },
                onChanged: (String newValue) {
                  String msg = ctrlValid(newValue);
                  setState(() => _activeBtn = (msg == null || msg.isEmpty));
                },
              ),
              SizedBox(height: 40),
              Button(
                isActive: _activeBtn,
                text: "Cập nhật",
                onPressed: () {
                  print("))))))))");
                  if (_con.formKeySales.currentState.validate()) {
                    Navigator.of(context).pop();

                    Helper.successMessenge("Đã cập nhật thông tin mục tiêu doanh số");
                  }
                },
              ),
            ],
          ),
        ),
      );
    }

    Widget _body() {
      return Column(
        children: <Widget>[
          TitlePageSimpleWidget("Nhập mục tiêu doanh số"),

          // Container(
          //   //height: 80,
          //   alignment: Alignment.bottomLeft,
          //
          //   padding: EdgeInsets.only(left: 15, right: 15, bottom: 60, top: 16),
          //
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //     children: [
          //       //AccentText("Vui lòng nhập mật khẩu để hoàn tất việc đăng ký tài khoản."),
          //       SizedBox(height: 80),
          //     ],
          //   ),
          // ),
          SizedBox(height: 80),
          _validateWidget(),
          _formWidget(),
        ],
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeySales,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText(""),
      ),
      body: Container(child: _body()),
    );
  }
}
