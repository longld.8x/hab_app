import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/profile_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitlePageSimpleWidget.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ChangePasswordWidget extends StatefulWidget {
  @override
  _ChangePasswordWidgetState createState() => _ChangePasswordWidgetState();
}

class _ChangePasswordWidgetState extends StateMVC<ChangePasswordWidget> {
  ProfileController _con;
  TextEditingController _ctrlPass0 = TextEditingController();
  TextEditingController _ctrlPass1 = TextEditingController();
  TextEditingController _ctrlPass2 = TextEditingController();
  bool _hidePassword1 = true;
  bool _hidePassword2 = true;
  bool _hidePassword0 = true;

  TextEditingController _ctrlPhoneNumber = TextEditingController();
  String _msg;
  bool _activeBtn = false;
  _ChangePasswordWidgetState() : super(ProfileController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.changePasswordState();
  }

  bool activeBtn() {
    setState(() => _activeBtn = (_ctrlPass0.text != null && _ctrlPass1.text.isNotEmpty) && (_ctrlPass1.text != null && _ctrlPass0.text.isNotEmpty) && (_ctrlPass2.text != null && _ctrlPass2.text.isNotEmpty));
  }

  @override
  Widget build(BuildContext context) {
    Widget _validateWidget() {
      if (_msg == null || _msg.isEmpty) return Container();
      return Container(
        padding: EdgeInsets.only(left: 15, right: 15, bottom: 0, top: 0),
        alignment: Alignment.topLeft,
        child: ErrorText(_msg),
      );
    }

    Widget _formWidget() {
      return Form(
        key: _con.formKeyChangePassword,
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15, bottom: 0, top: 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InputField(
                controller: _ctrlPass0,
                labelText: "Mật khẩu hiện tại",
                hintText: '••••••••',
                obscureText: _hidePassword0,
                validator: (String value) => pass0Valid(value),
                onChanged: (String value) => activeBtn(),
                suffixIcon: IconButton(
                  onPressed: () => setState(() => _hidePassword0 = !_hidePassword0),
                  icon: _hidePassword0 ? SvgPicture.asset('assets/icon/eye.svg') : SvgPicture.asset('assets/icon/eye2.svg'),
                ),
              ),
              SizedBox(height: 24),
              InputField(
                controller: _ctrlPass1,
                labelText: "Mật khẩu mới",
                hintText: '••••••••',
                obscureText: _hidePassword1,
                validator: (String value) => pass1Valid(value),
                onChanged: (String value) => activeBtn(),
                suffixIcon: IconButton(
                  onPressed: () => setState(() => _hidePassword1 = !_hidePassword1),
                  icon: _hidePassword1 ? SvgPicture.asset('assets/icon/eye.svg') : SvgPicture.asset('assets/icon/eye2.svg'),
                ),
              ),
              SizedBox(height: 24),
              InputField(
                controller: _ctrlPass2,
                labelText: "Xác nhận mật khẩu mới",
                hintText: '••••••••',
                obscureText: _hidePassword2,
                validator: (String value) => pass2Valid(value),
                onChanged: (String value) => activeBtn(),
                suffixIcon: IconButton(
                  onPressed: () => setState(() => _hidePassword2 = !_hidePassword2),
                  icon: _hidePassword2 ? SvgPicture.asset('assets/icon/eye.svg') : SvgPicture.asset('assets/icon/eye2.svg'),
                ),
              ),
              SizedBox(height: 40),
              Button(
                isActive: _activeBtn,
                text: "Hoàn thành",
                onPressed: () {
                  if (_con.formKeyChangePassword.currentState.validate()) {
                    _con.changePassword(_ctrlPass0.text, _ctrlPass1.text, _ctrlPass2.text).then((ResponseMessage result) {
                      if (result.success) {
                        Navigator.of(context).pop();
                        Helper.successMessenge("Thay đổi mật khẩu thành công!");
                        _con.logout();
                      } else
                        Helper.errorMessenge(result.message);
                    });
                  }
                },
              ),
            ],
          ),
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyChangePassword,
      appBar: null,
      body: Container(
        child: Column(
          children: <Widget>[
            AppbarProfileManager(""),
            TitlePageSimpleWidget("Đổi mật khẩu"),
            SizedBox(height: 80),
            _validateWidget(),
            _formWidget(),
          ],
        ),
      ),
    );
  }

  String pass0Valid(String value) {
    if (value.isEmpty) {
      return 'Vui lòng nhập mật khẩu hiện tại';
    }
    return null;
  }

  String pass1Valid(String value) {
    if (value.isEmpty) {
      return 'Vui lòng nhập mật khẩu mới';
    }
    if (value.length < 8) {
      return 'Mật khẩu mới cần có ít nhất 8 ký tự.';
    }
    return null;
  }

  String pass2Valid(String value) {
    if (value.isEmpty) {
      return 'Vui lòng nhập mật khẩu mới';
    }
    if (value.length < 8) {
      return 'Mật khẩu mới cần có ít nhất 8 ký tự.';
    }
    if (value != _ctrlPass1.text) {
      return 'Xác nhận mật khẩu mới chưa chính xác.';
    }
    return null;
  }
}
