import 'dart:io' show File;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/profile_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/CheckBoxItem.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/pages/profile/image_picker_handler.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class EditProfileWidget extends StatefulWidget {
  Function onLoad;
  EditProfileWidget({
    Key key,
    this.onLoad,
  }) : super();
  @override
  _EditProfileWidgetState createState() => new _EditProfileWidgetState();
}

class _EditProfileWidgetState extends StateMVC<EditProfileWidget> with TickerProviderStateMixin, ImagePickerListener {
  ProfileController _con;
  String _msg;
  ImageProvider _userAvatar;
  bool _activeBtn = false;

  File _image;
  AnimationController _controller;
  ImagePickerHandler imagePicker;

  TextEditingController _ctrlName;
  // TextEditingController _ctrlHeight;
  // TextEditingController _ctrlWeight;

  _EditProfileWidgetState() : super(ProfileController()) {
    _con = controller;
  }

  Widget avatar;
  @override
  void initState() {
    super.initState();
    _con.editState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 100),
    );

    _ctrlName = TextEditingController(text: _con.userEdit.fullName ?? "");
    // _ctrlHeight = TextEditingController(text: ((_con.userEdit.infoHeight != null && _con.userEdit.infoHeight > 0) ? _con.userEdit.infoHeight : "").toString());
    // _ctrlWeight = TextEditingController(text: ((_con.userEdit.infoWeight != null && _con.userEdit.infoWeight > 0) ? _con.userEdit.infoWeight : "").toString());

    imagePicker = new ImagePickerHandler(this, _controller);
    imagePicker.init();
    _userAvatar = userAvatar();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  userImage(File image) async {
    setState(() => _image = image);
    setState(() => _activeBtn = true);
    _userAvatar = userAvatar();
  }

  userAvatar() {
    if (_image != null) {
      List<int> imageBytes = _image.readAsBytesSync();
      return MemoryImage(imageBytes);
    }
    if ((_con.userEdit.profilePicture != null && _con.userEdit.profilePicture.isNotEmpty)) {
      return CachedNetworkImageProvider(_con.userEdit.profilePicture);
    }

    return AssetImage("assets/icon/avatar.png");
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _con.userEdit.birthday != null ? _con.userEdit.birthday : DateTime(DateTime.now().year - 18),
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != _con.userEdit.birthday) {
      setState(() {
        _con.userEdit.birthday = picked;
      });
      setState(() => _activeBtn = true);
    }
  }

  @override
  Widget build(BuildContext context) {
    var _appWidth = config.App.appWidth(100);

    Widget _avatarView() {
      var _imgView = Center(
        child: Container(
          alignment: Alignment.center,
          height: 125.0,
          width: 125.0,
          child: CircleAvatar(
            backgroundImage: _userAvatar,
            radius: 125.0 / 2,
          ),
        ),
      );
      return Container(
        width: _appWidth,
        height: 125,
        child: new Stack(
          children: <Widget>[
            _imgView,
            Positioned(
              right: (_appWidth - 32) / 2 - 125 / 2 - 7,
              bottom: 11.0,
              child: new Container(
                width: 38.0,
                height: 38.0,
                decoration: BoxDecoration(
                  color: config.Colors.primaryColor(),
                  borderRadius: BorderRadius.all(Radius.circular(38)),
                ),
                child: Icon(
                  Icons.camera_alt,
                  color: Colors.white,
                  size: 18,
                ),
                alignment: Alignment.center,
              ).onTap(() => imagePicker.showDialog(context)),
            ),
          ],
        ),
      );
    }

    Widget _formWidget() {
      return Form(
        key: _con.formKeyEdit,
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15, bottom: 0, top: 0),
          decoration: BoxDecoration(color: config.Colors.backgroundColor()),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 28),
              InputField(
                controller: _ctrlName,
                labelText: "Tên hiển thị",
                hintText: 'Nguyễn Văn A',
                keyboardType: TextInputType.text,
                maxLength: 60,
                validator: (String value) {
                  return null;
                },
                onEditingComplete: () {
                  if (_ctrlName.text != null) setState(() => _activeBtn = true);
                },
              ),
              SizedBox(height: 24),
              InputField(
                enableInteractiveSelection: false, // will disable paste operation
                readOnly: true,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: config.Colors.inputBgColor(),
                  labelText: "Số điện thoại",
                  hintText: "0985566909",
                ),

                controller: new TextEditingController(text: _con.userEdit.phoneNumber ?? ""),
                style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.hintColor()),
              ),
              SizedBox(height: 24),
              InputField(
                labelText: "Ngày sinh",
                readOnly: true,
                hintText: '01/01/1989',
                controller: new TextEditingController(text: (_con.userEdit.birthday == null ? "" : new DateFormat('dd/MM/yyyy').format(_con.userEdit.birthday))), // "${_con.userEdit.birthday.day}/${_con.userEdit.birthday.month}/${_con.userEdit.birthday.year}")),
                onTap: () => _selectDate(context),
              ),
              SizedBox(height: 24),
              Container(
                child: RowWidget(
                  RadioItem(
                    _con.userEdit.gender == 1,
                    "Nam",
                    () => setState(() {
                      _con.userEdit.gender = 1;
                      _activeBtn = true;
                    }),
                  ),
                  RadioItem(
                    _con.userEdit.gender == 0,
                    "Nữ",
                    () => setState(() {
                      _con.userEdit.gender = 0;
                      _activeBtn = true;
                    }),
                  ),
                ),
              ),
              SizedBox(height: 24),
              // InputField(
              //   controller: _ctrlHeight,
              //   labelText: "Chiều cao (cm)",
              //   hintText: '160',
              //   keyboardType: TextInputType.number,
              //   maxLength: 6,
              //   onChanged: (String newValue) {
              //     setState(() => _activeBtn = true);
              //   },
              // ),
              // SizedBox(height: 24),
              // InputField(
              //   controller: _ctrlWeight,
              //   labelText: "Cân nặng (kg)",
              //   hintText: '60',
              //   maxLength: 6,
              //   keyboardType: TextInputType.number,
              //   onChanged: (String newValue) {
              //     setState(() => _activeBtn = true);
              //   },
              // ),
              // SizedBox(height: 24),
              Button(
                isActive: _activeBtn,
                text: "Cập nhật",
                onPressed: () {
                  if (_ctrlName.text == null || _ctrlName.text.isEmpty) {
                    Helper.errorMessenge('Vui lòng nhập tên hiển thị.');
                    return;
                  }
                  print("InputField $_image");
                  _con.userEdit.fullName = _ctrlName.text;
                  // _con.userEdit.infoHeight = _ctrlHeight.text.isNotEmpty ? double.parse(_ctrlHeight.text) : 0;
                  // _con.userEdit.infoWeight = _ctrlWeight.text.isNotEmpty ? double.parse(_ctrlWeight.text) : 0;
                  if (_image != null) {
                    _con.userEdit.image = _image;
                  }
                  _con.editProfile(_con.userEdit).then((ResponseMessage result) {
                    if (result.success) {
                      Navigator.of(context).pop();
                      if (widget.onLoad != null) widget.onLoad(result.results);
                      Helper.successMessenge("Cập nhật thông tin tài khoản thành công!");
                    } else
                      Helper.errorMessenge(result.message);
                  });
                },
              ),
            ],
          ),
        ),
      );
    }

    Widget _bodyView() {
      return Container(
        width: _appWidth,
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
        ),
        child: ColumnStart(
          children: [
            _avatarView(),
            _formWidget(),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyEdit,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Thông tin tài khoản"),
      ),
      body: _bodyView(),
    );
  }
}
