import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/profile_controller.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/alert_dialog.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class ProfileWidget extends StatefulWidget {
  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends StateMVC<ProfileWidget> with SingleTickerProviderStateMixin {
  ProfileController _con;
  _ProfileWidgetState() : super(ProfileController()) {
    _con = controller;
  }
  Widget _userAvatar;

  @override
  void initState() {
    super.initState();
    _con.pageState();
    _userAvatar = UserAvatar(_con.user.profilePicture);
    settingRepo.getAuthenUser().then((value) {
      print("settingRepo.getAuthenUser $value");
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget rowMenu(int number, String title, Widget subWidget, String router) {
      return Container(
        margin: EdgeInsets.only(bottom: 1, right: 0.5),
        padding: EdgeInsets.only(top: 0, left: 24, right: 10, bottom: 0),
        height: 80,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
          borderRadius: BorderRadius.only(
            topLeft: number == 1 ? const Radius.circular(14.0) : const Radius.circular(4.0),
            topRight: number == 2 ? const Radius.circular(14.0) : const Radius.circular(4.0),
            bottomLeft: const Radius.circular(4.0),
            bottomRight: const Radius.circular(4.0),
          ),
          border: new Border.all(width: 1.0, color: config.Colors.customeColor("#E4F2F9")),
        ),
        child: RowWidget(
            Container(
              width: 32,
              height: 32,
              child: SvgPicture.asset('assets/icon/icon${number.toString()}.svg'),
              margin: EdgeInsets.only(top: 5, left: 0, right: 0, bottom: 5),
            ),
            Container(
              height: 80,
              alignment: Alignment.center,
              width: config.App.appWidth(50) - (24 + 10 + 32 + 8 + 3),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AccentText(title, style: TextStyle(fontSize: 14)),
                  subWidget,
                ],
              ),
            ),
            padding: 8),
      ).onTap(() {
        if (router == 'doing') {
          Helper.successMessenge('Đang phát triển...');
        } else
          Navigator.of(context).pushNamed(router);
      });
    }

    Widget profile() {
      var avatarTop = config.App.appHeight(15) / 2 - 50 / 2 + 10;
      return Positioned(
        top: 0,
        width: config.App.appWidth(100),
        height: config.App.appHeight(100) - 68,
        child: Container(
          width: config.App.appWidth(100),
          height: config.App.appHeight(15),
          alignment: Alignment.topLeft,
          decoration: BoxDecoration(
            color: config.Colors.inputBgColor(),
            image: DecorationImage(
              image: AssetImage("assets/icon/profile.png"),
              fit: BoxFit.fitWidth,
              alignment: Alignment.topLeft,
            ),
          ),
          child: Container(
            padding: EdgeInsets.only(top: avatarTop, left: 24),
            child: RowWidget(
              _userAvatar,
              Container(
                width: config.App.appWidth(100) - 50 - 24 - 10,
                height: 50,
                child: ColumnStart(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 2, bottom: 2),
                      child: TitleText(_con.user.phoneNumber, style: TextStyle(color: Colors.white)),
                    ),
                    Container(
                      child: SubText('Xem thông tin cá nhân', style: TextStyle(color: Colors.white)),
                    ),
                  ],
                ),
              ),
            ),
          ).onTap(() {
            Navigator.of(context).pushNamed('/Profile/Edit', arguments: (User _user) {
              setState(() {
                _con.user = _user;
                _userAvatar = UserAvatar(_user.profilePicture);
              });
            });
          }),
        ),
      );
    }

    double _menuBlockHeight = 3 * (80.0 + 1);
    Widget menuBlock() {
      return Positioned(
        top: config.App.appHeight(15) - 8,
        child: Container(
          width: config.App.appWidth(100),
          height: _menuBlockHeight,
          margin: EdgeInsets.only(top: 0),
          decoration: BoxDecoration(
            color: config.Colors.inputBgColor(),
            borderRadius: BorderRadius.only(topLeft: const Radius.circular(14.0), topRight: const Radius.circular(14.0)),
          ),
          child: ColumnStart(
            children: [
              RowWidget(
                rowMenu(1, "Lớp học của tôi", Container(), '/MyClass'),
                rowMenu(2, "Trò chuyện", Container(), '/Chat'),
                flex: 50,
              ),
              // RowWidget(
              //   rowMenu(3, "Mục tiêu cá nhân", Container(), 'doing'),
              //   rowMenu(4, "Mục tiêu doanh số", Container(padding: EdgeInsets.only(top: 8), child: SubText("1.000.000đ", style: TextStyle(color: config.Colors.primaryColor(), fontWeight: FontWeight.w500))), '/SalesTarget'),
              //   flex: 50,
              // ),
              RowWidget(
                rowMenu(3, "Chỉ số cơ thể", Container(), '/MyHealth'),
                rowMenu(4, "Công việc", Container(), '/Work'),
                flex: 50,
              ),
              RowWidget(
                rowMenu(5, "Cộng sự của tôi", Container(padding: EdgeInsets.only(top: 8), child: RowWidget(SubText("Tổng số: "), SubText(Helper.formatNumber((_con.user.totalNetwork ?? 0).toDouble()), style: TextStyle(color: config.Colors.primaryColor(), fontWeight: FontWeight.w500)), padding: 0)), '/F1/User'),
                rowMenu(6, "Người giới thiệu", Container(), '/ParentUser'),
                flex: 50,
              ),
              // RowWidget(
              //   rowMenu(7, "Định vị thương hiệu cá nhân", Container(), 'doing'),
              //   rowMenu(8, "Quản lý ví", Container(padding: EdgeInsets.only(top: 8), child: RowWidget(SubText("số dư:"), SubText("100.000.000đ", style: TextStyle(color: config.Colors.primaryColor(), fontWeight: FontWeight.w500)), padding: 0)), 'doing'),
              //   flex: 50,
              // ),
            ],
          ),
        ),
      );
    }

    Widget menuListTile(String icon, String title, Function onTap) {
      return Container(
        width: config.App.appWidth(100),
        alignment: Alignment.topLeft,
        margin: EdgeInsets.only(bottom: 2),
        decoration: BoxDecoration(color: config.Colors.backgroundColor()),
        child: ListTile(
          onTap: onTap,
          title: RowWidget(
              SvgPicture.asset(
                icon,
              ),
              SubText(title, style: TextStyle(fontSize: 14)),
              padding: 16),
          contentPadding: EdgeInsets.only(left: 16, top: 6, right: 16, bottom: 6),
        ),
      );
    }

    Widget menuList() {
      return Container(
        margin: EdgeInsets.only(top: 24, bottom: 24),
        child: ColumnStart(
          children: [
            menuListTile("assets/icon/setting_profile.svg", "Cài đặt đăng nhập", () => Navigator.of(context).pushNamed('/Profile/Settings')),
            menuListTile("assets/icon/lock.svg", "Đổi mật khẩu", () => Navigator.of(context).pushNamed('/Profile/ChangePassword')),

            // menuListTile(
            //     "assets/icon/setting_phone.svg",
            //     "Đổi số điện thoại đăng nhập",
            //     () => Helper.successMessenge('Đang phát triển...')),
            menuListTile("assets/icon/logout.svg", "Đăng xuất", () {
              // DialogHLS.confirmDialog(context, "", "Bạn có chắc chắn muốn đăng xuất tài khoản trên thiết bị này?", () {
              //   _con.logout().then((value) => settingRepo.navigatorKey.currentState.pushNamedAndRemoveUntil('/Login', (Route<dynamic> route) => false));
              // });
              showModalBottomSheet(
                context: context,
                isDismissible: false,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(top: Radius.circular(28.0)),
                  side: BorderSide.none,
                ),
                elevation: 0,
                builder: (_) {
                  return Container(
                    width: config.App.appWidth(100),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          child: Text(
                            'Bạn có chắc chắn muốn đăng xuất tài khoản trên thiết bị này?',
                            style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.subColor()),
                            textAlign: TextAlign.center,
                          ),
                          padding: EdgeInsets.only(left: 36, right: 36, top: 36, bottom: 36),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 36, right: 36, top: 0, bottom: 36),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Container(
                                  child: DialogHLS.actionBtn(
                                    context: context,
                                    text: "Đóng",
                                    color: config.Colors.customeColor('#E0E9F8'),
                                    textStyle: TextStyle(color: config.Colors.primaryColor()),
                                  ),
                                  padding: EdgeInsets.only(right: 5),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  child: DialogHLS.actionBtn(
                                    context: context,
                                    text: 'Xác nhận',
                                    action: () {
                                      // _con.logout().then((value) {
                                      //   // Navigator.of(settingRepo.navigatorKey.currentContext).pop();
                                      //   settingRepo.navigatorKey.currentState.pushNamedAndRemoveUntil('/Login', (Route<dynamic> route) => false);
                                      // });
                                      _con.logout();
                                    },
                                  ),
                                  padding: EdgeInsets.only(left: 5),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              );
            }),
            menuListTile("assets/icon/clock.svg", "Phiên bản: ${settingRepo.setting.value.appVersion}", () {}),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKey,
      onWillPop: Helper.of(context).onWillPop,
      appBar: null,
      body: Container(
        width: config.App.appWidth(100),
        constraints: BoxConstraints(minHeight: config.App.appHeight(100) - 68),
        decoration: BoxDecoration(color: config.Colors.inputBgColor()),
        alignment: Alignment.topLeft,
        child: Column(
          children: [
            Container(
              width: config.App.appWidth(100),
              height: config.App.appHeight(15) + _menuBlockHeight,
              child: Stack(
                children: [
                  profile(),
                  menuBlock(),
                ],
              ),
            ),
            menuList(),
            // Expanded(
            //   child: Container(
            //     decoration: BoxDecoration(color: config.Colors.inputBgColor()),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
