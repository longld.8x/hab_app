import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/parent_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitlePageSimpleWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ParentUserWidget extends StatefulWidget {
  @override
  _ParentUserWidgetState createState() => _ParentUserWidgetState();
}

class _ParentUserWidgetState extends StateMVC<ParentUserWidget> {
  ParentController _con;
  TextEditingController _ctrlPhoneNumber = TextEditingController();
  String _errorMsg;
  bool _isParent = true;
  bool _activeBtn = false;
  _ParentUserWidgetState() : super(ParentController()) {
    _con = controller;
  }
  Widget _userAvatar;

  @override
  void initState() {
    super.initState();
    _con.pageState();
  }

  @override
  Widget build(BuildContext context) {
    Widget _validateWidget() {
      if (_errorMsg == null || _errorMsg.isEmpty) return Container();
      return Container(
        padding: EdgeInsets.only(left: 15, right: 15),
        alignment: Alignment.topLeft,
        child: ErrorText(_errorMsg),
      );
    }

    Widget _parentWidget() {
      if (_con.parent == null) return SizedBox(height: 40);
      return Container(
        padding: EdgeInsets.only(top: 15, bottom: 40),
        alignment: Alignment.topLeft,
        child: TextIcon(AccentText("Tên người giới thiệu: "), PrimaryText(_con.parent.fullName), padding: 0),
      );
    }

    String phoneValid(String newValue) {
      if (newValue.length < 10) return "Số điện thoại không đúng định dạng";
      var msg = Helper.validPhonumber(newValue);
      if (msg != null && msg.isNotEmpty) return msg;
      return null;
    }

    Widget _formWidget() {
      return Form(
        key: _con.formKey,
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InputField(
                labelText: "Số điện thoại",
                controller: _ctrlPhoneNumber,
                hintText: '0912345678',
                keyboardType: TextInputType.number,
                maxLength: 15,
                validator: (newValue) {
                  if (newValue == _con.user.phoneNumber) return 'Bạn không thể điền số điện thoại của chính mình';
                  return phoneValid(newValue);
                },
                onChanged: (String newValue) {
                  setState(() {
                    _activeBtn = (newValue != null && newValue.isNotEmpty);
                    _con.parent = null;
                  });
                },
                onEditingComplete: () {
                  if (_con.formKey.currentState.validate() && _con.parent == null) {
                    searchParent();
                  }
                },
              ),
              _parentWidget(),
              Button(
                isActive: _activeBtn,
                text: _con.parent == null ? "Truy vấn" : "Hoàn thành",
                onPressed: () {
                  if (_con.formKey.currentState.validate()) {
                    if (_con.parent == null) {
                      searchParent();
                      return;
                    }
                    _con.setParent(_con.parent.phoneNumber).then((ResponseMessage result) {
                      if (result.success) {
                        Navigator.of(context).pop();
                        Helper.successMessenge("Cập nhật thông tin thành công");
                      } else
                        Helper.errorMessenge(result.message);
                    });
                  }
                },
              ),
            ],
          ),
        ),
      );
    }

    Widget _bodyView(User _parent) {
      _userAvatar = UserAvatar(_parent.profilePicture);
      return Container(
        width: config.App.appWidth(100),
        height: config.App.appHeight(100) - config.App.appBar(),
        padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
        ),
        child: ColumnStart(
          children: [
            Container(
              padding: EdgeInsets.only(top: 10, bottom: 10, left: 16, right: 16),
              decoration: BoxDecoration(
                color: config.Colors.backgroundColor(),
                borderRadius: BorderRadius.all(const Radius.circular(10.0)),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 2),
                    color: Color(0xFF000000).withOpacity(0.16),
                    spreadRadius: 1,
                    blurRadius: 4,
                  ),
                ],
              ),
              child: IconWidget(
                  _userAvatar,
                  Container(
                    width: config.App.appWidth(50) - (24 + 10 + 32 + 8 + 3),
                    child: ColumnStart(
                      children: [
                        Container(
                          padding: EdgeInsets.only(bottom: 5),
                          child: AccentText(_parent.fullName, style: TextStyle(fontWeight: FontWeight.w500)),
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 8),
                          child: SubText(_parent.phoneNumber, style: TextStyle(color: config.Colors.customeColor("#979797"))),
                        ),
                      ],
                    ),
                  ),
                  padding: 8),
            ).onTap(() {
              print("999999999999");
            }),
          ],
        ),
      );
    }

    Widget _body() {
      if (_con.checkParent == null) return Container();
      if (_con.checkParent.success) {
        var obj = User.fromJSON(_con.checkParent.results);
        return _bodyView(obj);
      }
      return Column(
        children: <Widget>[
          TitlePageSimpleWidget("Người giới thiệu"),
          Container(
            //height: 80,
            alignment: Alignment.bottomLeft,

            padding: EdgeInsets.only(left: 15, right: 15, bottom: 60, top: 16),

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //AccentText("Vui lòng nhập mật khẩu để hoàn tất việc đăng ký tài khoản."),
                SizedBox(height: 6),
                AccentText(
                  "Lưu ý: Bạn chỉ có thể nhập người giới thiệu trong vòng 24 giờ kể từ lúc đăng ký tài khoản..",
                  style: Theme.of(context).textTheme.caption.copyWith(fontStyle: FontStyle.italic, color: config.Colors.accentColor()),
                ),
              ],
            ),
          ),
          //SizedBox(height: 80),
          _validateWidget(),
          _formWidget(),
        ],
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKey,
      appBar: AppBar(
        leading: AppbarIcon(icon: (_isParent ? const Icon(Icons.close) : null)),
        title: TitleText(_isParent ? "Người giới thiệu" : ""),
      ),
      body: Container(child: _body()),
    );
  }

  searchParent() {
    _con.searchParent(_ctrlPhoneNumber.text).then((ResponseMessage result) {
      if (result.success) {
        setState(() {
          _errorMsg = '';
          _con.parent = result.results;
        });
      } else {
        setState(() => _errorMsg = "Số điện thoại này chưa có tài khoản trên HAB");
      }
    });
  }
}
