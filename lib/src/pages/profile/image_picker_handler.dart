import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/pages/profile/image_picker_dialog.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerHandler {
  ImagePickerDialog imagePicker;
  AnimationController _controller;
  ImagePickerListener _listener;

  ImagePickerHandler(this._listener, this._controller);

  openCamera() async {
    imagePicker.dismissDialog();
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    cropImage(image);
  }

  openGallery() async {
    imagePicker.dismissDialog();
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    cropImage(image);
  }

  void init() {
    imagePicker = new ImagePickerDialog(this, _controller);
    imagePicker.initState();
  }

  Future cropImage(File image) async {
    final bytes = await image.readAsBytes();
    final mb = bytes.length / 1024 / 1024;
    if (mb > 5) {
      Helper.errorMessenge("Vui lòng chọn ảnh dưới 5M");
      return;
    }

    File croppedFile = await ImageCropper.cropImage(
      sourcePath: image.path,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
      ],
      androidUiSettings: AndroidUiSettings(
        toolbarTitle: 'Đổi ảnh đại diện',
        toolbarColor: config.Colors.primaryColor(),
        toolbarWidgetColor: Colors.white,
        hideBottomControls: true,
      ),
      iosUiSettings: IOSUiSettings(
        title: 'Đổi ảnh đại diện',
      ),
      cropStyle: CropStyle.circle,
    );
    _listener.userImage(croppedFile);
  }

  showDialog(BuildContext context) {
    imagePicker.getImage(context);
  }

  // List<MealFileDto> _files;
  //
  // ImagePicker picker = ImagePicker();
  //
  // VideoPlayerController _videoPlayerController;
  // VideoPlayerController _cameraVideoPlayerController;
  //
  // // This funcion will helps you to pick and Image from Gallery
  // _pickImageFromGallery() async {
  //   PickedFile pickedFile = await picker.getImage(source: ImageSource.gallery, imageQuality: 50);
  //
  //   File image = File(pickedFile.path);
  //
  //   setState(() {
  //     _files.add(new MealFileDto(file: image, type: 'img', source: 'gallery'));
  //   });
  // }
  //
  // // This funcion will helps you to pick and Image from Camera
  // _pickImageFromCamera() async {
  //   PickedFile pickedFile = await picker.getImage(source: ImageSource.camera, imageQuality: 50);
  //
  //   File image = File(pickedFile.path);
  //
  //   setState(() {
  //     _files.add(new MealFileDto(file: image, type: 'img', source: 'camera'));
  //   });
  // }
  //
  // // This funcion will helps you to pick a Video File
  // _pickVideo() async {
  //   PickedFile pickedFile = await picker.getVideo(source: ImageSource.gallery);
  //
  //   var video = File(pickedFile.path);
  //
  //   _videoPlayerController = VideoPlayerController.file(video)
  //     ..initialize().then((_) {
  //       setState(() {
  //         setState(() {
  //           _files.add(new MealFileDto(file: video, type: 'video', source: 'gallery'));
  //         });
  //       });
  //       _videoPlayerController.play();
  //     });
  // }
  //
  // // This funcion will helps you to pick a Video File from Camera
  // _pickVideoFromCamera() async {
  //   PickedFile pickedFile = await picker.getVideo(source: ImageSource.camera);
  //
  //   var video = File(pickedFile.path);
  //
  //   _cameraVideoPlayerController = VideoPlayerController.file(video)
  //     ..initialize().then((_) {
  //       setState(() {
  //         setState(() {
  //           _files.add(new MealFileDto(file: video, type: 'video', source: 'camera'));
  //         });
  //       });
  //       _cameraVideoPlayerController.play();
  //     });
  // }
}

abstract class ImagePickerListener {
  userImage(File _image);
}
