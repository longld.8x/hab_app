import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/f1_controller.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/ListViewItem.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:mvc_pattern/mvc_pattern.dart';

class F1SearchUserWidget extends StatefulWidget {
  @override
  _F1SearchUserWidgetState createState() => _F1SearchUserWidgetState();
}

class _F1SearchUserWidgetState extends StateMVC<F1SearchUserWidget> {
  F1Controller _con;
  _F1SearchUserWidgetState() : super(F1Controller()) {
    _con = controller;
  }
  TextEditingController _ctrlSearch = TextEditingController();
  bool isSearchForm = true;
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _con.f1SearchState();
    _con.getSearchF1Keyword();
    initScroll();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    //_con.controller.close();
    super.dispose();
  }

  initScroll() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset && (_con.serverTotalCount > ((_con.currentPage + 1) * _con.itemPage))) {
        _con.loadF1Data(0, _con.currentPage + 1, _ctrlSearch.text);
      }
    });
  }

  void search() {
    FocusScope.of(context).requestFocus(FocusNode());
    _con.addSearchF1Keyword(_ctrlSearch.text);
    setState(() => isSearchForm = false);
    _con.loadF1Data(0, 0, _ctrlSearch.text);
  }

  @override
  Widget build(BuildContext context) {
    Widget listDataBuilder() {
      if (_con.dataList == null || _con.dataList.length == 0)
        return Container(
          alignment: Alignment.center,
          padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
          child: Center(
            child: Text(_con.isLoading ? 'Đang tải dữ liệu...' : 'Không có dữ liệu.'),
          ),
        );
      return ListView.builder(
        controller: _scrollController,
        itemCount: _con.dataList.length,
        itemBuilder: (_context, index) {
          var item = _con.dataList[index];
          if (index == 0)
            return UserViewItem(
              item,
              deco: BoxDecoration(
                color: config.Colors.backgroundColor(),
                borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
              ),
            );
          return UserViewItem(item);
        },
        padding: EdgeInsets.all(0),
      );
    }

    final double statusbarHeight = MediaQuery.of(context).padding.top;
    Widget _searchBox() {
      return Container(
        margin: EdgeInsets.only(top: 8, bottom: 8),
        padding: EdgeInsets.only(left: 16, right: 16),
        width: config.App.appWidth(100),
        alignment: Alignment.center,
        child: Container(
          alignment: Alignment.centerLeft,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.max,
            verticalDirection: VerticalDirection.up,
            children: [
              Expanded(
                flex: 10,
                child: Container(
                  height: 56,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.only(topLeft: Radius.circular(40), bottomLeft: Radius.circular(40)),
                    border: new Border.all(width: 0, color: Colors.white),
                  ),
                  child: IconButton(
                    color: Colors.transparent,
                    padding: EdgeInsets.fromLTRB(15, 11, 15, 11),
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.black,
                      size: 18,
                    ),
                    onPressed: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      if (isSearchForm == true) {
                        search();
                      } else {
                        Navigator.of(context).pop();
                      }
                    },
                  ),
                ),
              ),
              Expanded(
                flex: 90,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.only(topRight: Radius.circular(40), bottomRight: Radius.circular(40)),
                    border: new Border.all(width: 0, color: Colors.white),
                  ),
                  height: 56,
                  child: TextFormField(
                    autofocus: isSearchForm,
                    controller: _ctrlSearch,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: config.Colors.backgroundColor(),
                      labelText: null,
                      hintText: "Nhập tên hoặc số điện thoại cộng sự",
                      suffixIcon: _ctrlSearch.text.isEmpty
                          ? null
                          : IconButton(
                              icon: Icon(Icons.close, size: 16, color: config.Colors.customeColor("#979797")),
                              padding: EdgeInsets.all(12),
                              onPressed: () {
                                setState(() => _ctrlSearch.text = '');
                              },
                            ),
                      contentPadding: EdgeInsets.symmetric(horizontal: 5, vertical: 15),
                      // border: UnderlineInputBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(40.0), bottomRight: Radius.circular(40.0))),
                      focusedBorder: UnderlineInputBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(40.0), bottomRight: Radius.circular(40.0)), borderSide: BorderSide.none),
                      enabledBorder: UnderlineInputBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(40.0), bottomRight: Radius.circular(40.0)), borderSide: BorderSide.none),
                      border: UnderlineInputBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(40.0), bottomRight: Radius.circular(40.0)), borderSide: BorderSide.none),
                    ),
                    style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor()),
                    onEditingComplete: () => search(),
                    onTap: () {
                      print("onEditingComplete onTap");
                      _con.getSearchF1Keyword();
                      setState(() => isSearchForm = true);
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }

    Widget _bodySearchView() {
      final _bottom = MediaQuery.of(context).viewInsets.bottom;
      return ColumnStart(
        children: [
          Container(
            padding: EdgeInsets.only(top: 15, left: 24, right: 0, bottom: 15),
            child: AccentText("Lịch sử tìm kiếm", style: TextStyle(fontWeight: FontWeight.w500)),
            decoration: BoxDecoration(
              color: config.Colors.backgroundColor(),
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
            ),
            margin: EdgeInsets.only(bottom: 1),
          ),
          Container(
            child: Expanded(
              child: (_con.keywords == null || _con.keywords.length == 0)
                  ? Container()
                  : Container(
                      margin: EdgeInsets.only(bottom: _bottom),
                      child: ListViewItem(
                        _con.keywords,
                        (String item, _) => SearchViewItem(item,
                            onTap: (String v) {
                              setState(() => _ctrlSearch.text = v);
                              search();
                            },
                            onTapIcon: (String v) => _con.removeSearchF1Keyword(v)),
                      ),
                    ),
            ),
          ),
        ],
      );
    }

    Widget _bodyResultView() {
      final _bottom = MediaQuery.of(context).viewInsets.bottom;
      return Container(child: listDataBuilder());
    }

    Widget _bodyView() {
      return Container(
        width: config.App.appWidth(100),
        height: config.App.appHeight(100) - (statusbarHeight + 10 + 40 + 24),
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
        ),
        child: isSearchForm ? _bodySearchView() : _bodyResultView(),
      );
    }

    return LayoutAppbarBgManager(
      scaffoldKey: _con.scaffoldKeyF1Search,
      onWillPop: () {
        FocusScope.of(context).requestFocus(FocusNode());
        Navigator.of(context).pop();
      },
      bottom: 10,
      titleFull: true,
      title: _searchBox(),
      main: _bodyView(),
    );
  }
}
