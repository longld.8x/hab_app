import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/pages/profile/image_picker_handler.dart';

class ImagePickerDialog extends StatelessWidget {
  ImagePickerHandler _listener;
  AnimationController _controller;
  BuildContext context;

  ImagePickerDialog(this._listener, this._controller);

  Animation<double> _drawerContentsOpacity;
  Animation<Offset> _drawerDetailsPosition;

  void initState() {
    _drawerContentsOpacity = new CurvedAnimation(
      parent: new ReverseAnimation(_controller),
      curve: Curves.fastOutSlowIn,
    );
    _drawerDetailsPosition = new Tween<Offset>(
      begin: const Offset(0.0, 1.0),
      end: Offset.zero,
    ).animate(new CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    ));
  }

  getImage(BuildContext context) {
    if (_controller == null || _drawerDetailsPosition == null || _drawerContentsOpacity == null) {
      return;
    }
    _controller.forward();
    showDialog(
      context: context,
      builder: (BuildContext context) => new SlideTransition(
        position: _drawerDetailsPosition,
        child: new FadeTransition(
          opacity: new ReverseAnimation(_drawerContentsOpacity),
          child: this,
        ),
      ),
    );
  }

  void dispose() {
    _controller.dispose();
  }

  startTime() async {
    var _duration = new Duration(milliseconds: 200);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.pop(context);
  }

  dismissDialog() {
    _controller.reverse();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return new Material(
      type: MaterialType.transparency,
      color: config.Colors.accentColor(),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            decoration: BoxDecoration(
              color: config.Colors.backgroundColor(),
            ),
            child: ColumnStart(
              children: [
                Container(
                  padding: EdgeInsets.all(16),
                  child: TitleText('Chọn ảnh đại diện'),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
                  child: AccentText('Chọn ảnh từ thư viện'),
                ).onTap(() => _listener.openGallery()),
                Container(
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
                  child: AccentText('Chụp ảnh mới'),
                ).onTap(() => _listener.openCamera()),
                Container(
                  padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
                  child: ErrorText('Hủy'),
                ).onTap(() => dismissDialog()),
              ],
            ),
          ),
        ],
      ),
    );
    // return new Material(
    //     type: MaterialType.transparency,
    //     child: new Opacity(
    //       opacity: 1.0,
    //       child: new Container(
    //         padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 20.0),
    //         child: Column(
    //           mainAxisSize: MainAxisSize.max,
    //           mainAxisAlignment: MainAxisAlignment.end,
    //           children: <Widget>[
    //             new GestureDetector(
    //               onTap: () => _listener.openCamera(),
    //               child: roundedButton("Camera", EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0), const Color(0xFF167F67), const Color(0xFFFFFFFF)),
    //             ),
    //             new GestureDetector(
    //               onTap: () => _listener.openGallery(),
    //               child: roundedButton("Gallery", EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0), const Color(0xFF167F67), const Color(0xFFFFFFFF)),
    //             ),
    //             const SizedBox(height: 15.0),
    //             new GestureDetector(
    //               onTap: () => dismissDialog(),
    //               child: new Padding(
    //                 padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
    //                 child: roundedButton("Cancel", EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0), const Color(0xFF167F67), const Color(0xFFFFFFFF)),
    //               ),
    //             ),
    //           ],
    //         ),
    //       ),
    //     ));
  }

  Widget roundedButton(String buttonLabel, EdgeInsets margin, Color bgColor, Color textColor) {
    var loginBtn = new Container(
      margin: margin,
      padding: EdgeInsets.all(15.0),
      alignment: FractionalOffset.center,
      decoration: new BoxDecoration(
        color: bgColor,
        borderRadius: new BorderRadius.all(const Radius.circular(100.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: const Color(0xFF696969),
            offset: Offset(1.0, 6.0),
            blurRadius: 0.001,
          ),
        ],
      ),
      child: Text(
        buttonLabel,
        style: new TextStyle(color: textColor, fontSize: 20.0, fontWeight: FontWeight.bold),
      ),
    );
    return loginBtn;
  }
}
