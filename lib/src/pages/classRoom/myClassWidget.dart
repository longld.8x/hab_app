import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/my_class_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/pages/classRoom/classWidget.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MyClassWidget extends StatefulWidget {
  @override
  _MyClassWidgetState createState() => _MyClassWidgetState();
}

class _MyClassWidgetState extends StateMVC<MyClassWidget> {
  MyClassController _con;
  User data;
  _MyClassWidgetState() : super(MyClassController()) {
    _con = controller;
  }
  int _f1Level = 0;

  @override
  void initState() {
    super.initState();
    _con.pageState();
  }

  @override
  Widget build(BuildContext context) {
    Widget _bodyView() {
      if (_con.isLoading) return Container(padding: EdgeInsets.only(top: 24, left: 0, right: 0), child: LoadContent(warp: true));
      if (_con.dataMyClass == null || _con.dataMyClass.length == 0) return Container(padding: EdgeInsets.only(top: 24, left: 0, right: 0), child: NoContent(warp: true));

      return Container(
        padding: EdgeInsets.only(top: 24, left: 0, right: 0),
        child: new ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) => ClassWidget(model: _con.dataMyClass[index]),
          itemCount: _con.dataMyClass.length,
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKey,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Lớp học của tôi"),
      ),
      body: Container(
          width: config.App.appWidth(100),
          decoration: BoxDecoration(
            color: config.Colors.inputBgColor(),
          ),
          child: _bodyView()),
    );
  }
}
