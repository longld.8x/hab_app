import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/CourseCommon.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/ProgressBar.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/myClassDetailDto.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;

class ClassWidget extends StatefulWidget {
  final MyClassDetailDto model;
  final Function action;
  final bool horizontal;
  ClassWidget({Key key, this.model, this.action, this.horizontal = false}) : super(key: key);
  _ClassWidgetState createState() => _ClassWidgetState();
}

class _ClassWidgetState extends State<ClassWidget> {
  Function _onTap() {
    Navigator.of(settingRepo.navigatorKey.currentContext).pushNamed('/MyClass/Detail', arguments: RouteClassArgument(classId: widget.model.classId));
  }

  @override
  Widget build(BuildContext context) {
    Widget roleTag() {
      if (widget.model.labels != null && widget.model.labels.length > 0) {
        return new Positioned(
          left: 0.0,
          top: 0.0,
          child: new Container(
            height: 24.0,
            padding: EdgeInsets.fromLTRB(10, 4, 10, 4),
            decoration: new BoxDecoration(
              color: config.Colors.customeColor("#F3C41D"),
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(10),
              ),
            ),
            child: SubText(widget.model.labels.join(', '), style: TextStyle(fontSize: 14, color: Colors.white)),
            alignment: Alignment.center,
          ),
        );
      } else {
        return Container();
      }
    }

    Widget horizontal_row3() {
      if (widget.model.status == 6) {
        return Container(
          alignment: Alignment.centerRight,
          child: Container(
            margin: EdgeInsets.only(bottom: 0),
            decoration: BoxDecoration(
              color: config.Colors.customeColor('#388E3C', opacity: 0.1),
              borderRadius: BorderRadius.circular(4.0),
            ),
            child: SubText(
              'Hoàn thành khoá học',
              style: TextStyle(color: config.Colors.customeColor('#388E3C')),
            ),
            padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
          ),
        );
      }
      if (widget.model.nextSession != null && widget.model.nextSession.sessionId > 0 && widget.model.nextSession.startDate != null) {
        return Container(
            child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 2),
              child: SubText(
                "${widget.model.sessionPassTotal}/${widget.model.sessionTotal} bài",
                style: TextStyle(color: config.Colors.primaryColor()),
              ),
              alignment: Alignment.centerRight,
            ),
            Container(
              padding: EdgeInsets.only(bottom: 0, left: 0),
              child: ProgressBar(Helper.getPercent(widget.model.sessionPassTotal.toDouble(), widget.model.sessionTotal.toDouble())),
            ),
          ],
        ));
      }
      return Container();
    }

    Widget horizontal() {
      var width = config.App.appWidth(100);
      var _boxWidth = width * 0.6;
      print("widget.model.sessionPassTotal ${widget.model.sessionPassTotal} ${widget.model.sessionTotal}");
      return Container(
        width: _boxWidth,
        height: _boxWidth * (237 / 217),
        margin: EdgeInsets.only(left: 16, right: 8),
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 0),
              child: ClassHorizontalBox(
                image: (widget.model.images != null && widget.model.images.length > 0) ? widget.model.images[0] : '',
                info: Container(
                  padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 4.5),
                        child: TitleText(
                          widget.model.className,
                          style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                          maxLines: 2,
                        ),
                      ),
                      horizontal_row3(),
                    ],
                  ),
                ),
              ),
            ).onTap(_onTap),
            roleTag()
          ],
        ),
      );
    }

    Widget vertical_row1() {
      if (widget.model.nextSession != null && widget.model.nextSession.sessionId > 0 && widget.model.nextSession.sessionName != null)
        return Container(
          margin: EdgeInsets.only(bottom: 6, top: 4),
          child: SubText(
            'Bài tiếp: ' + widget.model.nextSession.sessionName,
            style: TextStyle(),
            maxLines: 1,
          ),
        );
      else
        return Container(
          margin: EdgeInsets.only(bottom: 6),
          child: CourseRowInfo(
            'assets/icon/task.svg',
            widget.model.sessionTotal.toString() + " bài",
            fontSize: 12,
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          ),
        );
    }

    Widget vertical_row2() {
      if (widget.model.nextSession != null && widget.model.nextSession.sessionId > 0 && widget.model.nextSession.startDate != null)
        return Container(
          margin: EdgeInsets.only(bottom: 2),
          child: CourseRowInfo(
            'assets/icon/clock.svg',
            Helper.showTime(widget.model.nextSession.startDate, (widget.model.status == 6) ? widget.model.nextSession.endDate : null),
            fontSize: 12,
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          ),
        );
      else
        return Container(
          margin: EdgeInsets.only(bottom: 6),
          child: CourseRowInfo(
            'assets/icon/clock.svg',
            Helper.showDate(widget.model.fromDate, widget.model.toDate),
            fontSize: 12,
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          ),
        );
    }

    Widget vertical_row3() {
      if (widget.model.status == 6) {
        return Container(
          margin: EdgeInsets.only(bottom: 0),
          decoration: BoxDecoration(
            color: config.Colors.customeColor('#388E3C', opacity: 0.1),
            borderRadius: BorderRadius.circular(4.0),
          ),
          child: SubText(
            'Hoàn thành khoá học',
            style: TextStyle(color: config.Colors.customeColor('#388E3C')),
          ),
          padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
        );
      }
      if (widget.model.nextSession != null && widget.model.nextSession.sessionId > 0 && widget.model.nextSession.startDate != null) {
        return Container(
            child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 2),
              child: SubText(
                "${widget.model.sessionPassTotal}/${widget.model.sessionTotal} bài",
                style: TextStyle(color: config.Colors.primaryColor()),
              ),
              alignment: Alignment.centerRight,
            ),
            Container(
              padding: EdgeInsets.only(bottom: 0, left: 0),
              child: ProgressBar(Helper.getPercent(widget.model.sessionPassTotal.toDouble(), widget.model.sessionTotal.toDouble())),
            ),
          ],
        ));
      }
      return Container();
    }

    Widget vertical() {
      return Container(
        margin: EdgeInsets.only(bottom: 1),
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 0),
              child: ClassVerticalBox(
                image: (widget.model.images != null && widget.model.images.length > 0) ? widget.model.images[0] : '',
                info: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: TitleText(
                        widget.model.className,
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                        maxLines: 2,
                      ),
                      margin: EdgeInsets.only(bottom: 4),
                    ),
                    vertical_row1(),
                    vertical_row2(),
                    vertical_row3(),
                  ],
                ),
              ),
            ).onTap(_onTap),
            roleTag()
          ],
        ),
      );
    }

    return widget.horizontal ? horizontal() : vertical();
  }
}
