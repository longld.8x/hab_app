import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/my_class_session_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/CourseCommon.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/elements/tab.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/pages/classRoom/session/document.dart';
import 'package:hab_app/src/pages/classRoom/session/exercises.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MyClassSessionDetailWidget extends StatefulWidget {
  final RouteClassArgument routeArgument;
  MyClassSessionDetailWidget({Key key, this.routeArgument}) : super(key: key);
  @override
  _MyClassSessionDetailWidgetState createState() => _MyClassSessionDetailWidgetState();
}

class _MyClassSessionDetailWidgetState extends StateMVC<MyClassSessionDetailWidget> with TickerProviderStateMixin {
  MyClassSessionController _con;
  _MyClassSessionDetailWidgetState() : super(MyClassSessionController()) {
    _con = controller;
  }
  ScrollController _scrollController;
  bool _bottomBtn = false;
  TabController _tabController;

  List<Tab> _listTab;

  @override
  void initState() {
    super.initState();
    _con.pageState(widget.routeArgument);
    initTab();
    initScroll();
  }

  @override
  void dispose() {
    _con.onExitSocket();
    super.dispose();
  }

  initTab() {
    _listTab = [
      TabTitle("BÀI TẬP"),
      TabTitle("TÀI LIỆU"),
    ];
    _tabController = TabController(length: _listTab.length, vsync: this);
  }

  bool isEnable() {
    var _offline = (_con.dataDetail.sessionType == 2 && _con.dataDetail.joined == false && (_con.role.indexOf(1) > -1));
    var _online = (_con.dataDetail.sessionType == 1 && (_con.zoomDto != null && _con.zoomDto.meetingId.isNotEmpty));
    return (_con.dataDetail != null) && _con.dataDetail.sessionStatus == 1 && (_offline || _online);
  }

  @override
  Widget build(BuildContext context) {
    var _width = config.App.appWidth(100);

    Widget _buttonOffline() {
      return Button(
        text: "Tham gia học offline",
        onPressed: () {
          _con.checkInOffline().then((ResponseMessage result) {
            if (result != null && result.success) {
              setState(() => _con.dataDetail.joined = true);
              if (widget.routeArgument.event != null) widget.routeArgument.event('join', true);
              Helper.successMessenge('Đã xác nhận tham gia buổi học');
            } else {
              setState(() => _con.dataDetail.joined = false);
              if (widget.routeArgument.event != null) widget.routeArgument.event('join', false);
              Helper.infoMessenge(result.message);
            }
          });
        },
      );
    }

    Widget _buttonOnline() {
      return Button(
        text: _con.role.indexOf(5) > -1
            ? "Bắt đầu"
            : _con.role.indexOf(2) > -1
                ? "Dạy bài"
                : _con.role.indexOf(1) > -1
                    ? "Học bài"
                    : "Tham gia",
        onPressed: () {
          if (_con.zoomDto == null || _con.zoomDto.meetingId.isEmpty) {
            Helper.infoMessenge("Không có thông tin lớp học!");
            return;
          }
          setState(() => _con.dataDetail.joined = true);
          if (widget.routeArgument.event != null) widget.routeArgument.event('join', true);
          var argument = widget.routeArgument;
          argument.param = _con.zoomDto;
          argument.role = _con.role;
          Navigator.of(context).pushNamed('/MyClass/Zoom', arguments: argument);
        },
      );
    }

    Widget _buttonJoin() {
      var _offline = (_con.dataDetail.sessionType == 2 && _con.dataDetail.joined == false && (_con.role.indexOf(1) > -1));
      var _online = (_con.dataDetail.sessionType == 1 && (_con.zoomDto != null && _con.zoomDto.meetingId.isNotEmpty));
      if (_offline || _online)
        return Container(
          height: 80,
          width: config.App.appWidth(100),
          padding: EdgeInsets.fromLTRB(24, 10, 24, 16),
          child: _offline ? _buttonOffline() : _buttonOnline(),
        );
      return Container();
    }

    Widget _buttonJoinBottom() {
      if (_bottomBtn == false) return Container();
      if (!isEnable()) return Container();
      var _offline = (_con.dataDetail.sessionType == 2 && _con.dataDetail.joined == false && (_con.role.indexOf(1) > -1));
      var _online = (_con.dataDetail.sessionType == 1 && (_con.zoomDto != null && _con.zoomDto.meetingId.isNotEmpty));
      if (_offline || _online)
        return Container(
          width: config.App.appWidth(100),
          decoration: BoxDecoration(
            color: Colors.transparent,
          ),
          padding: EdgeInsets.fromLTRB(24, 0, 24, 24),
          child: _offline ? _buttonOffline() : _buttonOnline(),
        );
      return Container();
    }

    Map _titleInfo() {
      var _height = 0.0; // fix
      List<Widget> _infoWidget = [];

      _infoWidget.add(Container(
        padding: EdgeInsets.fromLTRB(20, 0, 20, 16),
        child: TextTagBox(
          _con.dataDetail.sessionName * 1,
          LabelBoxSS(_con.dataDetail.sessionStatusView, _con.dataDetail.sessionStatus),
          style: TextStyle(fontSize: 16),
        ),
      ));
      var _heightSSName = Helper.getTextLineHeight(
        _con.dataDetail.sessionName * 1 + _con.dataDetail.sessionStatusView + " blabalbla ",
        Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor(), fontSize: 16, fontWeight: FontWeight.w500),
        (_width - 40),
      );
      _height += (_heightSSName + 0 + 16);
      var _heightRow = Helper.getTextLineHeight(
        "1 line",
        Theme.of(context).textTheme.bodyText1.copyWith(fontWeight: FontWeight.w400, fontSize: 16),
        (_width - 40),
      );
      _infoWidget.add(CourseRowInfo('assets/icon/clock.svg', Helper.showTime(_con.dataDetail.startDate, _con.dataDetail.endDate)));
      _height += (_heightRow + 0 + 16);

      var _typeStr = _con.dataDetail.sessionType == 1 ? 'Học Online' : (_con.dataDetail.sessionType == 2 ? 'Học Offline' : 'Học Online');
      _infoWidget.add(CourseRowInfo('assets/icon/task.svg', _typeStr));
      _height += (_heightRow + 0 + 16);

      if (_bottomBtn == false && isEnable()) {
        _infoWidget.add(_buttonJoin());
        _height += 80;
      }

      _infoWidget.add(Container(height: 16, decoration: BoxDecoration(color: config.Colors.inputBgColor())));
      _height += (16);

      return {
        "widget": new Container(
          decoration: BoxDecoration(
            color: config.Colors.backgroundColor(),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: _infoWidget,
          ),
        ),
        "height": _height
      };
    }

    Widget _bodyView() {
      if (_con.isLoading) return LoadContent(warp: true);
      if (_con.dataDetail == null) return NoContent(warp: true);
      var _title = _titleInfo();
      return Stack(
        alignment: Alignment.bottomLeft,
        children: <Widget>[
          DefaultTabController(
            length: 2,
            child: NestedScrollView(
              controller: _scrollController,
              headerSliverBuilder: (_, __) {
                return <Widget>[
                  SliverAppBar(
                    expandedHeight: _title['height'],
                    flexibleSpace: FlexibleSpaceBar(
                      background: _title['widget'],
                      collapseMode: CollapseMode.pin,
                    ),
                    leading: Container(),
                    titleSpacing: 0,
                  ),
                  SliverPersistentHeader(
                    delegate: SliverAppBarDelegate(
                      TabBar(
                        indicatorColor: config.Colors.primaryColor(),
                        labelColor: config.Colors.primaryColor(),
                        unselectedLabelColor: config.Colors.accentColor(),
                        tabs: _listTab,
                      ),
                    ),
                    pinned: true,
                  ),
                ];
              },
              body: Container(
                child: TabBarView(
                  children: [
                    TabViewBox(CourseExercises(_con), isEnable()),
                    TabViewBox(CourseDocument(_con), isEnable()),
                  ],
                ),
              ),
            ),
            //),
          ),
          _buttonJoinBottom(),
        ],
        // ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyDetail,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Thông tin bài học"),
      ),
      main: _bodyView(),
    );
  }

  initScroll() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.offset >= 500 && _bottomBtn == false)
        setState(() => _bottomBtn = true);
      else if (_scrollController.offset < 500 && _bottomBtn == true) setState(() => _bottomBtn = false);
    });
  }
}
