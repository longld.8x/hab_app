import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/my_class_controller.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/CourseCommon.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/models/sessionResultDto.dart';

class CourseSession extends StatefulWidget {
  final MyClassController con;
  final int classId;
  final Function event;
  const CourseSession(this.con, this.classId, {Key key, this.event}) : super(key: key);
  _CourseSessionState createState() => _CourseSessionState();
}

class _CourseSessionState extends State<CourseSession> {
  @override
  void initState() {
    widget.con.initStateSession(widget.classId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget listBuilder() {
      if (widget.con.isLoadingSession) return LoadContent(warp: true);
      if (widget.con.dataSession == null || widget.con.dataSession.length == 0) return NoContent(warp: true);

      return Container(
        padding: EdgeInsets.only(top: 0, left: 0, right: 0),
        child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: widget.con.dataSession.length,
          itemBuilder: (_context, i) {
            var _item = widget.con.dataSession[i];
            var _class = widget.con.dataDetail;
            return SessionViewItem(
              _item,
              onTap: () {
                print("_class.classRoles ${_class.classRoles}");
                if ((_class.classRoles != null && _class.classRoles.length > 0)) {
                  Navigator.of(context).pushNamed('/MyClass/Session/Detail',
                      arguments: RouteClassArgument(
                        classId: widget.classId,
                        sessionId: _item.sessionId,
                        classSessionId: _item.classSessionId,
                        role: _class.classRoles,
                        param: _item,
                        event: widget.event,
                      ));
                } else {
                  Helper.infoMessenge('Bạn không có quyền thực hiện thao tác này.');
                }
              },
            );
          },
        ),
      );
    }

    return Container(
      decoration: BoxDecoration(
        color: config.Colors.backgroundColor(),
      ),
      child: listBuilder(),
    );
  }
}

class SessionViewItem extends StatelessWidget {
  final SessionResultDto data;
  final Function onTap;
  const SessionViewItem(this.data, {Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _item = this.data;
    var _typeStr = (_item.sessionType == 2 ? 'Học Offline' : 'Học Online');
    Widget _addressWg = (_item.sessionType == 2 && _item.address.isNotEmpty)
        ? CourseRowInfo(
            'assets/icon/pin.svg',
            _item.address,
            padding: EdgeInsets.fromLTRB(0, 0, 0, 16),
          )
        : Container();
    return Container(
      padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
      child: Container(
        padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          borderRadius: BorderRadius.all(Radius.circular(8)),
          shape: BoxShape.rectangle,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 1),
              color: Color(0xFF000000).withOpacity(0.1),
              spreadRadius: 1,
              blurRadius: 8,
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 16),
              child: TextTagBox(
                _item.sessionName,
                LabelBoxSS(_item.sessionStatusView, _item.sessionStatus),
              ),
            ),
            CourseRowInfo(
              'assets/icon/clock.svg',
              Helper.showTime(_item.startDate, _item.endDate) + (_item.sessionStatus == 4 ? "\n(ngày học mới)" : ""),
              padding: EdgeInsets.fromLTRB(0, 0, 0, 16),
            ),
            CourseRowInfo(
              'assets/icon/task.svg',
              _typeStr,
              padding: EdgeInsets.fromLTRB(0, 0, 0, 16),
            ),
            _addressWg,
          ],
        ),
      ).onTap(this.onTap),
    );
  }
}
