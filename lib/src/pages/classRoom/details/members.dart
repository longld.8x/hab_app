import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/my_class_controller.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/ListViewItem.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;

class CourseMembers extends StatefulWidget {
  final MyClassController con;
  final int classId;
  const CourseMembers(this.con, this.classId, {Key key}) : super(key: key);
  _CourseMembersState createState() => _CourseMembersState();
}

class _CourseMembersState extends State<CourseMembers> {
  @override
  void initState() {
    widget.con.initStateMembers(widget.classId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget list2Builder() {
      if (widget.con.isLoadingMembers) return LoadContent(warp: true);
      if (widget.con.dataMembers == null || widget.con.dataMembers.length == 0) return NoContent(warp: true);
      return Container(
        padding: EdgeInsets.only(top: 0, left: 0, right: 0),
        child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: widget.con.dataMembers.length,
          itemBuilder: (_context, i) {
            var _item = widget.con.dataMembers[i];
            return CustomeViewItem(
              _item.fullName,
              _item.profilePicture,
              i,
              sub: SubText(_item.phoneNumber, style: TextStyle(color: config.Colors.hintColor())),
              status: _item.isOnline,
            );
          },
        ),
      );
    }

    return Container(
      decoration: BoxDecoration(
        color: config.Colors.backgroundColor(),
      ),
      child: list2Builder(),
    );
  }
}
