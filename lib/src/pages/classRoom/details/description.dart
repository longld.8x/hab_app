import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/my_class_controller.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/ListViewItem.dart';
import 'package:hab_app/src/elements/ReadMore.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;

class CourseDescription extends StatelessWidget {
  final MyClassController con;
  const CourseDescription(this.con, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget listHlvBuilder() {
      if (this.con.isLoadingHlv) return LoadContent(warp: true);
      if (this.con.dataHlv == null) return NoContent(warp: true);
      return Container(
        padding: EdgeInsets.only(top: 0, left: 0, right: 0),
        child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: con.dataHlv.length,
          itemBuilder: (_context, i) {
            var _item = this.con.dataHlv[i];
            return CustomeViewItem(
              _item.fullName,
              _item.profilePicture,
              i,
              sub: SubText(_item.phoneNumber, style: TextStyle(color: config.Colors.hintColor())),
              status: _item.isOnline,
            );
          },
        ),
      );
    }

    return new Column(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
          width: config.App.appWidth(100),
          decoration: BoxDecoration(
            color: config.Colors.backgroundColor(),
          ),
          child: AccentText("Giới thiệu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(16, 16, 16, 24),
          width: config.App.appWidth(100),
          decoration: BoxDecoration(
            color: config.Colors.backgroundColor(),
          ),
          child: ReadMoreText(
            this.con.dataDetail.courseDescription,
            trimLines: 8,
            colorClickableText: config.Colors.primaryColor(),
            trimMode: TrimMode.Line,
            trimCollapsedText: 'Xem thêm',
            trimExpandedText: '',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal, color: config.Colors.hintColor()),
            moreStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.normal, color: config.Colors.primaryColor()),
            onChange: (bool show) {
              print("1");
            },
          ),
        ),
        SizedBox(height: 16),
        Container(
          width: config.App.appWidth(100),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                padding: EdgeInsets.only(top: 16, bottom: 10, left: 16, right: 16),
                decoration: BoxDecoration(
                  color: config.Colors.backgroundColor(),
                ),
                child: AccentText("HUẤN LUYỆN VIÊN", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
              ),
              Container(
                decoration: BoxDecoration(
                  color: config.Colors.backgroundColor(),
                ),
                child: listHlvBuilder(),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
