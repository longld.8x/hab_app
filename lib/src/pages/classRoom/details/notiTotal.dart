import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/helpers/helper.dart';

class NotiClassRoomWidget extends StatelessWidget {
  final int total;
  final int chatRoomId;
  NotiClassRoomWidget({Key key, this.total, this.chatRoomId}) : super(key: key);

  Widget build(BuildContext context) {
    print("chatRoomId $chatRoomId");
    if (chatRoomId <= 0) return Container();
    return Container(
      width: 50,
      height: 50,
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed('/Chat/Room', arguments: chatRoomId);
        },
        child: Stack(
          children: [
            Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.only(bottom: 16, right: 20),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(40),
                  ),
                ),
                child: SvgPicture.asset('assets/icon/chat.svg'),
              ),
            ),
            Positioned(
              bottom: 25,
              right: 10,
              child: total > 0
                  ? Container(
                      padding: EdgeInsets.only(top: 3.0, bottom: 3.0, right: 4.0, left: 4.0),
                      width: 19,
                      child: Text(
                        total < 10 ? total.toString() : ("9+"),
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.caption.merge(
                              TextStyle(color: Colors.white, fontSize: 8), //Theme.of(context).primaryColor
                            ),
                      ),
                      decoration: BoxDecoration(
                        color: Color(0xFFCF1322),
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                    )
                  : Container(),
              //constraints: BoxConstraints(minWidth: 15, maxWidth: 15, minHeight: 15, maxHeight: 15),
            )
          ],
        ),
      ),
    );
  }
}
