import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/my_class_excercies_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ExercisePostAddWidget extends StatefulWidget {
  final RouteClassArgument routeArgument;
  ExercisePostAddWidget({Key key, this.routeArgument}) : super(key: key);
  @override
  _ExercisePostAddWidgetState createState() => _ExercisePostAddWidgetState();
}

class _ExercisePostAddWidgetState extends StateMVC<ExercisePostAddWidget> {
  /// ====================================================================
  MyClassExcerciesController _con;
  TextEditingController _ctrlInput = TextEditingController();

  _ExercisePostAddWidgetState() : super(MyClassExcerciesController()) {
    _con = controller;
  }

  /// ====================================================================
  List<Asset> images = <Asset>[];
  int _maxImages = 10;

  /// ====================================================================
  @override
  void initState() {
    super.initState();
    _con.pageAddState(widget.routeArgument);
  }

  /// ====================================================================
  Future<void> loadAssets() async {
    List<Asset> resultList = <Asset>[];
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: _maxImages,
        selectedAssets: images,
        enableCamera: true,
        materialOptions: MaterialOptions(
          allViewTitle: "Ảnh bài tập",
          actionBarColor: "#1A60FF",
          actionBarTitleColor: "#FFFFFF",
          lightStatusBar: false,
          statusBarColor: '#1A60FF',
          startInAllView: true,
          selectCircleStrokeColor: "#171051",
          selectionLimitReachedText: "Số lượng vượt quá giới hạn cho phép.",
          textOnNothingSelected: "Vui lòng chọn 1 ảnh.",
        ),
      );
      setState(() {
        images = resultList;
      });
    } on Exception catch (e) {
      Helper.errorMessenge(e.toString());
    }
  }

  /// ====================================================================
  @override
  Widget build(BuildContext context) {
    Widget buildImageView() {
      double _width = config.App.appWidth(100);
      return Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        padding: EdgeInsets.only(left: 8, right: 8),
        height: ((images.length + 1) / 3).ceil() * (_width / 3 - 3),
        child: GridView.count(
          crossAxisCount: 3,
          physics: NeverScrollableScrollPhysics(),
          children: List.generate(images.length + 1, (i) {
            var _w = (_width / 3) - 3.0;
            if (i != images.length) {
              var _img = images[i];
              return Container(
                width: _w,
                height: _w,
                margin: EdgeInsets.all(8),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  child: AssetThumb(
                    asset: _img,
                    width: _w.toInt(),
                    height: _w.toInt(),
                    spinner: const Center(
                      child: SizedBox(
                        width: 24,
                        height: 24,
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  ),
                ),
              );
            }
            if (images.length == _maxImages) return Container();
            return Container(
              width: _w,
              height: _w,
              margin: EdgeInsets.all(8),
              child: DottedBorder(
                color: config.Colors.primaryColor(),
                borderType: BorderType.RRect,
                strokeWidth: 1,
                radius: const Radius.circular(8.0),
                child: Container(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        child: SvgPicture.asset(
                          'assets/icon/addFrame.svg',
                        ),
                        padding: EdgeInsets.only(bottom: 10),
                      ),
                      Container(child: AccentText('Thêm ảnh')),
                    ],
                  ),
                ).onTap(() {
                  loadAssets();
                }),
              ),
            );
          }),
        ),
      );
    }

    Widget _bodyView() {
      return SingleChildScrollView(
        child: Container(
          child: Column(
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                padding: EdgeInsets.all(16),
                alignment: Alignment.centerLeft,
                child: AccentText(
                  _con.dataDetail.description,
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
              ),
              // Container(
              //   decoration: BoxDecoration(
              //       // color: config.Colors.inputBgColor(),
              //       ),
              //   height: 16,
              // ),
              // Container(
              //   padding: EdgeInsets.only(left: 16),
              //   child: AccentText(
              //     'Nội dung',
              //     style: TextStyle(fontWeight: FontWeight.w500),
              //   ),
              //   alignment: Alignment.centerLeft,
              // ),
              Container(
                padding: EdgeInsets.all(16),
                decoration: new BoxDecoration(
                  border: new Border(
                    top: BorderSide(color: config.Colors.customeColor('#E0E9F8'), width: 1),
                  ),
                ),
                child: Container(
                  // decoration: BoxDecoration(
                  //   color: config.Colors.inputBgColor(),
                  //   borderRadius: BorderRadius.circular(12.0),
                  // ),
                  child: TextField(
                    //autofocus: true,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: config.Colors.inputBgColor(),
                      labelText: null,
                      hintText: "Nội dung",
                    ),
                    controller: _ctrlInput,
                    keyboardType: TextInputType.multiline,
                    minLines: 6,
                    maxLines: null,
                    style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor()),
                  ),
                ),
              ),
              // Container(
              //   padding: EdgeInsets.only(left: 16),
              //   child: AccentText(
              //     'Hình ảnh',
              //     style: TextStyle(fontWeight: FontWeight.w500),
              //   ),
              //   alignment: Alignment.centerLeft,
              // ),
              buildImageView(),
            ],
          ),
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKey,
      // bottom: 0,
      // titleFull: false,
      // isSingleChildScrollView: false,
      //
      // title: Container(
      //   alignment: Alignment.center,
      //   child: TitleText(
      //     "Làm bài tập: ${_con.dataDetail.exerciseName}",
      //     style: TextStyle(
      //       color: config.Colors.customeColor("#ffffff"),
      //     ),
      //   ),
      // ),
      // titleRight: IconButton(
      //   // icon: SvgPicture.asset('assets/icon/check_w.svg'),
      //   icon: AccentText("Gửi", style: TextStyle(color: config.Colors.customeColor('#ffffff'))),
      //
      //   onPressed: () {
      //     _con.addPost(_ctrlInput.text).then((result) {
      //       if (result.success) {
      //         Navigator.of(context).pop();
      //         Helper.successMessenge('Làm bài tập thành công');
      //         if (widget.routeArgument.event != null) widget.routeArgument.event();
      //       } else
      //         Helper.errorMessenge(result.message);
      //     });
      //   },
      // ),
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Làm bài tập"),
        actions: [
          IconButton(
            // icon: SvgPicture.asset('assets/icon/check_w.svg'),
            icon: AccentText("Gửi", style: TextStyle(fontWeight: FontWeight.w500)),
            padding: EdgeInsets.only(right: 16),
            onPressed: () {
              FocusScope.of(context).requestFocus(FocusNode());
              _con.addExercise(images, _ctrlInput.text).then((result) {
                if (result.success) {
                  Navigator.of(context).pop();
                  Helper.successMessenge('Làm bài tập thành công');
                  if (widget.routeArgument.event != null) widget.routeArgument.event();
                } else
                  Helper.errorMessenge(result.message);
              });
            },
          ),
        ],
      ),
      main: Container(
        width: config.App.appWidth(100),
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
          //borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
        ),
        child: _bodyView(),
      ),
    );
  }
}
