import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/my_class_excercies_controller.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/ImagePick.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/foodChartWidget.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/exercisePostDto.dart';
import 'package:hab_app/src/models/foodDto.dart';
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:multi_image_picker/multi_image_picker.dart';

class ExercisePostItem extends StatefulWidget {
  final ExercisePostDto data;
  final MyClassExcerciesController controller;
  final Function event;
  final Function eventNext;

  const ExercisePostItem(this.data, this.controller, {this.event, this.eventNext, Key key}) : super(key: key);
  @override
  _ExercisePostItemState createState() => _ExercisePostItemState();
}

class _ExercisePostItemState extends State<ExercisePostItem> {
  bool _titleExpan = false;
  TextEditingController _ctrlComment = TextEditingController();
  List<Asset> images = <Asset>[];
  int _maxImages = 10;
  int _point = 0;
  int _pointSelected = 1;
  bool isLeader = false;

  @override
  void initState() {
    super.initState();
    isLeader = widget.data.user.leader != null && widget.data.user.leader.userId == userRepo.currentUser.value.id;
  }

  @override
  void dispose() {
    super.dispose();
  }

  _onAddComment() {
    if (_ctrlComment.text != null && _ctrlComment.text.isNotEmpty) {
      print("widget.data.post.id ${widget.data.post.id}");
      widget.controller.addComment(widget.data.post.id, _ctrlComment.text, images, _point).then((value) {
        if (value.success) {
          Helper.successMessenge('Bình luận thành công');
          _ctrlComment.text = "";
          _point = 0;
          images = <Asset>[];
          if (widget.event != null) widget.event('add_comment', widget.data.post.id);
        }
      });
    } else {
      Helper.errorMessenge('Vui lòng nhập nội dung bình luận');
    }
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = <Asset>[];
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: _maxImages,
        selectedAssets: images,
        enableCamera: true,
        materialOptions: MaterialOptions(
          allViewTitle: "Ảnh bình luận",
          actionBarColor: "#1A60FF",
          actionBarTitleColor: "#FFFFFF",
          lightStatusBar: false,
          statusBarColor: '#1A60FF',
          startInAllView: true,
          selectCircleStrokeColor: "#171051",
          selectionLimitReachedText: "Số lượng vượt quá giới hạn cho phép.",
          textOnNothingSelected: "Vui lòng chọn 1 ảnh.",
        ),
      );
      setState(() => images = resultList);
    } on Exception catch (e) {
      Helper.errorMessenge(e.toString());
    }
  }

  Future<void> _selectPoint(context) async {
    var points = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    setState(() => _point = 1);
    await showModalBottomSheet(
      isDismissible: false,
      context: context,
      builder: (BuildContext e) {
        return Container(
          height: 260,
          child: Column(
            children: [
              Container(
                color: config.Colors.backgroundColor(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: AccentText('Hủy', style: TextStyle(color: config.Colors.primaryColor())).onTap(() {
                        FocusScope.of(context).requestFocus(FocusNode());
                        Navigator.pop(context);
                        setState(() => _point = 0);
                      }),
                    ),
                    Expanded(
                      flex: 4,
                      child: Container(
                        alignment: Alignment.center,
                        child: _point > 0 ? TitleText('Chọn điểm ($_point)') : TitleText('Chọn điểm'),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: AccentText('Xác nhận', style: TextStyle(color: config.Colors.primaryColor())).onTap(() {
                        FocusScope.of(context).requestFocus(FocusNode());
                        Navigator.pop(context);
                        setState(() => _point = _pointSelected);
                      }),
                    ),
                  ],
                ).paddingAll(8.0),
              ),
              Expanded(
                child: CupertinoTheme(
                  data: CupertinoThemeData(
                    textTheme: CupertinoTextThemeData(
                      pickerTextStyle: TextStyle(fontSize: 18, color: config.Colors.accentColor(), fontWeight: FontWeight.w500),
                    ),
                  ),
                  child: CupertinoPicker(
                    backgroundColor: config.Colors.backgroundColor(),
                    itemExtent: 30,
                    children: points.map((e) {
                      return AccentText(
                        e.toString(),
                        style: TextStyle(fontSize: 22),
                      );
                    }).toList(),
                    onSelectedItemChanged: (int inde) {
                      setState(() => _pointSelected = points[inde]);
                    },
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var _width = config.App.appWidth(100);
    return Container(
      child: Column(
        children: [
          Container(
            child: Column(
              children: [
                _postHead(),
                _postContent(),
                _postImage(),
                // _postFood(),
              ],
            ),
            decoration: new BoxDecoration(
              border: new Border(
                bottom: _border,
              ),
            ),
          ),
          Container(
            child: Column(
              children: [
                _postComment(),
                _postInput(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  var _border = BorderSide(color: config.Colors.customeColor('#E0E9F8'), width: 1);
  Widget _postHead() {
    var _width = config.App.appWidth(100);
    return Container(
      padding: EdgeInsets.only(top: 16, bottom: 10, left: 16, right: 16),
      decoration: new BoxDecoration(
        border: new Border(
          bottom: _border,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          UserAvatar(widget.data.user.profilePicture, status: false),
          Container(
            padding: EdgeInsets.only(top: 4, left: 16, right: 16),
            width: _width - 32 - 50,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: AccentText(widget.data.user.fullName, style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16)),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: SubText(Helper.showTimeText(widget.data.post.createdDate), style: TextStyle(fontSize: 14, color: config.Colors.customeColor('#848688'))),
                ),
              ],
            ),
          )
        ],
      ),
    ).onTap(() {
      if (widget.eventNext != null) {
        ExercisePostDto _post = new ExercisePostDto();
        _post.post = widget.data.post;
        _post.comments = widget.data.comments;
        _post.totalComment = widget.data.totalComment;
        _post.viewTotalComment = widget.data.viewTotalComment;
        _post.user = widget.data.user;
        widget.eventNext(_post);
      }
    });
  }

  Widget _postContent() {
    if (widget.data.post.content == null || widget.data.post.content.isEmpty) return Container();
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.only(top: 10, bottom: 10, left: 16, right: 16),
      child: AccentText(widget.data.post.content, style: TextStyle(fontSize: 16)),
    );
  }

  Widget _postImage() {
    if (widget.data.post.mediaUrls == null || widget.data.post.mediaUrls.length == 0) return Container();

    var _width = config.App.appWidth(100);
    var _images = widget.data.post.mediaUrls;
    var _index = 3;
    if (_images.length < 3) _index = _images.length;
    return Container(
      height: _width / _index,
      child: new ListView.builder(
        shrinkWrap: false,
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: _images.length,
        itemBuilder: (BuildContext context, int index) {
          var _item = _images[index];
          return Container(
            decoration: new BoxDecoration(
              border: new Border(
                left: _border.copyWith(color: config.Colors.customeColor('#ffffff')),
                //top: _border,
                //bottom: _border,
                right: index == (_images.length - 1) ? _border.copyWith(color: config.Colors.customeColor('#ffffff')) : BorderSide.none,
              ),
            ),
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              alignment: Alignment.center,
              imageUrl: _item,
              width: _width / _index,
              height: _width / _index,
            ),
          ).onTap(() async {
            FocusScope.of(context).requestFocus(FocusNode());
            await showDialog(
              context: context,
              builder: (_) => ImageDialogUrl(_item),
            );
          });
        },
      ),
    );
  }

  Widget _postFood() {
    if (widget.data.post.activityDetail == null || widget.data.post.activityDetail.isEmpty) return Container();
    List<FoodDto> _foods = [];
    return Container(
      child: FoodChartWidget(
        dataItem: _foods,
        isExpan: true,
      ),
    );
  }

  Widget _postComment() {
    var _width = config.App.appWidth(100);
    var _commentShowLength = widget.data.comments != null ? widget.data.comments.length : 0;
    var _isMore = widget.data.totalComment > _commentShowLength;
    // if (_commentShowLength == 0) return Container();

    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 16, right: 16),
            width: _width,
            child: Container(
              decoration: new BoxDecoration(
                border: new Border(
                  bottom: _commentShowLength > 0 ? _border.copyWith(color: config.Colors.customeColor('#CDCDCD')) : BorderSide.none,
                ),
              ),
              padding: EdgeInsets.only(top: 16, bottom: 6),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  AccentText(
                    widget.data.totalComment > 0 ? 'Bình luận (${widget.data.totalComment})' : 'Bình luận',
                    style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500),
                  ),
                  (isLeader)
                      ? FlatButton(
                          color: config.Colors.primaryColor(),
                          padding: EdgeInsets.only(top: 4, bottom: 4, left: 8, right: 8),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4.0),
                          ),
                          height: 32,
                          child: SubText(
                            _point > 0 ? "Chấm điểm($_point)" : "Chấm điểm",
                            style: TextStyle(color: config.Colors.customeColor("#ffffff")),
                          ),
                          onPressed: () {
                            FocusScope.of(context).requestFocus(FocusNode());
                            _selectPoint(context);
                          },
                        )
                      : Container(),
                ],
              ),
            ),
          ),
          _commentShowLength > 0
              ? Column(
                  children: List.generate(_commentShowLength + 1, (i) {
                    if (i != _commentShowLength) {
                      var e = widget.data.comments[i];
                      var _isScore = (e.score != null && e.score > 0);
                      var _userInfo = Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          UserAvatar(e.user.profilePicture, status: false, width: 35),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(top: 4, left: 16, right: 16),
                              width: _width - 32 - 50 - (_isScore ? 100 : 0),
                              child: ColumnStart(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(bottom: 5),
                                    child: AccentText(e.user.fullName, style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14, color: config.Colors.customeColor('#171051'))),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(bottom: 5),
                                    child: SubText(Helper.showTimeText(e.createdDate), style: TextStyle(fontSize: 12, color: config.Colors.customeColor('#848688'))),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          (_isScore)
                              ? Container(
                                  decoration: new BoxDecoration(
                                    color: config.Colors.errorColor(opacity: 0.3),
                                    borderRadius: BorderRadius.all(Radius.circular(16)),
                                  ),
                                  padding: EdgeInsets.fromLTRB(24, 2, 24, 4),
                                  margin: EdgeInsets.only(left: 8, top: 0),
                                  child: AccentText(
                                    e.score.toString(),
                                    style: TextStyle(fontSize: 12, color: config.Colors.errorColor()),
                                  ),
                                )
                              : Container()
                        ],
                      );
                      var _imagesContent = Container();

                      if (e.mediaUrls != null && e.mediaUrls.length > 0) {
                        var _viewItem = 5;
                        var _w = _width / _viewItem;

                        _imagesContent = Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              // color: config.Colors.inputBgColor(),
                              ),
                          padding: EdgeInsets.all(0),
                          margin: EdgeInsets.all(0),
                          child: GridView.count(
                            crossAxisCount: _viewItem,
                            childAspectRatio: 1,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            padding: EdgeInsets.all(0),
                            children: List.generate(e.mediaUrls.length, (i) {
                              return Card(
                                child: ImageUrlView(e.mediaUrls[i], _w),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                              ).onTap(() async {
                                FocusScope.of(context).requestFocus(FocusNode());
                                await showDialog(
                                  context: context,
                                  builder: (_) => ImageDialogUrl(e.mediaUrls[i]),
                                );
                              });
                            }),
                          ),
                        );
                      }
                      var _content = Container(
                        alignment: Alignment.topLeft,
                        padding: EdgeInsets.only(top: 8, bottom: 8, left: 0, right: 0),
                        child: AccentText(e.content, style: TextStyle(fontSize: 14, color: config.Colors.customeColor('#848688'))),
                      );
                      return Container(
                        padding: EdgeInsets.only(top: 0, left: 16, right: 16, bottom: 8),
                        child: Container(
                          decoration: new BoxDecoration(
                            border: new Border(
                              bottom: (i != (_commentShowLength - 1) || _isMore) ? _border.copyWith(color: config.Colors.customeColor('#E4E4E4', opacity: 0.6)) : BorderSide.none,
                            ),
                          ),
                          child: Column(
                            children: [
                              _userInfo,
                              _content,
                              _imagesContent,
                            ],
                          ),
                        ),
                      );
                    } else if (_isMore) {
                      return Container(
                        padding: EdgeInsets.only(top: 0, bottom: 8, left: 8, right: 8),
                        child: PrimaryText('Xem các bình luận trước'),
                      ).onTap(() {
                        if (widget.event != null) widget.event('load_comment', widget.data.post.id);
                        print("load more");
                      });
                    } else {
                      return Container();
                    }
                  }).toList(),
                )
              : Container()
        ],
      ),
    );
  }

  Widget _postInputImage() {
    if (images == null || images.length == 0) return Container();
    double _width = config.App.appWidth(100);
    var _gridItem = 6;
    var _w = _width / _gridItem;
    return Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
        ),
        padding: EdgeInsets.all(0),
        margin: EdgeInsets.all(0),
        height: _w,
        // child: GridView.count(
        //   crossAxisCount: _gridItem,
        //   physics: NeverScrollableScrollPhysics(),
        //   padding: EdgeInsets.all(0),
        //   children: List.generate(images.length, (i) {
        //     var _img = images[i];
        //     return ImagePick(_img, _w, remove: (_image) {
        //       var _i = images.indexOf(_image);
        //       setState(() => images.removeAt(_i));
        //     });
        //   }),
        // ),
        child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int i) {
            var _img = images[i];
            return ImagePick(_img, _w, remove: (_image) {
              var _i = images.indexOf(_image);
              setState(() => images.removeAt(_i));
            });
          },
          itemCount: images != null ? images.length : 0,
          padding: EdgeInsets.all(0),
        ));
  }

  Widget _postInput() {
    var _width = config.App.appWidth(100);
    return Container(
      decoration: BoxDecoration(
        color: config.Colors.customeColor('#F0F3F4'),
      ),
      child: Column(
        children: [
          _postInputImage(),
          Container(
            height: 71,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: SvgPicture.asset('assets/icon/icon_image.svg'),
                ).onTap(() {
                  loadAssets();
                }),
                Expanded(
                  child: Container(
                    height: 50,
                    alignment: Alignment.center,
                    child: TextField(
                      controller: _ctrlComment,
                      enableInteractiveSelection: false, // will disable paste operation
                      minLines: 1,
                      maxLines: null,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: config.Colors.backgroundColor(),
                        labelText: null,
                        hintText: "Nhập bình luận",
                        contentPadding: EdgeInsets.all(12),
                        border: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide(color: config.Colors.customeColor('#F0F3F4'))),
                        focusedBorder: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide(color: config.Colors.customeColor('#F0F3F4'))),
                        enabledBorder: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide(color: config.Colors.customeColor('#F0F3F4'))),
                      ),
                      style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor()),
                      onEditingComplete: () {
                        _onAddComment();
                      },
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: SvgPicture.asset('assets/icon/icon_send.svg'),
                ).onTap(() {
                  _onAddComment();
                }),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
