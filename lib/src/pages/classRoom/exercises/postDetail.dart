import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/my_class_excercies_controller.dart';
import 'package:hab_app/src/elements/ImagePick.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/UrlImage.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/models/exercisePostDto.dart';
import 'package:hab_app/src/models/exercisesResultDto.dart';
import 'package:hab_app/src/pages/classRoom/exercises/postItem.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MyClassExercisesPostDetailWidget extends StatefulWidget {
  final ExercisesResultDto exercise;
  final String fullName;
  final ExercisePostDto post;
  final int classSessionId;
  final List<int> role;
  MyClassExercisesPostDetailWidget({Key key, this.role, this.fullName, this.exercise, this.post, this.classSessionId}) : super(key: key);
  @override
  _MyClassExercisesPostDetailWidgetState createState() => _MyClassExercisesPostDetailWidgetState();
}

class _MyClassExercisesPostDetailWidgetState extends StateMVC<MyClassExercisesPostDetailWidget> with TickerProviderStateMixin {
  MyClassExcerciesController _con;
  _MyClassExercisesPostDetailWidgetState() : super(MyClassExcerciesController()) {
    _con = controller;
  }
  ScrollController _scrollController;
  ExercisePostDto _post;
  @override
  void initState() {
    super.initState();
    _con.pagePostDetailState(widget.post, widget.exercise, widget.role, widget.classSessionId);
    _post = widget.post;
  }

  @override
  void dispose() {
    _con.onExitSocketDetail(widget.classSessionId, widget.exercise.exerciseClassModuleId);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double statusbarHeight = MediaQuery.of(context).padding.top;

    Widget _postView() {
      return Container(
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        width: config.App.appWidth(100),
        child: Column(
          children: [
            ExercisePostItem(_post, _con, event: (type, postId) {
              print("type, postId $type, $postId");
              _con.getDetailsPostExercise(_post, widget.exercise.classId, type).then((value) => setState(() => _post = value));
            }),
          ],
        ),
      );
    }

    Widget _exImage() {
      if (widget.exercise.content == null || widget.exercise.content.length == 0) return Container();
      var _border = BorderSide(color: config.Colors.customeColor('#ffffff'), width: 1);
      var _width = config.App.appWidth(100);
      var _images = widget.exercise.content;
      var _index = 3;
      var _len = _images.length;
      var _w = _width / _index;
      var _left = _len == 1
          ? _w
          : _len == 2
              ? (_w / 2)
              : 0;
      //if (_images.length < 3) _index = _images.length;
      return Container(
        padding: EdgeInsets.only(top: 16, left: _left),
        height: _width / _index,
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        child: new ListView.builder(
          shrinkWrap: false,
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: _images.length,
          itemBuilder: (BuildContext context, int index) {
            var _item = _images[index];
            return Container(
              decoration: new BoxDecoration(
                border: new Border(
                  left: _border,
                  right: index == (_images.length - 1) ? _border.copyWith(color: config.Colors.customeColor('#ffffff')) : BorderSide.none,
                ),
              ),
              child: UrlImage(
                _item,
                'assets/icon/default-course.png',
                width: _width / _index,
                height: _width / _index,
                fit: BoxFit.cover,
              ),
            ).onTap(() async {
              FocusScope.of(context).requestFocus(FocusNode());
              await showDialog(
                context: context,
                builder: (_) => ImageDialogUrl(_item),
              );
            });
          },
        ),
      );
    }

    Widget _exView() {
      var _isHv = _con.role.indexOf(1) > -1;
      return Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: config.Colors.backgroundColor(),
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
            ),
            width: config.App.appWidth(100),
            padding: EdgeInsets.fromLTRB(30, 16, 30, 0),
            child: AccentText(
              widget.exercise.exerciseName,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: config.Colors.backgroundColor(),
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
            ),
            width: config.App.appWidth(100),
            padding: EdgeInsets.fromLTRB(30, 16, 30, 0),
            child: AccentText(
              widget.exercise.description,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
            ),
          ),
          _exImage(),
          _postView()
        ],
      );
    }

    Widget _bodyView() {
      if (_post == null) return LoadContent(warp: true);
      return SingleChildScrollView(
        controller: _scrollController,
        child: Container(
            decoration: BoxDecoration(
              color: config.Colors.inputBgColor(),
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
            ),
            child: _exView()
            // Column(
            //   children: [
            //     Container(
            //       decoration: BoxDecoration(
            //         color: config.Colors.backgroundColor(),
            //         borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
            //       ),
            //       width: config.App.appWidth(100),
            //       padding: EdgeInsets.fromLTRB(24, 16, 24, 0),
            //       child: AccentText(
            //         widget.exercise.description,
            //         style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
            //       ),
            //     ),
            //     Container(
            //       decoration: BoxDecoration(
            //         color: config.Colors.backgroundColor(),
            //       ),
            //       width: config.App.appWidth(100),
            //       padding: EdgeInsets.fromLTRB(24, 16, 24, 16),
            //       margin: EdgeInsets.only(bottom: 16),
            //     ),
            //     _postView()
            //   ],
            // ),
            ),
      );
    }

    return LayoutAppbarBgManager(
      scaffoldKey: _con.scaffoldKeyPost,
      bottom: 0,
      titleFull: false,
      isSingleChildScrollView: false,
      title: Container(
        alignment: Alignment.center,
        child: TitleText(
          "Học viên: ${widget.fullName}",
          style: TextStyle(
            color: config.Colors.customeColor("#ffffff"),
          ),
        ),
      ),
      main: Container(
        width: config.App.appWidth(100),
        alignment: Alignment.topLeft,
        child: _bodyView(),
      ),
    );
  }
}
