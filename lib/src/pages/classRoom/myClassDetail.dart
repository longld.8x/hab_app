import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/my_class_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/CourseCommon.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/UrlImage.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/elements/tab.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/pages/classRoom/details/description.dart';
import 'package:hab_app/src/pages/classRoom/details/members.dart';
import 'package:hab_app/src/pages/classRoom/details/notiTotal.dart';
import 'package:hab_app/src/pages/classRoom/details/session.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MyClassDetailWidget extends StatefulWidget {
  final RouteClassArgument routeArgument;
  MyClassDetailWidget({Key key, this.routeArgument}) : super(key: key);
  @override
  _MyClassDetailWidgetState createState() => _MyClassDetailWidgetState();
}

class _MyClassDetailWidgetState extends StateMVC<MyClassDetailWidget> with TickerProviderStateMixin {
  MyClassController _con;
  _MyClassDetailWidgetState() : super(MyClassController()) {
    _con = controller;
  }
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  ScrollController _scrollController;
  int _classId = 0;
  bool _btnPositionBottom = false;
  TabController _tabController;

  List<Tab> _listTab;
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    _classId = widget.routeArgument.classId;
    _con.pageDetailState(_classId);
    initTab();
    initScroll();
  }

  @override
  void dispose() {
    _con.onExitSocket();
    super.dispose();
  }

  bool isEnable() {
    var _offline = (_con.nextSession != null && _con.nextSession.sessionType == 2 && _con.nextSession.joined == false && (_con.dataDetail.classRoles.indexOf(1) > -1));
    var _online = (_con.nextSession != null && _con.nextSession.sessionType == 1 && (_con.zoomDto != null && _con.zoomDto.meetingId.isNotEmpty));
    return _con.nextSession != null && _con.nextSession.sessionStatus == 1 && (_offline || _online);
  }

  initTab() {
    _listTab = [
      TabTitle("THÔNG TIN"),
      TabTitle("BÀI HỌC"),
      TabTitle("HỌC VIÊN"),
    ];
    _tabController = TabController(length: _listTab.length, vsync: this);
    _tabController.index = 0;
    _selectedIndex = _tabController.index;
    _tabController.addListener(() {
      setState(() => _selectedIndex = _tabController.index);
    });
  }

  @override
  Widget build(BuildContext context) {
    var _width = config.App.appWidth(100);
    // co quyen trong lop này
    var _inClassRoom = (_con.dataDetail != null && _con.dataDetail.classRoles != null && _con.dataDetail.classRoles.length > 0);
    // Widget _buttonHoc() {
    //   if (!_inClassRoom) return Container();
    //   if (_con.nextSession.sessionType == 2) {
    //     if (_con.dataDetail.classRoles.indexOf(1) > -1 && _con.nextSession.joined == false) {
    //       return Button(
    //         text: "Tham gia học offline",
    //         onPressed: () {
    //           _con.checkInOffline().then((ResponseMessage result) {
    //             if (result != null && result.success) {
    //               setState(() => _con.nextSession.joined = true);
    //               Helper.successMessenge('Đã xác nhận tham gia buổi học');
    //             } else {
    //               setState(() => _con.nextSession.joined = false);
    //               Helper.infoMessenge(result.message);
    //             }
    //           });
    //         },
    //       );
    //     }
    //
    //     return Container();
    //   }
    //   return Button(
    //     text: _con.dataDetail.classRoles.indexOf(2) > -1
    //         ? "Dạy bài"
    //         : _con.dataDetail.classRoles.indexOf(1) > -1
    //             ? "Học bài"
    //             : "Tham gia",
    //     onPressed: () {
    //       if (_con.zoomDto == null || _con.zoomDto.meetingId.isEmpty) {
    //         Helper.infoMessenge("Không có thông tin lớp học");
    //         return;
    //       }
    //
    //       var argument = RouteClassArgument(
    //         classId: _classId,
    //         sessionId: _con.nextSession.sessionId,
    //         param: _con.zoomDto,
    //       );
    //       setState(() => _con.nextSession.joined = true);
    //       Navigator.of(context).pushNamed('/MyClass/Zoom', arguments: argument);
    //     },
    //   );
    // }
    //

    Widget _buttonOffline() {
      return Button(
        text: "Tham gia học offline",
        onPressed: () {
          _con.checkInOffline().then((ResponseMessage result) {
            if (result != null && result.success) {
              setState(() => _con.nextSession.joined = true);
              Helper.successMessenge('Đã xác nhận tham gia buổi học');
            } else {
              setState(() => _con.nextSession.joined = false);
              Helper.infoMessenge(result.message);
            }
          });
        },
      );
    }

    Widget _buttonOnline() {
      return Button(
        text: _con.dataDetail.classRoles.indexOf(5) > -1
            ? "Bắt đầu"
            : _con.dataDetail.classRoles.indexOf(2) > -1
                ? "Dạy bài"
                : _con.dataDetail.classRoles.indexOf(1) > -1
                    ? "Học bài"
                    : "Tham gia",
        onPressed: () {
          if (_con.zoomDto == null || _con.zoomDto.meetingId.isEmpty) {
            Helper.infoMessenge("Không có thông tin lớp học!");
            return;
          }
          setState(() => _con.nextSession.joined = true);
          var argument = widget.routeArgument;
          argument.param = _con.zoomDto;
          argument.role = _con.dataDetail.classRoles;
          Navigator.of(context).pushNamed('/MyClass/Zoom', arguments: argument);
        },
      );
    }

    Widget _buttonJoin() {
      var _offline = (_con.nextSession != null && _con.nextSession.sessionType == 2 && _con.nextSession.joined == false && (_con.dataDetail.classRoles.indexOf(1) > -1));
      var _online = (_con.nextSession != null && _con.nextSession.sessionType == 1 && (_con.zoomDto != null && _con.zoomDto.meetingId.isNotEmpty));
      print("_offline $_offline _online $_online");
      if (_offline || _online)
        return Container(
          height: 80,
          width: config.App.appWidth(100),
          padding: EdgeInsets.fromLTRB(24, 10, 24, 16),
          child: _offline ? _buttonOffline() : _buttonOnline(),
        );
      return Container();
    }

    Widget _buttonJoinBottom() {
      if (_btnPositionBottom == false) return Container();
      if (!isEnable()) return Container();
      var _offline = (_con.nextSession != null && _con.nextSession.sessionType == 2 && _con.nextSession.joined == false && (_con.dataDetail.classRoles.indexOf(1) > -1));
      var _online = (_con.nextSession != null && _con.nextSession.sessionType == 1 && (_con.zoomDto != null && _con.zoomDto.meetingId.isNotEmpty));
      if (_offline || _online)
        return Container(
          width: config.App.appWidth(100),
          decoration: BoxDecoration(
            color: Colors.transparent,
          ),
          padding: EdgeInsets.fromLTRB(24, 0, 24, 24),
          child: _offline ? _buttonOffline() : _buttonOnline(),
        );
      return Container();
    }

    Map _titleInfo() {
      var _width = config.App.appWidth(100);
      var _height = 0.0;
      var _heightImg = _width * (281 / 375);
      var _sliderImages = Container(
        height: _heightImg,
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: Image.asset('assets/icon/course_detail.png', fit: BoxFit.fill),
      );
      if (_con.dataDetail.images.length > 0)
        _sliderImages = Container(
          height: _heightImg,
          child: PageView.builder(
            itemCount: _con.dataDetail.images.length,
            itemBuilder: (context, index) {
              return UrlImage(
                _con.dataDetail.images[index],
                'assets/icon/default-course.png',
                width: config.App.appWidth(100),
                height: _heightImg,
                fit: BoxFit.cover,
                radius: 0,
              );
            },
          ),
        );
      List<Widget> _infoWidget = [];
      _infoWidget.add(_sliderImages);
      _height += _heightImg;
      _infoWidget.add(Container(padding: EdgeInsets.fromLTRB(20, 19, 20, 16), child: CourseNameRankBox(_con.dataDetail.className, 5, "detail")));
      var _heightName = Helper.getTextLineHeight(
        _con.dataDetail.className * 1, //+ "xxxx",
        Theme.of(context).textTheme.headline1.copyWith(color: config.Colors.accentColor(), fontWeight: FontWeight.w500),
        (_width - 40),
      );
      _height += (_heightName + 19 + 16);
      var _heightRow = Helper.getTextLineHeight(
        "1 line",
        Theme.of(context).textTheme.bodyText1.copyWith(fontWeight: FontWeight.w400, fontSize: 16),
        (_width - 40),
      );

      if (_inClassRoom && _con.nextSession != null && (_con.nextSession.sessionId != null && _con.nextSession.sessionId > 0)) {
        _infoWidget.add(Container(
          padding: EdgeInsets.fromLTRB(20, 0, 20, 16),
          child: AccentText(_con.nextSession.sessionName, style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16)),
        ));
        var _heightSSName = Helper.getTextLineHeight(
            _con.nextSession.sessionName,
            Theme.of(context).textTheme.bodyText1.copyWith(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                ),
            (_width - 40));
        _height += (_heightSSName + 0 + 16);
        _infoWidget.add(CourseRowInfo('assets/icon/clock.svg', Helper.showTime(_con.nextSession.startDate, _con.nextSession.endDate)));
        _height += (_heightRow + 0 + 16);
        var _typeStr = _con.nextSession.sessionType == 1 ? 'Học Online' : (_con.nextSession.sessionType == 2 ? 'Học Offline' : 'Học Online');
        _infoWidget.add(CourseRowInfo('assets/icon/task.svg', _typeStr));
        _height += (_heightRow + 0 + 16);
        // địa chỉ
        if (_con.nextSession.sessionType == 2) {
          _infoWidget.add(CourseRowInfo('assets/icon/pin.svg', _con.nextSession.address));
          var _heightAddress = Helper.getTextLineHeight(
              "icon " + _con.nextSession.address,
              Theme.of(context).textTheme.bodyText1.copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
              (_width - 40));

          _height += (_heightAddress + 0 + 16);
        }
      } else {
        // if (_con.dataDetail.status == 6) {
        _infoWidget.add(CourseRowInfo('assets/icon/clock.svg', Helper.showDate(_con.dataDetail.fromDate, _con.dataDetail.toDate)));
        _height += (_heightRow + 0 + 16);
        _infoWidget.add(CourseRowInfo('assets/icon/task.svg', '${_con.dataDetail.sessionTotal} bài'));
        _height += (_heightRow + 0 + 16);
        _infoWidget.add(CourseRowInfo('assets/icon/user.svg', _con.dataDetail.classOwner));
        _height += (_heightRow + 0 + 16);
      }

      // btn học bài
      if (_btnPositionBottom == false && isEnable()) {
        _infoWidget.add(_buttonJoin());
        _height += 80;
      }

      _infoWidget.add(Container(height: 16, decoration: BoxDecoration(color: config.Colors.inputBgColor())));
      _height += (16);
      return {
        "widget": new Container(
          decoration: BoxDecoration(
            color: config.Colors.backgroundColor(),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: _infoWidget,
          ),
        ),
        "height": _height
      };
    }

    Widget _bodyView() {
      if (_con.isLoading) return LoadContent(warp: true);
      if (_con.dataDetail == null) return NoContent(warp: true);
      var _title = _titleInfo();
      return Stack(
        alignment: Alignment.bottomLeft,
        children: <Widget>[
          DefaultTabController(
            length: 3,
            child: NestedScrollView(
              controller: _scrollController,
              physics: const BouncingScrollPhysics(),
              headerSliverBuilder: (_, __) {
                return <Widget>[
                  SliverAppBar(
                    expandedHeight: _title['height'],
                    flexibleSpace: FlexibleSpaceBar(
                      background: _title['widget'],
                      collapseMode: CollapseMode.pin,
                    ),
                    leading: Container(),
                    titleSpacing: 0,
                  ),
                  SliverPersistentHeader(
                    delegate: SliverAppBarDelegate(
                      TabBar(
                        indicatorColor: config.Colors.primaryColor(),
                        labelColor: config.Colors.primaryColor(),
                        unselectedLabelColor: config.Colors.accentColor(),
                        tabs: _listTab,
                      ),
                    ),
                    pinned: true,
                  ),
                ];
              },
              body: Container(
                child: TabBarView(
                  children: [
                    TabViewBox(CourseDescription(_con), isEnable()),
                    TabViewBox(
                        CourseSession(_con, _classId, event: (String _typeEvent, bool _status) {
                          print("CourseExercises child event $_typeEvent, $_status");
                          if (_typeEvent == "join") {
                            setState(() => _con.nextSession.joined = _status);
                          }
                        }),
                        isEnable()),
                    TabViewBox(CourseMembers(_con, _classId), isEnable()),
                  ],
                ),
              ),
            ),
          ),
          _buttonJoinBottom(),
        ],
        // ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyDetail,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Thông tin khóa học"),
        actions: [
          NotiClassRoomWidget(
            chatRoomId: _con.dataDetail?.chatRoomId ?? 0,
            total: _con.myClassNotiTotal,
          ),
        ],
      ),
      main: _bodyView(),
    );
  }

  initScroll() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.offset >= 500 && _btnPositionBottom == false)
        setState(() => _btnPositionBottom = true);
      else if (_scrollController.offset < 500 && _btnPositionBottom == true) setState(() => _btnPositionBottom = false);
    });
  }
}
