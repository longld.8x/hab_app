import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/my_class_controller.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/pages/classRoom/classWidget.dart';
import 'package:hab_app/src/pages/home/HomeBox.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MyClassHomeBox extends StatefulWidget {
  const MyClassHomeBox({Key key}) : super(key: key);
  _MyClassHomeBoxState createState() => _MyClassHomeBoxState();
}

class _MyClassHomeBoxState extends StateMVC<MyClassHomeBox> {
  MyClassController _con;
  _MyClassHomeBoxState() : super(MyClassController()) {
    _con = controller;
  }
  @override
  void initState() {
    _con.homeWidgetState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_con.isLoading ||
        _con.dataMyClass == null ||
        _con.dataMyClass.length == 0) return Container();
    return HomeBox([
      HomeTitleBox(
        "Lớp học của tôi",
        action: () {
          Navigator.of(context).pushNamed('/MyClass');
        },
      ),
      Container(
        padding: EdgeInsets.only(left: 0, right: 0),
        color: config.Colors.inputBgColor(),
        child: new ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) =>
              ClassWidget(model: _con.dataMyClass[index]),
          itemCount: _con.dataMyClass.length,
        ),
      ),
    ]);
  }
}
