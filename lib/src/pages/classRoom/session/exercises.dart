import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/my_class_session_controller.dart';
import 'package:hab_app/src/elements/CourseCommon.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:intl/intl.dart';

class CourseExercises extends StatefulWidget {
  final MyClassSessionController con;
  const CourseExercises(this.con, {Key key}) : super(key: key);
  _CourseExercisesState createState() => _CourseExercisesState();
}

class _CourseExercisesState extends State<CourseExercises> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _sesstionDone = widget.con.dataDetail.sessionStatus == 2;
    Widget listBuilder() {
      if (widget.con.isLoadingExcercies) return LoadContent(warp: true);
      if (!_sesstionDone) return NoContent(warp: true, text: "Trạng thái buổi học không phù hợp");
      if (widget.con.dataExcercies == null || widget.con.dataExcercies.length == 0) return NoContent(warp: true);

      var _hlvO = (widget.con.role.indexOf(2) > -1 || widget.con.role.indexOf(5) > -1);
      return Container(
        padding: EdgeInsets.only(top: 0, left: 0, right: 0),
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
        ),
        child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: widget.con.dataExcercies.length,
          itemBuilder: (_context, i) {
            var _item = widget.con.dataExcercies[i];

            return InkWell(
              onTap: () {
                if (_sesstionDone) {
                  Navigator.of(context).pushNamed(
                    '/MyClass/Exercise/Detail',
                    arguments: RouteClassArgument(
                      classId: widget.con.classId,
                      sessionId: widget.con.sessionId,
                      classSessionId: widget.con.classSessionId,
                      exerciseClassModuleId: _item.exerciseClassModuleId,
                      role: widget.con.role,
                      param: _item,
                    ),
                  );
                } else {
                  Helper.errorMessenge('Trạng thái bài học không phù hợp.');
                }
              },
              child: Container(
                padding: EdgeInsets.fromLTRB(16, 12, 16, 12),
                decoration: BoxDecoration(
                  color: config.Colors.backgroundColor(),
                ),
                margin: EdgeInsets.only(bottom: 1),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.fromLTRB(0, 0, 0, _hlvO ? 0 : 16),
                            child: TextTagBox(
                              _item.exerciseName,
                              _hlvO ? Container() : LabelBoxEx(_item.exerciseStatusView, _item.exerciseStatus),
                            ),
                          ),
                          _hlvO
                              ? Container()
                              : Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        child: SvgPicture.asset(
                                          'assets/icon/clock2.svg',
                                          width: 15,
                                          height: 15,
                                        ),
                                        padding: EdgeInsets.fromLTRB(0, 0, 6, 0),
                                      ),
                                      Expanded(
                                        child: _item.deadlineTime == null
                                            ? Container()
                                            : RowWidget(
                                                SubText(
                                                  "Trả bài: ",
                                                  style: TextStyle(color: config.Colors.accentColor(), fontSize: 14),
                                                ),
                                                SubText(
                                                  new DateFormat('HH:mm dd/MM/yyyy').format(_item.deadlineTime),
                                                  style: TextStyle(color: config.Colors.primaryColor(), fontSize: 14),
                                                ),
                                                padding: 0,
                                              ),
                                      ),
                                    ],
                                  ),
                                ),
                        ],
                      ),
                    ),
                    (_sesstionDone)
                        ? Container(
                            width: 32,
                            height: 32,
                            child: Icon(
                              Icons.arrow_forward_ios,
                              size: 16,
                            ),
                          )
                        : Container()
                  ],
                ),
              ),
            );
          },
        ),
      );
    }

    return Container(
      decoration: BoxDecoration(
        color: config.Colors.backgroundColor(),
      ),
      child: listBuilder(),
    );
  }
}
