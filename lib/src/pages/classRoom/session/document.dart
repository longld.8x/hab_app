import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:hab_app/src/controllers/my_class_session_controller.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class CourseDocument extends StatefulWidget {
  final MyClassSessionController con;
  const CourseDocument(this.con, {Key key}) : super(key: key);
  _CourseDocumentState createState() => _CourseDocumentState();
}

class _CourseDocumentState extends State<CourseDocument> {
  @override
  void initState() {
    super.initState();
  }

  Future<String> _findLocalPath() async {
    final directory = Platform.isAndroid == TargetPlatform.android ? await (getExternalStorageDirectory() as FutureOr<Directory>) : await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<String> _requestDownload(String url) async {
    if (await Permission.storage.request().isGranted) {
      // var _localPath = (await _findLocalPath()) + Platform.pathSeparator + 'download';
      // var dir = Directory(_localPath);
      // bool dirExists = await dir.exists();
      // if (!dirExists) {
      //   await dir.create();
      // }
      Directory tempDir = await getApplicationDocumentsDirectory();
      var _localPath = tempDir.path + Platform.pathSeparator + 'hab';
      var dir = Directory(_localPath);
      bool dirExists = await dir.exists();
      if (!dirExists) {
        await dir.create();
      }
      print("dir.path ${dir.path}");
      return await FlutterDownloader.enqueue(
        url: url,
        savedDir: dir.path,
        showNotification: true,
        openFileFromNotification: true,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget listBuilder() {
      if (widget.con.isLoadingDocument) return LoadContent(warp: true);
      if (widget.con.dataDocuments == null || widget.con.dataDocuments.length == 0) return NoContent(warp: true);

      return Container(
        padding: EdgeInsets.only(top: 0, left: 0, right: 0),
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
        ),
        child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: widget.con.dataDocuments.length,
          itemBuilder: (_context, i) {
            var _item = widget.con.dataDocuments[i];

            return InkWell(
              onTap: () {
                _requestDownload(_item.contentUrl);
                Helper.successMessenge('Đang tải file...');
              },
              child: Container(
                padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
                decoration: BoxDecoration(
                  color: config.Colors.backgroundColor(),
                ),
                margin: EdgeInsets.only(bottom: 1),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      child: _item.docTypeView,
                      padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                    ),
                    Expanded(
                      child: Container(
                        child: AccentText(
                          _item.documentName,
                          style: TextStyle(color: config.Colors.accentColor(), fontSize: 14),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      );
    }

    return Container(
      decoration: BoxDecoration(
        color: config.Colors.backgroundColor(),
      ),
      child: listBuilder(),
    );
  }
}
