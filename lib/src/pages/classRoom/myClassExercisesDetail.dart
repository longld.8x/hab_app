import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/my_class_excercies_controller.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/ImagePick.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/UrlImage.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/models/exercisePostDto.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/pages/classRoom/exercises/exercisesAdd.dart';
import 'package:hab_app/src/pages/classRoom/exercises/postDetail.dart';
import 'package:hab_app/src/pages/classRoom/exercises/postItem.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MyClassExercisesDetailWidget extends StatefulWidget {
  final RouteClassArgument routeArgument;
  MyClassExercisesDetailWidget({Key key, this.routeArgument}) : super(key: key);
  @override
  _MyClassExercisesDetailWidgetState createState() => _MyClassExercisesDetailWidgetState();
}

class _MyClassExercisesDetailWidgetState extends StateMVC<MyClassExercisesDetailWidget> with TickerProviderStateMixin {
  MyClassExcerciesController _con;
  _MyClassExercisesDetailWidgetState() : super(MyClassExcerciesController()) {
    _con = controller;
  }
  TextEditingController _ctrlSearch = TextEditingController();
  ScrollController _scrollController;
  ScrollController _scrollController2;
  bool _buttonBottom = false;
  bool _searchOpen = false;
  List<int> _role = <int>[];
  TabController _tabController;
  List<Tab> _listTab;

  ///
  List<Map> tabs;
  int _tabIndex = 0;
  @override
  void initState() {
    super.initState();
    _con.pageState(widget.routeArgument);
    _role = widget.routeArgument.role;
    _con.getPostExercises(_ctrlSearch.text, 0, loading: false);
    _con.getMyExercises(_ctrlSearch.text, 0, loading: false);
    initTabs();
    initScroll();
  }

  @override
  void dispose() {
    if (_con.dataDetail != null) _con.onExitSocket(_con.classSessionId, _con.dataDetail?.exerciseClassModuleId);
    _tabController.dispose();
    super.dispose();
  }

  initTabs() {
    tabs = [
      {
        'text': 'Tất cả',
        'icon': 'all_ex',
      },
      {
        'text': 'Của tôi',
        'icon': 'my_ex',
      },
    ];
    _tabController = TabController(length: tabs.length, vsync: this);
    _tabController.addListener(() async => setState(() => _tabIndex = _tabController.index));
  }

  initScroll() async {
    _scrollController = ScrollController();
    _scrollController2 = ScrollController();
    _scrollController.addListener(() async {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset && (_con.serverTotalCount > ((_con.currentPage + 1) * _con.itemPage))) {
        await _con.getPostExercises(_ctrlSearch.text, _con.currentPage + 1);
      }
    });
    _scrollController2.addListener(() async {
      if (_scrollController2.position.maxScrollExtent == _scrollController2.offset && (_con.serverTotalCount2 > ((_con.currentPage2 + 1) * _con.itemPage2))) {
        await _con.getMyExercises(_ctrlSearch.text, _con.currentPage2 + 1);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final double statusbarHeight = MediaQuery.of(context).padding.top;

    Widget _buttonHoc() {
      return Button(
        text: "Làm bài tập",
        onPressed: () async {
          var _param = widget.routeArgument;
          // ko dùng nữa vì đã dùng socket
          // _param.event = () async {
          //   setState(() {
          //     _ctrlSearch.text = '';
          //   });
          //   await _con.getPostExercises(_ctrlSearch.text, 0);
          // };
          Navigator.of(context).push(
            new MaterialPageRoute(
              builder: (BuildContext context) => ExercisePostAddWidget(
                routeArgument: _param,
              ),
            ),
          );
          // Navigator.of(context).pushNamed('/MyClass/Exercise/Post/Add', arguments: _param);
        },
      );
    }

    Widget _postView(String type) {
      if (_con.isLoading)
        return Container(
          padding: EdgeInsets.only(top: 0, left: 0, right: 0),
          decoration: BoxDecoration(
            color: config.Colors.backgroundColor(),
          ),
          width: config.App.appWidth(100),
          height: 500,
          child: LoadContent(warp: true),
        );

      var _allData = (type == 'all' && (_con.dataPostExercises == null || _con.dataPostExercises.length == 0));
      var _myData = (type == 'my' && (_con.dataMyExercises == null || _con.dataMyExercises.length == 0));

      if (_allData || _myData)
        return Container(
          padding: EdgeInsets.only(top: 0, left: 0, right: 0),
          decoration: BoxDecoration(
            color: config.Colors.backgroundColor(),
          ),
          width: config.App.appWidth(100),
          height: 500,
          child: NoContent(warp: true),
        );
      var _data = type == 'all' ? _con.dataPostExercises : _con.dataMyExercises;
      return Container(
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        width: config.App.appWidth(100),
        child: Column(
            children: _data.map((e) {
          return new ExercisePostItem(
            e,
            _con,
            event: (type, postId) {
              _con.getPostExercisesById(postId, type);
            },
            eventNext: (ExercisePostDto _p) {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => MyClassExercisesPostDetailWidget(
                        exercise: _con.dataDetail,
                        classSessionId: _con.classSessionId,
                        post: _p,
                        role: _con.role,
                        fullName: _p.user.fullName,
                      )));
            },
          );
        }).toList()),
      );
    }

    Widget _exImage() {
      print("_con.dataDetail.content ${_con.dataDetail.content}");
      if (_con.dataDetail.content == null || _con.dataDetail.content.length == 0) return Container();
      var _border = BorderSide(color: config.Colors.customeColor('#ffffff'), width: 1);
      var _width = config.App.appWidth(100);
      var _images = _con.dataDetail.content;
      var _index = 3;
      var _len = _images.length;
      var _w = _width / _index;
      var _left = _len == 1
          ? _w
          : _len == 2
              ? (_w / 2)
              : 0;
      //if (_images.length < 3) _index = _images.length;
      return Container(
        padding: EdgeInsets.only(top: 16, left: _left),
        height: _width / _index,
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        child: new ListView.builder(
          shrinkWrap: false,
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: _images.length,
          itemBuilder: (BuildContext context, int index) {
            var _item = _images[index];
            return Container(
              decoration: new BoxDecoration(
                border: new Border(
                  left: _border,
                  right: index == (_images.length - 1) ? _border.copyWith(color: config.Colors.customeColor('#ffffff')) : BorderSide.none,
                ),
              ),
              child: UrlImage(
                _item,
                'assets/icon/default-course.png',
                width: _width / _index,
                height: _width / _index,
                fit: BoxFit.cover,
              ),
            ).onTap(() async {
              FocusScope.of(context).requestFocus(FocusNode());
              await showDialog(
                context: context,
                builder: (_) => ImageDialogUrl(_item),
              );
            });
          },
        ),
      );
    }

    Widget _exView(String type) {
      var _isHv = _con.role.indexOf(1) > -1;
      return Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: config.Colors.backgroundColor(),
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
            ),
            width: config.App.appWidth(100),
            padding: EdgeInsets.fromLTRB(30, 16, 30, 0),
            child: AccentText(
              _con.dataDetail.exerciseName,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: config.Colors.backgroundColor(),
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
            ),
            width: config.App.appWidth(100),
            padding: EdgeInsets.fromLTRB(30, 16, 30, 0),
            child: AccentText(
              _con.dataDetail.description,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
            ),
          ),
          _exImage(),
          Container(
            decoration: BoxDecoration(
              color: config.Colors.backgroundColor(),
            ),
            width: config.App.appWidth(100),
            padding: EdgeInsets.fromLTRB(24, 16, 24, _isHv ? 16 : 0),
            child: (_isHv) ? _buttonHoc() : Container(),
            margin: EdgeInsets.only(bottom: 16),
          ),
          _postView(type)
        ],
      );
    }

    Widget _allView() {
      if (_con.isLoading) {
        return LoadContent(warp: true);
      }
      return SingleChildScrollView(
        controller: _scrollController,
        child: Container(
          decoration: BoxDecoration(
            color: config.Colors.inputBgColor(),
            borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
          ),
          child: _exView('all'),
        ),
      );
    }

    Widget _myView() {
      if (_con.isLoading) {
        return LoadContent(warp: true);
      }
      return SingleChildScrollView(
        controller: _scrollController,
        child: Container(
          decoration: BoxDecoration(
            color: config.Colors.inputBgColor(),
            borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
          ),
          child: _exView('my'),
        ),
      );
    }

    Widget _tabTitle() {
      var wg = tabs.map((e) {
        var _active = _tabIndex == tabs.indexOf(e);
        return Tab(
          child: Container(
            //width: config.App.appWidth(100),
            // decoration: BoxDecoration(
            //   border: Border(right: _index == 0 ? BorderSide(width: 1, color: config.Colors.customeColor('#E0E9F8')) : BorderSide.none),
            // ),
            alignment: Alignment.center,
            child: IconWidget(
              Container(
                //margin: EdgeInsets.only(top: 2),
                child: SvgPicture.asset(
                  'assets/icon/${e['icon']}${_active ? '_active' : ''}.svg',
                  width: 18,
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 3),
                child: AccentText(
                  e['text'],
                  style: TextStyle(
                    fontSize: 16,
                    color: _active ? config.Colors.primaryColor() : config.Colors.customeColor('#A7A7A7'),
                  ),
                ),
              ),
              padding: 10,
            ),
          ),
        );
      }).toList();
      return Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
        //padding: EdgeInsets.fromLTRB(24, 0, 24, 0),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
          borderRadius: BorderRadius.all(Radius.circular(40)),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 2),
              color: Color(0xFF000000).withOpacity(0.16),
              spreadRadius: 1,
              blurRadius: 4,
            ),
          ],
        ),
        child: TabBar(
          indicatorColor: Colors.transparent,
          labelColor: config.Colors.primaryColor(),
          unselectedLabelColor: config.Colors.hintColor(),
          indicatorWeight: 4.0,
          labelStyle: TextStyle(fontWeight: FontWeight.w400),
          controller: _tabController,
          tabs: wg,
        ),
      );
    }

    pageTitle() {
      if (!_searchOpen) return _tabTitle();
      return Container(
        padding: EdgeInsets.only(top: 8, right: 16, left: 16, bottom: 8),
        child: TextFormField(
          controller: _ctrlSearch,
          autofocus: true,
          decoration: InputDecoration(
            filled: true,
            fillColor: config.Colors.backgroundColor(),
            labelText: null,
            hintText: "Nhập tên hoặc số điện thoại cộng sự",
            prefixIcon: Icon(
              Icons.search,
              color: config.Colors.hintColor(),
              size: 18,
            ),
            suffixIcon: IconButton(
                icon: PrimaryText(
                  'Hủy',
                  style: TextStyle(fontSize: 12),
                ),
                onPressed: () {
                  setState(() {
                    // _ctrlSearch.text = '';
                    _searchOpen = false;
                  });
                  //_con.getPostExercises(_ctrlSearch.text, 0);
                }),
            contentPadding: EdgeInsets.symmetric(horizontal: 1, vertical: 16),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(40.0), borderSide: BorderSide(width: 1, color: config.Colors.primaryColor())),
            focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(40.0), borderSide: BorderSide(width: 1, color: config.Colors.primaryColor())),
            enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(40.0), borderSide: BorderSide(width: 1, color: config.Colors.primaryColor())),
          ),
          style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor()),
          onEditingComplete: () {
            _con.getPostExercises(_ctrlSearch.text, 0);
          },
        ),
      );
    }

    return LayoutAppbarBgManager(
      scaffoldKey: _con.scaffoldKey,
      bgImage: _searchOpen ? "#ffffff" : "assets/icon/profile.png",
      bottom: 0,
      titleFull: _searchOpen,
      isSingleChildScrollView: false,
      title: pageTitle(),
      titleRight: IconButton(
        icon: SvgPicture.asset('assets/icon/search_w.svg'),
        onPressed: () {
          setState(() => _searchOpen = true);
          print("search");
        },
      ),
      main: Container(
        width: config.App.appWidth(100),
        alignment: Alignment.topLeft,
        child: Stack(
          children: <Widget>[
            Container(
              child: TabBarView(
                controller: _tabController,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  _allView(),
                  _myView(),
                ],
              ),
            ),
            //_tabTitle(),
          ],
        ),
      ),
    );
  }
}
