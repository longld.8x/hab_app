import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/work_controller.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/import.dart';
import 'package:hab_app/src/models/exercisesResultDto.dart';
import 'package:hab_app/src/models/myClassDetailDto.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/models/workDto.dart';
import 'package:hab_app/src/repositories/class_room_repository.dart' as classRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

/// WorkListWidget
class WorkListWidget extends StatefulWidget {
  const WorkListWidget({Key key}) : super(key: key);
  @override
  _WorkListWidgetState createState() => _WorkListWidgetState();
}

class _WorkListWidgetState extends StateMVC<WorkListWidget> {
  WorkController _con;
  _WorkListWidgetState() : super(WorkController()) {
    _con = controller;
  }
  ScrollController _scrollController;
  @override
  void initState() {
    _con.pageState();
    initScroll();
    super.initState();
  }

  initScroll() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset && (_con.serverTotalCount > ((_con.currentPage + 1) * _con.itemPage))) {
        _con.loadData(_con.currentPage + 1);
      }
    });
  }

  initCtrl() {
    _scrollController = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    /// title day
    Widget _dayTitle(DateTime day) {
      var _t = '';
      switch (day.weekday) {
        case 0:
        case 7:
          _t = "Chủ nhật";
          break;
        case 1:
          _t = "Thứ hai";
          break;
        case 2:
          _t = "Thứ ba";
          break;
        case 3:
          _t = "Thứ tư";
          break;
        case 4:
          _t = "Thứ năm";
          break;
        case 5:
          _t = "Thứ sáu";
          break;
        case 6:
          _t = "Thứ bảy";
          break;
      }
      return Container(
        padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
        child: TitleText(
          '${_t}, ${day.day}/${day.month}/${day.year}',
          style: TextStyle(fontSize: 14),
        ),
        alignment: Alignment.centerLeft,
      );
    }

    Widget _textContent(String text) {
      return Text(
        (text ?? ""),
        style: Theme.of(context).textTheme.subtitle1.copyWith(
              color: config.Colors.customeColor('#171051').withOpacity(0.65),
              fontSize: 12,
              fontWeight: FontWeight.w400,
            ),
      );
    }

    Widget _textDate(String text) {
      return Text(
        (text ?? ""),
        style: Theme.of(context).textTheme.subtitle1.copyWith(
              color: config.Colors.customeColor('#171051').withOpacity(0.65),
              fontSize: 12,
              fontWeight: FontWeight.w400,
            ),
        maxLines: 1,
      );
    }

    Widget _textStatus(String text) {
      return Text(
        (text ?? ""),
        style: Theme.of(context).textTheme.subtitle1.copyWith(
              color: config.Colors.primaryColor(),
              fontSize: 12,
              fontWeight: FontWeight.w400,
            ),
        maxLines: 1,
      );
    }

    /// workday
    Widget _workView(WorkDto dto) {
      var _now = DateTime.now();
      var _isPass = dto.startDate != null && dto.startDate.difference(_now).inMilliseconds > 0;
      var _isDoing = dto.startDate != null && dto.dueDate != null && dto.startDate.difference(_now).inMilliseconds > 0 && dto.dueDate.difference(_now).inMilliseconds < 0;
      return Container(
        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
          border: new Border(
            bottom: BorderSide(color: config.Colors.inputBgColor(), width: 1),
          ),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  child: Icon(Icons.circle, size: 9, color: (_isPass ? config.Colors.primaryColor() : config.Colors.hintColor())),
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.fromLTRB(0, 0, 5, 5),
                ),
                Expanded(
                  child: Container(
                    child: AccentText(dto.taskName, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                    padding: EdgeInsets.only(bottom: 8),
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  child: Column(
                    children: [
                      (_isDoing)
                          ? Container(
                              child: _textStatus("Đang diễn ra"),
                              alignment: Alignment.topCenter,
                              padding: EdgeInsets.only(bottom: 4),
                            )
                          : Container(),
                      Container(
                        child: _textDate("${Helper.dateString(dto.startDate.hour)}:${Helper.dateString(dto.startDate.minute)} - ${Helper.dateString(dto.dueDate?.hour)}:${Helper.dateString(dto.dueDate?.minute)} "),
                        alignment: Alignment.bottomCenter,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  width: 15,
                  alignment: Alignment.topCenter,
                ),
                Container(
                  width: config.App.appWidth(100) - 15 - 16 * 2,
                  child: _textContent(dto.description),
                  padding: EdgeInsets.only(bottom: 6),
                )
              ],
            ),
          ],
        ),
      ).onTap(() async {
        print("xxxxxxxxxx ${dto.taskType} ${dto.taskType == "Study"}");
        if (dto.taskType == "Study" || dto.taskType == "Coaching") {
          print("0000000000000000000");
          Navigator.of(context).pushNamed('/MyClass/Detail', arguments: RouteClassArgument(classId: dto.classId));
        } else if (dto.taskType == "Exercise" || dto.taskType == "CheckingExercise") {
          var _classRespon = await classRepo.getMyClassDetail(dto.classId);
          var _class = _classRespon.success ? _classRespon.results as MyClassDetailDto : null;
          var _exercisesRespon = await classRepo.getMyClassExcercieDetail(dto.exerciseClassModuleId);
          var _exercises = _exercisesRespon.success ? _exercisesRespon.results as ExercisesResultDto : null;

          Navigator.of(context).pushNamed('/MyClass/Exercise/Detail',
              arguments: RouteClassArgument(
                classId: dto.classId,
                sessionId: dto.sessionId,
                classSessionId: dto.classSessionId,
                role: _class != null ? _class.classRoles : [],
                exerciseClassModuleId: dto.exerciseClassModuleId,
                param: _exercises != null ? _exercises : null,
              ));
        }
        print("xxxxx $dto");
      });
    }

    Widget _dayView(DayWorkDto dayData) {
      var _day = dayData.day;
      var _works = dayData.works;
      if (_works == null || _works.length == 0) return Container();
      //
      List<Widget> _wWg = [];
      for (var _w in _works) {
        _wWg.add(_workView(_w));
      }
      _wWg.insert(0, _dayTitle(_day));
      //
      return Container(
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
        ),
        child: Column(
          children: _wWg,
        ),
      );
    }

    Widget _bodyView() {
      if (_con.isLoading) return LoadContent(warp: true);
      if (_con.dayWorks == null || _con.dayWorks.length == 0) return NoContent(warp: true, text: "Không có công việc mới");

      return new ListView.builder(
        shrinkWrap: true,
        controller: _scrollController,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) => _dayView(_con.dayWorks[index]),
        itemCount: _con.dayWorks.length,
        padding: EdgeInsets.all(0),
      );
    }

    return Container(
      width: config.App.appWidth(100),
      child: _bodyView(),
    );
  }
}
