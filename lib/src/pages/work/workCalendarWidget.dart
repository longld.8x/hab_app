import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/models/workDto.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

/// WorkCalendarWidget
class WorkCalendarWidget extends StatefulWidget {
  const WorkCalendarWidget(this.model, {Key key}) : super(key: key);
  final List<WorkDto> model;
  @override
  _WorkCalendarWidgetState createState() => _WorkCalendarWidgetState();
}

class _WorkCalendarWidgetState extends StateMVC<WorkCalendarWidget> with TickerProviderStateMixin {
  ScrollController _ctrlTop;
  ScrollController _ctrlLeft;
  ScrollController _ctrlRow;
  ScrollController _ctrlCol;
  int row;
  int col;
  @override
  void initState() {
    row = 24;
    col = 30;
    initCtrl();
    super.initState();
  }

  initCtrl() {
    _ctrlTop = new ScrollController();
    //_ctrlTop.addListener(_topListener);

    _ctrlRow = new ScrollController();
    _ctrlRow.addListener(_rowListener);

    //
    _ctrlLeft = new ScrollController();
    _ctrlLeft.addListener(_leftListener);

    _ctrlCol = new ScrollController();
    _ctrlCol.addListener(_colListener);
  }

  _topListener() {
    // print("_ctrlTop");
    // print(_ctrlTop.position.extentAfter);
    // _ctrlRow.jumpTo(_ctrlTop.position.pixels);
  }

  _rowListener() {
    //print("_ctrlRow");
    _ctrlTop.jumpTo(_ctrlRow.position.pixels);
  }

  _leftListener() {
    // print("_ctrlLeft");
    // print(_ctrlLeft);
  }

  _colListener() {
    // print("_ctrlCol");
    _ctrlLeft.jumpTo(_ctrlCol.position.pixels);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: config.App.appWidth(100),
      child: Stack(
        // crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Positioned(
            top: 0,
            child: columnHeader(3),
          ),
          Positioned(
            top: 50,
            child: rowHeader(),
          ),
          Positioned(
            top: 50,
            left: 60,
            child: bodyTable(),
          ),
        ],
      ),
    );
  }

  Widget columnHeader(int month) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: 60,
          height: 50,
          decoration: BoxDecoration(color: config.Colors.customeColor("#171051")),
          padding: EdgeInsets.all(0),
          child: Container(),
        ),
        Container(
          width: config.App.appWidth(100) - 60,
          height: 50,
          decoration: BoxDecoration(color: config.Colors.customeColor("#CDD2FD")),
          padding: EdgeInsets.all(0),
          child: SizedBox(
            height: 50,
            child: ListView.builder(
              controller: _ctrlTop,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              //physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) => block(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SubText(_getNumberView(index + 1), style: TextStyle(fontWeight: FontWeight.w500, color: config.Colors.accentColor())),
                      SubText("T2", style: TextStyle(fontWeight: FontWeight.w400, color: config.Colors.accentColor())),
                    ],
                  ),
                  isHeader: true),
              itemCount: col,
            ),
          ),
        ),
      ],
    );
  }

  Widget rowHeader() {
    return Container(
      width: 60,
      height: config.App.appHeight(100) - config.App.appBar() - config.App.bottomNavigationBar() - 50,
      decoration: BoxDecoration(color: config.Colors.customeColor("#171051")),
      padding: EdgeInsets.all(0),
      child: ListView.builder(
        controller: _ctrlLeft,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        //physics: BouncingScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int x) => Container(
          width: 60,
          height: 50,
          decoration: BoxDecoration(color: config.Colors.customeColor("#171051")),
          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
          alignment: Alignment.centerRight,
          child: SubText(_getNumberView(x) + ":00", style: TextStyle(fontWeight: FontWeight.w500, color: config.Colors.customeColor("#ffffff"))),
        ),
        itemCount: row,
      ),
    );
  }

  Widget bodyTable() {
    return SizedBox(
      width: config.App.appWidth(100) - 60,
      height: config.App.appHeight(100) - config.App.appBar() - config.App.bottomNavigationBar() - (50),
      child: SingleChildScrollView(
        controller: _ctrlRow,
        scrollDirection: Axis.horizontal,
        //     physics: NeverScrollableScrollPhysics(),
        physics: BouncingScrollPhysics(),
        child: SizedBox(
          width: 50.0 * 30,
          height: 50.5 * 30,
          child: SingleChildScrollView(
            controller: _ctrlCol,
            //     physics: NeverScrollableScrollPhysics(),
            physics: BouncingScrollPhysics(),
            child: SizedBox(
              width: 50.0 * col,
              height: 50.5 * row,
              child: Column(
                children: List.generate(row, (x) {
                  return Container(
                    decoration: BoxDecoration(
                        color: config.Colors.inputBgColor(),
                        border: Border(
                          top: BorderSide.none,
                          right: BorderSide.none,
                          left: BorderSide.none,
                          bottom: BorderSide(color: Colors.grey, width: 0.5),
                        )
                        //border: new Border.fromBorderSide(AxisDirection.right : width: 1.0, color: config.Colors.customeColor("#E4F2F9")),
                        ),
                    child: Row(
                      children: List.generate(col, (y) {
                        return blockText(x, y);
                      }),
                    ),
                  );
                }),
              ),
              // child: ListView.builder(
              //   physics: NeverScrollableScrollPhysics(),
              //   itemBuilder: (BuildContext context, int x) => Container(
              //     height: 50,
              //     child: ListView.builder(
              //       physics: NeverScrollableScrollPhysics(),
              //       scrollDirection: Axis.horizontal,
              //       itemBuilder: (BuildContext context, int y) => blockText(_getNumberView(x + 1) + "-" + _getNumberView(y + 1), color: config.Colors.accentColor()),
              //       itemCount: col,
              //     ),
              //   ),
              //   itemCount: row,
              // ),
            ),
          ),
        ),
      ),
    );
  }

  String _getNumberView(int val) {
    return val < 10 ? ("0" + val.toString()) : val.toString();
  }

  Widget block({Widget child, bool isHeader = false}) {
    return Container(
      height: 50,
      width: 49.5,
      child: child,
    );
  }

  Widget blockText(int h, int day) {
    String text = _getNumberView(h) + "-" + _getNumberView(day + 1);
    return block(
      child: Text(text, style: config.Style.subStyle()),
    );
  }
}
