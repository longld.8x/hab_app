import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/work_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/import.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/pages/work/workCalendarWidget.dart';
import 'package:hab_app/src/pages/work/workListWidget.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class WorkWidget extends StatefulWidget {
  RouteArgument routeArgument;
  WorkWidget({Key key, this.routeArgument}) : super(key: key);
  @override
  _WorkWidgetState createState() => _WorkWidgetState();
}

class _WorkWidgetState extends StateMVC<WorkWidget> with SingleTickerProviderStateMixin {
  WorkController _con;
  _WorkWidgetState() : super(WorkController()) {
    _con = controller;
  }
  String _typeView;

  @override
  void initState() {
    super.initState();
    _con.pageState();
    _typeView = widget.routeArgument.tag;
  }

  @override
  Widget build(BuildContext context) {
    Widget _body() {
      if (_typeView == 'calendar')
        return WorkCalendarWidget([]);
      else
        return WorkListWidget();
    }

    return Scaffold(
      key: _con.scaffoldKey,
      // resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: TitleText("Công việc"),
        leading: AppbarIcon(
          onPressed: () {
            Navigator.of(context).pushNamedAndRemoveUntil('/Page', (Route<dynamic> route) => false, arguments: 0);
          },
        ),
        actions: [
          IconButton(
            icon: SvgPicture.asset(_typeView == "calendar" ? 'assets/icon/list_arrow.svg' : "assets/icon/calendar.svg"),
            onPressed: () {
              Helper.infoMessenge("Đang phát triển...");
            },
            // onPressed: () => setState(() => _typeView = (_typeView == "calendar" ? "list" : "calendar")),
          )
        ],
      ),
      body: Container(
        width: config.App.appWidth(100),
        constraints: BoxConstraints(minHeight: config.App.appHeight(100) - 68),
        child: _body(),
      ),
      //body: Container(child: _bodyView()),
    );
  }
}
