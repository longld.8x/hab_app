// import 'package:flutter/material.dart';
// import 'package:hab_app/src/helpers/app_config.dart' as config;
// import 'package:hab_app/src/models/workDto.dart';
// import 'package:mvc_pattern/mvc_pattern.dart';
// import 'package:table_calendar/table_calendar.dart';
//
// /// TaskMonthWidget
// class TaskMonthWidget extends StatefulWidget {
//   const TaskMonthWidget(this.model, {Key key}) : super(key: key);
//   final List<TaskModel> model;
//   @override
//   _TaskMonthWidgetState createState() => _TaskMonthWidgetState();
// }
//
// class _TaskMonthWidgetState extends StateMVC<TaskMonthWidget> with TickerProviderStateMixin {
//   // Example holidays
//   final Map<DateTime, List> _holidays = {
//     DateTime(2021, 1, 1): ['New Year\'s Day'],
//     DateTime(2021, 1, 6): ['Epiphany'],
//     DateTime(2021, 2, 14): ['Valentine\'s Day'],
//     DateTime(2021, 4, 21): ['Easter Sunday'],
//     DateTime(2021, 4, 22): ['Easter Monday'],
//   };
//   Map<DateTime, List> _events;
//   List _selectedEvents;
//   AnimationController _animationController;
//   CalendarController _calendarController;
//
//   @override
//   void initState() {
//     super.initState();
//     final _selectedDay = DateTime.now();
//
//     _events = {
//       _selectedDay.subtract(Duration(days: 30)): ['Event A0', 'Event B0', 'Event C0'],
//       _selectedDay.subtract(Duration(days: 27)): ['Event A1'],
//       _selectedDay.subtract(Duration(days: 20)): ['Event A2', 'Event B2', 'Event C2', 'Event D2'],
//       _selectedDay.subtract(Duration(days: 16)): ['Event A3', 'Event B3'],
//       _selectedDay.subtract(Duration(days: 10)): ['Event A4', 'Event B4', 'Event C4'],
//       _selectedDay.subtract(Duration(days: 4)): ['Event A5', 'Event B5', 'Event C5'],
//       _selectedDay.subtract(Duration(days: 2)): ['Event A6', 'Event B6'],
//       _selectedDay: ['Event A7', 'Event B7', 'Event C7', 'Event D7'],
//       _selectedDay.add(Duration(days: 1)): ['Event A8', 'Event B8', 'Event C8', 'Event D8'],
//       _selectedDay.add(Duration(days: 3)): Set.from(['Event A9', 'Event A9', 'Event B9']).toList(),
//       _selectedDay.add(Duration(days: 7)): ['Event A10', 'Event B10', 'Event C10'],
//       _selectedDay.add(Duration(days: 11)): ['Event A11', 'Event B11'],
//       _selectedDay.add(Duration(days: 17)): ['Event A12', 'Event B12', 'Event C12', 'Event D12'],
//       _selectedDay.add(Duration(days: 22)): ['Event A13', 'Event B13'],
//       _selectedDay.add(Duration(days: 26)): ['Event A14', 'Event B14', 'Event C14'],
//     };
//
//     _selectedEvents = _events[_selectedDay] ?? [];
//     _calendarController = CalendarController();
//
//     _animationController = AnimationController(
//       vsync: this,
//       duration: const Duration(milliseconds: 400),
//     );
//
//     _animationController.forward();
//   }
//
//   @override
//   void dispose() {
//     _animationController.dispose();
//     _calendarController.dispose();
//     super.dispose();
//   }
//
//   void _onDaySelected(DateTime day, List events, List holidays) {
//     print('CALLBACK: _onDaySelected');
//     setState(() {
//       _selectedEvents = events;
//     });
//   }
//
//   void _onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {
//     print('CALLBACK: _onVisibleDaysChanged');
//   }
//
//   void _onCalendarCreated(DateTime first, DateTime last, CalendarFormat format) {
//     print('CALLBACK: _onCalendarCreated');
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: config.App.appWidth(100),
//       child: Column(
//         mainAxisSize: MainAxisSize.max,
//         children: <Widget>[
//           _buildTableCalendar(),
//           const SizedBox(height: 8.0),
//           Container(child: _buildEventList()),
//         ],
//       ),
//     );
//   }
//
//   // Simple TableCalendar configuration (using Styles)
//   Widget _buildTableCalendar() {
//     return TableCalendar(
//       calendarController: _calendarController,
//       events: _events,
//       holidays: _holidays,
//       startingDayOfWeek: StartingDayOfWeek.monday,
//       calendarStyle: CalendarStyle(
//         selectedColor: config.Colors.primaryColor(),
//         todayColor: config.Colors.primaryColor(opacity: 0.5),
//         holidayStyle: TextStyle(color: config.Colors.primaryColor()),
//       ),
//       headerStyle: HeaderStyle(
//         formatButtonVisible: false,
//       ),
//       onDaySelected: _onDaySelected,
//       onVisibleDaysChanged: _onVisibleDaysChanged,
//       onCalendarCreated: _onCalendarCreated,
//       builders: CalendarBuilders(
//         markersBuilder: (_, __, v, ____) => _buildMarker(v),
//       ),
//     );
//   }
//
//   List<Widget> _buildMarker(List v) {
//     print("v.length ${v.length}");
//     var view = v
//         .take(5)
//         .map((event) => Positioned(
//                 child: Container(
//               width: 3.0,
//               height: 3.0,
//               margin: const EdgeInsets.symmetric(horizontal: 0.3),
//               decoration: BoxDecoration(
//                 shape: BoxShape.circle,
//                 color: config.Colors.primaryColor(opacity: 0.5),
//               ),
//             )))
//         .toList();
//     return [Row(mainAxisSize: MainAxisSize.min, children: view)];
//   }
//
//   Widget _buildEventsMarker(DateTime date, List events) {
//     return AnimatedContainer(
//       duration: const Duration(milliseconds: 300),
//       decoration: BoxDecoration(
//         shape: BoxShape.rectangle,
//         color: _calendarController.isSelected(date)
//             ? Colors.brown[500]
//             : _calendarController.isToday(date)
//                 ? Colors.brown[300]
//                 : Colors.blue[400],
//       ),
//       width: 16.0,
//       height: 16.0,
//       child: Center(
//         child: Text(
//           '${events.length}',
//           style: TextStyle().copyWith(
//             color: Colors.white,
//             fontSize: 12.0,
//           ),
//         ),
//       ),
//     );
//   }
//
//   Widget _buildEventList() {
//     return new ListView.builder(
//       shrinkWrap: true,
//       physics: NeverScrollableScrollPhysics(),
//       scrollDirection: Axis.vertical,
//       itemBuilder: (BuildContext context, int index) {
//         var event = _selectedEvents[index];
//         return Container(
//           decoration: BoxDecoration(
//             border: Border.all(width: 0.8),
//             borderRadius: BorderRadius.circular(12.0),
//           ),
//           margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
//           child: ListTile(
//             title: Text(event.toString()),
//             onTap: () => print('$event tapped!'),
//           ),
//         );
//       },
//       itemCount: _selectedEvents.length,
//       padding: EdgeInsets.all(0),
//     );
//   }
// }
