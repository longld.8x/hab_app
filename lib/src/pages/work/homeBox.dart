import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/work_controller.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/pages/home/HomeBox.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class WorkHomeBox extends StatefulWidget {
  const WorkHomeBox({Key key}) : super(key: key);
  _WorkHomeBoxState createState() => _WorkHomeBoxState();
}

class _WorkHomeBoxState extends StateMVC<WorkHomeBox> {
  WorkController _con;
  _WorkHomeBoxState() : super(WorkController()) {
    _con = controller;
  }
  @override
  initState() {
    _con.homeWidgetState();
    super.initState();
  }

  Widget block(String type, String title, String description, DateTime date, {Function action, bool isEnd = false}) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: type == 'null' ? 0 : 16, left: 16, right: 16, bottom: type == 'null' ? 0 : 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                type == 'null'
                    ? Container(
                        child: AccentText(
                          title,
                          style: TextStyle(fontWeight: FontWeight.w400, fontSize: 14),
                        ),
                      )
                    : Flexible(
                        child: Container(
                          // width: width - 32 - 50,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: AccentText(
                                  title,
                                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                padding: EdgeInsets.only(bottom: 10),
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                    child: SvgPicture.asset('assets/icon/task.svg'),
                                    alignment: Alignment.centerLeft,
                                    padding: EdgeInsets.only(right: 8),
                                  ),
                                  Flexible(
                                    child: SubText(
                                      description * 10,
                                      maxLines: 1,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        flex: 90,
                      ),
                date == null
                    ? Container()
                    : Flexible(
                        child: Container(
                          // width: 50,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Container(
                                padding: EdgeInsets.only(bottom: 6),
                                child: SubText(
                                  "${Helper.dateString(date.hour)}:${Helper.dateString(date.minute)}",
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(right: 0),
                                child: SubText(
                                  "${Helper.dateString(date.day)}/${Helper.dateString(date.month)}",
                                ),
                              ),
                            ],
                          ),
                        ),
                        flex: 10,
                      ),
              ],
            ),
          ),
          !isEnd
              ? Divider(
                  height: 1,
                  color: config.Colors.customeColor("#E0E9F8"),
                )
              : Container(
                  padding: EdgeInsets.only(bottom: 16),
                )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _widgets = [];
    if (_con.isLoading || _con.homeWorks == null || _con.homeWorks.length == 0) {
      _widgets.add(block("null", 'Không có công việc mới', '', null, isEnd: true));
    } else {
      _widgets = _con.homeWorks.map((e) => block("task", e.taskName, e.description, e.startDate)).toList();
    }
    _widgets.insert(
        0,
        HomeTitleBox("Công việc của tôi", action: () {
          Navigator.of(context).pushNamed('/Work');
        }));
    return HomeBox(_widgets);
  }
}
