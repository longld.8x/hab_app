import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_zoom_plugin/zoom_options.dart';
import 'package:flutter_zoom_plugin/zoom_view.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hab_app/src/controllers/zoom_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/models/zoomDto.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ZoomWidget extends StatefulWidget {
  final RouteClassArgument routeArgument;
  ZoomWidget({Key key, this.routeArgument}) : super(key: key);
  @override
  _ZoomWidgetState createState() => _ZoomWidgetState();
}

class _ZoomWidgetState extends StateMVC<ZoomWidget> {
  ZoomController _con;
  _ZoomWidgetState() : super(ZoomController()) {
    _con = controller;
  }
  ZoomOptions zoomOptions;
  ZoomDto zoomDto;
  ZoomMeetingOptions meetingOptions;

  Timer timer;
  bool _owner;

  @override
  void initState() {
    super.initState();
    _con.pageState(widget.routeArgument);
    _owner = widget.routeArgument.role.indexOf(5) > -1;
    zoomDto = widget.routeArgument.param as ZoomDto;
    initZoom();
  }

  initZoom() {
    try {
      this.zoomOptions = new ZoomOptions(
        domain: GlobalConfiguration().getString("zoom.domain"),
        appKey: GlobalConfiguration().getString("zoom.appKey"),
        appSecret: GlobalConfiguration().getString("zoom.appSecret"),
      );

      print("meetingOptions ${zoomDto.userId.toString()} ${zoomDto.meetingId.toString()} ${zoomDto.meetingPassword.toString()}");
      print("zoomDto.meetingId $zoomDto");
      this.meetingOptions = new ZoomMeetingOptions(
        userId: zoomDto.userId,
        displayName: zoomDto.userId,
        meetingId: zoomDto.meetingId, //_con.meetingId,
        meetingPassword: zoomDto.meetingPassword, //_con.meetingPassword,
        disableDialIn: "false",
        disableDrive: "false",
        disableInvite: "false",
        disableShare: "false",
        noAudio: "false",
        noDisconnectAudio: "false",
        zoomToken: _owner ? GlobalConfiguration().getString("zoom.zoomToken") : '',
        zoomAccessToken: _owner ? zoomDto.zak : '',
      );
    } catch (e) {
      log('initZoom ${e.toString()}');
      // Some error occured, look at the exception message for more details
    }
    print("meetingOptions ${meetingOptions.toString()}");
  }

  bool _isMeetingEnded(String status) {
    print("_isMeetingEnded $status");
    var result = false;

    if (Platform.isAndroid)
      result = status == "MEETING_STATUS_DISCONNECTING" || status == "MEETING_STATUS_FAILED";
    else
      result = status == "MEETING_STATUS_IDLE";

    return result;
  }

  bool _isMeetingClosed(String status) {
    print("_isMeetingEnded $status");
    var result = false;
    if (Platform.isAndroid)
      result = status == "MEETING_STATUS_DISCONNECTING";
    else
      result = status == "MEETING_STATUS_IDLE";

    return result;
  }

  @override
  Widget build(BuildContext context) {
    Widget _zoomView() {
      return ZoomView(
        onViewCreated: (ctrl) {
          return ctrl.initZoom(this.zoomOptions).then((results) {
            if (results[0] == 0) {
              // Listening on the Zoom status stream (1)
              ctrl.zoomStatusEvents.listen((status) {
                print("Meeting Status Stream: " + status[0] + " - " + status[1]);

                if (_isMeetingEnded(status[0])) {
                  if (_isMeetingClosed(status[0])) Navigator.pop(context);
                  timer?.cancel();
                }
              });

              print(_owner ? "Meeting startMeeting chủ phòng học ${this.meetingOptions}" : "Meeting joinMeeting ko phải chủ phòng học ${this.meetingOptions}");
              var _join = _owner ? ctrl.startMeeting(this.meetingOptions) : ctrl.joinMeeting(this.meetingOptions);
              _join.then((joinMeetingResult) {
                // ctrl.joinMeeting(this.meetingOptions).then((joinMeetingResult) {
                // Polling the Zoom status (2)
                timer = Timer.periodic(new Duration(seconds: 2), (timer) {
                  ctrl.meetingStatus(this.meetingOptions.meetingId).then((status) {
                    print("Meeting Status Polling: " + status[0] + " - " + status[1]);
                  });
                });
              });
            }
          }).catchError((error) {
            print(error);
          }).whenComplete(() => Helper.hideLoaderOverlay());
        },
      );
    }

    Widget _bodyView() {
      if (_con.isLoading) return LoadContent(warp: true);
      if (_con.isPermission == false) {
        Navigator.pop(context);
        Helper.errorMessenge("Vui lòng cấp quyền để truy cập zoom");
      }
      // initZoom();
      return _zoomView();
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKey,
      appBar: AppBar(leading: AppbarIcon(), title: TitleText("Vào phòng học")),
      isSingleChildScrollView: false,
      main: _bodyView(),
    );
  }
}
