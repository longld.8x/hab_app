import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/otp_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/PinCode.dart';
import 'package:hab_app/src/elements/TitlePageSimpleWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class OTPWidget extends StatefulWidget {
  final OTPArgument argument;
  OTPWidget({Key key, this.argument}) : super(key: key);
  @override
  _OTPWidgetState createState() => _OTPWidgetState();
}

class _OTPWidgetState extends StateMVC<OTPWidget> {
  OTPController _con;
  String _msg;
  bool _activeBtn = false;
  String _title;
  String _sub;
  String _phone;
  String _type;
  int _seconds = 0;
  int _maxSeconds = 60;
  Timer _timer;
  TextEditingController _pinCodeCtrl = TextEditingController();
  bool _pinCodeHasError = true;

  _OTPWidgetState() : super(OTPController()) {
    _con = controller;
  }
  @override
  void initState() {
    super.initState();

    _con.OTPState();
    initLabel();

    Future.delayed(Duration.zero, () {
      _con.sendOtp(_type, _phone).then((ResponseMessage result) {
        if (result.success) {
          var _iTimer = initTimer();
          setState(() => _timer = _iTimer);
          Helper.successMessenge("OTP đã được gửi vào số điện thoại của bạn!");
        } else
          Helper.errorMessenge(result.message);
      });
    });
  }

  Timer initTimer() {
    if (_timer != null) _timer.cancel();
    setState(() => _seconds = _maxSeconds);
    return Timer.periodic(new Duration(seconds: 1), (t) {
      if (_seconds == 0) {
        _timer.cancel();
        return;
      }
      debugPrint("Timer.periodic tick");
      debugPrint(t.tick.toString());
      setState(() => _seconds = (_maxSeconds - t.tick));
    });
  }

  void initLabel() {
    debugPrint("OTP type ${widget.argument.type}");
    debugPrint("OTP phone ${widget.argument.phone}");
    _phone = widget.argument.phone;
    _type = widget.argument.type;
    if (_type == "forget_password") {
      _title = "Quên mật khẩu";
      _sub = "Nhập mã xác thực vừa được gửi tới số $_phone.";
    } else if (_type == "register") {
      _title = "Đăng ký";
      _sub = "Nhập mã xác thực vừa được gửi tới số $_phone.";
    }
  }

  @override
  void dispose() {
    if (_timer != null) _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget _subTextWidget() {
      if (_sub == null || _sub.isEmpty) return Container(height: 80);
      return Container(
        height: 80,
        alignment: Alignment.bottomLeft,
        child: AccentText(_sub),
        padding: EdgeInsets.only(bottom: 40, left: 15, right: 15),
      );
    }

    Widget _validateWidget() {
      if (_msg == null || _msg.isEmpty) return Container();
      return Container(
        padding: EdgeInsets.only(left: 15, right: 15),
        alignment: Alignment.topLeft,
        child: ErrorText(_msg),
      );
    }

    Widget _formWidget() {
      return Form(
        key: _con.formKey,
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              PinCodeField(
                length: 4,
                controller: _pinCodeCtrl,
                hasError: _pinCodeHasError,
                onChanged: (String v) {
                  setState(() {
                    _msg = null;
                    _pinCodeHasError = true;
                    _activeBtn = (v.length == 4);
                  });
                },
              ),
              _validateWidget(),
              SizedBox(height: 30),
              Button(
                isActive: _activeBtn,
                text: "Tiếp tục",
                onPressed: () {
                  if (widget.argument.isVerify != null && widget.argument.isVerify) {
                    _con.verify(_type, _pinCodeCtrl.text, _phone).then((ResponseMessage result) {
                      if (result.success) {
                        setState(() => _pinCodeHasError = true);
                        Navigator.of(context).pop();
                        widget.argument.success(_pinCodeCtrl.text);
                      } else {
                        setState(() => _msg = result.message ?? "Mã xác thực không chính xác.");
                        setState(() => _pinCodeHasError = false);
                      }
                    });
                  } else {
                    setState(() => _pinCodeHasError = true);
                    Navigator.of(context).pop();
                    widget.argument.success(_pinCodeCtrl.text);
                  }
                },
              ),
            ],
          ),
        ),
      );
    }

    List<Widget> _resendTextList() {
      if (_seconds > 0)
        return [
          new AccentText("Không nhận được mã?"),
          new PrimaryText("(${(_seconds < 10 ? "0" : "") + _seconds.toString()}s)"),
        ];
      else
        return [
          new PrimaryText("Gửi lại mã").onTap(() {
            _con.reSendOtp(_type, _phone).then((ResponseMessage result) {
              if (result.success) {
                var _iTimer = initTimer();
                setState(() {
                  _timer = _iTimer;
                  _pinCodeCtrl.text = "";
                });
                Helper.successMessenge("OTP đã được gửi vào số điện thoại của bạn!");
              } else
                Helper.errorMessenge(result.message);
            });
          }),
        ];
    }

    Widget _resendTextWidget() {
      return Builder(
        builder: (context) => Container(
          padding: EdgeInsets.only(top: 25, bottom: 25),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _resendTextList(),
          ),
        ),
      );
    }

    return LayoutManager(
      onWillPop: () {
        Navigator.of(context).pop();
        if (widget.argument.error != null) widget.argument.error("");
      },
      scaffoldKey: _con.scaffoldKey,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText(""),
      ), //AppBar(),
      body: Container(
        child: Column(
          children: <Widget>[
            TitlePageSimpleWidget(_title),
            _subTextWidget(),
            _formWidget(),
            _resendTextWidget(),
          ],
        ),
      ),
    );
  }
}
