import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/chat_search_controller.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/ListViewItem.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/chat/roomDto.dart';
import 'package:hab_app/src/models/friendUser.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ChatSearchWidget extends StatefulWidget {
  @override
  _ChatSearchWidgetState createState() => _ChatSearchWidgetState();
}

class _ChatSearchWidgetState extends StateMVC<ChatSearchWidget> with SingleTickerProviderStateMixin {
  ChatSearchController _con;
  _ChatSearchWidgetState() : super(ChatSearchController()) {
    _con = controller;
  }
  bool isSearchForm = true;
  TextEditingController _ctrlSearch;
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _con.pageState();
    _ctrlSearch = TextEditingController();
    initScroll();
  }

  initScroll() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset && (_con.serverTotalCount > ((_con.currentPage + 1) * _con.itemPage))) {
        _con.searchUsers(0, _con.currentPage + 1, _ctrlSearch.text);
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void search() {
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() => isSearchForm = false);
    _con.searchUsers(0, 0, _ctrlSearch.text);
  }

  void onChat(int userId) {
    _con.findOrCreateRoom(userId).then((result) {
      if (result.success) {
        Navigator.of(context).pushNamed('/Chat/Room', arguments: (result.results as RoomDto).id);
      } else {
        Helper.errorMessenge(result.message);
      }
    });
    // Navigator.of(context).pushNamed('/Chat/Room', arguments: RouteChatArgument(roomId: 0, userIds: [userId]));
  }

  @override
  Widget build(BuildContext context) {
    final double statusbarHeight = MediaQuery.of(context).padding.top;

    Widget _searchBox() {
      return Container(
        child: Row(
          children: [
            Expanded(flex: 10, child: Container()),
            Expanded(
              flex: 80,
              child: Container(
                padding: EdgeInsets.only(right: 0),
                child: Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  padding: EdgeInsets.only(left: 0, right: 0),
                  height: 56.0,
                  alignment: Alignment.centerLeft,
                  child: TextFormField(
                    controller: _ctrlSearch,
                    autofocus: true,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: config.Colors.backgroundColor(),
                      labelText: null,
                      hintText: "Nhập tên hoặc số điện thoại cộng sự",
                      prefixIcon: IconButton(
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: config.Colors.accentColor(),
                          size: 18,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      // suffixIcon: IconButton(
                      //     icon: PrimaryText(
                      //       'Hủy',
                      //       style: TextStyle(fontSize: 12),
                      //     ),
                      //     onPressed: () {
                      //       Navigator.of(context).pop();
                      //     }),
                      // suffixIcon: _ctrlSearch.text.isEmpty
                      //     ? null
                      //     : IconButton(
                      //         icon: Icon(Icons.close, size: 16, color: config.Colors.customeColor("#979797")),
                      //         padding: EdgeInsets.all(12),
                      //         onPressed: () => setState(() => _ctrlSearch.text = ''),
                      //       ),
                      contentPadding: EdgeInsets.symmetric(horizontal: 1, vertical: 15),
                      border: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide.none),
                      focusedBorder: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide.none),
                      enabledBorder: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide.none),
                    ),
                    style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor()),
                    onEditingComplete: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      search();
                    },
                    onTap: () => setState(() => isSearchForm = true),
                  ),
                ),
              ),
            ),
            Expanded(flex: 10, child: Container()),
          ],
        ),
      );
    }

    Widget _bodySearchView() {
      final _bottom = MediaQuery.of(context).viewInsets.bottom;
      return Container(
        width: config.App.appWidth(100),
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
        ),
        child: ColumnStart(
          children: [
            Container(
              padding: EdgeInsets.only(top: 15, left: 24, right: 0, bottom: 15),
              child: AccentText("Lịch sử tìm kiếm", style: TextStyle(fontWeight: FontWeight.w500)),
              decoration: BoxDecoration(
                color: config.Colors.backgroundColor(),
                borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
              ),
              margin: EdgeInsets.only(bottom: 1),
            ),
            Container(
              child: Expanded(
                child: (_con.dataFriends == null || _con.dataFriends.length == 0)
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(bottom: _bottom),
                        child: ListViewItem(
                          _con.dataFriends,
                          (FriendUser item, i) => CustomeViewItem(
                            item.friendFullName,
                            item.friendAvatar,
                            i,
                            sub: SubText(item.friendUserName, style: TextStyle(color: config.Colors.customeColor("#979797"))),
                            onTap: (_, __) {
                              onChat(item.friendUserId);
                            },
                          ),
                        ),
                      ),
              ),
            ),
          ],
        ),
      );
    }

    Widget _bodyResultView() {
      if (_con.isLoading) return LoadContent(warp: true, border: true);
      if (_con.dataFriends == null || _con.dataFriends.length == 0) return NoContent(warp: true, border: true);
      return ListView.builder(
        controller: _scrollController,
        itemCount: _con.dataFriends.length,
        itemBuilder: (_context, index) {
          var item = _con.dataFriends[index];
          if (index == 0)
            return CustomeViewItem(
              item.friendFullName,
              item.friendAvatar,
              index,
              sub: SubText(item.friendUserName, style: TextStyle(color: config.Colors.customeColor("#979797"))),
              deco: BoxDecoration(
                color: config.Colors.backgroundColor(),
                borderRadius: index == 0 ? const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)) : null,
              ),
              onTap: (_, __) {
                onChat(item.friendUserId);
              },
            );
        },
        padding: EdgeInsets.all(0),
      );
    }

    Widget _bodyView() {
      return Container(
        width: config.App.appWidth(100),
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
        ),
        alignment: Alignment.topLeft,
        child: isSearchForm ? _bodySearchView() : _bodyResultView(),
      );
    }

    return LayoutAppbarBgManager(
      scaffoldKey: _con.scaffoldKey,
      onWillPop: () {
        Navigator.of(context).pop();
      },
      bottom: 10,
      titleFull: true,
      title: _searchBox(),
      main: _bodyView(),
    );
  }
}
