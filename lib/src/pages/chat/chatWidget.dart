import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/chat_controller.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/ListViewItem.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/chat/roomDto.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ChatWidget extends StatefulWidget {
  @override
  _ChatWidgetState createState() => _ChatWidgetState();
}

class _ChatWidgetState extends StateMVC<ChatWidget> with SingleTickerProviderStateMixin {
  ChatController _con;
  TabController _tabController;
  ScrollController _scrollController;
  List<Map> tabs;
  _ChatWidgetState() : super(ChatController()) {
    _con = controller;
  }
  bool isSearchForm = true;
  int _tabIndex = 0;
  // TextEditingController _ctrlSearch;

  @override
  void initState() {
    super.initState();
    _con.pageState();
    initTabs();
    initScroll();
  }

  initTabs() {
    tabs = [
      {
        'text': 'Tin nhắn',
        'icon': 'chat_user',
      },
      {
        'text': 'Danh bạ',
        'icon': 'chat_room',
      },
    ];
    _tabController = TabController(length: tabs.length, vsync: this);
    _tabController.addListener(() async => setState(() => _tabIndex = _tabController.index));
  }

  initScroll() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset && _con.totalPageRooms > _con.currentPageRooms) {
        _con.getRooms(_con.currentPageRooms + 1);
      }
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    _con.onExitPage();
    super.dispose();
  }

  void onChat(int userId) {
    _con.findOrCreateRoom(userId).then((result) {
      if (result.success) {
        Navigator.of(context).pushNamed('/Chat/Room', arguments: (result.results as RoomDto).id);
      } else {
        Helper.errorMessenge(result.message);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final double statusbarHeight = MediaQuery.of(context).padding.top;
    Widget _searchBox() {
      return Container(
        child: Row(
          children: [
            Expanded(flex: 10, child: Container()),
            Expanded(
              flex: 80,
              child: Container(
                margin: EdgeInsets.only(top: 8, bottom: 8),
                padding: EdgeInsets.only(left: 0, right: 0),
                height: 56.0,
                alignment: Alignment.centerLeft,
                child: TextFormField(
                  // controller: _ctrlSearch,
                  enableInteractiveSelection: false, // will disable paste operation
                  readOnly: true,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: config.Colors.backgroundColor(),
                    labelText: null,
                    hintText: "Nhập tên hoặc số điện thoại cộng sự",
                    prefixIcon: Icon(
                      Icons.search,
                      color: config.Colors.hintColor(),
                      size: 18,
                    ),
                    contentPadding: EdgeInsets.symmetric(horizontal: 1, vertical: 16),
                    border: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide.none),
                    focusedBorder: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide.none),
                    enabledBorder: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide.none),
                  ),
                  style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor()),
                  onTap: () {
                    Navigator.of(context).pushNamed('/Chat/Search');
                  },
                ),
              ),
            ),
            Expanded(flex: 10, child: Container()),
          ],
        ),
      );
    }

    Widget _tabTitle() {
      var wg = tabs.map((e) {
        var _active = _tabIndex == tabs.indexOf(e);
        return Tab(
          child: Container(
            //width: config.App.appWidth(100),
            // decoration: BoxDecoration(
            //   border: Border(right: _index == 0 ? BorderSide(width: 1, color: config.Colors.customeColor('#E0E9F8')) : BorderSide.none),
            // ),
            alignment: Alignment.center,
            child: IconWidget(
              Container(
                //margin: EdgeInsets.only(top: 2),
                child: SvgPicture.asset(
                  'assets/icon/${e['icon']}${_active ? '_active' : ''}.svg',
                  width: 18,
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 3),
                child: AccentText(
                  e['text'],
                  style: TextStyle(
                    fontSize: 16,
                    color: _active ? config.Colors.primaryColor() : config.Colors.customeColor('#A7A7A7'),
                  ),
                ),
              ),
              padding: 10,
            ),
          ),
        );
      }).toList();
      return Container(
        margin: EdgeInsets.fromLTRB(24, 0, 24, 0),
        //padding: EdgeInsets.fromLTRB(24, 0, 24, 0),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
          borderRadius: BorderRadius.all(Radius.circular(40)),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 2),
              color: Color(0xFF000000).withOpacity(0.16),
              spreadRadius: 1,
              blurRadius: 4,
            ),
          ],
        ),
        child: TabBar(
          indicatorColor: Colors.transparent,
          labelColor: config.Colors.primaryColor(),
          unselectedLabelColor: config.Colors.hintColor(),
          indicatorWeight: 4.0,
          labelStyle: TextStyle(fontWeight: FontWeight.w400),
          controller: _tabController,
          tabs: wg,
        ),
      );
    }

    Widget _contentWrap(Widget child) {
      return Container(
          margin: EdgeInsets.only(top: 26, bottom: config.App.bottomNavigationBar()),
          padding: EdgeInsets.only(top: 24),
          width: config.App.appWidth(100),
          decoration: BoxDecoration(
            color: config.Colors.backgroundColor(),
            borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
          ),
          child: child);
    }

    Widget _roomsWidget() {
      if (_con.isLoadingRooms) return _contentWrap(LoadContent(warp: true));
      if (_con.dataRooms == null || _con.dataRooms.length == 0) return _contentWrap(NoContent(warp: true));
      return _contentWrap(Container(
        color: config.Colors.inputBgColor(),
        child: ListView.builder(
          controller: _scrollController,
          itemCount: _con.dataRooms == null ? 0 : _con.dataRooms.length,
          shrinkWrap: false,
          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          itemBuilder: (BuildContext context, int index) => RoomChatViewItem(_con.dataRooms[index], _con.user, index),
        ),
      ));
    }

    Widget _contactUser() {
      if (_con.isLoadingFriends) return _contentWrap(LoadContent(warp: true));
      if (_con.dataFriends == null || _con.dataFriends.length == 0) return _contentWrap(NoContent(warp: true));
      return _contentWrap(Container(
        color: config.Colors.inputBgColor(),
        child: ListViewItem(
          _con.dataFriends,
          (item, i) => CustomeViewItem(
            item.friendFullName,
            item.friendAvatar,
            i,
            sub: SubText(item.friendUserName, style: TextStyle(color: config.Colors.customeColor("#979797"))),
            onTap: (_, __) {
              onChat(item.friendUserId);
            },
          ),
        ),
      ));
    }

    return LayoutAppbarBgManager(
      scaffoldKey: _con.scaffoldKey,
      onWillPop: () {},
      bottom: 10,
      titleFull: true,
      title: _searchBox(),
      main: Container(
        width: config.App.appWidth(100),
        alignment: Alignment.topLeft,
        child: Stack(
          children: <Widget>[
            // _contact(),
            Container(
              child: TabBarView(
                controller: _tabController,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  _roomsWidget(),
                  _contactUser(),
                ],
              ),
            ),
            _tabTitle(),
          ],
        ),
      ),
    );
  }
}
