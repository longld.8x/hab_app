//import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/chat_room_info_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/ImagePick.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/elements/tab.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/models/chat/roomDataDto.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ChatRoomInfoMediasWidget extends StatefulWidget {
  final RoomDataDto roomData;
  ChatRoomInfoMediasWidget({Key key, this.roomData});

  @override
  _ChatRoomInfoMediasWidgetState createState() => _ChatRoomInfoMediasWidgetState();
}

class _ChatRoomInfoMediasWidgetState extends StateMVC<ChatRoomInfoMediasWidget> with SingleTickerProviderStateMixin {
  ChatRoomInfoController _con;
  _ChatRoomInfoMediasWidgetState() : super(ChatRoomInfoController()) {
    _con = controller;
  }
  TabController _tabController;

  List<Widget> _listTab = [
    TabTitle("Ảnh"),
    //TabTitle("Video"),
    TabTitle("File"),
  ];
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    _con.pageInfoMediasState(widget.roomData);
    _tabController = TabController(length: _listTab.length, vsync: this);
    _tabController.index = 0;
    _selectedIndex = _tabController.index;
    _tabController.addListener(() {
      setState(() => _selectedIndex = _tabController.index);
    });
  }

  @override
  void dispose() {
    print("dispose ChatRoomInfoMediasWidget");
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget _headerWidget() {
      return Container(
        // decoration: BoxDecoration(
        //   border: Border(top: BorderSide(width: 1, color: config.Colors.customeColor("#E0E9F8"))),
        // ),
        padding: EdgeInsets.all(0),
        alignment: Alignment.center,
        child: TitleText(
          "Kho lưu trữ",
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 16,
            color: config.Colors.customeColor("#171051"),
          ),
        ),
      );
    }

    Widget _bodyImg(String type) {
      if (_con.loadingDataImgMessages == true) return LoadContent(warp: true);
      if (_con.dataImgMessages == null || _con.dataImgMessages.length == 0) return NoContent(warp: true);
      List<Widget> _wg = [];
      var _crossAxisCount = 3;
      var _wid = config.App.appWidth(100) / _crossAxisCount;
      for (var _item in _con.dataImgMessages) {
        var _file = _item.message.split('|');
        for (var _f in _file) {
          _wg.add(Container(
            width: _wid,
            height: _wid,
            margin: EdgeInsets.all(1),
            child: ImageUrlView(_f, _wid),
          ).onTap(() async {
            FocusScope.of(context).requestFocus(FocusNode());
            await showDialog(
              context: context,
              builder: (_) => ImageDialogUrl(_f),
            );
          }));
        }
      }
      var _total = _wg.length;
      var _rows = (_total / 3).ceil();
      return Container(
        decoration: BoxDecoration(color: config.Colors.backgroundColor()),
        height: _rows * _wid,
        child: GridView.count(
          crossAxisCount: _crossAxisCount,
          physics: NeverScrollableScrollPhysics(),
          children: _wg,
        ),
      );
    }

    Widget _bodyFile() {
      if (_con.loadingDataFileMessages == true) return LoadContent(warp: true);
      if (_con.dataFileMessages == null || _con.dataFileMessages.length == 0) return NoContent(warp: true);
      List<Widget> _wg = [];
      return Container(
        decoration: BoxDecoration(color: config.Colors.backgroundColor()),
        height: config.App.appHeight(150),
        child: AccentText("Đang tải dữ liệu"),
      );
    }

    Widget _bodyWidget() {
      return Expanded(
        child: TabBarView(
          controller: _tabController,
          children: [
            TabViewBox(_bodyImg('IMG'), false),
            // TabViewBox(_bodyImg([1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 9, 9, 9, 9, 9, 9, 9, 9], 'VIDEO'), false),
            TabViewBox(_bodyFile(), false),
          ],
        ),
      );
    }

    Widget _titleWidget() {
      return Container(
        child: TabBar(
          indicatorColor: config.Colors.primaryColor(),
          labelColor: config.Colors.primaryColor(),
          unselectedLabelColor: config.Colors.accentColor(),
          controller: _tabController,
          tabs: _listTab,
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyInfoMedias,
      appBar: AppBar(
        leading: AppbarIcon(),
        titleSpacing: 0,
        title: _headerWidget(),
        centerTitle: false,
        actions: [
          Container(
            width: 50,
          )
        ],
      ),
      main: Container(
        width: config.App.appWidth(100),
        alignment: Alignment.topLeft,
        height: config.App.appHeight(100),
        decoration: BoxDecoration(
          border: Border(top: BorderSide(width: 1, color: config.Colors.customeColor("#E0E9F8"))),
        ),
        child: Column(
          children: <Widget>[
            _titleWidget(),
            _bodyWidget(),
          ],
        ),
      ),
    );
  }
}
