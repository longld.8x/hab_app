//import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/chat_room_info_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/ListViewItem.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/models/chat/roomDataDto.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ChatRoomInfoMembersWidget extends StatefulWidget {
  final RoomDataDto roomData;
  ChatRoomInfoMembersWidget({Key key, this.roomData});

  @override
  _ChatRoomInfoMembersWidgetState createState() => _ChatRoomInfoMembersWidgetState();
}

class _ChatRoomInfoMembersWidgetState extends StateMVC<ChatRoomInfoMembersWidget> with SingleTickerProviderStateMixin {
  ChatRoomInfoController _con;
  _ChatRoomInfoMembersWidgetState() : super(ChatRoomInfoController()) {
    _con = controller;
  }
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _con.pageInfoMembersState(widget.roomData);
    initScroll();
  }

  initScroll() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset && (widget.roomData.members > ((_con.currentPage + 1) * _con.itemPage))) {
        _con.getMembers(widget.roomData.room.id, _con.currentPage + 1);
      }
    });
  }

  @override
  void dispose() {
    print("dispose ChatRoomInfoMembersWidget");
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget _headerWidget() {
      return Container(
        padding: EdgeInsets.all(0),
        alignment: Alignment.center,
        child: TitleText(
          "Thành viên nhóm",
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 16,
            color: config.Colors.customeColor("#171051"),
          ),
        ),
      );
    }

    Widget listDataBuilder() {
      if (_con.isLoadingMembers) return LoadContent(warp: true);
      if (_con.dataMembers == null || _con.dataMembers.length == 0) return NoContent(warp: true);
      return ListView.builder(
        controller: _scrollController,
        itemCount: _con.dataMembers.length,
        itemBuilder: (_context, index) => CustomeViewItem(
          _con.dataMembers[index].name,
          _con.dataMembers[index].avatar,
          index,
          sub: SubText(_con.dataMembers[index].phoneNumber),
          onTap: (_, __) {
            print("click");
          },
        ),
      );
    }

    Widget _bodyWidget() {
      if (_con.isLoadingInfo) return LoadContent(warp: true);
      return Container(
        child: ColumnStart(
          children: [
            Container(
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(width: 1, color: config.Colors.customeColor("#E0E9F8"))),
              ),
              padding: EdgeInsets.only(top: 5, bottom: 16, left: 16, right: 16),
              child: AccentText("Tổng số thành viên (${widget.roomData.members})"),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: config.Colors.backgroundColor(),
                ),
                child: listDataBuilder(),
              ),
            ),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyInfoMembers,
      appBar: AppBar(
        leading: AppbarIcon(),
        titleSpacing: 0,
        title: _headerWidget(),
        centerTitle: false,
        actions: [
          Container(
            width: 50,
          )
        ],
      ),
      main: Container(
        width: config.App.appWidth(100),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        child: _bodyWidget(),
      ),
    );
  }
}
