//import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/chat_room_info_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/ImagePick.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'info/medias.dart';
import 'info/members.dart';

class ChatRoomInfoWidget extends StatefulWidget {
  final int roomId;
  ChatRoomInfoWidget({Key key, this.roomId});

  @override
  _ChatRoomInfoWidgetState createState() => _ChatRoomInfoWidgetState();
}

class _ChatRoomInfoWidgetState extends StateMVC<ChatRoomInfoWidget> with SingleTickerProviderStateMixin {
  ChatRoomInfoController _con;
  _ChatRoomInfoWidgetState() : super(ChatRoomInfoController()) {
    _con = controller;
  }
  @override
  void initState() {
    super.initState();
    _con.pageInfoState(widget.roomId);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget _avatar() {
      if (_con.isLoadingInfo) return Container();
      if (_con.isRoomGroup) {
        return UserAvatar(
          _con.roomDataDto.room.photo,
          width: 100,
        );
      } else {
        var _url = (_con.dataMembers[0].ref_user_id == _con.user.id) ? _con.dataMembers[1].avatar : _con.dataMembers[0].avatar;
        return UserAvatar(_url == null ? "" : _url, width: 100);
      }
    }

    Widget _headerWidget() {
      if (_con.isLoadingInfo) return Container();
      String _title = "Thông tin nhóm";

      if (!_con.isRoomGroup) {
        _title = (_con.dataMembers[0].ref_user_id == _con.user.id) ? _con.dataMembers[1].name : _con.dataMembers[0].name;
      } else {
        _title = "Thông tin nhóm";
      }
      return Container(
        padding: EdgeInsets.all(0),
        alignment: Alignment.center,
        child: TitleText(
          _title,
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 16,
            color: config.Colors.customeColor("#171051"),
          ),
        ),
      );
    }

    Widget _mediaViewWidget() {
      if (_con.dataFile == null || _con.dataFile.length == 0) return Container();
      List<Widget> _wg = [];
      var _w = 60.0;
      for (var _item in _con.dataFile) {
        if (_item.message_type == "IMAGE") {
          var _file = _item.message.split('|');
          for (var _f in _file) {
            _wg.add(Container(
              width: _w,
              height: _w,
              margin: EdgeInsets.only(left: 0, right: 4, top: 2),
              child: ImageUrlView(_f, _w),
            ));
          }
        }
      }
      return Container(
        height: _w,
        child: new ListView(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          children: _wg,
        ),
      );
    }

    Widget _mediaWidget() {
      return Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: config.Colors.customeColor("#E0E9F8"))),
        ),
        // padding: EdgeInsets.only(top: 8, bottom: 8, right: 16, left: 16),
        padding: EdgeInsets.only(top: 24, bottom: 24, right: 16, left: 16),
        child: Column(
          children: [
            Container(
              child: RowWidget(
                Container(
                  child: ColumnStart(
                    children: [
                      AccentText(
                        "Kho lưu trữ (${_con.roomDataDto.media})",
                      ),
                      (_con.roomDataDto.media > 0)
                          ? SubText(
                              "Xem ảnh, video, file đã chia sẻ",
                              style: TextStyle(fontSize: 12, color: config.Colors.customeColor('#848688')),
                            )
                          : Container(),
                    ],
                  ),
                ),
                (_con.roomDataDto.media > 0)
                    ? Container(
                        padding: EdgeInsets.only(top: 0, left: 16, right: 16, bottom: 0),
                        child: Icon(
                          Icons.arrow_forward_ios,
                          size: 16,
                        ),
                      )
                    : Container(),
                flex: 90,
              ),
            ),
            _mediaViewWidget(),
          ],
        ),
      ).onTap(() {
        if (_con.roomDataDto.media > 0)
          Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) => ChatRoomInfoMediasWidget(
              roomData: _con.roomDataDto,
            ),
          ));
      });
    }

    Widget _membersWidget() {
      if (!_con.isRoomGroup) return Container();
      return Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: config.Colors.customeColor("#E0E9F8"))),
        ),
        padding: EdgeInsets.only(top: 24, bottom: 24, right: 16, left: 16),
        child: RowWidget(
          Container(
            child: AccentText(
              "Thành viên nhóm (${_con.roomDataDto.members})",
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 0, left: 16, right: 16, bottom: 0),
            child: Icon(
              Icons.arrow_forward_ios,
              size: 16,
            ),
          ),
          flex: 90,
        ),
      ).onTap(() {
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => ChatRoomInfoMembersWidget(
            roomData: _con.roomDataDto,
          ),
        ));
      });
    }

    Widget _outWidget() {
      if (!_con.isRoomGroup) return Container();
      return Container();
      return Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: config.Colors.customeColor("#E0E9F8"))),
        ),
        padding: EdgeInsets.only(top: 24, bottom: 24, right: 16, left: 16),
        child: ErrorText("Rời nhóm"),
      );
    }

    Widget _bodyWidget() {
      if (_con.isLoadingInfo) return LoadContent(warp: true);
      return SingleChildScrollView(
        reverse: true,
        // reverse: true,
        child: ColumnStart(
          children: [
            Container(
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(width: 1, color: config.Colors.customeColor("#E0E9F8"))),
              ),
              padding: EdgeInsets.only(top: 55, bottom: 55),
              child: Center(
                child: _avatar(),
              ),
            ),
            _mediaWidget(),
            _membersWidget(),
            _outWidget(),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyChatRoomInfo,
      appBar: AppBar(
        leading: AppbarIcon(),
        titleSpacing: 0,
        title: _headerWidget(),
        centerTitle: false,
        actions: [
          Container(
            width: 50,
          )
        ],
      ),
      main: Container(
        width: config.App.appWidth(100),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        child: _bodyWidget(),
      ),
    );
  }
}
