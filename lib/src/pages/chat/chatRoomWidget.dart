// import 'package:file_picker/file_picker.dart';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/chat_room_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/ImagePick.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/Message.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/elements/customStyle.dart';
import 'package:hab_app/src/elements/typing.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/chat/messageDto.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'chatRoomInfoWidget.dart';

class ChatRoomWidget extends StatefulWidget {
  final int roomId;
  ChatRoomWidget({Key key, this.roomId});

  @override
  _ChatRoomWidgetState createState() => _ChatRoomWidgetState();
}

class _ChatRoomWidgetState extends StateMVC<ChatRoomWidget> with SingleTickerProviderStateMixin {
  ChatRoomController _con;
  _ChatRoomWidgetState() : super(ChatRoomController()) {
    _con = controller;
  }
  ScrollController _scrollController;
  TextEditingController _ctrlText = new TextEditingController();

  ValueNotifier<bool> _isTyping = ValueNotifier(false);
  @override
  void initState() {
    super.initState();
    _con.pageChatRoomState(widget.roomId);
    initScroll();
    _isTyping.addListener(() {
      (_isTyping.value == true) ? _con.onTyping() : _con.onEndTyping();
    });
  }

  initScroll() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset && _con.serverMessageTotalPage > _con.currentMessagePage) {
        _con.getMessages(_con.roomChat.id, _con.currentMessagePage + 1, true);
      }
    });
  }

  @override
  void dispose() {
    print("dispose onExitRoom");
    _con.onExitRoom();
    super.dispose();
  }

  // List<Asset> images = <Asset>[];
  List<File> pickFiles = <File>[];

  setTyping() async {
    if (!_con.isConnect) setState(() => _isTyping.value = false);
    // var _isT = (_ctrlText.text.trim().isNotEmpty) || (images != null && images.length > 0) || (pickFiles != null && pickFiles.length > 0);
    var _isT = (_ctrlText.text.trim().isNotEmpty) || (pickFiles != null && pickFiles.length > 0);
    setState(() => _isTyping.value = _isT);
  }

  // Future<void> loadAssets() async {
  //   List<Asset> resultList = <Asset>[];
  //   try {
  //     resultList = await MultiImagePicker.pickImages(
  //       maxImages: 3,
  //       selectedAssets: images,
  //       enableCamera: true,
  //       materialOptions: MaterialOptions(
  //         allViewTitle: "Gửi ảnh",
  //         actionBarColor: "#1A60FF",
  //         actionBarTitleColor: "#FFFFFF",
  //         lightStatusBar: false,
  //         statusBarColor: '#1A60FF',
  //         startInAllView: true,
  //         selectCircleStrokeColor: "#171051",
  //         selectionLimitReachedText: "Số lượng vượt quá giới hạn cho phép.",
  //         textOnNothingSelected: "Vui lòng chọn ảnh",
  //       ),
  //     );
  //
  //     setState(() {
  //       images = resultList;
  //     });
  //     setTyping();
  //   } on Exception catch (e) {
  //     Helper.errorMessenge(e.toString());
  //   }
  // }

  void _openFileExplorer() async {
    FilePickerResult result = await FilePicker.platform.pickFiles(allowMultiple: true);
    if (result != null) {
      List<File> _files = result.paths.map((path) => File(path)).toList();
      setState(() {
        pickFiles = _files;
      });
    } else {
      setState(() {
        pickFiles = null;
      });
    }
  }

  Future<void> loadFile() async {
    _openFileExplorer();
  }

  @override
  Widget build(BuildContext context) {
    final _bottom = MediaQuery.of(context).viewInsets.bottom;
    Widget _headerWidget() {
      if (_con.isLoadingMembers || (_con.dataMembers == null || _con.dataMembers.length == 0)) return Container();
      Widget _avatar;
      String _name;
      String _phone;
      if (_con.isRoomGroup) {
        _avatar = UserAvatar(
          _con.roomChat.photo,
          width: 38,
        );
        _name = _con.roomChat.name;
        _phone = "${_con.roomData.members} người";
      } else if (_con.dataMembers.length == 1) {
        _avatar = UserAvatar(_con.dataMembers[0].avatar, width: 38);
        _name = (_con.dataMembers[0].name);
        _phone = (_con.dataMembers[0].phoneNumber);
      } else {
        var _u1 = _con.dataMembers.where((e) => e.ref_user_id == _con.user.id)?.first;
        var _u2 = _con.dataMembers.where((e) => e.ref_user_id != _con.user.id)?.first;
        _avatar = UserAvatar(_u1.ref_user_id == _con.user.id ? _u2.avatar : _u1.avatar, width: 38);
        _name = _u1.ref_user_id == _con.user.id ? _u2.name : _u1.name;
        _phone = _u1.ref_user_id == _con.user.id ? _u2.phoneNumber : _u1.phoneNumber;
      }
      return Container(
        padding: EdgeInsets.all(0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _avatar,
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 10, right: 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(bottom: 0, top: (_phone != null && _phone.isNotEmpty) ? 1 : 10),
                      child: AccentText(_name, style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14)),
                    ),
                    Container(
                      padding: EdgeInsets.only(bottom: 0),
                      child: AccentText(
                        _phone,
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                          color: config.Colors.customeColor("#2A2A2A"),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ).onTap(() {
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => ChatRoomInfoWidget(
            roomId: _con.roomChat.id,
          ),
        ));
      });
    }

    List<Widget> _messagesWidget() {
      if ((_con.dataMessages == null || _con.dataMessages.length == 0)) return [Container()];

      List wg = <Widget>[];
      if (_con.isLoadingMessage == true) {
        wg.add(FakeMessage(false));
        wg.add(FakeMessage(true));
        wg.add(FakeMessage(false));
        wg.add(FakeMessage(false));
        return wg;
      }

      //(var item in _con.dataMessages)
      for (int i = 0; i < _con.dataMessages.length; i++) {
        var _item = _con.dataMessages[i];
        MessageDto _item0;
        MessageDto _item2;
        if (i < _con.dataMessages.length - 1) _item2 = _con.dataMessages[i + 1];
        if (i > 0) _item0 = _con.dataMessages[i - 1];
        wg.add(MessageWidget(
          context,
          message: _item,
          isMe: (_item.user != null && (_item.user.ref_user_id) == _con.user.id),
          isGroup: _con.isRoomGroup,
          messageNext: _item2,
          messagePrev: _item0,
        ));
      }

      if (_con.isTyping == true) {
        // wg.add(
        //   TypingWidget(),
        // );
        wg.add(
          TypingIndicator(showIndicator: true),
        );
      }
      return wg;
    }

    Widget _bodyWidget() {
      if (_con.isLoadingMessage || _con.isLoadingMembers) return LoadContent(warp: true);
      return Flexible(
        fit: FlexFit.tight,
        // height: 500,
        child: Container(
          decoration: BoxDecoration(
            border: Border(top: BorderSide(width: 1, color: config.Colors.customeColor("#E0E9F8"))),
          ),
          //margin: EdgeInsets.only(bottom: _bottom),
          child: SingleChildScrollView(
            controller: _scrollController,
            reverse: true,
            padding: EdgeInsets.only(top: 10),

            // reverse: true,
            child: Container(
              margin: EdgeInsets.only(bottom: 8),
              child: ColumnStart(
                children: _messagesWidget(),
              ),
            ),
          ),
        ),
      );
    }

    Widget _bottomInputImageWidget() {
      if (pickFiles == null || pickFiles.length == 0) return Container();
      double _width = config.App.appWidth(100);
      var _w = _width / 6;
      return Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: config.Colors.inputBgColor(),
          ),
          padding: EdgeInsets.all(0),
          margin: EdgeInsets.all(0),
          height: _w,
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int i) {
              var _img = pickFiles[i];
              return FilePick(_img, _w, remove: (_image) {
                var _i = pickFiles.indexOf(_image);
                setState(() => pickFiles.removeAt(_i));
              });
            },
            itemCount: pickFiles != null ? pickFiles.length : 0,
            padding: EdgeInsets.all(0),
          ));
    }

    Widget _bottomWidget() {
      if (_con.isLoadingMessage || _con.isLoadingMembers) return Container();
      return Container(
        decoration: boxDecorations(bgColor: config.Colors.customeColor('#F0F3F4')),
        child: Column(
          children: [
            _bottomInputImageWidget(),
            Container(
              height: 71,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  // Container(
                  //   alignment: Alignment.center,
                  //   padding: EdgeInsets.only(left: 16, right: 9),
                  //   child: SvgPicture.asset('assets/icon/icon_image.svg'),
                  // ).onTap(() {
                  //   loadAssets();
                  // }),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 9, right: 16),
                    //margin: EdgeInsets.only(left: 0, right: 5),
                    child: SvgPicture.asset('assets/icon/icon_image.svg'),
                    // child: SvgPicture.asset('assets/icon/icon_file.svg'),
                  ).onTap(() {
                    loadFile();
                  }),
                  Expanded(
                    child: Container(
                      height: 50,
                      alignment: Alignment.center,
                      child: TextField(
                        controller: _ctrlText,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: config.Colors.backgroundColor(),
                          labelText: null,
                          hintText: "Nhập nội dung",
                          contentPadding: EdgeInsets.all(12),
                          border: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide(color: config.Colors.customeColor('#F0F3F4'))),
                          focusedBorder: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide(color: config.Colors.customeColor('#F0F3F4'))),
                          enabledBorder: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide(color: config.Colors.customeColor('#F0F3F4'))),
                        ),
                        style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor()),
                        onEditingComplete: () {
                          print("onEditingComplete");
                        },
                        onChanged: (e) {
                          setTyping();
                        },
                        onTap: () {
                          print("onTap");
                        },
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 16, right: 16),
                    child: IconButton(
                      icon: _isTyping.value ? SvgPicture.asset('assets/icon/icon_send_active.svg') : SvgPicture.asset('assets/icon/icon_send.svg'),
                      onPressed: () {
                        if (_con.isConnect == false) {
                          Helper.socketNoti(false, 'Vui lòng kiểm tra kết nối mạng!');
                          return;
                        }
                        if (!_isTyping.value) return;
                        _con.onSendMessage(_ctrlText.text, pickFiles);
                        setState(() {
                          _ctrlText.text = '';
                          pickFiles = <File>[];
                          _isTyping.value = false;
                        });
                        _scrollController.jumpTo(0);
                      },
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyChatRoom,
      appBar: AppBar(
        leading: AppbarIcon(),
        titleSpacing: 0,
        title: _headerWidget(),
        centerTitle: false,
      ),
      main: Container(
        width: config.App.appWidth(100),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        child: ColumnStart(
          children: <Widget>[
            _bodyWidget(),
            // Divider(height: 0, color: Colors.black26),
            _bottomWidget(),
          ],
        ),
      ),
    );
  }
}
