import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/UrlImage.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/models/notificationDto.dart';
import 'package:intl/intl.dart';

class NotificationDetailWidget extends StatefulWidget {
  final NotificationDto model;
  NotificationDetailWidget({Key key, this.model}) : super(key: key);
  @override
  _NotificationDetailWidgetState createState() => _NotificationDetailWidgetState();
}

class _NotificationDetailWidgetState extends State<NotificationDetailWidget> with SingleTickerProviderStateMixin {
  ScrollController _scrollController;
  NotificationDto dto;
  @override
  void initState() {
    super.initState();
    dto = widget.model;
  }

  @override
  Widget build(BuildContext context) {
    Widget _bodyView() {
      if (dto == null) return NoContent(warp: true, text: "Không có dữ liệu");
      var width = config.App.appWidth(100) - 32;
      var _boxImg = 55.0;
      var _boxWidth = width - _boxImg;
      return Container(
        color: config.Colors.backgroundColor(),
        padding: EdgeInsets.only(top: 5, left: 16, right: 16),
        child: ColumnStart(
          children: [
            RowWidget(
              Container(
                width: _boxImg,
                alignment: Alignment.centerLeft,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(_boxImg),
                  child: UrlImage(dto.icon, 'assets/icon/default-noti.png', fit: BoxFit.contain),
                ),
                padding: EdgeInsets.only(right: 8),
              ),
              Container(
                padding: EdgeInsets.only(left: 6),
                width: _boxWidth,
                child: ColumnStart(
                  children: [
                    Container(
                      child: LineText(
                        dto.title,
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: config.Colors.customeColor('#171051')),
                      ),
                      padding: EdgeInsets.only(bottom: 8),
                    ),
                    Container(
                      child: Text(
                        (dto.body ?? ""),
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                              color: config.Colors.customeColor('#171051').withOpacity(0.65),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                            ),
                      ),
                      padding: EdgeInsets.only(bottom: 8),
                    ),
                    Container(
                      child: SubText(
                        new DateFormat('HH:mm dd/MM/yyyy').format(dto.createdDate != null ? dto.createdDate : DateTime.now()),
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                              fontSize: 14,
                              color: config.Colors.customeColor("#979797"),
                              fontWeight: FontWeight.w400,
                            ),
                      ),
                      padding: EdgeInsets.only(bottom: 8),
                      // alignment: Alignment.centerRight,
                    ),
                  ],
                ),
              ),
              padding: 0,
            ),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: new GlobalKey<ScaffoldState>(),
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Thông báo"),
      ),
      body: _bodyView(),
    );
  }
}
