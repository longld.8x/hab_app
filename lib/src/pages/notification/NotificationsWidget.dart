import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/notification_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/UrlImage.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/notificationDto.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class NotificationsWidget extends StatefulWidget {
  @override
  _NotificationsWidgetState createState() => _NotificationsWidgetState();
}

class _NotificationsWidgetState extends StateMVC<NotificationsWidget> with SingleTickerProviderStateMixin {
  NotificationController _con;
  _NotificationsWidgetState() : super(NotificationController()) {
    _con = controller;
  }
  bool _clickNotify = false;
  ScrollController _scrollController;
  @override
  void initState() {
    super.initState();
    _con.pageState();
    initScroll();
  }

  initScroll() {
    _scrollController = ScrollController();
    _scrollController.addListener(() async {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset && (_con.serverTotalCount > ((_con.currentPage + 1) * _con.itemPage))) {
        _con.loadData(_con.currentPage + 1);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget notifiItem(int index, NotificationDto item) {
      var width = config.App.appWidth(100) - 32;
      var _boxImg = width * 0.15;
      var _boxWidth = width * 0.85;
      return Slidable(
        actionPane: SlidableDrawerActionPane(),
        actionExtentRatio: 0.15,
        child: Container(
          color: config.Colors.backgroundColor(),
          padding: EdgeInsets.only(top: 5, left: 16, right: 16),
          child: ColumnStart(
            children: [
              RowWidget(
                  Container(
                    // decoration: BoxDecoration(color: config.Colors.primaryColor()),
                    width: _boxImg,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.only(right: 10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(_boxImg),
                      child: UrlImage(
                        item.icon,
                        'assets/icon/default-noti.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 6),
                    width: _boxWidth,
                    child: ColumnStart(
                      children: [
                        Container(
                          padding: EdgeInsets.only(bottom: 5),
                          child: RowWidget(
                            Container(
                              padding: EdgeInsets.fromLTRB(0, 0, 6, 0),
                              child: Icon(Icons.circle, size: 9, color: (item.state == 0) ? config.Colors.primaryColor() : config.Colors.hintColor()),
                            ),
                            LineText(
                              item.title,
                              style: TextStyle(fontWeight: FontWeight.w500, color: config.Colors.customeColor('#171051')),
                              maxLines: 1,
                            ),
                            flex: 0,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 5),
                          child: Text(
                            item.body ?? "",
                            style: Theme.of(context).textTheme.subtitle1.copyWith(
                                  color: config.Colors.customeColor('#171051').withOpacity(0.65),
                                  fontWeight: FontWeight.w400,
                                ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 10),
                          child: SubText(
                            new DateFormat('HH:mm dd/MM/yyyy').format(item.createdDate),
                            style: Theme.of(context).textTheme.subtitle1.copyWith(
                                  color: config.Colors.customeColor("#979797"),
                                  fontWeight: FontWeight.w400,
                                ),
                          ),
                        ),
                      ],
                    ),
                  ).onTap(() {
                    if (item.state != 1) {
                      setState(() => item.state = 1);
                      _con.read(item.id);
                    }
                    _con.notificationToPage(item);
                    // Navigator.of(context).push(new MaterialPageRoute(
                    //   builder: (BuildContext context) => NotificationDetailWidget(model: item),
                    // ));
                  }),
                  padding: 0),
              Divider(
                height: 1,
                color: config.Colors.customeColor("#E0E9F8"),
              )
            ],
          ),
        ),
        actions: <Widget>[],
        secondaryActions: <Widget>[
          new IconSlideAction(
            color: config.Colors.customeColor("#FF0B27"),
            iconWidget: Container(
              child: SvgPicture.asset('assets/icon/trash.svg'),
              alignment: Alignment.center,
            ),
            onTap: () {
              _con.delete(item.id).then((ResponseMessage result) {
                if (result.success) {
                  var idx = _con.dataList.indexOf(item);
                  setState(() => _con.dataList.removeAt(idx));
                  Helper.successMessenge('Đã xoá thông báo');
                } else {
                  Helper.errorMessenge('Lỗi: ${result.message}');
                }
              });
            },
          ),
        ],
      );
    }

    Widget _bodyView() {
      if (_con.isLoading) return LoadContent(warp: true);
      if (_con.dataList == null || _con.dataList.length == 0)
        return NoContent(
          warp: true,
          text: "Không có thông báo mới",
        );
      return Container(
        padding: EdgeInsets.only(top: 3),
        color: config.Colors.inputBgColor(),
        child: new ListView.builder(
          shrinkWrap: true,
          controller: _scrollController,
          scrollDirection: Axis.vertical,
          itemCount: _con.dataList.length,
          itemBuilder: (BuildContext context, int i) {
            var _item = _con.dataList[i];
            return notifiItem(i, _item);
          },
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKey,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Thông báo"),
        actions: [
          PopupMenuButton<String>(
            onSelected: (String s) {
              _con.readAll().then((ResponseMessage result) {
                if (result.success) {
                  setState(() {
                    for (var x in _con.dataList.where((e) => e.state != 1)) {
                      x.state = 1;
                    }
                  });
                  Helper.successMessenge('Đánh dấu đã đọc');
                } else {
                  Helper.errorMessenge('Lỗi: ${result.message}');
                }
              });
            },
            offset: const Offset(0, 26),
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem<String>(
                  value: "read",
                  child: Container(
                    child: Text("Đánh dấu tất cả là đã đọc"),
                    padding: EdgeInsets.only(top: 5, right: 0, left: 0, bottom: 5),
                  ),
                ),
              ];
            },
            icon: SvgPicture.asset('assets/icon/more_option.svg'),
          ),
        ],
      ),
      main: _bodyView(),
    );
  }
}
