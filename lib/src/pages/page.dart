import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hab_app/src/controllers/home_controller.dart';
import 'package:hab_app/src/pages/chat/chatSearchWidget.dart';
import 'package:hab_app/src/pages/chat/chatWidget.dart';
import 'package:hab_app/src/pages/home/home.dart';
import 'package:hab_app/src/pages/layout/FloatActionButton.dart';
import 'package:hab_app/src/pages/profile/profile.dart';
import 'package:hab_app/src/pages/work/work.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../models/route_argument.dart';

// ignore: must_be_immutable
class PagesWidget extends StatefulWidget {
  dynamic currentTab;
  RouteArgument routeArgument;
  Widget currentPage = HomeWidget();
  PagesWidget({Key key, this.currentTab}) {
    if (currentTab != null) {
      if (currentTab is RouteArgument) {
        routeArgument = currentTab;
        currentTab = int.parse(currentTab.id);
      }
    } else {
      currentTab = 0;
    }
  }
  @override
  _PagesWidgetState createState() => _PagesWidgetState();
}

class _PagesWidgetState extends StateMVC<PagesWidget> {
  HomeController _con;
  _PagesWidgetState() : super(HomeController()) {
    _con = controller;
  }
  @override
  initState() {
    _con.mainState();
    _selectTab(widget.currentTab);
    super.initState();
  }

  // @override
  // void didUpdateWidget(PagesWidget oldWidget) {
  //   _selectTab(oldWidget.currentTab);
  //   super.didUpdateWidget(oldWidget);
  // }

  void _selectTab(int tabItem) {
    setState(() => widget.currentTab = tabItem);
    dynamic _page;
    switch (tabItem) {
      case 0:
        _page = HomeWidget();
        break;
      case 1:
        _page = WorkWidget(routeArgument: new RouteArgument(tag: "list"));
        break;
      case 3:
        _page = ChatWidget();
        break;
      case 4:
        _page = ProfileWidget();
        break;
      case 30:
        _page = ChatSearchWidget();
        break;
    }
    setState(() => widget.currentPage = _page);
  }

  @override
  Widget build(BuildContext context) {
    Widget barItem(String title, int index, Widget icon, Widget activeIcon) {
      var _currentTab = widget.currentTab;
      if (_currentTab == 30) _currentTab = 3;
      return MaterialButton(
        onPressed: () {
          _selectTab(index);
        },
        padding: EdgeInsets.all(0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            (_currentTab == index ? activeIcon : icon),
            SizedBox(height: 10),
            Text(
              title,
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    fontSize: 10,
                    color: _currentTab == index ? Theme.of(context).primaryColor : Theme.of(context).accentColor,
                  ),
            )
          ],
        ),
      );
    }

    return Scaffold(
      key: _con.scaffoldKeyMain,
      body: widget.currentPage,
      floatingActionButton: FloatActionButton(scaffoldKey: _con.scaffoldKeyMain, controller: _con),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: Theme.of(context).backgroundColor,
        elevation: 0,
        notchMargin: 0,
        child: Container(
          height: 68,
          decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            shape: BoxShape.rectangle,
            boxShadow: [
              BoxShadow(
                offset: Offset(0, 2),
                color: Color(0xFF000000).withOpacity(0.16),
                spreadRadius: 1,
                blurRadius: 4,
              ),
            ],
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: barItem('Trang chủ', 0, SvgPicture.asset('assets/icon/tab1.svg'), SvgPicture.asset('assets/icon/tab1Active.svg')),
              ),
              Expanded(
                flex: 2,
                child: barItem('Công việc', 1, SvgPicture.asset('assets/icon/tab2.svg'), SvgPicture.asset('assets/icon/tab2Active.svg')),
              ),
              Expanded(
                flex: 2,
                child: Container(),
              ),
              Expanded(
                flex: 2,
                child: barItem('Trò chuyện', 3, SvgPicture.asset('assets/icon/tab4.svg'), SvgPicture.asset('assets/icon/tab4Active.svg')),
              ),
              Expanded(
                flex: 2,
                child: barItem('Danh mục', 4, SvgPicture.asset('assets/icon/tab5.svg'), SvgPicture.asset('assets/icon/tab5Active.svg')),
              ),
            ],
          ),
        ),
      ),
      //),
    );
  }
}
