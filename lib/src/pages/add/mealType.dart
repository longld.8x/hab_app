import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/models/mealTypeDto.dart';

class MealType extends StatelessWidget {
  final List<MealTypeDto> data;
  const MealType({Key key, this.data}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var _height = 51 + 40 + (data == null || data.isEmpty ? 0 : data.length) * 50.0;
    return Container(
      height: _height,
      padding: EdgeInsets.all(0),
      child: Stack(
        children: [
          Container(
            width: config.App.appWidth(100),
            height: 50,
            alignment: Alignment.center,
            child: TitleText("Chọn Bữa Ăn"),
            decoration: BoxDecoration(
              border: new Border(bottom: BorderSide(width: 1, color: config.Colors.customeColor('#E0E9F8'))),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 51.0),
            child: data == null || data.isEmpty
                ? Center(child: Text('Không có dữ liệu.'))
                : Column(
                    children: data.map((d) {
                      return Container(
                        width: config.App.appWidth(100),
                        height: 49,
                        alignment: Alignment.centerLeft,
                        decoration: BoxDecoration(
                          border: new Border(bottom: BorderSide(width: 1, color: config.Colors.customeColor('#E0E9F8'))),
                        ),
                        child: Container(
                          padding: const EdgeInsets.only(left: 24.0, right: 24.0),
                          child: AccentText(d.mealName),
                        ),
                      ).onTap(() {
                        Navigator.of(context).pop();
                        Navigator.of(context).pushNamed('/Add/Meal', arguments: d);
                      });
                    }).toList(),
                  ),
          ),
        ],
      ),
    );
  }
}
