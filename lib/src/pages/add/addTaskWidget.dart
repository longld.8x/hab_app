import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/add_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class AddTaskWidget extends StatefulWidget {
  AddTaskWidget({Key key}) : super(key: key);
  @override
  _AddTaskWidgetState createState() => _AddTaskWidgetState();
}

class _AddTaskWidgetState extends StateMVC<AddTaskWidget> {
  AddController _con;
  double _btnBottom = 0;
  _AddTaskWidgetState() : super(AddController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _width = config.App.appWidth(100);

    Widget buildImageView() {
      double _width = config.App.appWidth(100);

      return Container(
        height: 500,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              child: SvgPicture.asset('assets/icon/camera.svg'),
              padding: EdgeInsets.all(24),
            ),
          ],
        ),
      );
    }

    Widget _bottomButtons() {
      setState(() => _btnBottom = 90);
      return Positioned(
        bottom: 0,
        // left: 0,
        // width: _width,
        // height: 90,
        child: Container(
          decoration: BoxDecoration(color: Colors.transparent),
          padding: EdgeInsets.only(bottom: 16, top: 16, left: 24, right: 24),
          width: _width,
          child: Button(
            isActive: true,
            text: "Hoàn thành",
            onPressed: () {
              Navigator.of(context).pop();
              Helper.successMessenge('Hoàn thành');
            },
          ),
        ),
      );
    }

    Widget _bodyView() {
      return Container(
        width: config.App.appWidth(100),
        height: config.App.appHeight(100),
        child: Stack(
          alignment: Alignment.topLeft,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: config.Colors.backgroundColor(),
              ),
              child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    children: <Widget>[
                      buildImageView(),
                    ],
                  ),
                  padding: EdgeInsets.only(bottom: _btnBottom),
                ),
              ),
            ),
            _bottomButtons(),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyAddMeal,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Thêm công việc"),
      ),
      isSingleChildScrollView: false,
      main: _bodyView(),
    );
  }
}
