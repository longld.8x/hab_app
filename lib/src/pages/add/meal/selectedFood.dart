import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/add_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/elements/foodChartWidget.dart';
import 'package:hab_app/src/elements/iconTotal.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/foodDto.dart';
import 'package:hab_app/src/pages/add/meal/addFood.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class SelectedFoodWidget extends StatefulWidget {
  final Function event;
  final List<FoodDto> foodsSelected;
  SelectedFoodWidget({Key key, this.foodsSelected, this.event}) : super(key: key);
  @override
  _SelectedFoodWidgetState createState() => _SelectedFoodWidgetState();
}

class _SelectedFoodWidgetState extends StateMVC<SelectedFoodWidget> {
  AddController _con;
  _SelectedFoodWidgetState() : super(AddController()) {
    _con = controller;
  }
  ScrollController _scrollController = ScrollController();
  TextEditingController _ctrlSearch = TextEditingController();

  @override
  void initState() {
    super.initState();
    _con.pageSelectedFoodState();
    initScroll();
  }

  initScroll() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset && (_con.serverTotalCount > ((_con.currentPage + 1) * _con.itemPage))) {
        _con.loadFoods(_con.currentPage + 1, _ctrlSearch.text);
      }
    });
  }

  void onAddFood(FoodDto item) {
    var _typeA = (item == null) ? 'add' : 'edit';
    Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (BuildContext context) => AddFoodWidget(
          actionType: _typeA,
          model: item,
          event: (FoodDto newDto, String t) {
            Helper.successMessenge(_typeA == 'add' ? "Tạo món ăn thành công!" : "Chỉnh sửa món ăn thành công!");
            setState(() => widget.foodsSelected.add(newDto));
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _width = config.App.appWidth(100);

    Widget _inputSearch() {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
          controller: _ctrlSearch,
          decoration: InputDecoration(
            filled: true,
            fillColor: config.Colors.inputBgColor(),
            labelText: null,
            hintText: "Tìm theo tên món ăn, đồ uống",
            prefixIcon: IconButton(
              onPressed: () {},
              icon: SvgPicture.asset('assets/icon/search.svg'),
            ),
            contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 15),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(40.0), borderSide: BorderSide(color: config.Colors.customeColor('#F1F1F1'))),
            focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(40.0), borderSide: BorderSide(color: config.Colors.customeColor('#F1F1F1'))),
            enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(40.0), borderSide: BorderSide(color: config.Colors.customeColor('#F1F1F1'))),
          ),
          style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor()),
          onEditingComplete: () {
            FocusScope.of(context).requestFocus(FocusNode());
            _con.loadFoods(0, _ctrlSearch.text);
          },
        ),
      );
    }

    Widget _titleSearch() {
      return Container(
        padding: EdgeInsets.all(16),
        alignment: Alignment.centerLeft,
        child: TitleText(_ctrlSearch.text.isEmpty ? "Gợi ý" : "Kết quả tìm kiếm"),
      );
    }

    Widget _dataView() {
      if (_con.isLoadingFoods) return LoadContent(warp: true);
      if (_con.dataFoods == null || _con.dataFoods.length == 0)
        return Container(
          width: config.App.appWidth(100),
          decoration: BoxDecoration(
            color: config.Colors.backgroundColor(),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: _width * 0.15, vertical: 50),
                child: AccentText("Không tìm thấy kết quả phù hợp với từ khoá. HAB chưa có món ăn này trong danh sách?."),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                width: _width,
                child: Button(
                  text: "Tạo món mới",
                  onPressed: () {
                    onAddFood(null);
                  },
                ),
              ),
            ],
          ),
        );

      return Container(
        child: ListView.builder(
          controller: _scrollController,
          itemCount: _con.dataFoods.length,
          itemBuilder: (_context, index) {
            var _item = _con.dataFoods[index];
            return Container(
              child: FoodItemWidget(
                data: _item,
                actionType: ['edit', 'add'],
                action: (String type) {
                  // them mon
                  if (type == 'add') {
                    setState(() {
                      widget.foodsSelected.add(_item);
                    });
                  } else if (type == 'edit') {
                    onAddFood(_item);
                  }
                },
              ),
            );
          },
          padding: EdgeInsets.all(0),
        ),
      );
    }

    Widget _bodyView() {
      return Container(
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        child: Column(
          children: <Widget>[
            _inputSearch(),
            _titleSearch(),
            Expanded(
              child: _dataView(),
            ),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeySelectedFood,
      appBar: AppBar(
        leading: AppbarIcon(
          onPressed: () {
            Helper.hideLoaderOverlay();
            Navigator.of(context).pop();
            widget.event(widget.foodsSelected);
          },
        ),
        title: TitleText("Chọn món"),
        actions: [
          IconTotalWidget(
            icon: SvgPicture.asset(
              'assets/icon/restaurant.svg',
              width: 20,
              height: 20,
            ),
            total: widget.foodsSelected != null ? widget.foodsSelected.length : 0,
            action: () {
              Helper.hideLoaderOverlay();
              Navigator.of(context).pop();
              widget.event(widget.foodsSelected);
            },
          )
        ],
      ),
      isSingleChildScrollView: false,
      body: _bodyView(),
    );
  }
}
