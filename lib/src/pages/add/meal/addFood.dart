import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/add_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/foodDto.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class AddFoodWidget extends StatefulWidget {
  final Function event;
  final FoodDto model;
  final String actionType;
  AddFoodWidget({Key key, this.model, this.actionType, this.event}) : super(key: key);
  @override
  _AddFoodWidgetState createState() => _AddFoodWidgetState();
}

class _AddFoodWidgetState extends StateMVC<AddFoodWidget> {
  AddController _con;
  _AddFoodWidgetState() : super(AddController()) {
    _con = controller;
  }
  FoodDto _model = null;
  bool _activeBtn = false;
  TextEditingController _ctrlName = TextEditingController();
  TextEditingController _ctrlQty = TextEditingController();
  TextEditingController _ctrlCalo = TextEditingController();
  TextEditingController _ctrlCarb = TextEditingController();
  TextEditingController _ctrlFat = TextEditingController();
  TextEditingController _ctrlProtein = TextEditingController();
  String _unit = '';

  @override
  void initState() {
    super.initState();
    _con.pageAddFoodState();
    _model = widget.model;

    setCtrl();
    _activeBtn = onActiveBtn();
  }

  void setCtrl() {
    if (_model != null) {
      _ctrlName.text = _model.foodName;
      _ctrlQty.text = _model.quantity.toString();
      _ctrlCalo.text = _model.calories.toString();
      _ctrlCarb.text = _model.carb.toString();
      _ctrlFat.text = _model.fat.toString();
      _ctrlProtein.text = _model.protein.toString();
      _unit = _model.foodUnitUnitName;
    }
  }

  bool onActiveBtn() {
    var t1 = (_ctrlName.text.isNotEmpty && _ctrlName.text.trim().isNotEmpty);
    var t2 = (_ctrlQty.text.isNotEmpty && _ctrlQty.text.trim().isNotEmpty);
    var t3 = (_ctrlCalo.text.isNotEmpty && _ctrlCalo.text.trim().isNotEmpty);
    var t4 = (_ctrlCarb.text.isNotEmpty && _ctrlCarb.text.trim().isNotEmpty);
    var t5 = (_ctrlFat.text.isNotEmpty && _ctrlFat.text.trim().isNotEmpty);
    var t6 = (_ctrlProtein.text.isNotEmpty && _ctrlProtein.text.trim().isNotEmpty);
    var t7 = (_unit.isNotEmpty && _unit.trim().isNotEmpty);
    return t1 && t2 && t3 && t4 && t5 && t6 && t7;
  }

  @override
  Widget build(BuildContext context) {
    double _width = config.App.appWidth(100);

    Widget _formWidget() {
      if (_con.dataUnits == null || _con.dataUnits.length == 0) return LoadContent();
      if (_unit == null || _unit.isEmpty) _unit = _con.dataUnits.first.unitName;
      print('_unit $_unit');
      return Form(
        key: _con.formKeyEdit,
        child: Container(
          padding: EdgeInsets.only(left: 24, right: 24, bottom: 0, top: 24),
          decoration: BoxDecoration(color: config.Colors.backgroundColor()),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InputField(
                controller: _ctrlName,
                labelText: 'Tên món ăn',
                hintText: 'Tên món ăn',
                keyboardType: TextInputType.text,
                onChanged: (String newValue) {
                  setState(() => _activeBtn = onActiveBtn());
                },
                validator: (String value) {
                  return null;
                },
              ),
              SizedBox(height: 24),
              Container(
                child: Row(
                  children: [
                    Expanded(
                      child: InputField(
                        controller: _ctrlQty,
                        labelText: 'Số lượng',
                        hintText: 'Số lượng',
                        keyboardType: TextInputType.number,
                        maxLength: 3,
                        onChanged: (String newValue) {
                          setState(() => _activeBtn = onActiveBtn());
                        },
                      ),
                    ),
                    SizedBox(width: 16),
                    Expanded(
                      child: Container(
                        child: DropdownButtonFormField(
                          decoration: InputDecoration(
                            hintText: 'Loại đơn vị',
                            labelText: 'Loại đơn vị',
                            fillColor: config.Colors.inputBgColor(),
                            filled: true,
                            contentPadding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                          ),
                          //isExpanded: true,
                          value: _unit,
                          icon: Icon(
                            Icons.keyboard_arrow_down,
                            color: config.Colors.accentColor(),
                          ),
                          onChanged: (String v) {
                            setState(() => _activeBtn = onActiveBtn());
                          },
                          items: _con.dataUnits.map((c) {
                            print("DropdownMenuItem $c");
                            return DropdownMenuItem(
                              child: Text(
                                c.unitName,
                              ),
                              value: c.unitName,
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 24),
              InputField(
                controller: _ctrlCalo,
                labelText: 'Calo',
                hintText: 'Calo',
                maxLength: 6,
                keyboardType: TextInputType.number,
                onChanged: (String newValue) {
                  setState(() => _activeBtn = onActiveBtn());
                },
              ),
              SizedBox(height: 24),
              InputField(
                controller: _ctrlCarb,
                labelText: 'Carbs(g)',
                hintText: 'Carbs(g)',
                maxLength: 6,
                keyboardType: TextInputType.number,
                onChanged: (String newValue) {
                  setState(() => _activeBtn = onActiveBtn());
                },
              ),
              SizedBox(height: 24),
              InputField(
                controller: _ctrlFat,
                labelText: 'Chất béo (g)',
                hintText: 'Chất béo (g)',
                maxLength: 6,
                keyboardType: TextInputType.number,
                onChanged: (String newValue) {
                  setState(() => _activeBtn = onActiveBtn());
                },
              ),
              SizedBox(height: 24),
              InputField(
                controller: _ctrlProtein,
                labelText: 'Protein (g)',
                hintText: 'Protein (g)',
                maxLength: 6,
                keyboardType: TextInputType.number,
                onChanged: (String newValue) {
                  setState(() => _activeBtn = onActiveBtn());
                },
              ),
              SizedBox(height: 24),
              Button(
                isActive: _activeBtn,
                text: "Cập nhật",
                onPressed: () {
                  if (_con.formKeyEdit.currentState.validate()) {
                    var model = new FoodDto();
                    try {
                      model.foodName = _ctrlName.text;
                      model.quantity = int.parse(_ctrlQty.text);
                      var u = _con.dataUnits.firstWhere((x) => x.unitName == _unit);
                      if (u == null) {
                        Helper.errorMessenge("Lỗi: Vui lòng kiểm tra lại loại đơn vị");
                        return;
                      }
                      model.foodUnitUnitName = u.unitName;
                      model.foodUnitId = u.id;
                      model.calories = double.parse(_ctrlCalo.text);
                      model.carb = double.parse(_ctrlCarb.text);
                      model.fat = double.parse(_ctrlFat.text);
                      model.protein = double.parse(_ctrlProtein.text);
                    } catch (e) {
                      Helper.errorMessenge("Lỗi: ${e.toString()}");
                    }
                    var t1 = (model.foodName == _model.foodName);
                    var t2 = (model.quantity == _model.quantity);
                    var t3 = (model.foodUnitUnitName == _model.foodUnitUnitName && model.foodUnitId == _model.foodUnitId);
                    var t4 = (model.calories == _model.calories);
                    var t5 = (model.carb == _model.carb);
                    var t6 = (model.fat == _model.fat);
                    var t7 = (model.protein == _model.protein);
                    if (t1 && t2 && t3 && t4 && t5 && t6 && t7) {
                      Navigator.of(context).pop();
                      print("ko update");
                    } else {
                      _con.addFood(model).then((ResponseMessage result) {
                        if (result.success) {
                          Navigator.of(context).pop();

                          if (widget.event != null) widget.event(model, widget.actionType);
                        } else
                          Helper.errorMessenge(result.message);
                      });
                    }
                  }
                },
              ),
            ],
          ),
        ),
      );
    }

    Widget _bodyView() {
      return Container(
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        child: Column(
          children: <Widget>[
            _formWidget(),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyAddFood,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText(widget.actionType == 'add' ? "Tạo món ăn" : "Chỉnh sửa món ăn"),
      ),
      body: _bodyView(),
    );
  }
}
