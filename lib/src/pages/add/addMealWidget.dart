import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/add_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/elements/foodChartWidget.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/foodDto.dart';
import 'package:hab_app/src/models/mealTypeDto.dart';
import 'package:hab_app/src/pages/add/meal/addFood.dart';
import 'package:hab_app/src/pages/add/meal/selectedFood.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class AddMealWidget extends StatefulWidget {
  final MealTypeDto type;
  AddMealWidget({Key key, this.type}) : super(key: key);
  @override
  _AddMealWidgetState createState() => _AddMealWidgetState();
}

class _AddMealWidgetState extends StateMVC<AddMealWidget> {
  AddController _con;
  MealTypeDto _type;
  List<FoodDto> _foods = [];
  _AddMealWidgetState() : super(AddController()) {
    _con = controller;
  }
  List<Asset> images = <Asset>[];
  double _btnBottom = 0;

  @override
  void initState() {
    _type = widget.type;
    super.initState();
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = <Asset>[];
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 10,
        selectedAssets: images,
        enableCamera: true,
        materialOptions: MaterialOptions(
          allViewTitle: "Chụp bữa ăn",
          actionBarColor: "#1A60FF",
          actionBarTitleColor: "#FFFFFF",
          lightStatusBar: false,
          statusBarColor: '#1A60FF',
          startInAllView: true,
          selectCircleStrokeColor: "#171051",
          selectionLimitReachedText: "Số lượng vượt quá giới hạn cho phép.",
          textOnNothingSelected: "Vui lòng chọn 1 ảnh.",
        ),
      );
      setState(() {
        images = resultList;
      });
    } on Exception catch (e) {
      Helper.errorMessenge(e.toString());
    }
  }

  Widget buildImageView() {
    double _width = config.App.appWidth(100);
    if (images == null || images.length == 0)
      return Container(
        height: 300,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              child: SvgPicture.asset('assets/icon/camera.svg'),
              padding: EdgeInsets.all(24),
            ),
            Container(
              alignment: Alignment.center,
              child: AccentText('Chụp ảnh bữa ăn. \n Bạn có thể chụp nhiều ảnh cho 1 bữa ăn.', textAlign: TextAlign.center),
              padding: EdgeInsets.symmetric(horizontal: 24, vertical: 0),
            ),
          ],
        ),
      ).onTap(() {
        loadAssets();
      });
    return Container(
      height: 300,
      decoration: BoxDecoration(
        color: config.Colors.backgroundColor(),
      ),
      // child: PageView.builder(
      //   itemCount: images.length,
      //   itemBuilder: (context, index) {
      //     return AssetThumb(
      //       asset: images[index],
      //       height: 300,
      //       width: _width.ceil(),
      //     );
      //   },
      // ),
      child: CarouselSlider(
        options: CarouselOptions(
          height: 300,
          enableInfiniteScroll: false, // vòng lặp
          viewportFraction: 1,
          initialPage: 0,
          autoPlay: false,
        ),
        items: images.map((item) {
          var _i = images.indexOf(item);
          var _len = images.length;
          return Builder(
            builder: (BuildContext context) {
              return Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(horizontal: 0),
                child: Stack(
                  children: [
                    AssetThumb(
                      asset: item,
                      height: 300,
                      width: _width.ceil(),
                    ),
                    Positioned(
                      bottom: 8.0,
                      right: 8.0,
                      child: Container(
                        child: AccentText(
                          "${_i + 1}/${_len}",
                          style: TextStyle(color: config.Colors.customeColor('#ffffff'), fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 8.0,
                      left: _width / 2 - (((6 + 2 + 4.0) * _len + 6) / 2),
                      child: Container(
                        height: 6,
                        width: (6 + 2 + 4.0) * _len + 6,
                        alignment: Alignment.center,
                        child: ListView.builder(
                          itemCount: _len,
                          shrinkWrap: false,
                          physics: NeverScrollableScrollPhysics(),
                          scrollDirection: Axis.horizontal,
                          padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              width: index == _i ? 12 : 6,
                              height: 6,
                              padding: EdgeInsets.fromLTRB(2, 0, 2, 0),
                              margin: EdgeInsets.fromLTRB(2, 0, 2, 0),
                              decoration: BoxDecoration(
                                color: index == _i ? config.Colors.primaryColor() : config.Colors.customeColor('#FFFFFF', opacity: 0.5),
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    Positioned(
                      top: 16.0,
                      right: 16.0,
                      child: Container(
                        width: 32,
                        height: 32,
                        decoration: BoxDecoration(
                          color: config.Colors.primaryColor(),
                          borderRadius: BorderRadius.all(Radius.circular(32)),
                        ),
                        padding: EdgeInsets.all(8),
                        child: SvgPicture.asset('assets/icon/edit.svg'),
                      ).onTap(() {
                        loadAssets();
                      }),
                    ),
                  ],
                ),
              );
            },
          );
        }).toList(),
      ),
    );
  }

  onSelectedFood() {
    Navigator.of(context).push(new MaterialPageRoute(
      builder: (BuildContext context) => SelectedFoodWidget(
        foodsSelected: _foods,
        event: (List<FoodDto> foodsSelected) {
          setState(() => _foods = foodsSelected);
        },
      ),
    ));
  }

  Widget buildFoodItems() {
    double _width = config.App.appWidth(100);
    if (_foods == null || _foods.length == 0) {
      return Container(
        child: Column(
          children: [
            Container(
              child: FoodChartWidget(
                dataItem: [],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: _width * 0.15, vertical: 50),
              child: AccentText("Nhập các món bạn đã ăn/uống trong bữa."),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 24, vertical: 24),
              width: _width,
              child: Button(
                text: "Thêm món",
                onPressed: onSelectedFood,
              ),
            ),
          ],
        ),
      );
    }
    return Container(
      child: FoodChartWidget(
        dataItem: _foods,
        onAdd: onSelectedFood,
        actionType: ['edit', 'remove'],
        actionEvent: (FoodDto item, String type) {
          if (type == 'remove') {
            var _i = _foods.indexOf(item);
            setState(() {
              _foods.removeAt(_i);
            });
            Helper.successMessenge("Xóa món ăn thành công!");
          } else if (type == 'edit') {
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (BuildContext context) => AddFoodWidget(
                actionType: 'edit',
                model: item,
                event: (FoodDto newDto, String t) {
                  var _i = _foods.indexOf(item);
                  setState(() {
                    _foods[_i] = newDto;
                  });
                  Helper.successMessenge("Chỉnh sửa món ăn thành công!");
                },
              ),
            ));
          }
        },
      ),
      padding: EdgeInsets.only(bottom: _btnBottom),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _width = config.App.appWidth(100);

    Widget _bottomButtons() {
      if (_foods == null || _foods.length == 0) {
        setState(() => _btnBottom = 0);
        return Container();
      }
      setState(() => _btnBottom = 90);
      return Positioned(
        bottom: 0,
        // left: 0,
        // width: _width,
        // height: 90,
        child: Container(
          decoration: BoxDecoration(color: Colors.transparent),
          padding: EdgeInsets.only(bottom: 16, top: 16, left: 24, right: 24),
          width: _width,
          child: Button(
            isActive: true,
            text: "Hoàn thành",
            onPressed: () {
              FocusScope.of(context).requestFocus(FocusNode());
              _con.addMeal(_type, images, _foods).then((result) {
                if (result.success) {
                  Navigator.of(context).pop();
                  Helper.successMessenge('Chụp bữa ăn thành công');
                } else
                  Helper.errorMessenge(result.message);
              });
            },
          ),
        ),
      );
    }

    Widget _bodyView() {
      if (_type == null) return NoContent();
      return Container(
        width: config.App.appWidth(100),
        height: config.App.appHeight(100),
        child: Stack(
          alignment: Alignment.topLeft,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: config.Colors.backgroundColor(),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    buildImageView(),
                    buildFoodItems(),
                  ],
                ),
              ),
            ),
            _bottomButtons(),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyAddMeal,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText(_type == null ? "Thêm bữa ăn" : _type.mealName),
      ),
      isSingleChildScrollView: false,
      main: _bodyView(),
    );
  }
}
