import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/add_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/ImagePick.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class AddWorkoutWidget extends StatefulWidget {
  AddWorkoutWidget({Key key}) : super(key: key);
  @override
  _AddWorkoutWidgetState createState() => _AddWorkoutWidgetState();
}

class _AddWorkoutWidgetState extends StateMVC<AddWorkoutWidget> {
  AddController _con;
  double _btnBottom = 0;
  _AddWorkoutWidgetState() : super(AddController()) {
    _con = controller;
  }

  ScrollController _scrollController = ScrollController();
  List<Asset> images = <Asset>[];
  TextEditingController _ctrlTotalTime = TextEditingController();
  TextEditingController _ctrlCalo = TextEditingController();
  DateTime _tmpTime = null;
  int _totalMinute = 0;
  bool _activeBtn = false;
  bool _showBtn = true;

  var keyboardVisibilityController = KeyboardVisibilityController();
  @override
  void initState() {
    super.initState();
    _con.pageWorkoutState();
    keyboardVisibilityController.onChange.listen(onVisibilityChange);
  }

  onVisibilityChange(bool visible) {
    setState(() => _showBtn = !visible);
  }

  bool onActiveBtn() {
    setState(() => _activeBtn = (images.length > 0 && _ctrlTotalTime.text.isNotEmpty && _ctrlCalo.text.isNotEmpty));
  }

  void onCalculateCalo() {
    print("onCalculateCalo ${_con.workoutType} -  ${_totalMinute}");
    double _calo = 0;
    if (_con.workoutType == null && _totalMinute <= 0) {
      _calo = 0;
    } else {
      _calo = (_con.workoutType.workoutCalo / _con.workoutType.workoutUnit) * _totalMinute;
    }

    setState(() => _ctrlCalo.text = _calo.toStringAsFixed(0).toString());
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = <Asset>[];
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 10,
        selectedAssets: images,
        enableCamera: true,
        materialOptions: MaterialOptions(
          allViewTitle: "Ảnh vận động",
          actionBarColor: "#1A60FF",
          actionBarTitleColor: "#FFFFFF",
          lightStatusBar: false,
          statusBarColor: '#1A60FF',
          startInAllView: true,
          selectCircleStrokeColor: "#171051",
          selectionLimitReachedText: "Số lượng vượt quá giới hạn cho phép.",
          textOnNothingSelected: "Vui lòng chọn 1 ảnh.",
        ),
      );
      setState(() {
        images = resultList;
      });
      onActiveBtn();
    } on Exception catch (e) {
      Helper.errorMessenge(e.toString());
    }
  }

  Future<void> _selectDate(context) async {
    var now = DateTime.now();
    var today = DateTime(now.year, now.month, now.day);
    if (_tmpTime == null) _tmpTime = today;
    print("_tmpTime = today $_tmpTime = $today");
    await showModalBottomSheet(
      isDismissible: false,
      context: context,
      builder: (BuildContext e) {
        return Container(
          height: 260,
          child: Column(
            children: [
              Container(
                color: config.Colors.backgroundColor(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: AccentText('Hủy', style: TextStyle(color: config.Colors.primaryColor())).onTap(() {
                        Navigator.pop(context);
                        setState(() => _ctrlTotalTime.text = '');
                        onActiveBtn();
                      }),
                    ),
                    Expanded(
                      flex: 4,
                      child: Container(
                        alignment: Alignment.center,
                        child: TitleText('Chọn thời gian'),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: AccentText('Xác nhận', style: TextStyle(color: config.Colors.primaryColor())).onTap(() {
                        Navigator.pop(context);
                        if (_tmpTime != null) {
                          var str = "";
                          if (_tmpTime.hour > 0) str += '${_tmpTime.hour} giờ';
                          if (str.length > 0) str += ' ';
                          if (_tmpTime.minute > 0) str += '${_tmpTime.minute} phút';

                          setState(() {
                            _totalMinute = _tmpTime.hour * 60 + _tmpTime.minute;
                            _ctrlTotalTime.text = str;
                          });
                        }
                        onActiveBtn();
                        onCalculateCalo();
                      }),
                    ),
                  ],
                ).paddingAll(8.0),
              ),
              Expanded(
                child: CupertinoTheme(
                  data: CupertinoThemeData(
                    textTheme: CupertinoTextThemeData(dateTimePickerTextStyle: TextStyle()),
                  ),
                  child: CupertinoDatePicker(
                    backgroundColor: config.Colors.backgroundColor(),
                    minimumDate: today,
                    minuteInterval: 1,
                    mode: CupertinoDatePickerMode.time,
                    use24hFormat: true,
                    onDateTimeChanged: (DateTime dateTime) {
                      setState(() {
                        _tmpTime = dateTime;
                      });
                    },
                    initialDateTime: today,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget buildFromView() {
    return Form(
      key: _con.formKeyWorkout,
      child: Container(
        decoration: BoxDecoration(color: config.Colors.backgroundColor()),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(left: 24, right: 24),
              child: DropdownButtonFormField(
                decoration: InputDecoration(
                  hintText: 'Chọn loại vận động *',
                  labelText: 'Chọn loại vận động *',
                  fillColor: config.Colors.inputBgColor(),
                  filled: true,
                  contentPadding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                ),
                //isExpanded: true,
                value: _con.workoutType.workoutCategoryName,
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: config.Colors.accentColor(),
                ),
                onChanged: (v) {
                  print("onChanged $v");
                  var _index = _con.dataWorkoutType.indexWhere((e) => e.workoutCategoryName == v);
                  setState(() => _con.workoutType = _con.dataWorkoutType[_index]);
                  onCalculateCalo();
                },
                items: _con.dataWorkoutType.map((c) {
                  return DropdownMenuItem(
                    child: Text(
                      c.workoutCategoryName.toString(),
                    ),
                    value: c.workoutCategoryName.toString(),
                  );
                }).toList(),
              ),
            ),
            SizedBox(height: 24),
            Container(
              padding: EdgeInsets.only(left: 24, right: 24),
              child: InputField(
                readOnly: true,
                controller: _ctrlTotalTime,
                labelText: 'Thời gian vận động *',
                hintText: '',
                maxLength: 6,
                keyboardType: TextInputType.number,
                suffixIcon: IconButton(
                  onPressed: () {},
                  icon: SvgPicture.asset(
                    'assets/icon/clock2.svg',
                    width: 16,
                  ),
                ),
                validator: (newValue) {
                  if (newValue.isEmpty) return "Vui lòng chọn thời gian vận động";
                  return null;
                },
                onTap: () => _selectDate(context),
                onChanged: (_) {
                  onActiveBtn();
                },
              ),
            ),
            SizedBox(height: 24),
            Container(
              padding: EdgeInsets.only(left: 24, right: 24),
              margin: EdgeInsets.only(bottom: 10),
              child: InputField(
                controller: _ctrlCalo,
                labelText: 'Lượng calo tiêu hao',
                hintText: 'Lượng calo tiêu hao',
                maxLength: 6,
                keyboardType: TextInputType.number,
                validator: (newValue) {
                  if (newValue.isEmpty) return "Vui lòng nhập lượng calo tiêu hao";
                  return null;
                },
                onChanged: (String newValue) {
                  onActiveBtn();
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildImageView() {
    double _width = config.App.appWidth(100);
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: config.Colors.backgroundColor(),
      ),
      padding: EdgeInsets.only(left: 16, right: 16),
      height: ((images.length + 1) / 3).ceil() * (_width / 3),
      child: GridView.count(
        crossAxisCount: 3,
        physics: NeverScrollableScrollPhysics(),
        children: List.generate(images.length + 1, (i) {
          var _w = (_width / 3) - 3.0;
          if (i != images.length) {
            var _img = images[i];
            return ImagePick(_img, _w, remove: (_image) {
              var _i = images.indexOf(_image);
              setState(() => images.removeAt(_i));
            });
          }
          return Container(
            width: _w,
            height: _w,
            margin: EdgeInsets.all(8),
            child: DottedBorder(
              color: config.Colors.primaryColor(),
              borderType: BorderType.RRect,
              strokeWidth: 1,
              radius: const Radius.circular(8.0),
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      child: SvgPicture.asset(
                        'assets/icon/addFrame.svg',
                      ),
                      padding: EdgeInsets.only(bottom: 10),
                    ),
                    Container(child: AccentText('Thêm ảnh')),
                  ],
                ),
              ).onTap(() {
                loadAssets();
              }),
            ),
          );
        }),
      ),
    );
  }

  Widget _bottomButtons() {
    double _width = config.App.appWidth(100);
    if (!_showBtn) {
      return Container();
    }
    setState(() => _btnBottom = 90);
    return Positioned(
      bottom: 0,
      child: Container(
        decoration: BoxDecoration(color: Colors.transparent),
        padding: EdgeInsets.only(bottom: 16, top: 16, left: 24, right: 24),
        width: _width,
        child: Button(
          isActive: _activeBtn,
          text: "Hoàn thành",
          onPressed: () {
            FocusScope.of(context).requestFocus(FocusNode());
            _con.addWorkout(_con.workoutType, _tmpTime, _ctrlTotalTime.text, _ctrlCalo.text, images).then((result) {
              if (result.success) {
                Navigator.of(context).pop();
                Helper.successMessenge('Thêm vận động thành công');
              } else
                Helper.errorMessenge(result.message);
            });
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _width = config.App.appWidth(100);

    Widget _bodyView() {
      if (_con.dataWorkoutType == null || _con.dataWorkoutType.length == 0)
        return Container(
          padding: EdgeInsets.only(top: 0, left: 0, right: 0),
          child: LoadContent(warp: true),
        );
      return Container(
        width: config.App.appWidth(100),
        height: config.App.appHeight(100),
        child: Stack(
          alignment: Alignment.topLeft,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: config.Colors.backgroundColor(),
              ),
              child: SingleChildScrollView(
                controller: _scrollController,
                child: Container(
                  child: Column(
                    children: <Widget>[
                      buildFromView(),
                      buildImageView(),
                    ],
                  ),
                  padding: EdgeInsets.only(bottom: _btnBottom),
                ),
              ),
            ),
            _bottomButtons(),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyWorkout,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Thêm vận động"),
      ),
      isSingleChildScrollView: false,
      main: _bodyView(),
    );
  }
}
