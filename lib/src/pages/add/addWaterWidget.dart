import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hab_app/src/controllers/add_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:path_drawing/path_drawing.dart';

class AddWaterWidget extends StatefulWidget {
  AddWaterWidget({Key key}) : super(key: key);
  @override
  _AddWaterWidgetState createState() => _AddWaterWidgetState();
}

class _AddWaterWidgetState extends StateMVC<AddWaterWidget> {
  AddController _con;
  _AddWaterWidgetState() : super(AddController()) {
    _con = controller;
  }
  double _btnBottom = 0;
  bool _activeBtn = false;
  bool _showBtn = true;
  double _waterPercent = 0.0;
  double _waterVal = 0.0;

  List<double> labels = [250, 500, 750, 1000];
  final _ctrlQty = TextEditingController(text: '0');
  var keyboardVisibilityController = KeyboardVisibilityController();
  @override
  void initState() {
    super.initState();
    _con.pageWaterState();
    keyboardVisibilityController.onChange.listen(onVisibilityChange);
    _ctrlQty.addListener(onQtyChange);
  }

  onVisibilityChange(bool visible) {
    setState(() => _showBtn = !visible);
  }

  onQtyChange() {
    double v = 0;
    try {
      v = double.parse(_ctrlQty.text);
    } catch (e) {
      v = 0;
    }
    var _v = v / 1000;
    setState(() {
      _waterVal = v;
      _waterPercent = (_v > 1 ? 1 : _v);
      _activeBtn = _waterPercent > 0;
    });
  }

  GlobalKey keyPainter = GlobalKey();

  onTabPainterUp(details) {
    RenderBox box = keyPainter.currentContext.findRenderObject();
    Offset _position = box.localToGlobal(Offset.zero);
    // double _positionX = _position.dx;
    // double _currentX = details.globalPosition.dx;
    // var _vx = _currentX - _positionX;
    // print("_v _v_v_v_v_v_v $_vx");
    double _positionY = _position.dy;
    double _currentY = details.globalPosition.dy;
    double _vy = 250 - (_currentY - _positionY - 90);
    var _height = (_vy > 250 ? 250 : (_vy < 0 ? 0 : _vy));

    var _v = (_height / 250);
    print("_vy_vy_vy_vy $_vy _height $_height _v_v_v_v $_v");
    setState(() {
      _waterPercent = _v;
      _waterVal = _v * 1000;
      _activeBtn = _waterPercent > 0;
      _ctrlQty.text = _waterVal.toStringAsFixed(0);
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    _ctrlQty.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double _width = config.App.appWidth(100);

    Widget buildImageView() {
      var tMin = 334.0 - 5;
      var tMax = 90.0;
      var h = (tMin - tMax) * _waterPercent;
      var _position = (tMin - h);
      return Container(
        height: 400,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: config.Colors.primaryColor(),
        ),
        child: Stack(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              children: [
                Expanded(
                  child: Container(
                    height: 400,
                    alignment: Alignment.bottomRight,
                    child: Stack(children: <Widget>[
                      Positioned(
                        top: _position,
                        right: 0,
                        child: Container(
                          child: Text(
                            '${Helper.formatNumber(_waterVal, prefix: 'ml')}',
                            style: TextStyle(color: config.Colors.customeColor('#ffffff'), fontSize: 20),
                          ),
                          padding: EdgeInsets.only(right: 30),
                          decoration: BoxDecoration(
                            border: new Border(
                              bottom: BorderSide(color: config.Colors.customeColor('#ffffff'), width: 2),
                            ),
                          ),
                        ),
                      ),
                    ]),
                  ),
                ),
                Expanded(
                  child: Container(
                    key: keyPainter,
                    width: 115,
                    height: 345,
                    child: new GestureDetector(
                      onTapUp: onTabPainterUp,
                      child: CustomPaint(
                        painter: WaterPainter(_waterPercent),
                        child: Container(),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      );
    }

    Widget buildFromView() {
      return Form(
        key: _con.formKeyWater,
        child: Container(
          decoration: BoxDecoration(color: config.Colors.backgroundColor()),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 10, top: 24),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Container(
                    padding: EdgeInsets.only(left: 24, right: 24),
                    child: Row(
                      children: List<Widget>.generate(
                        labels.length,
                        (int i) {
                          var _v = labels[i];
                          return Container(
                            margin: EdgeInsets.only(right: 10),
                            child: FilterChip(
                              showCheckmark: false,
                              label: TextIcon(
                                AccentText(
                                  '${Helper.formatNumber(_v)}ml',
                                  style: TextStyle(color: _v == _waterVal ? config.Colors.customeColor('#ffffff') : config.Colors.accentColor()),
                                ),
                                (_v == _waterVal ? Icon(Icons.check, color: config.Colors.customeColor('#ffffff')) : Container()),
                                padding: 6,
                              ),
                              padding: EdgeInsets.only(left: 12, right: _v == _waterVal ? 5 : 12, top: 10, bottom: 10),
                              selected: _v == _waterVal,
                              selectedColor: _v == _waterVal ? config.Colors.primaryColor() : config.Colors.backgroundColor(),
                              backgroundColor: _v == _waterVal ? config.Colors.primaryColor() : config.Colors.backgroundColor(),
                              shape: StadiumBorder(
                                side: BorderSide(
                                  color: _v == _waterVal ? config.Colors.primaryColor() : config.Colors.customeColor('#E4E4E4', opacity: 0.6),
                                ),
                              ),

                              onSelected: (bool selected) {
                                setState(() => _ctrlQty.text = _v.toStringAsFixed(0));
                                onQtyChange();
                              },
                              //autofocus: true,
                            ),
                          );
                        },
                      ).toList(),
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 24, right: 24),
                margin: EdgeInsets.only(bottom: 10),
                child: AccentText('Hoặc nhập số'),
              ),
              Container(
                padding: EdgeInsets.only(left: 24, right: 24),
                margin: EdgeInsets.only(bottom: 10),
                child: InputField(
                  controller: _ctrlQty,
                  labelText: 'Nhập lượng nước đã uống (ml)',
                  hintText: 'Nhập lượng nước đã uống (ml)',
                  maxLength: 6,
                  keyboardType: TextInputType.number,

                  // onEditingComplete: () {
                  //   setState(() => _showBtn = true);
                  // },
                  // onTap: () {
                  //   setState(() => _showBtn = false);
                  // },
                ),
              ),
            ],
          ),
        ),
      );
    }

    Widget _bottomButtons() {
      if (!_showBtn) {
        return Container();
      }
      setState(() => _btnBottom = 90);
      return Positioned(
        bottom: 0,
        child: Container(
          decoration: BoxDecoration(color: Colors.transparent),
          padding: EdgeInsets.only(bottom: 16, top: 16, left: 24, right: 24),
          width: _width,
          child: Button(
            isActive: _activeBtn,
            text: "Hoàn thành",
            onPressed: () {
              FocusScope.of(context).requestFocus(FocusNode());
              _con.addWater(_waterVal).then((result) {
                if (result.success) {
                  Navigator.of(context).pop();
                  Helper.successMessenge('Thêm nước uống thành công');
                } else
                  Helper.errorMessenge(result.message);
              });
            },
          ),
        ),
      );
    }

    Widget _bodyView() {
      return Container(
        width: config.App.appWidth(100),
        height: config.App.appHeight(100),
        child: Stack(
          alignment: Alignment.topLeft,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: config.Colors.backgroundColor(),
              ),
              child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    children: <Widget>[
                      buildImageView(),
                      buildFromView(),
                    ],
                  ),
                  padding: EdgeInsets.only(bottom: _btnBottom),
                ),
              ),
            ),
            _bottomButtons(),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyWater,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Thêm nước uống"),
      ),
      isSingleChildScrollView: false,
      main: _bodyView(),
    );
  }
}

class WaterPainter extends CustomPainter {
  final double value;
  WaterPainter(this.value);
  @override
  void paint(Canvas canvas, Size size) {
    final String path =
        'M114.952 228.211C114.952 220.558 112.217 213.739 107.926 209.362C112.216 204.984 115 198.165 115 190.512C115 182.454 111.812 175.32 107.127 170.989C111.844 166.115 114.759 158.575 114.759 150.12V99.9347L77.8255 37.0257H85.9406V0H28.8181V37.0257H37.4472L0 99.9347V150.122C0 158.577 3.16435 166.116 7.88081 170.99C3.19619 175.322 0.19312 182.455 0.19312 190.513C0.19312 198.167 2.9928 204.986 7.28348 209.363C2.9928 213.74 0.241397 220.559 0.241397 228.212C0.241397 236.27 3.18797 243.404 7.87259 247.736C3.15613 252.609 0 260.149 0 268.604V311.688C0 326.35 9.37644 338.615 20.5844 338.615H41.1688V338.173C44.5951 341.613 48.8765 344.001 53.5195 344.001H61.7532C66.3962 344.001 70.6777 341.613 74.1039 338.173V338.615H94.6883C105.897 338.615 114.758 326.35 114.758 311.688V268.604C114.758 260.149 111.851 252.61 107.135 247.736C111.82 243.403 114.952 236.269 114.952 228.211ZM36.5376 26.9274V10.0976H78.2215V26.9274H36.5376ZM47.1862 37.0257H68.086L103.317 96.9394H11.9547L47.1862 37.0257ZM96.7474 214.747C102.565 214.747 107.297 220.938 107.297 228.548C107.297 236.157 102.565 242.348 96.7474 242.348H86.4552V252.446H94.6889C101.641 252.446 107.04 259.508 107.04 268.602V311.686C107.04 320.781 101.642 328.516 94.6889 328.516H80.2154C81.4095 324.477 82.0808 321.035 82.0808 317.072H74.3618C74.3618 326.166 68.7059 333.902 61.7537 333.902H53.5195C46.5678 333.902 40.9115 326.167 40.9115 317.072H33.1925C33.1925 321.036 33.8637 324.477 35.0579 328.516H20.5844C13.6327 328.516 7.71902 320.781 7.71902 311.686V268.602C7.71902 259.508 13.6322 252.446 20.5844 252.446H61.7537V242.348H18.5264C12.7092 242.348 7.97685 236.157 7.97685 228.548C7.97685 220.938 12.7092 214.747 18.5264 214.747H61.7537V204.65H18.5264C12.7092 204.65 7.97685 198.459 7.97685 190.849C7.97685 183.24 12.7092 177.049 18.5264 177.049H61.7537V166.951H20.5844C13.6327 166.951 7.71902 159.217 7.71902 150.122V107.038H107.04V150.122C107.04 159.216 101.642 166.951 94.6889 166.951H86.4552V177.049H96.7474C102.565 177.049 107.297 183.24 107.297 190.849C107.297 198.459 102.565 204.65 96.7474 204.65H86.4552V214.747H96.7474Z';
    var p = parseSvgPathData(path);
    Paint bd = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill;
    canvas.drawPath(p, bd);
    var tf = 334.0;
    Paint bg = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill;

    if (value > 0) {
      /// fix đáy chai
      Rect rect1 = Rect.fromLTRB(15, tf - 5, 100, tf);
      canvas.drawRect(
        rect1,
        bg,
      );

      /// 98%
      var tMin = 334.0 - 5;
      var tMax = 90.0;
      var l = 7.0;
      var w = 108.0;
      var h = (tMin - tMax) * value;
      print("hhhhhhhh $value $h");
      Rect rect2 = Rect.fromLTRB(l, (tMin - h), w, tMin);
      canvas.drawRect(
        rect2,
        bg,
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
