import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/home_controller.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/homeSettingsModel.dart';
import 'package:hab_app/src/pages/classRoom/myClassHomeBox.dart';
import 'package:hab_app/src/pages/courses/coursesHomeBox.dart';
import 'package:hab_app/src/pages/home/CaloBox.dart';
import 'package:hab_app/src/pages/home/FollowBox.dart';
import 'package:hab_app/src/pages/home/NotificationButtonWidget.dart';
import 'package:hab_app/src/pages/work/homeBox.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class HomeWidget extends StatefulWidget {
  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends StateMVC<HomeWidget> with SingleTickerProviderStateMixin {
  HomeController _con;
  double _width = 0;
  List<HomeSettingsModel> homeSettings = settingRepo.setting.value.homeSettings.value;
  GlobalKey<AnimatedCircularChartState> chartKey = new GlobalKey<AnimatedCircularChartState>();

  _HomeWidgetState() : super(HomeController()) {
    _con = controller;
  }
  @override
  void initState() {
    super.initState();
    _con.pageState();
    settingRepo.setting.value.homeSettings.addListener(() {
      if (!mounted) return;
      print("settingRepo.setting.value.homeSettings");
      setState(() => body());
    });
    settingRepo.setting.value.totalNotifications.addListener(() {
      if (!mounted) return;
      print("totalNotiWg");
      setState(() => totalNotiWg());
    });

    _con.getCalo();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setWidth();
  }

  void setWidth() {
    var __width = config.App.appWidth(100);
    setState(() => _width = __width);
  }

  Widget totalNotiWg() {
    return NotificationButtonWidget(
      total: settingRepo.setting.value.totalNotifications.value,
    );
  }

  List<Widget> body() {
    if (homeSettings == null) return [];
    List<Widget> _body = [];
    for (var item in homeSettings) {
      if (item.id == 'calo' && item.active) _body.add(CaloBox(ctrl: _con));
      if (item.id == 'follow' && item.active) _body.add(FollowBox(ctrl: _con));
      if (item.id == 'work' && item.active) _body.add(WorkHomeBox());
      if (item.id == 'my_courses' && item.active) _body.add(MyClassHomeBox());
      if (item.id == 'courses' && item.active) _body.add(CoursesHomeBox());
    }
    return _body;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutManager(
      scaffoldKey: _con.scaffoldKey,
      onWillPop: Helper.of(context).onWillPop,
      appBar: AppBar(
        leading: IconButton(
          icon: SvgPicture.asset('assets/icon/settings.svg'),
          onPressed: () {
            Navigator.of(context).pushNamed('/Home/Seting');
          },
        ),
        title: Image.asset(
          'assets/icon/logo.png',
          width: 60,
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Colors.black87,
        ),
        elevation: 0.0,
        actions: [totalNotiWg()],
      ),
      body: Container(
        child: Column(children: body()),
      ),
    );
  }
}
