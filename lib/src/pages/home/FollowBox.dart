import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/home_controller.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/ProgressBar.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/import.dart';
import 'package:hab_app/src/pages/home/HomeBox.dart';

class FollowBox extends StatefulWidget {
  final HomeController ctrl;
  const FollowBox({Key key, this.ctrl}) : super(key: key);
  _FollowBoxState createState() => _FollowBoxState();
}

class _FollowBoxState extends State<FollowBox> {
  @override
  void initState() {
    super.initState();
  }

  Widget block(String title, String value, double percent, String color, {Function action, bool isEnd = false}) {
    if (widget.ctrl.loadingCalo) return Container();
    //if (percent <= 0) return blockNone(title, value, color, action: action, isEnd: isEnd);
    var width = config.App.appWidth(100) - 32;
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RowWidget(
                    Icon(Icons.circle, size: 8, color: config.Colors.customeColor(color)),
                    SecondText(
                      title,
                      style: TextStyle(fontWeight: FontWeight.w500),
                    )),
                Container(
                  child: AccentText(
                    value,
                    style: Theme.of(context).textTheme.subtitle2.copyWith(color: config.Colors.accentColor(), fontWeight: FontWeight.w400),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 16, right: 16, bottom: 5),
            child: ProgressBar((percent / 100), activeColor: config.Colors.customeColor(color)),
          ),
          !isEnd
              ? Container()
              : Container(
                  padding: EdgeInsets.only(bottom: 10),
                )
        ],
      ),
    );
  }

  Widget blockNone(String title, String value, String color, {Function action, bool isEnd = false}) {
    var width = config.App.appWidth(100) - 32;
    var widthContent = width * 0.8;
    var widthButton = width * 0.6;
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      child: Icon(Icons.circle, size: 8, color: config.Colors.customeColor(color)),
                    ),
                    SecondText(title, style: TextStyle(fontWeight: FontWeight.w500)),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          width: widthContent,
          padding: EdgeInsets.only(bottom: 15),
          child: SecondText(
            value,
            style: Theme.of(context).textTheme.subtitle2.copyWith(color: config.Colors.accentColor(), fontWeight: FontWeight.w400),
          ),
        ),
        Container(
          width: widthButton,
          child: Button(
              text: "Đặt mục tiêu",
              onPressed: () {
                if (action != null)
                  action();
                else
                  print("");
              }
              //style: Theme.of(context).textTheme.subtitle1.copyWith(color: config.Colors.accentColor(), fontWeight: FontWeight.w400),
              ),
          padding: EdgeInsets.only(bottom: 16),
        ),
        !isEnd
            ? Divider(
                height: 1,
                color: config.Colors.customeColor("#E0E9F8"),
              )
            : Container(
                padding: EdgeInsets.only(bottom: 16),
              )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    print("widget.ctrl.caloInfo ${widget.ctrl.caloInfo}");
    var _isEat = !(widget.ctrl.caloInfo == null || widget.ctrl.caloInfo.eatLimit == null);
    var _eat = block(
      "Ăn uống",
      _isEat ? "${Helper.formatNumber(widget.ctrl.caloInfo.eat)} calo/${Helper.formatNumber(widget.ctrl.caloInfo.eatLimit)} calo" : "Đặt mục tiêu và theo dõi lượng calo, các chất nạp vào cơ thể mỗi ngày giúp bạn tăng/giảm/giữ cân hiệu quả hơn.",
      _isEat ? (widget.ctrl.caloInfo.eat / widget.ctrl.caloInfo.eatLimit) * 100 : 0,
      "#41CD2A",
      action: () {
        print("Mục tiêu 1");
      },
    );
    var _isDrink = !(widget.ctrl.caloInfo == null || widget.ctrl.caloInfo.drinkLimit == null);
    var _drink = block(
      "Uống nước",
      _isEat ? "${Helper.formatNumber(widget.ctrl.caloInfo.drink)} ml/${Helper.formatNumber(widget.ctrl.caloInfo.drinkLimit)} ml" : "Đặt mục tiêu và theo dõi lượng calo, các chất nạp vào cơ thể mỗi ngày giúp bạn tăng/giảm/giữ cân hiệu quả hơn.",
      _isEat ? (widget.ctrl.caloInfo.drink / widget.ctrl.caloInfo.drinkLimit) * 100 : 0,
      "#26A6FE",
      action: () {
        print("Mục tiêu 2");
      },
    );
    var _isExercise = !(widget.ctrl.caloInfo == null || widget.ctrl.caloInfo.exerciseLimit == null);
    var _exercise = block(
      "Vận động",
      _isExercise ? "${Helper.formatNumber(widget.ctrl.caloInfo.exercise)} %/${Helper.formatNumber(widget.ctrl.caloInfo.exerciseLimit)} %" : "Đặt mục tiêu và theo dõi lượng calo, các chất nạp vào cơ thể mỗi ngày giúp bạn tăng/giảm/giữ cân hiệu quả hơn.",
      _isExercise ? (widget.ctrl.caloInfo.exercise / widget.ctrl.caloInfo.exerciseLimit) * 100 : 0,
      "#1A60FF",
      action: () {
        print("Mục tiêu 3");
      },
      isEnd: true,
    );
    return HomeBox([
      HomeTitleBox(
        'Theo dõi ăn uống vận động',
        labed: "Chỉ số cơ thể",
        action: () {
          Navigator.of(context).pushNamed('/MyHealth');
        },
      ),
      _eat,
      _drink,
      _exercise,
    ]);
  }
}
