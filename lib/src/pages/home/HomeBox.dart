import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';

class HomeBox extends StatelessWidget {
  final List<Widget> listWidget;
  final bool bottom;
  const HomeBox(this.listWidget, {Key key, this.bottom = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: config.App.appWidth(100),
      padding: EdgeInsets.only(bottom: bottom ? 16 : 0),
      //margin: EdgeInsets.only(bottom: bottom ? 0 : 16),
      decoration: BoxDecoration(
        color: config.Colors.inputBgColor(),
      ),
      child: Container(
        padding: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 0),
        width: config.App.appWidth(100),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        //padding: EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: listWidget,
        ),
      ),
    );
  }
}

class HomeTitleBox extends StatelessWidget {
  final String title;
  final Function action;
  final String labed;
  const HomeTitleBox(this.title, {Key key, this.action, this.labed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (action == null) {
      return Container(
        padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
        child: TitleText(
          title,
          style: Theme.of(context).textTheme.headline6.copyWith(color: config.Colors.accentColor(), fontSize: 16),
        ),
      );
    }

    return Container(
      padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TitleText(
            title,
            style: Theme.of(context).textTheme.headline6.copyWith(color: config.Colors.accentColor(), fontSize: 16),
          ),
          PrimaryText(
            labed != null ? labed : "Xem tất cả",
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
          ).onTap(action),
        ],
      ),
    );
  }
}
