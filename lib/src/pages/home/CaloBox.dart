import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:hab_app/src/controllers/home_controller.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';

class CaloBox extends StatefulWidget {
  final HomeController ctrl;
  const CaloBox({Key key, this.ctrl}) : super(key: key);

  _CaloBoxState createState() => _CaloBoxState();
}

class _CaloBoxState extends State<CaloBox> {
  GlobalKey<AnimatedCircularChartState> chartKey = new GlobalKey<AnimatedCircularChartState>();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _width = config.App.appWidth(100);
    if (widget.ctrl.caloInfo == null) return Container();
    Widget caloInfoWidget() {
      var _boxWidth = _width - 48 - 30;
      var _titleWidget = TitleText(
        "THÔNG TIN CALO",
        style: Theme.of(context).textTheme.headline6.copyWith(color: config.Colors.accentColor()),
      );
      var _inputCalo = widget.ctrl.caloInfo.caloInput;
      var _maxCalo = widget.ctrl.caloInfo.caloLimit;
      var _exCalo = widget.ctrl.caloInfo.caloExchange;
      var _exChangeCalo = _maxCalo - _inputCalo;

      var _chartWidget = new AnimatedCircularChart(
        key: chartKey,
        size: new Size(_boxWidth * 0.65, _boxWidth * 0.65),
        initialChartData: <CircularStackEntry>[
          new CircularStackEntry(
            <CircularSegmentEntry>[
              new CircularSegmentEntry(
                (_inputCalo),
                config.Colors.primaryColor(),
                rankKey: 'Calo',
              ),
              new CircularSegmentEntry(
                _exChangeCalo == 0 ? 1 : _exChangeCalo,
                config.Colors.inputBgColor(),
                rankKey: 'input',
              ),
            ],
            rankKey: 'points',
          ),
        ],
        chartType: CircularChartType.Radial,
        duration: Duration(milliseconds: 600),
        edgeStyle: SegmentEdgeStyle.round,
        percentageValues: false,
        holeRadius: _boxWidth * 0.2,
      );
      print("_exChangeCalo $_exChangeCalo");
      if (_exChangeCalo < 0)
        _chartWidget = new AnimatedCircularChart(
          key: chartKey,
          size: new Size(_boxWidth * 0.65, _boxWidth * 0.65),
          initialChartData: <CircularStackEntry>[
            new CircularStackEntry(
              <CircularSegmentEntry>[
                new CircularSegmentEntry(
                  0 - _exChangeCalo,
                  config.Colors.errorColor(),
                  rankKey: 'Calo',
                ),
                new CircularSegmentEntry(
                  (_inputCalo),
                  config.Colors.primaryColor(),
                  rankKey: 'input',
                ),
              ],
              rankKey: 'points',
            ),
          ],
          chartType: CircularChartType.Radial,
          duration: Duration(milliseconds: 600),
          edgeStyle: SegmentEdgeStyle.round,
          percentageValues: false,
          holeRadius: _boxWidth * 0.2,
        );

      var _chartContentWidget = new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Center(
            child: TitleText(
              Helper.formatNumber(_inputCalo),
              style: Theme.of(context).textTheme.headline1.copyWith(
                    color: config.Colors.primaryColor(),
                    fontWeight: FontWeight.w800,
                  ),
            ),
          ),
          Center(
            child: TitleText(
              _exChangeCalo >= 0 ? "Còn thiếu" : "Thừa",
              style: Theme.of(context).textTheme.headline6.copyWith(
                    fontSize: 14,
                    color: config.Colors.accentColor(),
                  ),
            ),
          ),
          Center(
            child: TitleText(
              Helper.formatNumber(_exChangeCalo >= 0 ? _exChangeCalo : 0 - _exChangeCalo),
              style: Theme.of(context).textTheme.headline3.copyWith(
                    color: _exChangeCalo >= 0 ? config.Colors.accentColor() : config.Colors.errorColor(),
                    fontWeight: FontWeight.w800,
                  ),
            ),
          ),
        ],
      );
      var _caloBlockWidgetContent = (String title, String value, Color cl) {
        return Container(
          width: 80,
          alignment: Alignment.center,
          child: Column(
            children: [
              Text(
                title,
                style: Theme.of(context).textTheme.bodyText1.copyWith(
                      fontSize: 12,
                      color: config.Colors.accentColor(),
                      fontWeight: FontWeight.w400,
                    ),
                textAlign: TextAlign.center,
              ),
              Text(value,
                  style: Theme.of(context).textTheme.headline3.copyWith(
                        color: cl,
                        fontWeight: FontWeight.w700,
                      )),
            ],
          ),
        );
      };
      var _caloBlockWidget = Container(
        padding: EdgeInsets.only(left: 15, right: 15),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _caloBlockWidgetContent("Lượng calo mục tiêu", Helper.formatNumber(_maxCalo), config.Colors.primaryColor()),
            _caloBlockWidgetContent("Lượng calo nạp vào", Helper.formatNumber(_inputCalo), config.Colors.customeColor("#41CD2A")),
            _caloBlockWidgetContent("Lượng calo tiêu hao", Helper.formatNumber(_exCalo), config.Colors.customeColor("#FF5722")),
          ],
        ),
      );
      return Container(
        width: config.App.appWidth(100),
        height: config.App.appWidth(100),
        decoration: BoxDecoration(
          color: config.Colors.inputBgColor(),
        ),
        padding: EdgeInsets.all(24),
        child: Container(
          decoration: BoxDecoration(
            color: config.Colors.backgroundColor(),
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          padding: EdgeInsets.all(15),
          child: Stack(
            children: <Widget>[
              Positioned(
                top: 0,
                width: _boxWidth,
                child: _titleWidget,
              ),
              Positioned(
                top: 0,
                width: _boxWidth,
                height: _boxWidth * 0.75,
                child: Container(
                  alignment: Alignment.center,
                  child: _chartWidget,
                ),
              ),
              Positioned(
                top: 0,
                width: _boxWidth,
                height: _boxWidth * 0.75,
                child: _chartContentWidget,
              ),
              Positioned(
                top: _boxWidth * 0.75,
                width: _boxWidth,
                child: _caloBlockWidget,
              ),
            ],
          ),
        ),
      );
    }

    return caloInfoWidget();
  }
}
