import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/home_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/CheckBoxItem.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/models/homeSettingsModel.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:mvc_pattern/mvc_pattern.dart';

class HomeSetingWidget extends StatefulWidget {
  @override
  _HomeSetingWidgetState createState() => _HomeSetingWidgetState();
}

class _HomeSetingWidgetState extends StateMVC<HomeSetingWidget> with SingleTickerProviderStateMixin {
  HomeController _con;
  List<HomeSettingsModel> homeSettings = settingRepo.setting.value.homeSettings.value;
  _HomeSetingWidgetState() : super(HomeController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.pageSettingState();
  }

  void _updateItems(int oldIndex, int newIndex) {
    if (newIndex > oldIndex) newIndex -= 1;
    final item = homeSettings.removeAt(oldIndex);
    homeSettings.insert(newIndex, item);
    _con.saveSetting(homeSettings);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> listSetting() {
      if (homeSettings == null) return [];
      return [
        for (var item in homeSettings)
          Container(
            key: ValueKey(item),
            height: 60,
            //margin: EdgeInsets.only(top: 15, bottom: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  width: config.App.appWidth(100),
                  height: 59,
                  padding: EdgeInsets.only(left: 15, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        child: CheckBoxItem(
                          item.active,
                          item.name,
                          () => setState(() {
                            item.active = !item.active;
                            _con.saveSetting(homeSettings);
                          }),
                        ),
                      ),
                      IconButton(
                        icon: SvgPicture.asset('assets/icon/move.svg', width: 20, height: 20),
                        padding: EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 0),
                        onPressed: () {},
                      ),
                    ],
                  ),
                ),
                Divider(
                  height: 1,
                  color: config.Colors.customeColor("#E0E9F8"),
                ),
              ],
            ),
          ),
      ];
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeySettings,
      // title: "Thiết lập trang chủ",
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Thiết lập trang chủ"),
      ),
      body: Container(
        child: ColumnStart(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: config.Colors.inputBgColor(),
              ),
              height: 8,
            ),
            Container(
              height: config.App.appHeight(100) - config.App.appBar() - 16 - 40,
              child: ReorderableListView(
                header: Container(
                  padding: EdgeInsets.only(left: 16, top: 16, right: 16, bottom: 40),
                  child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(text: 'Tích  ', style: Theme.of(context).textTheme.bodyText1),
                        WidgetSpan(child: SvgPicture.asset('assets/icon/check.svg')),
                        TextSpan(text: '  để ẩn mục.', style: Theme.of(context).textTheme.bodyText1),
                        TextSpan(text: ' Giữ  ', style: Theme.of(context).textTheme.bodyText1),
                        WidgetSpan(child: SvgPicture.asset('assets/icon/move.svg')),
                        TextSpan(text: '  và di chuyển để sắp xếp thứ tự các mục hiển thị ở trang chủ', style: Theme.of(context).textTheme.bodyText1),
                      ],
                    ),
                  ),
                ),
                onReorder: (oldIndex, newIndex) {
                  setState(() {
                    _updateItems(oldIndex, newIndex);
                  });
                },
                children: listSetting(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
