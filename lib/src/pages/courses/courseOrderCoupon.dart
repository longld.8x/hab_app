import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/courses_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/course.dart';
import 'package:hab_app/src/pages/courses/courseOrderPickTime.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class CourseOrderCouponWidget extends StatefulWidget {
  final CoursePreview course;
  CourseOrderCouponWidget({Key key, this.course}) : super(key: key);
  @override
  _CourseOrderCouponWidgetState createState() => _CourseOrderCouponWidgetState();
}

class _CourseOrderCouponWidgetState extends StateMVC<CourseOrderCouponWidget> {
  CoursesController _con;
  double _couponAmount = 0;
  String _queryCoupon = 'null'; // null = ko truy van; done = truy van ok; error = truy van loi; doing = cho truy van
  String _queryMsg = '';

  TextEditingController _ctrlCoupon = TextEditingController();
  _CourseOrderCouponWidgetState() : super(CoursesController()) {
    _con = controller;
  }
  @override
  void initState() {
    super.initState();
    _con.pageCouponState();
  }

  _nextPage() {
    Navigator.of(context).push(new MaterialPageRoute(
      builder: (BuildContext context) => CoursesPickTimeWidget(course: widget.course, couponAmount: _couponAmount, coupon: _ctrlCoupon.text),
    ));
  }

  _onNext() {
    if (_queryCoupon != 'doing') {
      _nextPage();
      return;
    }
    if (_queryMsg.isNotEmpty) {
      return;
    }
    _con.getCoupon(_ctrlCoupon.text).then((result) {
      double _am = 0;
      if (result.success) {
        try {
          _am = double.parse(result.results['discountAmount'].toString());
        } catch (e) {
          _am = 0;
          log("double.parse discountAmount $e ");
        }
        setState(() {
          _queryMsg = '';
          _couponAmount = _am;
          _queryCoupon = 'done';
        });
      } else {
        setState(() {
          _queryMsg = result.message;
          _couponAmount = _am;
          _queryCoupon = 'error';
        });
      }
      print("resultresultresultresultresultresultresult ${result.success}");
    });
    // Navigator.of(context).push(new MaterialPageRoute(
    //   builder: (BuildContext context) => CoursesPickTimeWidget(course: widget.course, coupon: _ctrlCoupon.text),
    // ));
  }

  @override
  Widget build(BuildContext context) {
    Widget _rowDivider() {
      return Container(
        padding: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
        child: Divider(
          height: 1,
          color: config.Colors.customeColor("#E0E9F8"),
        ),
      );
    }

    Widget _rowText(String label, String content) {
      return Container(
        padding: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
        child: RowWidget(
          AccentText(label, style: TextStyle(color: config.Colors.hintColor())),
          Container(
            alignment: Alignment.centerRight,
            child: AccentText(content, style: TextStyle(fontWeight: FontWeight.w500)),
          ),
          flex: 40,
        ),
      );
    }

    Widget _bodyView() {
      return Container(
        width: config.App.appWidth(100),
        padding: EdgeInsets.only(top: 0, left: 0, right: 0),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
        ),
        child: ColumnStart(
          children: [
            Container(
              padding: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
              alignment: Alignment.center,
              child: InputField(
                controller: _ctrlCoupon,
                labelText: "Mã giảm giá",
                hintText: 'Mã giảm giá',
                toUpperCase: true,
                suffixIcon: IconButton(
                  onPressed: () {
                    if (_queryCoupon != 'null') {
                      setState(() {
                        _queryCoupon = 'null';
                        _queryMsg = '';
                        _ctrlCoupon.text = '';
                        _couponAmount = 0;
                      });
                    }
                  },
                  icon: Icon(_queryCoupon == 'null' ? Icons.card_giftcard : Icons.close, size: 16, color: config.Colors.accentColor()),
                ),
                onChanged: (String v) {
                  setState(() {
                    _queryCoupon = (v != null && v.isNotEmpty) ? 'doing' : 'null';
                    _queryMsg = '';
                  });
                },
              ),
            ),
            (_queryMsg.isNotEmpty)
                ? Container(
                    padding: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                    child: ErrorText(_queryMsg),
                  )
                : Container(),
            (_couponAmount > 0)
                ? Container(
                    padding: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                    child: AccentText(
                      'Đã áp dụng Mã giảm giá: ${_ctrlCoupon.text}',
                      style: TextStyle(color: config.Colors.primaryColor(), fontWeight: FontWeight.w500),
                    ),
                  )
                : Container(),
            _rowDivider(),
            _rowText("Học phí", widget.course.paymentAmount > 0 ? Helper.formatNumber(widget.course.paymentAmount, prefix: "đ") : "Liên hệ"),
            _rowDivider(),
            _rowText("Giảm giá", Helper.formatNumber(_couponAmount, prefix: "đ")),
            _rowDivider(),
            _rowText("Thanh toán", widget.course.paymentAmount > 0 ? Helper.formatNumber(widget.course.paymentAmount - _couponAmount, prefix: "đ") : "Liên hệ"),
            Container(
              width: config.App.appWidth(100),
              padding: EdgeInsets.only(left: 24, right: 24, bottom: 0, top: 60),
              child: Button(
                text: (_queryCoupon == 'doing' || _queryCoupon == 'error') ? "Truy vấn" : "Tiếp theo",
                onPressed: _onNext,
                isActive: (_queryMsg.isEmpty),
              ),
            ),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyCoupon,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Mã giảm giá"),
      ),
      body: _bodyView(),
    );
  }
}
