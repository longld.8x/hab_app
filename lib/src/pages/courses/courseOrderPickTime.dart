import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/courses_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/models/course.dart';
import 'package:hab_app/src/pages/courses/courseOrderRespond.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class CoursesPickTimeWidget extends StatefulWidget {
  final CoursePreview course;
  final String coupon;
  final double couponAmount;
  CoursesPickTimeWidget({Key key, this.course, this.coupon, this.couponAmount}) : super(key: key);
  @override
  _CoursesPickTimeWidgetState createState() => _CoursesPickTimeWidgetState();
}

class _CoursesPickTimeWidgetState extends StateMVC<CoursesPickTimeWidget> {
  CoursesController _con;

  _CoursesPickTimeWidgetState() : super(CoursesController()) {
    _con = controller;
  }
  @override
  void initState() {
    super.initState();
    _con.pagePickTimeState();
  }

  TextStyle _styleTitle = TextStyle(color: config.Colors.customeColor("#ffffff"), fontSize: 16);
  DateTime _time;
  DateTime _date;
  DateTime _now = DateTime.now();

  ///
  Future<void> _selectTime(context) async {
    var __timeTmp = _time == null ? _now : _time;
    await showModalBottomSheet(
        isDismissible: false,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(28.0)),
          side: BorderSide.none,
        ),
        builder: (BuildContext e) {
          return Container(
            height: 260,
            child: Column(
              children: [
                Container(
                  color: config.Colors.backgroundColor(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 2,
                        child: AccentText('Hủy', style: TextStyle(color: config.Colors.primaryColor())).onTap(() {
                          Navigator.pop(context);
                        }),
                      ),
                      Expanded(
                        flex: 4,
                        child: Container(
                          alignment: Alignment.center,
                          child: TitleText('Chọn thời gian'),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: AccentText('Xác nhận', style: TextStyle(color: config.Colors.primaryColor())).onTap(() {
                          setState(() => _time = __timeTmp);
                          Navigator.pop(context);
                        }),
                      ),
                    ],
                  ).paddingAll(10.0),
                ),
                Expanded(
                  child: CupertinoDatePicker(
                    initialDateTime: _time,
                    onDateTimeChanged: (DateTime newdate) {
                      setState(() => __timeTmp = newdate);
                    },
                    minimumDate: _now,
                    minimumYear: _now.year,
                    maximumYear: _now.year + 10,
                    mode: CupertinoDatePickerMode.time,
                    use24hFormat: true,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future<void> _selectDate(context) async {
    var __dateTmp = _date == null ? _now : _date;
    await showModalBottomSheet(
        isDismissible: false,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(28.0)),
          side: BorderSide.none,
        ),
        builder: (BuildContext e) {
          return Container(
            height: 260,
            child: Column(
              children: [
                Container(
                  color: config.Colors.backgroundColor(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 4,
                        child: AccentText('Hủy', style: TextStyle(color: config.Colors.primaryColor())).onTap(() {
                          Navigator.pop(context);
                        }),
                      ),
                      Expanded(
                        flex: 4,
                        child: Container(
                          alignment: Alignment.center,
                          child: TitleText('Chọn ngày'),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: AccentText('Xác nhận', style: TextStyle(color: config.Colors.primaryColor())).onTap(() {
                          Navigator.pop(context);
                          setState(() => _date = __dateTmp);
                        }),
                      ),
                    ],
                  ).paddingAll(10.0),
                ),
                Expanded(
                  child: CupertinoDatePicker(
                    initialDateTime: _date,
                    onDateTimeChanged: (DateTime newdate) {
                      setState(() => __dateTmp = newdate);
                    },
                    minimumDate: _now,
                    minimumYear: _now.year,
                    maximumYear: _now.year + 10,
                    mode: CupertinoDatePickerMode.date,
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    Widget _bodyView() {
      return Container(
        padding: EdgeInsets.only(top: 0, left: 0, right: 0),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
        ),
        child: ColumnStart(
          children: [
            Container(
              padding: EdgeInsets.only(left: 16, right: 16),
              child: AccentText(
                "Lưu ý: Trường hợp không chọn thời gian học, HAB sẽ sắp xếp lịch học và thông báo đến bạn.",
                style: Theme.of(context).textTheme.caption.copyWith(fontStyle: FontStyle.italic, color: config.Colors.accentColor()),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
              child: InputField(
                labelText: "Chọn thời gian để học",
                readOnly: true,
                hintText: '01/01/1989',
                suffixIcon: IconButton(
                  onPressed: () {},
                  icon: SvgPicture.asset(
                    'assets/icon/clock2.svg',
                    width: 16,
                  ),
                ),
                controller: new TextEditingController(text: (_time == null ? "" : "${_time.hour}:${_time.minute}")),
                onTap: () => _selectTime(context),
              ),
              alignment: Alignment.center,
            ),
            Container(
              padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
              child: InputField(
                labelText: "Chọn thời gian để học",
                readOnly: true,
                hintText: '01/01/1989',
                suffixIcon: IconButton(
                  onPressed: () {},
                  icon: SvgPicture.asset(
                    'assets/icon/calendar2.svg',
                    width: 16,
                  ),
                ),
                controller: new TextEditingController(text: (_date == null ? "" : "${_date.day}/${_date.month}/${_date.year}")),
                onTap: () => _selectDate(context),
              ),
              alignment: Alignment.center,
            ),
            Container(
              padding: EdgeInsets.only(left: 24, right: 24, bottom: 0, top: 60),
              child: Button(
                text: "Xác nhận",
                onPressed: () {
                  DateTime _dateJoin;
                  if (_date != null && _time != null)
                    _dateJoin = new DateTime(_date.year, _date.month, _date.day, _time.hour, _time.minute);
                  else if (_date != null)
                    _dateJoin = new DateTime(_date.year, _date.month, _date.day, 0, 0);
                  else if (_time != null) {
                    var _now = DateTime.now();
                    _dateJoin = new DateTime(_now.year, _now.month, _now.day, _time.hour, _time.minute);
                  }
                  _con.buyCourse(widget.course.productId, widget.coupon, widget.couponAmount, _dateJoin).then((result) {
                    Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => CourseOrderRespondWidget(result: result),
                    ));
                  });
                },
              ),
            ),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyPickTime,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Thời gian học"),
      ),
      main: _bodyView(),
    );
  }
}
