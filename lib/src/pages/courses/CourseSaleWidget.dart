import 'package:flutter/material.dart';
import 'package:hab_app/src/elements/CourseCommon.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/course.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;

class CourseSaleWidget extends StatefulWidget {
  final Course model;
  final Function action;
  final bool horizontal;
  CourseSaleWidget({Key key, this.model, this.action, this.horizontal = false}) : super(key: key);
  _CourseSaleWidgetState createState() => _CourseSaleWidgetState();
}

class _CourseSaleWidgetState extends State<CourseSaleWidget> {
  @override
  Widget build(BuildContext context) {
    Function _onTap() {
      Navigator.of(settingRepo.navigatorKey.currentContext).pushNamed('/Course/Detail', arguments: RouteArgument(id: widget.model.id));
    }

    Widget saleTag() {
      if (widget.model.tag != null && widget.model.tag.isNotEmpty) {
        return new Positioned(
          right: 0.0,
          top: 12.0,
          child: new Container(
            width: 56.0,
            height: 24.0,
            decoration: new BoxDecoration(
              color: config.Colors.customeColor("#FF0B27").withOpacity(0.85),
              borderRadius: BorderRadius.only(topLeft: Radius.circular(40), bottomLeft: Radius.circular(40)),
            ),
            child: SubText(widget.model.tag, style: TextStyle(fontSize: 14, color: Colors.white)),
            alignment: Alignment.center,
          ),
        );
      } else {
        return Container();
      }
    }

    Widget info() {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 6),
            child: CourseNameRankBox(
              widget.model.productName,
              widget.model.rank,
              widget.horizontal ? "horizontal" : "vertical",
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 4),
            child: PrimaryText(
              (widget.model.paymentAmount > 0) ? Helper.formatNumber(widget.model.paymentAmount, prefix: "đ") : "Liên hệ",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
          ),
          ((widget.model.paymentAmount > 0) && widget.model.productValue > 0 && widget.model.paymentAmount != widget.model.productValue)
              ? Container(
                  padding: EdgeInsets.only(bottom: 4),
                  child: Text(
                    Helper.formatNumber(widget.model.productValue, prefix: "đ"),
                    style: Theme.of(context).textTheme.bodyText1.copyWith(color: config.Colors.accentColor(), fontWeight: FontWeight.w400, decoration: TextDecoration.lineThrough, height: 1),
                  ),
                )
              : Container(),
          Container(
            child: Text(
              (widget.model.totalBuy != null && widget.model.totalBuy > 0) ? Helper.formatNumber(widget.model.totalBuy.toDouble(), prefix: " người đã mua") : "",
              style: Theme.of(context).textTheme.subtitle1.copyWith(color: config.Colors.customeColor("#999999"), height: 1.35),
            ),
          ),
        ],
      );
    }

    Widget horizontal() {
      var _boxWidth = config.App.appWidth(60);
      return Container(
        width: _boxWidth,
        height: _boxWidth * (242 / 217),
        margin: EdgeInsets.only(left: 16, right: 8, bottom: 0),
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 0),
              child: CourseHorizontalBox(
                image: widget.model.thumbnail,
                info: Container(
                  padding: EdgeInsets.only(top: 0, left: 10, right: 10, bottom: 0),
                  child: info(),
                ),
                width: _boxWidth,
                height: _boxWidth * (242 / 217),
              ),
            ).onTap(_onTap),
            saleTag()
          ],
        ),
      );
    }

    Widget vertical() {
      return Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 1),
            child: CourseVerticalBox(
              image: widget.model.thumbnail,
              info: Container(
                padding: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 0),
                child: info(),
              ),
            ),
          ).onTap(_onTap),
          //saleTag()
        ],
      );
    }

    return widget.horizontal ? horizontal() : vertical();
  }
}
