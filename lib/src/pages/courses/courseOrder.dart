import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/courses_controller.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/RowWidget.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/course.dart';
import 'package:hab_app/src/pages/courses/courseOrderCoupon.dart';
import 'package:hab_app/src/pages/courses/courseOrderPickTime.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class CourseOrderWidget extends StatefulWidget {
  final CoursePreview course;
  CourseOrderWidget({Key key, this.course}) : super(key: key);
  @override
  _CourseOrderWidgetState createState() => _CourseOrderWidgetState();
}

class _CourseOrderWidgetState extends StateMVC<CourseOrderWidget> {
  CoursesController _con;
  _CourseOrderWidgetState() : super(CoursesController()) {
    _con = controller;
  }
  @override
  void initState() {
    super.initState();
    _con.pageOrderState();
  }

  @override
  Widget build(BuildContext context) {
    Widget _bodyView() {
      return Container(
        padding: EdgeInsets.only(top: 24, left: 0, right: 0),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(28), topRight: Radius.circular(28)),
        ),
        child: ColumnStart(
          children: [
            Container(
              padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
              child: RowWidget(
                AccentText(
                  "Tên khóa học",
                  style: TextStyle(color: config.Colors.hintColor()),
                ),
                Container(
                  child: AccentText(
                    widget.course.productName,
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  alignment: Alignment.center,
                ),
                flex: 40,
              ),
            ),
            Divider(
              height: 1,
              color: config.Colors.customeColor("#E0E9F8"),
            ),
            Container(
              padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
              child: RowWidget(
                AccentText(
                  "Học phí",
                  style: TextStyle(color: config.Colors.hintColor()),
                ),
                Container(
                  child: AccentText(
                    widget.course.paymentAmount > 0 ? Helper.formatNumber(widget.course.paymentAmount, prefix: "đ") : "Liên hệ",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  alignment: Alignment.centerRight,
                ),
                flex: 40,
              ),
            ),
            Divider(
              height: 1,
              color: config.Colors.customeColor("#E0E9F8"),
            ),
            Container(
              padding: EdgeInsets.only(left: 24, right: 24, bottom: 0, top: 60),
              child: Button(
                text: "Tiếp theo",
                onPressed: () {
                  if (widget.course.paymentAmount > 0)
                    Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => CourseOrderCouponWidget(course: widget.course),
                    ));
                  else
                    Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => CoursesPickTimeWidget(course: widget.course, coupon: ''),
                    ));
                },
              ),
            ),
          ],
        ),
      );
    }

    return LayoutAppbarBgManager(
      scaffoldKey: _con.scaffoldKeyOrder,
      bottom: 0,
      title: Center(
        child: TitleText(
          "Đăng ký học",
          style: TextStyle(color: config.Colors.customeColor("#ffffff")),
        ),
      ),
      main: _bodyView(),
    );
  }
}
