import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/models/response.dart';

class CourseOrderRespondWidget extends StatefulWidget {
  final ResponseMessage result;
  CourseOrderRespondWidget({Key key, this.result}) : super(key: key);
  @override
  _CourseOrderRespondWidgetState createState() => _CourseOrderRespondWidgetState();
}

class _CourseOrderRespondWidgetState extends State<CourseOrderRespondWidget> {
  GlobalKey<ScaffoldState> _scaffoldKey;
  ResponseMessage result;
  @override
  void initState() {
    super.initState();
    _scaffoldKey = new GlobalKey<ScaffoldState>();
    result = widget.result;
  }

  @override
  Widget build(BuildContext context) {
    Widget _bodyView() {
      return Container(
        padding: EdgeInsets.only(top: 24, left: 0, right: 0),
        child: ColumnStart(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              child: SvgPicture.asset(result.success ? 'assets/icon/respond_success.svg' : 'assets/icon/respond_success.svg'),
            ),
            Container(
              padding: EdgeInsets.only(left: 30, right: 30, bottom: 24, top: 60),
              alignment: Alignment.center,
              child: TitleText(
                result.success ? "Đăng ký khoá học thành công!" : "Đăng ký khoá học thất bại!",
                style: TextStyle(color: config.Colors.accentColor()),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 30, right: 30, bottom: 200, top: 0),
              alignment: Alignment.center,
              child: AccentText(
                result.success ? "Chúc bạn có những giờ phút học tập hiệu quả cùng với các huấn luyện viên của HAB." : result.message,
                style: TextStyle(),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 30, right: 30),
              width: config.App.appWidth(100),
              child: Button(
                text: "Về trang chủ",
                onPressed: () {
                  Navigator.of(context).pushNamedAndRemoveUntil('/Page', (Route<dynamic> route) => false, arguments: 0);
                },
              ),
            ),
          ],
        ),
      );
    }

    return LayoutManager(
      onWillPop: () {
        Navigator.of(context).pushNamedAndRemoveUntil('/Page', (Route<dynamic> route) => false, arguments: 0);
      },
      scaffoldKey: _scaffoldKey,
      appBar: null,
      main: _bodyView(),
    );
  }
}
