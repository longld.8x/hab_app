import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/courses_controller.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/pages/courses/CourseSaleWidget.dart';
import 'package:hab_app/src/pages/home/HomeBox.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class CoursesHomeBox extends StatefulWidget {
  const CoursesHomeBox({Key key}) : super(key: key);
  _CoursesHomeBoxState createState() => _CoursesHomeBoxState();
}

class _CoursesHomeBoxState extends StateMVC<CoursesHomeBox> {
  CoursesController _con;
  _CoursesHomeBoxState() : super(CoursesController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.homeWidgetState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_con.isLoading || _con.dataCourses == null || _con.dataCourses.length == 0) return Container();
    return HomeBox([
      HomeTitleBox(
        "Danh sách khóa học",
        action: () {
          Navigator.of(context).pushNamed('/Courses');
        },
      ),
      Container(
        height: (config.App.appWidth(100)) * 0.6 * (262 / 217) + 24,
        padding: EdgeInsets.fromLTRB(0, 5, 0, 24),
        child: new ListView.builder(
          shrinkWrap: false,
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) => CourseSaleWidget(model: _con.dataCourses[index], horizontal: true),
          itemCount: _con.dataCourses.length,
        ),
      ),
    ], bottom: false);
  }
}
