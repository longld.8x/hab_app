import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/courses_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/CourseCommon.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/ReadMore.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/UrlImage.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/elements/tab.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/pages/courses/courseOrder.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class CourseDetailWidget extends StatefulWidget {
  final RouteArgument routeArgument;
  CourseDetailWidget({Key key, this.routeArgument}) : super(key: key);
  @override
  _CourseDetailWidgetState createState() => _CourseDetailWidgetState();
}

class _CourseDetailWidgetState extends StateMVC<CourseDetailWidget> with TickerProviderStateMixin {
  CoursesController _con;
  _CourseDetailWidgetState() : super(CoursesController()) {
    _con = controller;
  }
  TabController _tabController;

  List<Tab> _listTab;
  int _selectedIndex = 0;
  int _productId = 0;

  @override
  void initState() {
    super.initState();
    _productId = widget.routeArgument.id;
    _con.pagePreviewState(_productId);
    initTab();
  }

  initTab() {
    _listTab = [
      TabTitle("THÔNG TIN"),
      TabTitle("BÀI HỌC"),
    ];
    _tabController = TabController(length: _listTab.length, vsync: this);
    _tabController.index = 0;
    _selectedIndex = _tabController.index;
    _tabController.addListener(() {
      setState(() => _selectedIndex = _tabController.index);
    });
  }

  @override
  Widget build(BuildContext context) {
    Map _titleInfo() {
      var _width = config.App.appWidth(100);
      var _height = 0.0;
      var _heightImg = _width * (281 / 375);
      var _sliderImages = Container(
        height: _heightImg,
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: Image.asset(
          'assets/icon/course_detail.png',
          fit: BoxFit.fill,
        ),
      );
      if (_con.coursePreview.images.length > 0)
        _sliderImages = Container(
          height: _heightImg,
          child: PageView.builder(
            itemCount: _con.coursePreview.images.length,
            itemBuilder: (context, index) {
              return UrlImage(
                _con.coursePreview.images[index],
                'assets/icon/default-course.png',
                width: config.App.appWidth(100),
                height: _heightImg,
                fit: BoxFit.cover,
                radius: 0,
              );
            },
          ),
        );
      List<Widget> _infoWidget = [];
      _infoWidget.add(_sliderImages);
      _height += _heightImg;
      _infoWidget.add(Container(padding: EdgeInsets.fromLTRB(20, 19, 20, 19), child: CourseNameRankBox(_con.coursePreview.productName, 5.0, "detail")));
      var _heightName = Helper.getTextLineHeight(
        _con.coursePreview.productName * 1, // + "xxxx",
        Theme.of(context).textTheme.headline1.copyWith(color: config.Colors.accentColor(), fontWeight: FontWeight.w500),
        (_width - 40),
      );
      _height += (_heightName + 19 + 19);

      _infoWidget.add(Container(height: 16, decoration: BoxDecoration(color: config.Colors.inputBgColor())));
      _height += (16);
      return {
        "widget": new Container(
          decoration: BoxDecoration(
            color: config.Colors.backgroundColor(),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: _infoWidget,
          ),
        ),
        "height": _height
      };
    }

    Widget _bottomButtons() {
      var _productValue = Container();
      if (_con.coursePreview.paymentAmount > 0 && _con.coursePreview.productValue > 0 && _con.coursePreview.paymentAmount != _con.coursePreview.productValue) {
        _productValue = Container(
          child: AccentText(
            Helper.formatNumber(_con.coursePreview.productValue, prefix: "đ"),
            style: TextStyle(
              color: config.Colors.hintColor(),
              fontWeight: FontWeight.w400,
              decoration: TextDecoration.lineThrough,
            ),
            maxLines: 1,
            overflow: TextOverflow.visible,
          ),
        );
      }
      var _salePrice = Container(
        child: TitleText(
          _con.coursePreview.paymentAmount > 0
              ? Helper.formatNumber(_con.coursePreview.paymentAmount, prefix: "đ")
              : _con.coursePreview.productValue > 0
                  ? Helper.formatNumber(_con.coursePreview.productValue, prefix: "đ")
                  : "Liên hệ",
          style: TextStyle(
            color: config.Colors.primaryColor(),
            fontWeight: FontWeight.w600,
            fontSize: Theme.of(context).textTheme.headline2.fontSize,
          ),
          maxLines: 1,
        ),
      );
      return Container(
        height: 90,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(color: Colors.grey.withOpacity(0.7), blurRadius: 16, spreadRadius: 2, offset: Offset(3, 1)),
          ],
          color: config.Colors.backgroundColor(),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 50,
              child: Container(
                  color: config.Colors.backgroundColor(),
                  padding: EdgeInsets.fromLTRB(12, 18, 24, 18),
                  height: double.infinity,
                  child: ColumnStart(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _salePrice,
                      _productValue,
                    ],
                  )),
            ),
            Expanded(
              flex: 50,
              child: Container(
                color: config.Colors.backgroundColor(),
                height: double.infinity,
                padding: EdgeInsets.fromLTRB(12, 18, 24, 18),
                child: Button(
                  text: "Đăng ký học",
                  onPressed: () {
                    Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => CourseOrderWidget(course: _con.coursePreview),
                    ));
                    // Navigator.of(context).pushNamed('/Course/Order', arguments: RouteArgument(param: _con.coursePreview));
                  },
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget _courseDescription() {
      return Container(
        padding: EdgeInsets.fromLTRB(16, 16, 16, 20),
        //margin: EdgeInsets.only(bottom: 100),
        width: config.App.appWidth(100),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        child: ReadMoreText(
          _con.coursePreview.courseDescription,
          trimLines: 8,
          colorClickableText: config.Colors.primaryColor(),
          trimMode: TrimMode.Line,
          trimCollapsedText: 'Xem thêm',
          trimExpandedText: '',
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal, color: config.Colors.hintColor()),
          moreStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.normal, color: config.Colors.primaryColor()),
          onChange: (bool show) {
            print("1");
          },
        ),
      );
    }

    Widget _courseDescription2() {
      if (_con.coursePreview.courseResults == null || _con.coursePreview.courseResults.length == 0) return Container();
      return Container(
        width: config.App.appWidth(100),
        //margin: EdgeInsets.only(bottom: 100),
        padding: EdgeInsets.fromLTRB(16, 16, 16, 20),
        decoration: BoxDecoration(
          color: config.Colors.backgroundColor(),
        ),
        child: ColumnStart(
          children: _con.coursePreview.courseResults
              .map(
                (e) => Container(
                  padding: EdgeInsets.only(bottom: 16),
                  child: Text(
                    " - " + e,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal, color: config.Colors.hintColor()),
                  ),
                ),
              )
              .toList(),
        ),
      );
    }

    Widget _bodyView() {
      if (_con.isLoading) return LoadContent();
      if (_con.coursePreview == null) return NoContent();
      var _title = _titleInfo();
      return Stack(
        alignment: Alignment.bottomLeft,
        children: <Widget>[
          DefaultTabController(
            length: 2,
            child: NestedScrollView(
              headerSliverBuilder: (_, __) {
                return <Widget>[
                  SliverAppBar(
                    expandedHeight: _title['height'],
                    flexibleSpace: FlexibleSpaceBar(
                      background: _title['widget'],
                      collapseMode: CollapseMode.pin,
                    ),
                    leading: Container(),
                    titleSpacing: 0,
                  ),
                  SliverPersistentHeader(
                    delegate: SliverAppBarDelegate(
                      TabBar(
                        indicatorColor: config.Colors.primaryColor(),
                        labelColor: config.Colors.primaryColor(),
                        unselectedLabelColor: config.Colors.accentColor(),
                        tabs: _listTab,
                      ),
                    ),
                    pinned: true,
                  ),
                ];
              },
              body: TabBarView(
                children: [
                  TabViewBox(_courseDescription(), true),
                  TabViewBox(_courseDescription2(), true),
                ],
              ),
            ),
          ),
          _bottomButtons(),
        ],
        // ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyPreview,
      isSingleChildScrollView: false,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Thông tin khóa học"),
      ),
      body: _bodyView(),
    );
  }
}
