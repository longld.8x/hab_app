import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/courses_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/TitleText.dart';
import 'package:hab_app/src/elements/Waiting.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/pages/courses/CourseSaleWidget.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class CoursesWidget extends StatefulWidget {
  @override
  _CoursesWidgetState createState() => _CoursesWidgetState();
}

class _CoursesWidgetState extends StateMVC<CoursesWidget> {
  CoursesController _con;
  User data;
  _CoursesWidgetState() : super(CoursesController()) {
    _con = controller;
  }
  int _f1Level = 0;

  @override
  void initState() {
    super.initState();
    _con.pageState();
  }

  @override
  Widget build(BuildContext context) {
    Widget _bodyView() {
      if (_con.isLoading) return Container(padding: EdgeInsets.only(top: 24, left: 0, right: 0), child: LoadContent(warp: true));
      if (_con.dataCourses == null || _con.dataCourses.length == 0) return Container(padding: EdgeInsets.only(top: 24, left: 0, right: 0), child: NoContent(warp: true));

      return Container(
        padding: EdgeInsets.only(top: 24, left: 0, right: 0),
        child: new ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) => CourseSaleWidget(model: _con.dataCourses[index]),
          itemCount: _con.dataCourses.length,
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKey,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: TitleText("Danh sách khóa học"),
      ),
      body: Container(
          width: config.App.appWidth(100),
          decoration: BoxDecoration(
            color: config.Colors.inputBgColor(),
          ),
          child: _bodyView()),
    );
  }
}
