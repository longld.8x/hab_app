import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/register_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitlePageSimpleWidget.dart';
import 'package:hab_app/src/helpers/alert_dialog.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class RegisterWidget extends StatefulWidget {
  @override
  _RegisterWidgetState createState() => _RegisterWidgetState();
}

class _RegisterWidgetState extends StateMVC<RegisterWidget> {
  RegisterController _con;
  TextEditingController _ctrlPhoneNumber = TextEditingController();
  String _msg;
  bool _activeBtn = false;
  _RegisterWidgetState() : super(RegisterController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.registerState();
  }

  @override
  Widget build(BuildContext context) {
    Widget _validateWidget() {
      if (_msg == null || _msg.isEmpty) return Container();
      return Container(
        padding: EdgeInsets.only(left: 15, right: 15),
        alignment: Alignment.topLeft,
        child: ErrorText(_msg),
      );
    }

    String phoneValid(String newValue) {
      if (newValue.length < 10) return "Số điện thoại không đúng định dạng";
      var msg = Helper.validPhonumber(newValue);
      if (msg != null && msg.isNotEmpty) return msg;
      return null;
    }

    void activeBtn() {
      setState(() => _activeBtn = (_ctrlPhoneNumber.text != null && _ctrlPhoneNumber.text.isNotEmpty));
    }

    Widget _formWidget() {
      return Form(
        key: _con.formKeyRegister,
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InputField(
                labelText: "Số điện thoại",
                controller: _ctrlPhoneNumber,
                hintText: '0912345678',
                keyboardType: TextInputType.number,
                maxLength: 10,
                validator: (newValue) {
                  return phoneValid(newValue);
                },
                onChanged: (String newValue) {
                  activeBtn();
                },
              ),
              SizedBox(height: 40),
              Button(
                isActive: _activeBtn,
                text: "Tiếp tục",
                onPressed: () {
                  if (_con.formKeyRegister.currentState.validate()) {
                    _con.checkPhoneNumber(_ctrlPhoneNumber.text).then((ResponseMessage result) {
                      print("result.toMap() ${result.toMap()}");
                      if (result.success)
                        DialogHLS.confirmDialog(context, "Xác nhận số điện thoại", "HAB sẽ gửi mã xác thực tới số ${_ctrlPhoneNumber.text}. Bạn vui lòng xác nhận số điện thoại đã nhập là đúng.", () {
                          Navigator.of(context).pushNamed(
                            '/OTP',
                            arguments: OTPArgument(
                                type: 'register',
                                phone: _ctrlPhoneNumber.text,
                                isVerify: true,
                                success: (String otp) {
                                  Navigator.of(context).pushNamed('/Register/SetPassword', arguments: RouteArgument(tag: _ctrlPhoneNumber.text, code: otp));
                                }),
                          );
                        });
                      else {
                        var _actions = Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              child: DialogHLS.actionBtn(
                                  context: context,
                                  text: "Đăng nhập",
                                  action: () {
                                    Navigator.of(context).pushNamedAndRemoveUntil('/Login', (Route<dynamic> route) => false, arguments: _ctrlPhoneNumber.text);
                                  }),
                              padding: EdgeInsets.only(bottom: 10),
                            ),
                            Container(
                              child: DialogHLS.actionBtn(
                                context: context,
                                text: "Quên mật khẩu",
                                action: () {
                                  Navigator.of(context).pushNamed('/ForgetPassword', arguments: _ctrlPhoneNumber.text);
                                },
                                color: config.Colors.customeColor("#ffffff"),
                                textStyle: TextStyle(color: config.Colors.primaryColor()),
                              ),
                              padding: EdgeInsets.only(left: 5),
                            )
                          ],
                        );
                        DialogHLS.DialogBase(context: context, title: "Đăng ký", subtitle: result.message, actions: _actions);
                      }
                    });
                  }
                },
              ),
            ],
          ),
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyRegister,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: null,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            TitlePageSimpleWidget("Đăng ký"),
            SizedBox(height: 80),
            _validateWidget(),
            _formWidget(),
          ],
        ),
      ),
    );
  }
}
