import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/user_controller.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Common.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/flix_widget_extensions.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/setting.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class LoginWidget extends StatefulWidget {
  final String phoneNumber;
  LoginWidget({Key key, this.phoneNumber}) : super(key: key);
  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends StateMVC<LoginWidget> with SingleTickerProviderStateMixin {
  UserController _con;
  AuthenUser _userAuthen;
  Setting _setting = settingRepo.setting.value;
  final LocalAuthentication _auth = LocalAuthentication();
  List<BiometricType> _availableBiometrics;
  bool _onAuthen = false;
  String _authenType = '';
  bool _canCheckBiometrics = false;
  bool _activeBtn = false;

  bool _hidePassword1 = true;
  _LoginWidgetState() : super(UserController()) {
    _con = controller;
  }
  Widget _userAvatar;
  TextEditingController _ctrlUserName;
  TextEditingController _ctrlPass;

  @override
  void initState() {
    super.initState();
    _userAvatar = UserAvatar(
      '',
      width: 100,
    );
    _con.loginState();
    _ctrlUserName = TextEditingController(text: widget.phoneNumber != null ? widget.phoneNumber : "");
    _ctrlPass = TextEditingController();
    settingRepo.getAuthenUser().then((value) {
      if (value != null)
        setState(() {
          print("value.profilePicture  ${value.profilePicture}");
          _userAvatar = UserAvatar(value.profilePicture, width: 100);
          _userAuthen = value;
        });
    });
    if (mounted) {
      _checkBiometrics();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.phoneNumber == null) {
      fingerprintAuth();
    }

    Widget _registerWidget() {
      return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AccentText("Bạn chưa có tài khoản? "),
            Container(
              padding: EdgeInsets.only(top: 5, right: 0, left: 0, bottom: 5),
              child: PrimaryText("Đăng ký ngay"),
            ).onTap(() {
              Navigator.of(context).pushNamed('/Register');
            })
          ],
        ),
      );
    }

    String userNameValid(String newValue) {
      if (newValue.length < 10) return "Số điện thoại không đúng định dạng";
      var msg = Helper.validPhonumber(newValue);
      if (msg != null && msg.isNotEmpty) return msg;
      return null;
    }

    String passValid(String newValue) {
      if (newValue.length == 0) return "Vui lòng nhập mật khẩu";
      return null;
    }

    void activeBtn() {
      setState(() => _activeBtn = (_ctrlUserName.text != null && _ctrlUserName.text.isNotEmpty && _ctrlPass.text != null && _ctrlPass.text.isNotEmpty));
    }

    Widget _iconLoginFingerprintWidget() {
      if (!_canCheckBiometrics) return Container();
      if (_authenType == null || _authenType.isEmpty) return Container();
      var _title = "Đăng nhập bằng " + Helper.securityTypeName(_authenType).toLowerCase();
      var _icon = _authenType == BiometricType.fingerprint.toString()
          ? Icon(
              Icons.fingerprint,
              color: Colors.redAccent,
              size: 20,
            )
          : _authenType == BiometricType.face.toString()
              ? SvgPicture.asset(
                  'assets/icon/face_id.svg',
                  height: 20,
                )
              : _authenType == BiometricType.iris.toString()
                  ? SvgPicture.asset(
                      'assets/icon/face_id.svg',
                      height: 20,
                    )
                  : SvgPicture.asset(
                      'assets/icon/face_id.svg',
                      height: 20,
                    );
      return InkWell(
        onTap: () {
          setState(() => _onAuthen = true);
        },
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: _icon,
                padding: EdgeInsets.fromLTRB(0, 0, 8, 0),
              ),
              Text(_title, style: Theme.of(context).textTheme.bodyText1.copyWith(fontWeight: FontWeight.normal)),
            ],
          ),
          padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
        ),
      );
    }

    Widget _formLoginUserWidget() {
      return Form(
        key: _con.formKeyLogin,
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(child: _userAvatar),
              SizedBox(height: 20),
              Center(
                  child: Text(
                _userAuthen.fullName,
                style: Theme.of(context).textTheme.headline6,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              )),
              SizedBox(height: 10),
              Center(child: Text(_userAuthen.phoneNumber, style: Theme.of(context).textTheme.bodyText1)),
              SizedBox(height: 20),
              InputField(
                controller: _ctrlPass,
                labelText: "Mật khẩu",
                hintText: '••••••••',
                obscureText: _hidePassword1,
                suffixIcon: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    _ctrlPass.text.isNotEmpty
                        ? IconButton(
                            onPressed: () => setState(() => _ctrlPass.text = ''),
                            icon: Icon(Icons.close, size: 16, color: config.Colors.primaryColor()),
                            padding: EdgeInsets.only(right: 0, left: 0),
                          )
                        : Container(),
                    IconButton(
                      onPressed: () => setState(() => _hidePassword1 = !_hidePassword1),
                      icon: _hidePassword1 ? SvgPicture.asset('assets/icon/eye.svg') : SvgPicture.asset('assets/icon/eye2.svg'),
                      padding: EdgeInsets.only(right: 0, left: 0),
                    ),
                  ],
                ),
                validator: (String newValue) {
                  return passValid(newValue);
                },
                onChanged: (String newValue) {
                  setState(() => _activeBtn = (_ctrlPass.text != null && _ctrlPass.text.isNotEmpty));
                },
                onEditingComplete: () {
                  if (_con.formKeyLogin.currentState.validate()) {
                    _onLogin(_userAuthen.userName, _ctrlPass.text);
                  }
                },
              ),
              SizedBox(height: 10),
              Container(
                width: config.App.appWidth(100),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 5, right: 0, left: 0, bottom: 5),
                        child: AccentText(
                          "Quên mật khẩu?",
                          style: TextStyle(decoration: TextDecoration.underline),
                        )).onTap(
                      () {
                        Navigator.of(context).pushNamed('/ForgetPassword');
                      },
                    ),
                    Container(
                        padding: EdgeInsets.only(top: 5, right: 0, left: 0, bottom: 5),
                        child: AccentText(
                          "Thoát tài khoản?",
                          style: TextStyle(decoration: TextDecoration.underline),
                        )).onTap(
                      () {
                        _con.logoutClear();
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(height: 31),
              Button(
                isActive: _activeBtn,
                text: "Đăng nhập",
                onPressed: () {
                  if (_con.formKeyLogin.currentState.validate()) {
                    _onLogin(_ctrlUserName.text, _ctrlPass.text);
                  }
                },
              ),
              SizedBox(height: 15),
              _iconLoginFingerprintWidget(),
              SizedBox(height: 15),
            ],
          ),
        ),
      );
    }

    Widget _formWidget() {
      if (_userAuthen != null) return _formLoginUserWidget();
      return Form(
        key: _con.formKeyLogin,
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InputField(
                controller: _ctrlUserName,
                labelText: "Số điện thoại",
                hintText: '0912345678',
                keyboardType: TextInputType.number,
                maxLength: 10,
                validator: (String newValue) {
                  return userNameValid(newValue);
                },
                onChanged: (String newValue) {
                  activeBtn();
                },
                suffixIcon: _ctrlUserName.text.isNotEmpty
                    ? IconButton(
                        onPressed: () => setState(() => _ctrlUserName.text = ''),
                        icon: Icon(Icons.close, size: 16, color: config.Colors.primaryColor()),
                        padding: EdgeInsets.only(right: 0, left: 0),
                      )
                    : Container(
                        alignment: Alignment.centerRight,
                        width: 1,
                      ),
              ),
              SizedBox(height: 15),
              InputField(
                controller: _ctrlPass,
                labelText: "Mật khẩu",
                hintText: '••••••••',
                obscureText: _hidePassword1,
                suffixIcon: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    _ctrlPass.text.isNotEmpty
                        ? IconButton(
                            onPressed: () => setState(() => _ctrlPass.text = ''),
                            icon: Icon(Icons.close, size: 16, color: config.Colors.primaryColor()),
                            padding: EdgeInsets.only(right: 0, left: 0),
                          )
                        : Container(),
                    IconButton(
                      onPressed: () => setState(() => _hidePassword1 = !_hidePassword1),
                      icon: _hidePassword1 ? SvgPicture.asset('assets/icon/eye.svg') : SvgPicture.asset('assets/icon/eye2.svg'),
                      padding: EdgeInsets.only(right: 0, left: 0),
                    ),
                  ],
                ),
                // suffixIcon: IconButton(
                //   onPressed: () => setState(() => _hidePassword1 = !_hidePassword1),
                //   icon: _hidePassword1 ? SvgPicture.asset('assets/icon/eye.svg') : SvgPicture.asset('assets/icon/eye2.svg'),
                // ),
                validator: (String newValue) {
                  return passValid(newValue);
                },
                onChanged: (String newValue) {
                  activeBtn();
                },
                onEditingComplete: () {
                  if (_con.formKeyLogin.currentState.validate()) {
                    _onLogin(_ctrlUserName.text, _ctrlPass.text);
                  }
                },
                // onChanged: (String newValue) {
                //   setState(() => _activeBtn = (userNameValid(_ctrlUserName.text) == null && passValid(_ctrlPass.text) == null));
                // },
              ),
              SizedBox(height: 10),
              Container(
                width: config.App.appWidth(100),
                child: Container(
                    padding: EdgeInsets.only(top: 5, right: 0, left: 0, bottom: 5),
                    child: AccentText(
                      "Quên mật khẩu?",
                      style: TextStyle(decoration: TextDecoration.underline),
                    )).onTap(() {
                  Navigator.of(context).pushNamed('/ForgetPassword');
                }),
                alignment: Alignment.centerLeft,
              ),
              SizedBox(height: 31),
              Button(
                isActive: _activeBtn,
                text: "Đăng nhập",
                onPressed: () {
                  if (_con.formKeyLogin.currentState.validate()) {
                    _onLogin(_ctrlUserName.text, _ctrlPass.text);
                  }
                },
              ),
              SizedBox(height: 15),
              _registerWidget(),
              SizedBox(height: 15),
            ],
          ),
        ),
      );
    }

    Widget _logoWidget() {
      return Container(
        height: _userAuthen != null ? config.App.appHeight(25) : config.App.appHeight(35),
        padding: EdgeInsets.only(bottom: _userAuthen != null ? 20 : 50),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Image.asset(
              'assets/icon/logo.png',
              height: 100,
            ),
          ],
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeyLogin,
      appBar: null,
      onWillPop: Helper.of(context).onWillPop,
      body: Container(
        child: Column(
          children: <Widget>[
            _logoWidget(),
            //_validateWidget(),
            _formWidget(),
          ],
        ),
      ),
    );
  }

  // _getAuthenType
  // get
  Future<void> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await _auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }
    print("canCheckBiometrics $canCheckBiometrics");

    if (!mounted) return;
    setState(() => _canCheckBiometrics = canCheckBiometrics);
    if (!_canCheckBiometrics) return;
    settingRepo.getAuthenType().then((value) => setState(() => _authenType = value));
    settingRepo.getBiometricTimeout().then((value) => setState(() => _onAuthen = value));
  }

  // _authen
  Future<void> _authenticateFingerprint() async {
    bool authenticated = false;
    print("_userAuthen $_userAuthen");
    if (_userAuthen == null) return;
    var _pName = Helper.securityTypeName(_authenType);
    try {
      authenticated = await _auth.authenticateWithBiometrics(
        localizedReason: 'Sử dụng ' + _pName + ' để đăng nhập app HAB!',
        useErrorDialogs: true,
        stickyAuth: true,
        androidAuthStrings: new AndroidAuthMessages(
          signInTitle: "Đăng nhập bằng " + _pName,
          cancelButton: "Hủy bỏ",
        ),
      );
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;
    if (authenticated) {
      setState(() => _onAuthen = false);
      await _onLogin(_userAuthen.userName, _userAuthen.password);
    } else {
      _onAuthen = false;
    }
  }

  // register
  void _onLogin(String userName, String password) async {
    _con.login(userName, password).then((ResponseMessage result) {
      print("result.toMap() ${result.toMap()}");
      if (result.success)
        Navigator.of(context).pushNamedAndRemoveUntil('/Page', (Route<dynamic> route) => false, arguments: 0);
      else
        Helper.errorMessenge(result.message);
    });
  }

  void fingerprintAuth() async {
    if (!_canCheckBiometrics) return;
    if (!_onAuthen) return;
    if (_authenType == null || _authenType.isEmpty) return;
    await _authenticateFingerprint();
  }
}
