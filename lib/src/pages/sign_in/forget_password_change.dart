import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/forget_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitlePageSimpleWidget.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ForgetPasswordChangeWidget extends StatefulWidget {
  final RouteArgument routeArgument;
  ForgetPasswordChangeWidget({Key key, this.routeArgument}) : super(key: key);
  @override
  _ForgetPasswordChangeWidgetState createState() => _ForgetPasswordChangeWidgetState();
}

class _ForgetPasswordChangeWidgetState extends StateMVC<ForgetPasswordChangeWidget> {
  ForgetController _con;
  TextEditingController _ctrlPass1 = TextEditingController();
  TextEditingController _ctrlPass2 = TextEditingController();
  String _msg;
  String _phoneNumber;
  String _otp;
  bool _hidePassword1 = true;
  bool _hidePassword2 = true;
  bool _activeBtn = false;
  _ForgetPasswordChangeWidgetState() : super(ForgetController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _phoneNumber = widget.routeArgument.tag;
    _otp = widget.routeArgument.code;
    _con.pagePasswordState();
  }

  void activeBtn() {
    setState(() => _activeBtn = (_ctrlPass1.text != null && _ctrlPass1.text.isNotEmpty) && (_ctrlPass2.text != null && _ctrlPass2.text.isNotEmpty));
  }

  @override
  Widget build(BuildContext context) {
    Widget _validateWidget() {
      if (_msg == null || _msg.isEmpty) return Container();
      return Container(
        padding: EdgeInsets.only(left: 15, right: 15),
        alignment: Alignment.topLeft,
        child: ErrorText(_msg),
      );
    }

    Widget _formWidget() {
      return Form(
        key: _con.formKeySetPassword,
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InputField(
                controller: _ctrlPass1,
                labelText: "Mật khẩu mới",
                hintText: '••••••••',
                obscureText: _hidePassword1,
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Vui lòng nhập mật khẩu';
                  }
                  if (value.length < 8) {
                    return 'Mật khẩu cần có ít nhất 8 ký tự.';
                  }
                  return null;
                },
                onChanged: (String value) {
                  activeBtn();
                },
                suffixIcon: IconButton(
                  onPressed: () => setState(() => _hidePassword1 = !_hidePassword1),
                  icon: _hidePassword1 ? SvgPicture.asset('assets/icon/eye.svg') : SvgPicture.asset('assets/icon/eye2.svg'),
                ),
              ),
              SizedBox(height: 24),
              InputField(
                controller: _ctrlPass2,
                labelText: "Xác nhận mật khẩu mới",
                hintText: '••••••••',
                obscureText: _hidePassword2,
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Vui lòng nhập mật khẩu';
                  }
                  if (value.length < 8) {
                    return 'Mật khẩu cần có ít nhất 8 ký tự.';
                  }
                  if (value != _ctrlPass1.text) {
                    return 'Xác nhận mật khẩu không chính xác.';
                  }
                  return null;
                },
                onChanged: (String value) {
                  activeBtn();
                },
                suffixIcon: IconButton(
                  onPressed: () => setState(() => _hidePassword2 = !_hidePassword2),
                  icon: _hidePassword2 ? SvgPicture.asset('assets/icon/eye.svg') : SvgPicture.asset('assets/icon/eye2.svg'),
                ),
              ),
              SizedBox(height: 40),
              Button(
                isActive: _activeBtn,
                text: "Hoàn tất",
                onPressed: () {
                  if (_con.formKeySetPassword.currentState.validate()) {
                    _con.forgetPassword(_phoneNumber, _ctrlPass2.text)
                      ..then((ResponseMessage result) {
                        if (result.success) {
                          Navigator.of(context).pushNamedAndRemoveUntil('/Login', (Route<dynamic> route) => false, arguments: _phoneNumber);
                          Helper.successMessenge("Thay đổi mật khẩu thành công");
                        } else {
                          Helper.errorMessenge(result.message);
                        }
                      });
                  }
                },
              ),
            ],
          ),
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKeySetPassword,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: null,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            TitlePageSimpleWidget("Nhập mật khẩu mới"),
            SizedBox(height: 80),
            _validateWidget(),
            _formWidget(),
          ],
        ),
      ),
    );
  }
}
