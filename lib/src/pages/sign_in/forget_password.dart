import 'package:flutter/material.dart';
import 'package:hab_app/src/controllers/forget_controller.dart';
import 'package:hab_app/src/elements/AppBarWidget.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/elements/Input.dart';
import 'package:hab_app/src/elements/Layout.dart';
import 'package:hab_app/src/elements/LineText.dart';
import 'package:hab_app/src/elements/TitlePageSimpleWidget.dart';
import 'package:hab_app/src/helpers/alert_dialog.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ForgetPasswordWidget extends StatefulWidget {
  final String phoneNumber;
  ForgetPasswordWidget({Key key, this.phoneNumber}) : super(key: key);
  @override
  _ForgetPasswordWidgetState createState() => _ForgetPasswordWidgetState();
}

class _ForgetPasswordWidgetState extends StateMVC<ForgetPasswordWidget> {
  ForgetController _con;
  TextEditingController _ctrlPhoneNumber;
  String _msg;
  bool _activeBtn = false;
  _ForgetPasswordWidgetState() : super(ForgetController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.pageState();
    _ctrlPhoneNumber = TextEditingController(text: widget.phoneNumber != null ? widget.phoneNumber : "");
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget _validateWidget() {
      if (_msg == null || _msg.isEmpty) return Container();
      return Container(
        padding: EdgeInsets.only(left: 15, right: 15),
        alignment: Alignment.topLeft,
        child: ErrorText(_msg),
      );
    }

    void activeBtn() {
      setState(() => _activeBtn = (_ctrlPhoneNumber.text != null && _ctrlPhoneNumber.text.isNotEmpty));
    }

    Widget _formWidget() {
      return Form(
        key: _con.formKey,
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InputField(
                labelText: "Số điện thoại",
                controller: _ctrlPhoneNumber,
                hintText: '0912345678',
                keyboardType: TextInputType.number,
                maxLength: 10,
                validator: (newValue) {
                  if (newValue.length < 10) return "Số điện thoại không đúng định dạng";
                  var msg = Helper.validPhonumber(newValue);
                  if (msg != null && msg.isNotEmpty) return msg;
                  return null;
                },
                onChanged: (String newValue) => activeBtn(),
              ),
              SizedBox(height: 40),
              Button(
                isActive: _activeBtn,
                text: "Tiếp tục",
                onPressed: () {
                  if (_con.formKey.currentState.validate()) {
                    _con.checkPhoneNumber(_ctrlPhoneNumber.text).then((ResponseMessage result) {
                      print("result.toMap() ${result.toMap()}");
                      if (!result.success)
                        DialogHLS.confirmDialog(context, "Xác nhận số điện thoại", "HAB sẽ gửi mã xác thực tới số ${_ctrlPhoneNumber.text}. Bạn vui lòng xác nhận số điện thoại đã nhập là đúng.", () {
                          Navigator.of(context).pushNamed(
                            '/OTP',
                            arguments: OTPArgument(
                                type: 'forget_password',
                                phone: _ctrlPhoneNumber.text,
                                isVerify: true,
                                success: (String otp) {
                                  Navigator.of(context).pushNamed('/ForgetPassword/ChangePassword', arguments: RouteArgument(tag: _ctrlPhoneNumber.text, code: otp));
                                }),
                          );
                        });
                      else {
                        DialogHLS.confirmDialog(context, "Quên mật khẩu", "Tài khoản ${_ctrlPhoneNumber.text} không tồn tại. Vui lòng kiểm tra lại?", () {
                          Navigator.of(context).pop();
                        }, textAction: "Đăng nhập");
                      }
                    });
                  }
                },
              ),
            ],
          ),
        ),
      );
    }

    return LayoutManager(
      scaffoldKey: _con.scaffoldKey,
      appBar: AppBar(
        leading: AppbarIcon(),
        title: null,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            TitlePageSimpleWidget("Quên mật khẩu"),
            SizedBox(height: 80),
            _validateWidget(),
            _formWidget(),
          ],
        ),
      ),
    );
  }
}
