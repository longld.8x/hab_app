import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hab_app/src/controllers/home_controller.dart';
import 'package:hab_app/src/elements/Button.dart';
import 'package:hab_app/src/helpers/app_config.dart' as config;
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/pages/add/mealType.dart';

class FloatActionButton extends StatefulWidget {
  Function onChange;
  HomeController controller;
  GlobalKey<ScaffoldState> scaffoldKey;
  FloatActionButton({Key key, this.onChange, this.controller, this.scaffoldKey}) : super(key: key);
  _FloatActionButtonState createState() => _FloatActionButtonState();
}

class _FloatActionButtonState extends State<FloatActionButton> with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation degOneTranslationAnimation;
  //Animation degTwoTranslationAnimation;
  Animation rotationAnimation;
  double getRadiansFromDegree(double degree) {
    double unitRadian = 57.295779513;
    return degree / unitRadian;
  }

  @override
  initState() {
    animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 250));
    degOneTranslationAnimation = TweenSequence(<TweenSequenceItem>[
      TweenSequenceItem<double>(tween: Tween<double>(begin: 0.0, end: 1.2), weight: 75.0),
      TweenSequenceItem<double>(tween: Tween<double>(begin: 1.2, end: 1.0), weight: 25.0),
    ]).animate(animationController);
    // degTwoTranslationAnimation = TweenSequence(<TweenSequenceItem>[
    //   TweenSequenceItem<double>(tween: Tween<double>(begin: 0.0, end: 1.2), weight: 75.0),
    //   TweenSequenceItem<double>(tween: Tween<double>(begin: 1.2, end: 1.0), weight: 25.0),
    // ]).animate(animationController);

    rotationAnimation = Tween<double>(begin: 180.0, end: 0.0).animate(CurvedAnimation(parent: animationController, curve: Curves.easeOut));
    super.initState();
    animationController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget _mainButton(BuildContext context) {
      return Container(
        width: config.App.appWidth(100),
        height: config.App.appWidth(100),
        alignment: Alignment.bottomCenter,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            // OverflowBox(
            //   alignment: Alignment.bottomCenter,
            //   child: Transform.translate(
            //     offset: Offset.fromDirection(getRadiansFromDegree(190), degOneTranslationAnimation.value * 105),
            //     child: Transform(
            //       transform: Matrix4.rotationZ(getRadiansFromDegree(rotationAnimation.value))..scale(degOneTranslationAnimation.value),
            //       alignment: Alignment.center,
            //       child: CircularButton2(
            //           icon: SvgPicture.asset('assets/icon/other1.svg'),
            //           text: "Chụp bữa ăn",
            //           onClick: () {
            //             animationController.reverse();
            //             widget.controller.getMealType().then((result) {
            //               if (result.success) {
            //                 showModalBottomSheet(
            //                   context: context,
            //                   isDismissible: false,
            //                   shape: RoundedRectangleBorder(
            //                     borderRadius: BorderRadius.vertical(top: Radius.circular(28.0)),
            //                     side: BorderSide.none,
            //                   ),
            //                   elevation: 0,
            //                   builder: (_) => MealType(data: result.results),
            //                 );
            //               } else {
            //                 Helper.errorMessenge(result.message);
            //               }
            //             });
            //           }),
            //     ),
            //   ),
            // ),
            // OverflowBox(
            //   alignment: Alignment.bottomCenter,
            //   child: Transform.translate(
            //     offset: Offset.fromDirection(getRadiansFromDegree(245), degOneTranslationAnimation.value * 125),
            //     child: Transform(
            //       transform: Matrix4.rotationZ(getRadiansFromDegree(rotationAnimation.value))..scale(degOneTranslationAnimation.value),
            //       alignment: Alignment.center,
            //       child: CircularButton2(
            //           icon: SvgPicture.asset('assets/icon/other2.svg'),
            //           text: "Thêm nước uống",
            //           onClick: () {
            //             animationController.reverse();
            //             Navigator.of(context).pushNamed('/Add/Water');
            //           }),
            //     ),
            //   ),
            // ),
            // OverflowBox(
            //   alignment: Alignment.bottomCenter,
            //   child: Transform.translate(
            //     offset: Offset.fromDirection(getRadiansFromDegree(295), degOneTranslationAnimation.value * 125),
            //     child: Transform(
            //       transform: Matrix4.rotationZ(getRadiansFromDegree(rotationAnimation.value))..scale(degOneTranslationAnimation.value),
            //       alignment: Alignment.center,
            //       child: CircularButton2(
            //           icon: SvgPicture.asset('assets/icon/other3.svg'),
            //           text: "Tạo vận động",
            //           onClick: () {
            //             animationController.reverse();
            //             Navigator.of(context).pushNamed('/Add/Workout');
            //           }),
            //     ),
            //   ),
            // ),
            // OverflowBox(
            //   alignment: Alignment.bottomCenter,
            //   child: Transform.translate(
            //     offset: Offset.fromDirection(getRadiansFromDegree(350), degOneTranslationAnimation.value * 105),
            //     child: Transform(
            //       transform: Matrix4.rotationZ(getRadiansFromDegree(rotationAnimation.value))..scale(degOneTranslationAnimation.value),
            //       alignment: Alignment.center,
            //       child: CircularButton2(
            //           icon: SvgPicture.asset('assets/icon/other4.svg'),
            //           text: "Tạo công việc",
            //           onClick: () {
            //             animationController.reverse();
            //             Helper.successMessenge('Tính năng đang phát triển...');
            //             //Navigator.of(context).pushNamed('/Add/Task');
            //           }),
            //     ),
            //   ),
            // ),

            OverflowBox(
              alignment: Alignment.bottomCenter,
              child: Transform.translate(
                offset: Offset.fromDirection(getRadiansFromDegree(190), degOneTranslationAnimation.value * 85),
                child: Transform(
                  transform: Matrix4.rotationZ(getRadiansFromDegree(rotationAnimation.value))..scale(degOneTranslationAnimation.value),
                  alignment: Alignment.center,
                  child: CircularButton2(
                      icon: SvgPicture.asset('assets/icon/other1.svg'),
                      text: "Chụp bữa ăn",
                      onClick: () {
                        animationController.reverse();
                        widget.controller.getMealType().then((result) {
                          if (result.success) {
                            showModalBottomSheet(
                              context: context,
                              isDismissible: false,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.vertical(top: Radius.circular(28.0)),
                                side: BorderSide.none,
                              ),
                              elevation: 0,
                              builder: (_) => MealType(data: result.results),
                            );
                          } else {
                            Helper.errorMessenge(result.message);
                          }
                        });
                      }),
                ),
              ),
            ),
            OverflowBox(
              alignment: Alignment.bottomCenter,
              child: Transform.translate(
                offset: Offset.fromDirection(getRadiansFromDegree(270), degOneTranslationAnimation.value * 100),
                child: Transform(
                  transform: Matrix4.rotationZ(getRadiansFromDegree(rotationAnimation.value))..scale(degOneTranslationAnimation.value),
                  alignment: Alignment.center,
                  child: CircularButton2(
                      icon: SvgPicture.asset('assets/icon/other2.svg'),
                      text: "Thêm nước uống",
                      onClick: () {
                        animationController.reverse();
                        Navigator.of(context).pushNamed('/Add/Water');
                      }),
                ),
              ),
            ),
            OverflowBox(
              alignment: Alignment.bottomCenter,
              child: Transform.translate(
                offset: Offset.fromDirection(getRadiansFromDegree(350), degOneTranslationAnimation.value * 85),
                child: Transform(
                  transform: Matrix4.rotationZ(getRadiansFromDegree(rotationAnimation.value))..scale(degOneTranslationAnimation.value),
                  alignment: Alignment.center,
                  child: CircularButton2(
                      icon: SvgPicture.asset('assets/icon/other3.svg'),
                      text: "Thêm vận động",
                      onClick: () {
                        animationController.reverse();
                        Navigator.of(context).pushNamed('/Add/Workout');
                      }),
                ),
              ),
            ),
            OverflowBox(
              alignment: Alignment.bottomCenter,
              child: Transform(
                transform: Matrix4.rotationZ(getRadiansFromDegree(rotationAnimation.value)),
                alignment: Alignment.center,
                child: Container(
                  width: 150,
                  height: 120,
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                  ),
                  alignment: Alignment.bottomCenter,
                  child: CircularButton(
                    icon: Icon(
                      animationController.isCompleted ? Icons.close : Icons.add,
                      color: Colors.white,
                      size: animationController.isCompleted ? IconTheme.of(context).size : IconTheme.of(context).size + 4,
                    ),
                    onClick: () {
                      if (animationController.isCompleted) {
                        animationController.reverse();
                        if (widget.onChange != null) widget.onChange(true);
                      } else {
                        animationController.forward();
                        if (widget.onChange != null) widget.onChange(false);
                      }
                      //  Helper.successMessenge('Helper.suc cessM esse essMess enge ');
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Material(
      type: animationController.isCompleted ? MaterialType.card : MaterialType.transparency,
      color: Colors.black.withOpacity(0.5),
      child: Container(
        alignment: Alignment.bottomCenter,
        width: config.App.appWidth(100),
        height: config.App.appHeight(100),
        child: _mainButton(context),
      ),
    );
  }
}
