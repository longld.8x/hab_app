import 'dart:developer';

import 'package:flutter/services.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:quickblox_sdk/auth/module.dart';
import 'package:quickblox_sdk/models/qb_session.dart';
import 'package:quickblox_sdk/models/qb_user.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';

/// init
Future<void> initQb() async {
  try {
    var _appId = GlobalConfiguration().getString("quickblox.APP_ID");
    var _authKey = GlobalConfiguration().getString("quickblox.AUTH_KEY");
    var _authSecret = GlobalConfiguration().getString("quickblox.AUTH_SECRET");
    var _accountKey = GlobalConfiguration().getString("quickblox.ACCOUNT_KEY");
    print("quickblox $_appId - $_authKey - $_authSecret - $_accountKey ");
    await QB.settings.init(_appId, _authKey, _authSecret, _accountKey);
    // Map<String, Object> map = await QB.settings.get();
  } on PlatformException catch (e) {
    log('initQb ${e.toString()}');
    // Some error occured, look at the exception message for more details
  }
}

/// login
Future<void> loginQb() async {
  try {
    QBLoginResult result = await QB.auth.login('hoanglt001', '12345678');
    QBUser qbUser = result.qbUser;
    QBSession qbSession = result.qbSession;
    DataHolder.getInstance().setSession(qbSession);
    DataHolder.getInstance().setUser(qbUser);
    await connectChat();
  } on PlatformException catch (e) {
    log('loginQb ${e.toString()}');
  }
}

/// connect chat
Future<void> connectChat() async {
  try {
    var qbUser = DataHolder.getInstance().getUser();
    await QB.chat.connect(qbUser.id, '12345678');
  } on PlatformException catch (e) {
    log('connectChat ${e.toString()}');
  }
}

/// logout
Future<void> logoutQb() async {
  try {
    await disconnectChat();
    await QB.auth.logout();
  } on PlatformException catch (e) {
    log('logoutQb ${e.toString()}');
  }
}

/// disconnect chat
Future<void> disconnectChat() async {
  try {
    await QB.chat.disconnect();
  } on PlatformException catch (e) {
    log('disconnectChat ${e.toString()}');
  }
}

/// send message
Future<bool> sendMessage(String dialogId, String body) async {
  try {
    await QB.chat.sendMessage(dialogId, body: body, saveToHistory: true);
    return true;
  } on PlatformException catch (e) {
    log('sendMessage ${e.toString()}');
    return false;
  }
}

/// data holder
class DataHolder {
  QBSession _session;
  QBUser _qbUser;

  static DataHolder _instance;

  static DataHolder getInstance() {
    if (_instance == null) {
      _instance = DataHolder();
    }
    return _instance;
  }

  void setSession(QBSession session) {
    this._session = session;
  }

  QBSession getSession() {
    return _session;
  }

  void setUser(QBUser qbUser) {
    this._qbUser = qbUser;
  }

  QBUser getUser() {
    return _qbUser;
  }
}
