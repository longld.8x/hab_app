import 'package:hab_app/src/models/healthIndex.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/repositories/callApi.dart' as callApi;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:intl/intl.dart' as intl;

/// danh sách chỉ số đã nhập
Future<PageResult<HealthIndexDto>> getMyIndexes(DateTime fromDate, DateTime toDate, int page, int itemPage) async {
  return await callApi.getApiList<HealthIndexDto>('/personal_health/health_indexes_list', {
    'AccountCode': userRepo.currentUser.value.accountCode,
    'FromDate': '${new intl.DateFormat('yyyy-MM-dd HH:mm:ss').format(fromDate)}',
    'ToDate': '${new intl.DateFormat('yyyy-MM-dd HH:mm:ss').format(toDate)}',
  });
}

Future<ResponseMessage> addMyIndexes(HealthIndexDto dto) async {
  var map = {
    "AccountCode": userRepo.currentUser.value.accountCode,
    "Indexes": [
      {"IndexType": "Height", "Text": "Height", "Value": dto.height},
      {"IndexType": "Weight", "Text": "Weight", "Value": dto.weight},
      {"IndexType": "Bust", "Text": "Bust", "Value": dto.bust},
      {"IndexType": "Waist", "Text": "Waist", "Value": dto.waist},
      {"IndexType": "Hip", "Text": "Hip", "Value": dto.hip},
      {"IndexType": "TextInfo", "Text": "TextInfo", "Value": ''},
      {"IndexType": "Gender", "Text": "Gender", "Value": userRepo.currentUser.value.gender},
      {"IndexType": "WorkoutIntensity", "Text": "WorkoutIntensity", "Value": 1}
    ],
    "CreatedDate": '${new intl.DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}',
    "Description": "addMyIndexes"
  };
  return callApi.postApi('/personal_health/health_indexes', map);
}

Future<ResponseMessage> editMyIndexes(HealthIndexDto dto) async {
  var map = {
    "Id": dto.id,
    "AccountCode": userRepo.currentUser.value.accountCode,
    "Indexes": [
      {"IndexType": "Height", "Text": "Height", "Value": dto.height},
      {"IndexType": "Weight", "Text": "Weight", "Value": dto.weight},
      {"IndexType": "Bust", "Text": "Bust", "Value": dto.bust},
      {"IndexType": "Waist", "Text": "Waist", "Value": dto.waist},
      {"IndexType": "Hip", "Text": "Hip", "Value": dto.hip},
      {"IndexType": "TextInfo", "Text": "TextInfo", "Value": ''},
      {"IndexType": "Gender", "Text": "Gender", "Value": userRepo.currentUser.value.gender},
      {"IndexType": "WorkoutIntensity", "Text": "WorkoutIntensity", "Value": 1}
    ],
    "CreatedDate": '${new intl.DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}',
    "Description": "editMyIndexes"
  };
  return callApi.patchApi('/personal_health/health_indexes', map);
}
