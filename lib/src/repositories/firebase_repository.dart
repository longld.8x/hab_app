import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hab_app/src/helpers/custom_trace.dart';
import 'package:hab_app/src/models/chat/roomDto.dart';
import 'package:hab_app/src/models/chat/userChatDto.dart';
import 'package:hab_app/src/models/notificationDto.dart';
import 'package:hab_app/src/repositories/chat_repository.dart' as chatRepo;
import 'package:hab_app/src/repositories/notification_repository.dart' as notiRepo;
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;

import './notification_repository.dart' as notiRepo;

final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

ValueNotifier<FirebaseEvent> firebaseEvent = new ValueNotifier(FirebaseEvent());

Future<void> subDevice() async {
  _firebaseMessaging.getToken().then((String token) async {
    print("subDevice token: $token");
    assert(token != null);
    await notiRepo.subscribe(token);
    await chatRepo.subscribe(token);
  });
}

Future<void> unSubDevice(int userId, String accountCode) async {
  _firebaseMessaging.getToken().then((String token) async {
    print("un sub Device token: $token");
    assert(token != null);
    await notiRepo.unSubscribe(accountCode);
    await chatRepo.unSubscribe(userId);
  });
}

Future onMessageFirebase(Map<String, dynamic> message) async {
  print("onMessageFirebase");
  handlerNotification('onMessage', message);
}

Future onLaunchFirebase(Map<String, dynamic> message) async {
  print("onLaunchFirebase");
  handlerNotification('onLaunch', message);
}

Future onResumeFirebase(Map<String, dynamic> message) async {
  print("onResumeFirebase");
  handlerNotification('onResume', message);
}

Future onBackgroundMessageFirebase(Map<String, dynamic> message) async {
  print("onBackgroundMessageFirebase");
  handlerNotification('onBackground', message);
}

void configureFirebase(FirebaseMessaging _firebaseMessaging) {
  try {
    _firebaseMessaging.configure(
      onMessage: onMessageFirebase,
      onLaunch: onLaunchFirebase,
      onResume: onResumeFirebase,
      onBackgroundMessage: onBackgroundMessageFirebase,
    );
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: e));
    print(CustomTrace(StackTrace.current, message: 'Error Config Firebase'));
  }
}

showNotification(Map<String, dynamic> message) async {
  var android = new AndroidNotificationDetails(
    'default',
    'HAB Notification Channel',
    'HAB Notification Channel',
    priority: Priority.high,
    importance: Importance.max,
    icon: '@mipmap/ic_notification',
  );

  var iOS = new IOSNotificationDetails();
  var mac = new MacOSNotificationDetails();
  var platform = new NotificationDetails(
    android: android,
    iOS: iOS,
    macOS: mac,
  );
  var _notiType = message['data']['type'];
  if (_notiType == 'chat_new_message') {
    try {
      var _message = json.decode(message['data']['message']);
      print("_message $_message");
      var _msg = message['notification']['body'];
      if (_message['message_type'] == 'IMAGE') {
        _msg = '[Hình ảnh]';
      }
      await flutterLocalNotificationsPlugin.show(
        0,
        message['notification']['title'],
        _msg,
        platform,
        payload: json.encode(message['data']),
      );
      return;
    } catch (e) {
      log("showNotification $e");
    }
  }
  // noti
  await flutterLocalNotificationsPlugin.show(
    0,
    message['notification']['title'],
    message['notification']['body'],
    platform,
    payload: json.encode(message['data']),
  );
}

Future onSelectNotification(String payload) async {
  print("onSelectNotification ${(payload)}");
  try {
    var dto = json.decode(payload);
    if (dto != null && dto['type'] != null && dto['room'] != null && dto['type'] == 'chat_new_message') {
      RoomDto _room = RoomDto.fromJSON(json.decode(dto['room']));
      nextRoomChat(_room.id);
      return;
    }
    if (dto != null && dto['type'] != null && (dto['type']).toString().startsWith("App.")) {
      var _model = NotificationDto(
        appNotificationName: dto['type'],
        data: dto['properties'],
      );
      notiRepo.notificationToPage(_model);
      return;
    }
  } catch (e) {
    print("onSelectNotification catch ${e}");
  }
  //Navigator.of(state?.context).push(MaterialPageRoute(builder: (_) => NotificationsWidget()));
}

Future onDidReceiveLocalNotification(int id, String title, String body, String payload) async {
  print("Selected notifi $payload");
}

void configChannelNotifi() async {
  try {
    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    const AndroidInitializationSettings android = AndroidInitializationSettings('@mipmap/ic_notification');
    final IOSInitializationSettings IOS = IOSInitializationSettings(onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final InitializationSettings settings = InitializationSettings(android: android, iOS: IOS);
    flutterLocalNotificationsPlugin.initialize(settings, onSelectNotification: onSelectNotification);
    _createNotificationChannel();
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: e));
    print(CustomTrace(StackTrace.current, message: 'Error channel Firebase'));
  }
}

Future<void> _createNotificationChannel() async {
  const AndroidNotificationChannel androidNotificationChannel = AndroidNotificationChannel(
    'com.hls.hab_app.noti',
    'HAB Notification Channel',
    'HAB Notification Channel',
  );
  await flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()?.createNotificationChannel(androidNotificationChannel);
}

Future<void> initFcm() async {
  _firebaseMessaging.requestNotificationPermissions(const IosNotificationSettings(sound: true, badge: true, alert: true));
  configureFirebase(_firebaseMessaging);
  await configChannelNotifi();
}

Future<void> getTotalNotifications() async {
  // var total = await notiRepo.getTotal();
  settingRepo.setting.value.totalNotifications.value = await notiRepo.getTotalNotifications();
  settingRepo.setting.value.totalNotifications.notifyListeners();
  // SharedPreferences prefs = await SharedPreferences.getInstance();
  // await prefs.setString('__total_noti', total.toString());
}

nextRoomChat(int roomId) {
  Navigator.of(settingRepo.navigatorKey.currentContext).pushNamed('/Chat/Room', arguments: roomId);
}

handlerNotification(String action, Map<String, dynamic> message) async {
  var _msgType = message['data']['type'];
  // var routeName = ModalRoute.of(settingRepo.navigatorKey.currentContext).settings.name;
  // print("$routeName  ");
  log("handlerNotification $action  $_msgType  $message");

  if (action == "onLaunch") return;
  if (action == "onBackground") {
    showNotification(message);
    return;
  }
  if (action == "onResume") {
    if (_msgType == 'chat_new_message') {
      RoomDto _room = RoomDto.fromJSON(json.decode(message['data']['room']));
      nextRoomChat(_room.id);
    } else {
      if (_msgType.toString().startsWith('App.')) {
        var _model = NotificationDto(
          appNotificationName: _msgType,
          data: message['data']['properties'],
        );
        notiRepo.notificationToPage(_model);
      }
      await getTotalNotifications();
    }
    return;
  }
  if (action == "onMessage") {
    log("onMessage $action  $_msgType  $message");
    if (_msgType == 'chat_new_message') {
      // nếu là thằng gửi
      var _user = UserChatDto.fromJSON(json.decode(message['data']['user']));
      if (_user.ref_user_id == userRepo.currentUser.value.id) return;
      // nếu ko phải thằng gửi => có bắt event này ko
      firebaseEvent.value = FirebaseEvent(event: _msgType, data: message);
      log("firebaseEvent.hasListeners ${firebaseEvent.hasListeners}");
      if (!firebaseEvent.hasListeners) showNotification(message);
      return;
    }
    await getTotalNotifications();
    showNotification(message);
    return;
  }
}

class FirebaseEvent {
  String event;
  dynamic data;
  FirebaseEvent({this.event, this.data});
}
