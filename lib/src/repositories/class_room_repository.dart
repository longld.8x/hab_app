import 'dart:convert';

import 'package:hab_app/src/models/accountClassDto.dart';
import 'package:hab_app/src/models/documentDto.dart';
import 'package:hab_app/src/models/exercisesResultDto.dart';
import 'package:hab_app/src/models/myClassDetailDto.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/sessionResultDto.dart';
import 'package:hab_app/src/repositories/callApi.dart' as callApi;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;

/// danh sách khóa học
Future<PageResult<MyClassDetailDto>> getMyClass(int page, int itemInPage, {int userId}) async {
  return await callApi.getApiList<MyClassDetailDto>('/ClassManagement/GetMyClassRooms', {
    'SkipCount': itemInPage * page,
    'userId': userId != null ? userId : userRepo.currentUser.value.id,
    'MaxResultCount': itemInPage,
  });
}

/// chi tiết khóa học
Future<ResponseMessage> getMyClassDetail(int classId) async {
  final response = await callApi.get('/ClassManagement/GetMyClassDetail', {
    'ClassId': classId,
    'IsGetSession': false,
    'IsGetModule': false,
    'IsGetStudent': false,
  });
  if (response.statusCode == 200) {
    return ResponseMessage.success(result: MyClassDetailDto.fromJSON(json.decode(response.body)));
  } else {
    return ResponseMessage.error();
  }
}

/// danh sách hl - my class
Future<PageResult<AccountClassDto>> getMyClassHLV(int classId, int page, int itemInPage) async {
  return await callApi.getApiList<AccountClassDto>('/ClassManagement/GetCoachesClass', {
    'ClassId': classId,
    'SkipCount': itemInPage * page,
    'MaxResultCount': itemInPage,
  });
}

/// danh sách bai học - my class
Future<PageResult<SessionResultDto>> getMyClassSession(int classId, int page, int itemInPage) async {
  return await callApi.getApiList<SessionResultDto>('/ClassManagement/GetSessionsClass', {
    'ClassId': classId,
    'SkipCount': itemInPage * page,
    'MaxResultCount': itemInPage,
  });
}

/// danh sách học viên - my class
Future<PageResult<AccountClassDto>> getMyClassMembers(int classId, int page, int itemInPage) async {
  return await callApi.getApiList<AccountClassDto>('/ClassManagement/GetStudentsClass', {
    'ClassId': classId,
    'SkipCount': itemInPage * page,
    'MaxResultCount': itemInPage,
  });
}

/// chi tiết buổi học
Future<SessionResultDto> getMyClassSessionDetai(int classSessionId) async {
  var response = await callApi.get('/ClassManagement/GetClassSessionDetail', {'ClassSessionId': classSessionId});
  if (response.statusCode == 200) return SessionResultDto.fromJSON(json.decode(response.body));
  return null;
}

/// danh sách bài tập -  class - session
Future<PageResult<ExercisesResultDto>> getMyClassExcercies(int classSessionId, int page, int itemInPage) async {
  return await callApi.getApiList<ExercisesResultDto>('/ClassManagement/GetExerciesClass', {
    //ClassSessionId
    // 'ClassId': classId,
    // 'SessionId': sessionId,
    'classSessionId': classSessionId,
    'SkipCount': itemInPage * page,
    'MaxResultCount': itemInPage,
  });
}

/// chi tiết bài tập -  class - session
Future<ResponseMessage> getMyClassExcercieDetail(int exerciseClassModuleId, {int userId: null}) async {
  var response = await callApi.get('/ClassManagement/GetDetailExerciesClass', {
    'exerciseClassModuleId': exerciseClassModuleId,
    'userId': userId != null ? userId : userRepo.currentUser.value.id,
  });
  if (response != null && response.statusCode == 200) {
    ExercisesResultDto _data = ExercisesResultDto.fromJSON(json.decode(response.body));
    return ResponseMessage.success(result: _data);
  } else {
    return ResponseMessage.error();
  }
}

/// danh sách tài liệu -  class - session
Future<PageResult<DocumentDto>> getMyClassDocument(int classSessionId, int page, int itemInPage) async {
  return await callApi.getApiList<DocumentDto>('/ClassManagement/GetDocumentClass', {
    'classSessionId': classSessionId,
    'SkipCount': itemInPage * page,
    'MaxResultCount': itemInPage,
  });
}

Future<ResponseMessage> getMyClassNotiTotal(int classId) async {
  return ResponseMessage.success(result: 11);
}

Future<ResponseMessage> getZoomData(int classId, int sessionId) async {
  final response = await callApi.post('/live_meetings/room?Type=zoom', {});
  if (response.statusCode == 200) {
    var _body = json.decode(response.body);
    return ResponseMessage.success(result: {'id': _body['id'], 'password': _body['password']});
  }
  return ResponseMessage.error();
}

/// danh sách học viên - my class
Future<List<AccountLeaderClassDto>> getUsersInLeaderClass(int classId, List<int> userIds) async {
  var _uIds = userIds.toSet().toList();
  var response = await callApi.get('/ClassManagement/GetUsersInLeaderClass', {
    'classId': classId,
    'userIds': _uIds,
  });
  if (response.statusCode != 200) return [];
  var rs = (json.decode(response.body) as List<dynamic>).map<AccountLeaderClassDto>((e) {
    return AccountLeaderClassDto.fromJSON(e);
  }).toList();

  return rs;
}

/// danh sách học viên - my class
Future<List<int>> getSubUserOnLeaderClass(int classId) async {
  var response = await callApi.get('/ClassManagement/GetMyLearnersClass', {
    'classId': classId,
  });
  if (response.statusCode != 200) return [];
  var rs = (json.decode(response.body) as List<dynamic>)
      .map<int>((e) {
        return int.parse(e['id'] != null ? e['id'].toString() : '0');
      })
      .toSet()
      .toList();
  print("getSubUserOnLeaderClass ${rs}");
  return rs;
}
