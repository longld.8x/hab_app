import 'dart:convert';

import 'package:hab_app/src/models/activityDto.dart';
import 'package:hab_app/src/models/foodDto.dart';
import 'package:hab_app/src/models/mealTypeDto.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/unitDto.dart';
import 'package:hab_app/src/models/workoutTypeDto.dart';

import '../repositories/callApi.dart' as callApi;
import '../repositories/user_repository.dart' as userRepo;

Future<ResponseMessage> getMealType() async {
  var response = await callApi.get('/FoodsManagement/GetFoodMeals', {});
  if (response.statusCode == 200) {
    try {
      List<MealTypeDto> _data = json.decode(response.body).map<MealTypeDto>((val) {
        if (val != null) return MealTypeDto.fromJSON(val);
      }).toList();
      return ResponseMessage.success(result: _data);
    } catch (e) {
      return ResponseMessage.error(message: e.toString());
    }
  } else {
    return ResponseMessage.error();
  }
}

Future<ResponseMessage> getCaloInfo() async {
  return callApi.getApi('/reports/checkTargetUser', {
    'UserId': userRepo.currentUser.value.id,
  });
}

Future<ResponseMessage> getUnits() async {
  var response = await callApi.get('/FoodsManagement/GetFoodUnits', {});
  if (response.statusCode == 200) {
    try {
      List<UnitDto> _data = json.decode(response.body).map<UnitDto>((val) {
        if (val != null) return UnitDto.fromJSON(val);
      }).toList();
      return ResponseMessage.success(result: _data);
    } catch (e) {
      return ResponseMessage.error(message: e.toString());
    }
  } else {
    return ResponseMessage.error();
  }
}

Future<ResponseMessage> addFood(FoodDto model) async {
  return callApi.postApi('/FoodsManagement/CreateFood', {
    "foodCode": model.foodName,
    "foodName": model.foodName,
    "quantity": model.quantity,
    "calories": model.calories,
    "protein": model.protein,
    "carb": model.carb,
    "fat": model.fat,
    "foodGroupId": 2,
    "description": "App Add",
    "status": 1,
    "foodUnitId": model.foodUnitId,
    "userId": userRepo.currentUser.value.id,
  });
}

Future<PageResult<FoodDto>> getFoods(String filter, int page, int itemInPage) async {
  return callApi.getApiList<FoodDto>('/FoodsManagement/GetFoods', {
    'SkipCount': itemInPage * page,
    'MaxResultCount': itemInPage,
    'Filter': filter != null ? filter : "",
  });
}

Future<ResponseMessage> getWorkoutType() async {
  var response = await callApi.get('/ActivitiesManagement/GetWorkoutCategories', {});
  if (response.statusCode == 200) {
    try {
      List<WorkoutTypeDto> _data = json.decode(response.body).map<WorkoutTypeDto>((val) {
        if (val != null) return WorkoutTypeDto.fromJSON(val);
      }).toList();
      return ResponseMessage.success(result: _data);
    } catch (e) {
      return ResponseMessage.error(message: e.toString());
    }
  } else {
    return ResponseMessage.error();
  }
}

String newObjectIdentify(int classId, int classSessionId, int classModuleExerciseId) {
  var _key = "${classId}_${classSessionId}_${classModuleExerciseId}";
  print('newObjectIdentify $_key');
  return (_key);
}

/// ================ getComments
Future<PageResult<ActivityDto>> getActivityByLeader(String objectIdentify, String type, String userIds, String filter, int page, int itemInPage) async {
  return callApi.getApiList<ActivityDto>('/account_activity/activities/multi_user', {
    'skipCount': itemInPage * page,
    'maxResultCount': itemInPage,
    'userIds': userIds,
    'filter': filter != null ? filter : "",
    'objectType': type,
    'objectIdentify': objectIdentify,
  });
}

Future<PageResult<ActivityDto>> getActivity(String objectIdentify, String type, String filter, int page, int itemInPage) async {
  return callApi.getApiList<ActivityDto>('/account_activity/activities', {
    'skipCount': itemInPage * page,
    'maxResultCount': itemInPage,
    'filter': filter != null ? filter : "",
    'objectType': type,
    'objectIdentify': objectIdentify,
  });
}

Future<List<PageResult<ActivityDto>>> getActivityMulti(String objectIdentify, String type, String objectIdentifies, String filter, int page, int itemInPage) async {
  var response = await callApi.getApi('/account_activity/activities/multiple', {
    'skipCount': itemInPage * page,
    'maxResultCount': itemInPage,
    'filter': filter != null ? filter : "",
    'objectIdentifies': objectIdentifies,
    'objectType': type,
    'objectIdentify': objectIdentify,
  });
  if (!response.success) return [];
  var rs = (response.results as List<dynamic>).map<PageResult<ActivityDto>>((e) {
    var _totalCount = e['totalCount'] != null ? e['totalCount'] : 0;
    var _items = (e['items']).map<ActivityDto>((json) {
      return ActivityDto.fromJSON(json);
    }).toList();
    return new PageResult<ActivityDto>(items: _items, totalCount: _totalCount);
  }).toList();

  return rs.where((e) => e.items != null && e.items.length > 0).toList();
}

Future<ResponseMessage> addActivity(String objectIdentify, String type, String content, List<String> mediaUrls, List<int> userRole, int point, String details) async {
  var map = {
    'content': content,
    'userId': userRepo.currentUser.value.id,
    'objectType': type,
    'objectIdentify': objectIdentify,
  };
  if (point > 0) map['score'] = point;
  if (details != null && details.isNotEmpty) map['activityDetail'] = details;
  if (mediaUrls != null && mediaUrls.isNotEmpty) map['mediaUrls'] = mediaUrls.join('|');
  if (userRole != null && userRole.isNotEmpty) map['userRole'] = userRole.join(',');

  return callApi.postApi('/account_activity', map);
}

Future<ResponseMessage> getActivityById(String id) async {
  var map = {
    'Id': id,
  };
  var response = await callApi.get('/account_activity', map);
  if (response.statusCode == 200) {
    try {
      ActivityDto _data = ActivityDto.fromJSON(json.decode(response.body));
      return ResponseMessage.success(result: _data);
    } catch (e) {
      return ResponseMessage.error(message: e.toString());
    }
  } else {
    return ResponseMessage.error();
  }
}

Future<ResponseMessage> addExercise(String objectIdentify, String content, List<String> mediaUrls, List<int> userRole, String details) async {
  return addActivity(objectIdentify, 'Exercise', content, mediaUrls, userRole, 0, details);
}

Future<ResponseMessage> addComment(String objectIdentify, String content, List<String> mediaUrls, List<int> userRole, int point) async {
  return addActivity(objectIdentify, 'Comment', content, mediaUrls, userRole, point, '');
}

Future<ResponseMessage> addMeal(String objectIdentify, String type, List<String> mediaUrls, String details) async {
  return addActivity(objectIdentify, 'Meals', '', mediaUrls, [], 0, details);
}

Future<ResponseMessage> addWater(String objectIdentify, double water) async {
  return addActivity(objectIdentify, 'Water', '', null, [], 0, water.toString());
}

Future<ResponseMessage> addWorkout(String objectIdentify, List<String> mediaUrls, String details) async {
  return addActivity(objectIdentify, 'Workout', '', mediaUrls, [], 0, details);
}

Future<ResponseMessage> checkInOffline(String objectIdentify) async {
  return addActivity(objectIdentify, 'CheckIn', 'JOIN CLASS OFFLINE', null, [], 0, '');
}
