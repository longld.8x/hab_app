import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:hab_app/src/helpers/custom_trace.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/exercisesResultDto.dart';
import 'package:hab_app/src/models/myClassDetailDto.dart';
import 'package:hab_app/src/models/notificationDto.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/route_argument.dart';
import 'package:hab_app/src/pages/notification/NotificationDetailWidget.dart';
import 'package:hab_app/src/repositories/callApi.dart' as callApi;
import 'package:hab_app/src/repositories/class_room_repository.dart' as classRepo;
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<ResponseMessage> subscribe(String token) async {
  try {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    final PackageInfo appInfo = await PackageInfo.fromPlatform();
    var deviceVersion = "1.1";
    var os = "";
    var deviceName = "";
    if (Platform.isAndroid) {
      os = "Android";
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      deviceName = androidInfo.device;
      deviceVersion = androidInfo.version.baseOS;
    } else {
      os = "IOS";
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      deviceName = iosInfo.utsname.machine;
      deviceVersion = iosInfo.utsname.version;
    }

    final String url = '/common/notification/subscribe';
    Map data = {
      "appVersion": appInfo.version,
      "deviceType": os,
      "token": token,
      "deviceName": deviceName,
      "deviceVersion": deviceVersion,
      "accountCode": userRepo.currentUser.value.accountCode,
    };
    final response = await callApi.post(url, data);
    var rs = ResponseMessage.fromJSON(json.decode(response.body));
    if (rs.success) {
      await setFcmToken(token);
    }
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: e));
    return null;
  }
}

Future<ResponseMessage> unSubscribe(String accountCode) async {
  var tokenFcm = await getFcmToken();
  if (tokenFcm != null) {
    final String url = '/common/notification/un_subscribe';
    Map data = {"token": tokenFcm, "accountCode": accountCode};
    final response = await callApi.post(url, data, needToken: false);
    return new ResponseMessage.fromJSON(json.decode(response.body));
  }
  return new ResponseMessage.error();
}

Future<void> setFcmToken(token) async {
  try {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('current_fcm_token', token);
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: e));
    throw new Exception(e);
  }
}

Future<String> getFcmToken() async {
  try {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('current_fcm_token')) {
      return await prefs.get('current_fcm_token');
    }
    return null;
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: e));
    throw new Exception(e);
  }
}

Future<int> getTotalNotifications() async {
  try {
    var _user = userRepo.currentUser.value;
    if (_user.accountCode == null || _user.accountCode.isEmpty) return 0;
    final String url = '/common/notification/getall';
    final response = await callApi.get(url, {
      'AccountCode': _user.accountCode,
      'State': 0,
      'SkipCount': 0,
      'MaxResultCount': 1,
    });
    if (response.statusCode == 200) {
      int total = (json.decode(response.body))['totalCount'] != null ? json.decode(response.body)['totalCount'] : 0;
      return total;
    }
    return 0;
  } catch (e) {
    return 0;
  }
}

/// danh sách noti
Future<PageResult<NotificationDto>> getNotifications(int page, int itemInPage) async {
  return await callApi.getApiList<NotificationDto>('/common/notification/getall', {
    'SkipCount': itemInPage * page,
    'AccountCode': userRepo.currentUser.value.accountCode,
    'MaxResultCount': itemInPage,
  });
}

/// danh dấu đã đọc
Future<ResponseMessage> read(String id) async {
  return await callApi.postApi('/common/notification/set_as_read', {
    'id': id,
    'accountCode': userRepo.currentUser.value.accountCode,
  });
}

/// danh dấu đã đọc - tất cả
Future<ResponseMessage> readAll() async {
  return await callApi.postApi('/common/notification/set_all_as_read', {
    'accountCode': userRepo.currentUser.value.accountCode,
  });
}

Future<ResponseMessage> getNotification(String id) async {
  return await callApi.getApi('/common/notification/getall', {
    'accountCode': userRepo.currentUser.value.accountCode,
    'id': id,
  });
}

/// danh dấu đã đọc - tất cả
Future<ResponseMessage> deletetNotification(String id) async {
  return await callApi.deleteApi('/common/notification/delete', {
    'accountCode': userRepo.currentUser.value.accountCode,
    'id': id,
  });
}

/// danh dấu đã đọc - tất cả
// buổi học
pageSessionDetail(int classId, int sessionId, int classSessionId) async {
  await classRepo.getMyClassDetail(classId).then((result) {
    if (result.success) {
      MyClassDetailDto _classDetail = (result.results);

      Navigator.of(settingRepo.navigatorKey.currentContext).pushNamed('/MyClass/Session/Detail',
          arguments: RouteClassArgument(
            classId: classId,
            sessionId: sessionId,
            classSessionId: classSessionId,
            role: _classDetail.classRoles,
          ));
    } else {
      Helper.errorMessenge("Có lỗi trong quá trình xử lý!");
    }
  });
}

// lớp học
pageClassDetail(int classId) async {
  Navigator.of(settingRepo.navigatorKey.currentContext).pushNamed('/MyClass/Detail', arguments: RouteClassArgument(classId: classId));
}

// bài tập
pageClassExerciseDetail(int classId, int classSessionId, int sessionId, int exerciseClassModuleId) async {
  try {
    var _classRespon = await classRepo.getMyClassDetail(classId);
    var _class = _classRespon.success ? _classRespon.results : null;
    var _exercisesRespon = await classRepo.getMyClassExcercieDetail(exerciseClassModuleId);
    var _exercises = _exercisesRespon.results as ExercisesResultDto;
    if (_class != null)
      Navigator.of(settingRepo.navigatorKey.currentContext).pushNamed(
        '/MyClass/Exercise/Detail',
        arguments: RouteClassArgument(
          classId: classId,
          sessionId: sessionId,
          exerciseClassModuleId: exerciseClassModuleId,
          classSessionId: classSessionId,
          role: _class.classRoles,
          param: _exercises,
        ),
      );
    else
      Helper.errorMessenge("Có lỗi trong quá trình xử lý!");
  } catch (e) {
    print("pageClassExerciseDetail pageClassExerciseDetail $e");
    Helper.errorMessenge("Có lỗi trong quá trình xử lý!");
  }
}

pageClassExerciseDetailItem(int classId, int classSessionId, int sessionId, int exerciseClassModuleId) async {
  try {
    var _classRespon = await classRepo.getMyClassDetail(classId);
    var _class = _classRespon.success ? _classRespon.results : null;
    var _exercisesRespon = await classRepo.getMyClassExcercieDetail(exerciseClassModuleId);
    var _exercises = _exercisesRespon.results as ExercisesResultDto;
    if (_class != null) {
      Navigator.of(settingRepo.navigatorKey.currentContext).pushNamed(
        '/MyClass/Exercise/Detail',
        arguments: RouteClassArgument(
          classId: classId,
          sessionId: sessionId,
          exerciseClassModuleId: exerciseClassModuleId,
          classSessionId: classSessionId,
          role: _class.classRoles,
          param: _exercises,
        ),
      );
    } else
      Helper.errorMessenge("Có lỗi trong quá trình xử lý!");
  } catch (e) {
    print("pageClassExerciseDetail pageClassExerciseDetail $e");
    Helper.errorMessenge("Có lỗi trong quá trình xử lý!");
  }
}

notificationToPage(NotificationDto item) async {
  if (userRepo.currentUser.value == null || userRepo.currentUser.value.id == null) {
    return;
  }
  print("notificationToPage $item");
  //  buổi học
  if (([
        'App.EndSesionToUser',
        'App.StartSessionRemindToUser',
        'App.StartSessionRemindToCoach',
        'App.StartSessionRemindToChecked',
        'App.StartSessionRemindToPrecenter',
        'App.StartSessionRemindToOwner',
        'App.EndSesionToChecker',
        'App.EndSesionToCoach',
        'App.EndSesionToPresenter',
        'App.EndSesionToOwner',
      ]).indexOf(item.appNotificationName) >
      -1) {
    //{\"ClassId\":2207,\"SessionId\":1113,\"ClassSessionId\":2406,\"OwnerId\":40,\"ClassName\":\"1405.2021.0917\",\"SessionName\":\"Buổi học số 3. Thanh Lam\",\"CourseName\":\"Tên khóa học. Học cùng với các Ngôi sao\",\"EndDate\":null}
    var _data = json.decode(item.data);
    await pageSessionDetail(_data['ClassId'], _data['SessionId'], _data['ClassSessionId']);
    return;
  }
  //  lớp học
  if (([
        'App.UserClassToUser',
        'App.UserClassToPresenter',
        'App.UserClassToOwner',
        'App.EndClassToUser',
        'App.EndClassToPresenter',
        'App.EndClassToOwner',
      ]).indexOf(item.appNotificationName) >
      -1) {
    //{\"ClassId\":2205,\"OwnerId\":117,\"ClassName\":\"Diễn viên.1305.1639\",\"CourseName\":\"Tên khóa học. Học cùng với các Ngôi sao\",\"EndDate\":\"2021-05-13T17:24:29.1106681+07:00\",\"StartDate\":\"2021-05-13T00:00:00\"}
    var _data = json.decode(item.data);
    await pageClassDetail(_data['ClassId']);
    return;
  }
  // bài tập học
  if (([
        'App.ExerciseRemindToUser',
      ]).indexOf(item.appNotificationName) >
      -1) {
    //{"ExerciseClassModuleId":4584,"ExerciseId":158,"ClassSessionId":4450,"ClassId":4234,"ClassName":"Sa Tăng.1905.1339","CheckerId":137,"UserId":135}
    var _data = json.decode(item.data);
    await pageClassExerciseDetail(_data['ClassId'], _data['ClassSessionId'], _data['SessionId'], _data['ExerciseClassModuleId']);
    return;
  } // bài tập học - chi tiết
  if (([
        'App.ExerciseToPresenter',
        'App.ExerciseToChecked',
        'App.ExerciseCheckedToUser',
        'App.ExerciseCheckedToPresenter',
      ]).indexOf(item.appNotificationName) >
      -1) {
    //{"ExerciseClassModuleId":4584,"ExerciseId":158,"ClassSessionId":4450,"ClassId":4234,"ClassName":"Sa Tăng.1905.1339","CheckerId":137,"UserId":135}
    var _data = json.decode(item.data);
    await pageClassExerciseDetailItem(_data['ClassId'], _data['ClassSessionId'], _data['SessionId'], _data['ExerciseClassModuleId']);
    return;
  }
  // người giới thiệu
  if (item.appNotificationName == 'App.ConfirmPresenterToUser') {
    Navigator.of(settingRepo.navigatorKey.currentContext).pushNamed('/ParentUser');
    return;
  }
  if (item.appNotificationName == 'App.ConfirmPresenter') {
    Navigator.of(settingRepo.navigatorKey.currentContext).pushNamed('/F1/User');
    return;
  }
  Navigator.of(settingRepo.navigatorKey.currentContext).push(new MaterialPageRoute(
    builder: (BuildContext context) => NotificationDetailWidget(model: item),
  ));
}
