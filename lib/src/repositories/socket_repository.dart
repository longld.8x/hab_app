import 'package:adhara_socket_io/manager.dart';
import 'package:adhara_socket_io/options.dart';
import 'package:adhara_socket_io/socket.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hab_app/src/models/chat/messageDto.dart';
import 'package:hab_app/src/models/chat/userChatDto.dart';
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;

SocketIOManager manager = SocketIOManager();
SocketIO socket;

Future<SocketIO> _initSocket() async {
  return await manager.createInstance(SocketOptions(
    GlobalConfiguration().getString('api_socket'),
    namespace: '/',
    query: {'user_id': userRepo.currentUser.value.id.toString(), 'timestamp': DateTime.now().toString()},
    //Enable or disable platform channel logging
    enableLogging: true,
    transports: [
      Transports.webSocket,
      //Transports.polling,
    ], //Enable required transport
  ));
}

Future<void> connect() async {
  print("socket connect $socket");
  if (socket == null) {
    socket = await _initSocket();
  }
  var _connected = await socket.isConnected();
  if (!_connected) await socket.connect();
}

Future<void> disconnect() async {
  print("socket disconnect");
  await manager.clearInstance(socket);
  socket = null;
}

// Future<void> initSocket() async {
//   // socket.onConnect.listen((e) {
//   //   progress.value = SocketProgress('event', '');
//   // });
//   // socket.onConnectError.listen((e) => print("socket.onConnectError.listen $e"));
//   // socket.onConnectTimeout.listen((e) => print("onConnsocket.onConnectTimeout.listenectTimeout $e"));
//   // socket.onError.listen((e) => print("socket.onError.listen $e"));
//   // socket.onDisconnect.listen((e) {
//   //   progress.value = SocketProgress('disconnect', '');
//   // });
//   // socket.on('new_room').listen((data) {
//   //   progress.value = SocketProgress('new_room', data[0]['room_id']);
//   // });
//   // socket.on('on_exit_room').listen((data) {
//   //   int _roomId = (data[0]['room_id']);
//   //   UserChatDto _user = UserChatDto.fromJSON(data[0]['user']);
//   //   progress.value = SocketProgress('on_exit_room', {'user': _user, 'roomId': _roomId});
//   //   //  progress.notifyListeners();
//   // });
//   // socket.on('on_join_room').listen((data) {
//   //   int _roomId = (data[0]['room_id']);
//   //   UserChatDto _user = UserChatDto.fromJSON(data[0]['user']);
//   //   progress.value = SocketProgress('on_join_room', {'user': _user, 'roomId': _roomId});
//   //   //progress.notifyListeners();
//   // });
//   // socket.on('on_message').listen((data) {
//   //   var _data = data[0];
//   //   //{user: socket.user, message: message, messageType: data.messageType, room_id: data.room_id }
//   //   MessageDto _message = MessageDto.fromJSON(_data['message']);
//   //   _message.user = UserChatDto.fromJSON(_data['user']);
//   //   progress.value = SocketProgress('on_message', _message);
//   //   // progress.notifyListeners();
//   // });
// }

onConnect(Function event) {
  return socket.onConnect.listen((e) {
    event({});
  });
}

onDisconnect(Function event) {
  return socket.onDisconnect.listen((e) {
    event({});
  });
}

onNewRoom(Function event) {
  return socket.on('new_room').listen((data) {
    //{room_id: 1}
    event(data[0]['room_id']);
  });
}

onExitRoom(Function event) {
  return socket.on('on_exit_room').listen((data) {
    //{room_id: 1}
    int _roomId = int.parse(data[0]['room_id'].toString());
    UserChatDto _user = UserChatDto.fromJSON(data[0]['user']);
    event(_user, _roomId);
  });
}

onJoinRoom(Function event) {
  return socket.on('on_join_room').listen((data) {
    print("socket.on on_join_room");
    //{room_id: 1}
    int _roomId = int.parse(data[0]['room_id'].toString());
    UserChatDto _user = UserChatDto.fromJSON(data[0]['user']);
    event(_user, _roomId);
  });
}

onMessage(Function event) {
  return socket.on('on_message').listen((data) {
    //{user: socket.user, message: message, messageType: data.messageType, room_id: data.room_id }
    MessageDto _message = MessageDto.fromJSON(data[0]['message']);
    _message.user = UserChatDto.fromJSON(data[0]['user']);
    event(_message);
  });
}

onTyping(Function event) {
  return socket.on('on_typing').listen((data) {
    //{room_id: 1}
    int _roomId = int.parse(data[0]['room_id'].toString());
    UserChatDto _user = UserChatDto.fromJSON(data[0]['user']);
    event(_user, _roomId);
  });
}

onEndTyping(Function event) {
  return socket.on('on_end_typing').listen((data) {
    //{room_id: 1}
    int _roomId = int.parse(data[0]['room_id'].toString());
    UserChatDto _user = UserChatDto.fromJSON(data[0]['user']);
    event(_user, _roomId);
  });
}

onSeen(Function event) {
  return socket.on('seen_update').listen((data) {
    //{room_id: 1}
    int _roomId = int.parse(data[0]['room_id'].toString());
    event(_roomId);
  });
}

typing(int roomId) async {
  socket.emit('typing', [
    {"room_id": "${roomId}"}
  ]);
}

endTyping(int roomId) async {
  socket.emit('end_typing', [
    {"room_id": "${roomId}"}
  ]);
}

createRoom(int userId) async {
  socket.emit('create_room', [
    {"roomName": "${userRepo.currentUser.value.id}_${userId}"}
  ]);
}

seenMessages(int roomId) async {
  socket.emit('seen', [
    {"room_id": "${roomId}"}
  ]);
}

sendMessage(MessageDto dto) async {
  print("socket $socket");
  socket.emit('send_message', [
    {
      "room_id": dto.room_id,
      "message": dto.message,
      "messageCode": dto.message_code,
      "messageType": dto.message_type,
    }
  ]);
}

joinRoom(int roomId) async {
  socket.emit('join_room', [
    {"room_id": roomId}
  ]);
}

exitRoom(int roomId) async {
  socket.emit('exit_room', [
    {"room_id": roomId}
  ]);
}

addUser(int roomId, List<int> userIds) async {
  socket.emit('add_user', [
    {'room_id': roomId, 'ids': userIds}
  ]);
}

/// group
joinGroup(String type, String groupId) async {
  socket.emit(type, [
    {"group_id": groupId}
  ]);
}

onServerEvent(Function event) {
  return socket.on('server_event').listen(event);
}

exitGroup(String groupId) async {
  socket.emit('exit_group', [
    {"group_id": groupId}
  ]);
}
