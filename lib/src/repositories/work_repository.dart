import 'dart:convert';

import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/workDto.dart';
import 'package:hab_app/src/repositories/callApi.dart' as callApi;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
//
// /// danh sách khóa học
// Future<PageResult<WorkDto>> getWorks(int page, int itemPage) async {
//   return await callApi.getApiList<WorkDto>('/plan/task_list', {
//     'AccountCode': 'HAB000000001',
//     //'AccountCode': userRepo.currentUser.value.accountCode,
//   });
// }

/// danh sách khóa học
Future<PageResult<WorkDto>> getWorks(DateTime fromDate, DateTime toDate, int page, int itemPage) async {
  final response = await callApi.get('/plan/task_list', {
    'AccountCode': userRepo.currentUser.value.accountCode,
    'FromDate': fromDate,
    'ToDate': toDate,
    'GetTop': itemPage == 3 ? itemPage : 999999999,
  });
  if (response != null && response.statusCode == 200) {
    List<WorkDto> _data = (response.body == null && response.body.toString().isNotEmpty)
        ? []
        : json.decode(response.body).map<WorkDto>((js) {
            return WorkDto.fromJSON(js);
          })?.toList();

    return PageResult(totalCount: itemPage, items: _data);
  }
  return PageResult(totalCount: 0, items: []);
}
