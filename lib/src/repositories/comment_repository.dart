import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/commentDto.dart';
import 'package:hab_app/src/models/response.dart';

import '../repositories/callApi.dart' as callApi;
import '../repositories/user_repository.dart' as userRepo;

/// ================ getComments
/// classId, sessionId, exercisesId, page, itemInPage
Future<PageResult<CommentDto>> getComments(String type, int classId, int sessionId, int exercisesId, String filter, int page, int itemInPage) async {
  return callApi.getApiList<CommentDto>('/common/comments', {
    'SkipCount': itemInPage * page,
    'MaxResultCount': itemInPage,
    'Filter': filter != null ? filter : "",
    'UserId': userRepo.currentUser.value.id,
    'ObjectType': type,
    'ObjectIdentify': Helper.generateMd5(classId.toString() + sessionId.toString() + exercisesId.toString() + "do_biet_la_cai_gi"),
  });
}

Future<PageResult<CommentDto>> getNoteComments(int userViewId) async {
  return callApi.getApiList<CommentDto>('/common/comments', {
    'SkipCount': 0,
    'MaxResultCount': 10,
    'Filter': "",
    'UserId': userRepo.currentUser.value.id,
    'ObjectType': 'Person',
    'ObjectIdentify': userViewId,
  });
}

Future<ResponseMessage> addNoteComment(String content, int userViewId) async {
  return callApi.postApi('/common/comment', {
    'Content': content,
    'UserId': userRepo.currentUser.value.id,
    'Status': 1,
    'ObjectType': 'Person',
    'ObjectIdentify': userViewId,
  });
}

Future<ResponseMessage> editNoteComment(String id, String content, int userViewId) async {
  return callApi.postApi('/common/comment', {
    'Id': id,
    'Content': content,
    'UserId': userRepo.currentUser.value.id,
    'Status': 1,
    'ObjectType': 'Person',
    'ObjectIdentify': userViewId,
  });
}
