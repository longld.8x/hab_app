import 'dart:convert';
import 'dart:io' show File;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hab_app/src/helpers/custom_trace.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../repositories/callApi.dart' as callApi;
import '../repositories/file_repository.dart' as fileRepo;
import '../repositories/firebase_repository.dart' as firebaseRepo;
import '../repositories/setting_repository.dart' as settingRepo;
import '../repositories/socket_repository.dart' as socketRepo;

ValueNotifier<User> currentUser = new ValueNotifier(User());
var _arrBiometricType = [BiometricType.fingerprint.toString().toLowerCase(), BiometricType.face.toString().toLowerCase(), BiometricType.iris.toString().toLowerCase()];

/// ============== đăng nhập
Future<ResponseMessage> login(String userName, String password) async {
  final String url = '/TokenAuth/Authenticate';
  Map data = {
    'userNameOrEmailAddress': userName,
    'password': password,
  };
  final response = await callApi.post(url, data, needToken: false);
  if (response != null && response.statusCode == 200) {
    var result = ResponseMessage.fromJSON(json.decode(response.body));
    if (!result.success) return result;
    var token = result.results['accessToken'];
    await userClientUpdate(token, userName: userName, password: password);

    await firebaseRepo.subDevice();
    await firebaseRepo.getTotalNotifications();
    await socketRepo.connect();
    return ResponseMessage.success();
  } else {
    print(CustomTrace(StackTrace.current, message: response?.body).toString());
    return ResponseMessage.error();
  }
}

Future<void> userClientUpdate(String token, {String userName, String password}) async {
  currentUser.value = await getProfile(token);
  AuthenUser _authUser;
  if (userName != null && password != null)
    _authUser = (new AuthenUser(currentUser.value.id, userName, currentUser.value.fullName, password, currentUser.value.phoneNumber, currentUser.value.accountCode, currentUser.value.profilePicture));
  else {
    _authUser = await settingRepo.getAuthenUser();
    _authUser.fullName = currentUser.value.fullName;
    _authUser.phoneNumber = currentUser.value.phoneNumber;
    _authUser.profilePicture = currentUser.value.profilePicture;
    //callApi.post('/chat-gateway/login', {'usernameOrEmail': userName, 'password': password});
  }
  await settingRepo.setAuthenUser(_authUser);
}

Future<User> getProfile(String token) async {
  final String url = '/Profile/GetProfile';
  final response = await callApi.get(url, {}, needToken: true, token: token);
  if (response.statusCode == 200) {
    var data = json.decode(response.body);
    data['api_token'] = token;
    setCurrentUser(data);
    var _user = User.fromJSON(data);
    _user.auth = true;
    return _user;
  } else {
    print(CustomTrace(StackTrace.current, message: response.body).toString());
    throw json.decode(response.body)['error']["message"];
  }
}

/// ============== logout
Future<void> logout() async {
  try {
    await socketRepo.disconnect();
    currentUser.value = new User();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('current_user');
    await settingRepo.setBiometricTimeout(DateTime.now());
  } catch (e) {
    print("logout $e");
  }
}

/// ============== logout -
Future<void> logoutClear() async {
  try {
    try {
      await socketRepo.disconnect();
    } catch (e1) {
      print("socketRepo.disconnect $e1");
    }
    AuthenUser _user = await settingRepo.getAuthenUser();
    await settingRepo.setBiometricTimeout(DateTime.now());
    settingRepo.setAuthenType(null);
    settingRepo.setAuthenUser(null);
    await firebaseRepo.unSubDevice(_user != null ? _user.id : 0, _user != null ? _user.accountCode : '');
    currentUser.value = new User();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('current_user');
  } catch (e) {
    print("logout $e");
  }
}

Future<void> setCurrentUser(jsonString) async {
  try {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (jsonString != null)
      await prefs.setString('current_user', json.encode(jsonString));
    else
      await prefs.remove('current_user');
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: jsonString).toString());
    throw new Exception(e);
  }
}

Future<User> getCurrentUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (currentUser.value.auth == null && prefs.containsKey('current_user')) {
    currentUser.value = User.fromJSON(json.decode(await prefs.get('current_user')));
    currentUser.value.auth = true;
  } else {
    currentUser.value.auth = false;
  }
  currentUser.notifyListeners();
  return currentUser.value;
}

/// ============== đăng ký
Future<ResponseMessage> checkPhoneNumber(String phoneNumber) async {
  return callApi.postApi('/Account/CheckValidAccount', {'phoneNumber': phoneNumber}, needToken: false);
}

/// ============== đăng ký
Future<ResponseMessage> registerAccount(String phoneNumber, String password) async {
  return callApi.postApi('/Account/RegisterAccount', {"phoneNumber": phoneNumber, "password": password}, needToken: false);
}

/// ============== quên mk
Future<ResponseMessage> forgetPassword(String phoneNumber, String password) async {
  return callApi.postApi(
      '/Account/ForgetPassword',
      {
        "phoneNumber": phoneNumber,
        "password": password,
      },
      needToken: false);
}

/// ============== đổi mk
Future<ResponseMessage> changePassword(String currentPassword, String newPassword) async {
  return callApi.putApi('/Profile/UpdatePassword', {
    "currentPassword": currentPassword,
    "newPassword": newPassword,
  });
}

/// ============== đổi ảnh đại diện
Future<ResponseMessage> uploadProfilePicture(File image) async {
  try {
    var check = await fileRepo.uploadFile(image);
    // final String url = '/File/UploadProfileAvatar';
    // final response = await callApi.postFile(url, image);
    // var check = ResponseMessage.fromJSON(json.decode(response.body));
    if (check.success) {
      return await callApi.putApi('/Profile/UpdateAvatar', {"imageUrl": check.results});
    }
    return ResponseMessage.error();
  } catch (e) {
    print("uploadProfilePicture error: $e");
    return ResponseMessage.error();
  }
}

/// ============== thay đổi thông tin cá nhân
Future<ResponseMessage> editProfile(User user) async {
  print("editProfile(User user) ${user.toMap()}");
  if (user.image != null) {
    await uploadProfilePicture(user.image);
  }
  var map = {
    "fullName": user.fullName,
    "height": user.infoHeight,
    "weight": user.infoWeight,
    "gender": user.gender,
    "address": "",
  };
  if (user.birthday != null) map["doB"] = user.birthday.toIso8601String();
  return callApi.putApi('/Profile/UpdateProfile', map);
}

/// ============== ảnh đại diện
Future<ResponseMessage> getProfilePictureAccount(int userId) async {
  final response = await callApi.get('/Profile/GetProfilePictureAccount', {"UserId": userId});
  if (response.statusCode == 200) {
    return ResponseMessage.success(result: response.body);
  } else {
    return ResponseMessage.error();
  }
}

/// ============== người giới thiệu
Future<ResponseMessage> getParent() async {
  try {
    return callApi.getApi('/NetworkManagement/GetPresenter', {});
  } catch (e) {
    return ResponseMessage.error();
  }
}

Future<ResponseMessage> setParent(String phoneNumber) async {
  return callApi.putApi('/NetworkManagement/UpdatePresenter', {"phoneNumber": phoneNumber});
}

Future<ResponseMessage> searchUser(String phoneNumber) async {
  final response = await callApi.get('/CommonLookup/GetUserInfo', {"filter": phoneNumber});
  if (response.statusCode == 200) {
    return ResponseMessage.success(result: User.fromJSON(json.decode(response.body)));
  } else {
    return ResponseMessage.error();
  }
}

/// ================ f1 cộng sự của tôi
Future<PageResult<User>> getNetworks(int userId, String keyword, int page, int itemInPage) async {
  return callApi.getApiList<User>('/NetworkManagement/GetNetworks', {
    'SkipCount': itemInPage * page,
    'MaxResultCount': itemInPage,
    'Filter': keyword != null ? keyword : "",
    'UserId': userId,
  });
}

/// ================ danh sách user by IDs
Future<List<User>> getUserByIds(List<int> userIds) async {
  var query = userIds.map((u) => "userIds=${u}").join("&");
  final client = new http.Client();
  final header = await callApi.headerRequest(needToken: true);
  final String _url = '${GlobalConfiguration().getString('api_url')}api/v1' + '/CommonLookup/GetUsersByIds?${query}';
  final response = await client.get(Uri.parse(_url), headers: header);
  callApi.logResponse('GET', _url, {'userIds': userIds.toString()}, response);
  if (response.statusCode == 200) {
    var rs = (json.decode(response.body) as List<dynamic>).map<User>((u) {
      return User.fromJSON(u);
    }).toList();
    return rs;
  } else {
    print(CustomTrace(StackTrace.current, message: response.body).toString());
    return <User>[];
  }
}
