import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hab_app/src/helpers/custom_trace.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/repositories/setting_repository.dart' as settingRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:multi_image_picker/multi_image_picker.dart' as multi;
import 'package:package_info/package_info.dart';

void logRequest(String method, String url, Map data, {Map headers}) {
  log("logRequest call API: ${method.toUpperCase()} url: $url; data: $data; headers: ${headers != null ? headers.toString() : ""};");
}

void logException(String method, String url, Map data, dynamic e) {
  log("logException call API: ${method.toUpperCase()} url: $url; data: $data; dynamic: ${e != null ? e.toString() : ""};");
}

void logResponse(String method, String url, Map data, http.Response rs) {
  var msg = "";
  if (rs != null) {
    msg = ("logResponse api  ${method.toUpperCase()}; url: ${url}; data: ${data}; response: ${rs.statusCode}|${rs.body}");
  } else {
    msg = ("logResponse api  ${method.toUpperCase()}; url: ${url}; data: ${data}; response: NULL");
  }
  log(msg);
}

Future<Map<String, String>> appHeaderMap(Map<String, String> data) async {
  final _user = userRepo.currentUser.value;
  final _settings = settingRepo.setting.value;
  data['app_version'] = _settings.appVersion;
  data['app_code'] = _settings.buildNumber;
  data['app_user_id'] = (_user != null ? _user.id : 0).toString();
  data['app_user_accountCode'] = _user != null ? _user.accountCode : "";
  data['app_request_date'] = new DateFormat('HH:mm:ss - dd/MM/yyyy').format(DateTime.now());
  data['app_channel'] = (2).toString();
  data['app_token'] = _user != null ? _user.apiToken : "";
  return data;
}

Future<Map> appDataMap(Map data) async {
  final PackageInfo info = await PackageInfo.fromPlatform();
  final _user = userRepo.currentUser.value;
  data['app_version'] = info.version;
  data['app_code'] = info.buildNumber;
  data['channel'] = '2';
  return data;
}

Future<Map<String, String>> headerRequest({bool needToken = true, String token = null}) async {
  Map headers = await appHeaderMap({});
  headers[HttpHeaders.contentTypeHeader] = 'application/json';
  if (needToken) {
    if (token != null) {
      headers[HttpHeaders.authorizationHeader] = 'Bearer ' + token;
    } else {
      final _user = userRepo.currentUser.value;
      if (_user.apiToken == null) {
        headers[HttpHeaders.authorizationHeader] = 'Bearer null';
      } else {
        headers[HttpHeaders.authorizationHeader] = 'Bearer ' + _user.apiToken;
      }
    }
  }
  return headers;
}

String appUrl(String url) {
  if (url.startsWith('http')) return url;
  return '${GlobalConfiguration().getString('api_url')}api/v1' + url;
}

Future<http.Response> responseAuth(String method, String url, Map data, http.Response response) async {
  logResponse(method, url, data, response);
  if (response != null && response.statusCode == 401) {
    var user = userRepo.getCurrentUser();
    if (user != null) {
      await userRepo.logout();
      settingRepo.navigatorKey.currentState.pushReplacementNamed('/Login');
    }
    throw new Exception("User null");
  }
  return response;
}

Future<http.Response> send(String url, String method, Map data, {bool needToken = true, String token = null}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  print("connectivityResult $connectivityResult");

  final _data = await appDataMap(data);
  String _url = appUrl(url);
  var isQuery = (method == "GET" || method == "POST_QUERY" || method == "PUT_QUERY");
  String _method = method;
  if (method == "POST_QUERY") _method = "POST";
  if (method == "PUT_QUERY") _method = "PUT";
  if (isQuery) {
    var query = mapToQuery(_data);
    _url = _url + '?${query}';
  }
  var request = http.Request(_method, Uri.parse(_url));
  final headers = await headerRequest(needToken: needToken, token: token);
  request.headers.addAll(headers);
  if (!isQuery) request.body = json.encode(_data);
  logRequest(_method, _url, _data);
  try {
    http.StreamedResponse streamedResponse = await request.send();
    var response = await http.Response.fromStream(streamedResponse);
    if (response == null) logResponse(method, _url, _data, null);

    return await responseAuth(method, _url, _data, response);
  } on SocketException catch (e) {
    logException(method, _url, _data, e);
  } catch (e) {
    logResponse(method, _url, _data, e);
    throw new Exception(e);
  }
}

Future<http.Response> sendFiles(String url, List<http.MultipartFile> files) async {
  final String _url = appUrl(url);
  var request = http.MultipartRequest('POST', Uri.parse(_url));
  request.files.addAll(files);
  final headers = await headerRequest(needToken: true);
  request.headers.addAll(headers);
  logRequest('POST_FILE', _url, {'length': request.files.length});
  try {
    http.StreamedResponse streamedResponse = await request.send();
    http.Response response = await http.Response.fromStream(streamedResponse);
    logResponse('POST', _url, {}, response);
    return await responseAuth('POST', _url, {}, response);
    // return response;
  } catch (e) {
    logResponse('POST', _url, {'length': request.files.length}, e);
    throw new Exception(e);
  }
}

Future<http.Response> post(String url, Map data, {bool needToken = true}) async {
  return send(url, 'POST', data, needToken: needToken);
}

Future<http.Response> patch(String url, Map data, {bool needToken = true}) async {
  return send(url, 'PATCH', data, needToken: needToken);
}

Future<http.Response> postUrl(String url, Map data, {bool needToken = true}) async {
  return send(url, 'POST_QUERY', data, needToken: needToken);
}

Future<http.Response> put(String url, Map data) async {
  return send(url, 'PUT', data, needToken: true);
}

Future<http.Response> putUrl(String url, Map data, {bool needToken = true}) async {
  return send(url, 'PUT_QUERY', data, needToken: needToken);
}

Future<http.Response> get(String url, Map data, {bool needToken = true, String token}) async {
  return send(url, 'GET', data, needToken: needToken, token: token);
}

Future<http.Response> delete(String url, Map data, {bool needToken = true}) async {
  return send(url, 'DELETE', data, needToken: needToken);
}

/// ===================================
Future<http.Response> postFile(String url, File file) async {
  List<http.MultipartFile> _files = [];
  _files.add(await http.MultipartFile.fromPath('file', file.path));
  return sendFiles(url, _files);
}

Future<http.Response> postFiles(String url, List<File> files) async {
  List<http.MultipartFile> _files = [];
  for (var file in files) {
    _files.add(await http.MultipartFile.fromPath('files', file.path));
  }
  print("_files $_files");
  return sendFiles(url, _files);
}

Future<List<File>> saveTempFile(List<multi.Asset> files) async {
  final temp = await Directory.systemTemp.createTemp();
  List<File> images = List<File>();
  String d = new DateFormat('yyyyMMddHHmmss').format(DateTime.now());
  for (int i = 0; i < files.length; i++) {
    final data = await files[i].getByteData();
    images.add(await File('${temp.path}/img.$d.$i.${files[i].name}').writeAsBytes(data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes)));
  }
  return images;
}

Future<http.Response> postAssetFiles(String url, List<multi.Asset> files) async {
  List<http.MultipartFile> _files = [];
  List<File> images = await saveTempFile(files);
  for (var f in images) {
    _files.add(await http.MultipartFile.fromPath('files', f.path));
    print("postAssetFiles: ${f.path}");
  }
  return sendFiles(url, _files);
}

Future<http.Response> postAssetFile(String url, multi.Asset file) async {
  List<http.MultipartFile> _files = [];
  List<File> images = await saveTempFile([file]);
  for (var f in images) {
    _files.add(await http.MultipartFile.fromPath('file', f.path));
    print("postAssetFile: ${f.path}");
  }
  return sendFiles(url, _files);
}

String mapToQuery(Map<dynamic, dynamic> map) {
  if (map.isEmpty) return '';
  if (map.length == 0) return '';
  List<String> str = <String>[];
  map.forEach((key, value) {
    if (value is List) {
      value.forEach((v) {
        str.add("${Uri.encodeComponent(key.toString())}=${Uri.encodeComponent(v.toString())}");
      });
    } else {
      str.add("${Uri.encodeComponent(key.toString())}=${Uri.encodeComponent(value.toString())}");
    }
  });
  return str.join('&');
}

Future<ResponseMessage> postApi(String url, Map data, {bool needToken = true}) async {
  final response = await post(url, data, needToken: needToken);
  if (response != null && response.statusCode == 200) {
    return ResponseMessage.fromJSON(json.decode(response.body));
  } else {
    print(CustomTrace(StackTrace.current, message: response?.body).toString());
    return ResponseMessage.error();
  }
}

Future<ResponseMessage> patchApi(String url, Map data, {bool needToken = true}) async {
  final response = await patch(url, data, needToken: needToken);
  if (response != null && response.statusCode == 200) {
    return ResponseMessage.fromJSON(json.decode(response.body));
  } else {
    print(CustomTrace(StackTrace.current, message: response?.body).toString());
    return ResponseMessage.error();
  }
}

Future<ResponseMessage> putApi(String url, Map data) async {
  final response = await put(url, data);
  if (response != null && response.statusCode == 200) {
    return ResponseMessage.fromJSON(json.decode(response.body));
  } else {
    print(CustomTrace(StackTrace.current, message: response?.body).toString());
    return ResponseMessage.error();
  }
}

Future<ResponseMessage> getApi(String url, Map data) async {
  final response = await get(url, data);
  if (response != null && response.statusCode == 200) {
    return ResponseMessage.fromJSON(json.decode(response.body));
  } else {
    print(CustomTrace(StackTrace.current, message: response?.body).toString());
    return ResponseMessage.error();
  }
}

Future<ResponseMessage> deleteApi(String url, Map data, {bool needToken = true}) async {
  final response = await delete(url, data, needToken: needToken);
  if (response != null && response.statusCode == 200) {
    return ResponseMessage.fromJSON(json.decode(response.body));
  } else {
    print(CustomTrace(StackTrace.current, message: response?.body).toString());
    return ResponseMessage.error();
  }
}

Future<PageResult<T>> getApiList<T>(String url, Map data) async {
  final response = await get(url, data);
  if (response != null && response.statusCode == 200) {
    return PageResult<T>.fromJSON(json.decode(response.body));
  } else {
    print("url: $url ${CustomTrace(StackTrace.current, message: response?.body).toString()}");
    return PageResult<T>.noData();
  }
}

Future<http.Response> getNone(String url) async {
  final client = new http.Client();
  final header = await headerRequest(needToken: false);
  final response = await client.get(Uri.parse(url), headers: header);
  return await responseAuth('GET', url, {}, response);
}
