import 'package:hab_app/src/models/response.dart';

import '../repositories/callApi.dart' as callApi;

Future<ResponseMessage> sendOtpAuth(int type, {bool isResend = false}) async {
  return callApi.postApi('/Otp/sendOtpAuth', {'isResend': isResend, 'type': type});
}

Future<ResponseMessage> sendOtp(int type, String phoneNumber, {bool isResend = false}) async {
  return callApi.postApi('/Otp/SendOtp', {'isResend': isResend, 'phoneNumber': phoneNumber, 'type': type}, needToken: false);
}

Future<ResponseMessage> verifyOtpAuth(int type, String otp) async {
  return callApi.postApi('/Otp/VerifyOtpAuth', {'otp': otp, 'type': type});
}

Future<ResponseMessage> verifyOtp(int type, String phoneNumber, String otp) async {
  return callApi.postApi('/Otp/VerifyOtp', {'otp': otp, 'phoneNumber': phoneNumber, 'type': type}, needToken: false);
}
