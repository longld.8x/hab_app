import 'dart:convert';

import 'package:hab_app/src/models/course.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:intl/intl.dart' as intl;

import '../repositories/callApi.dart' as callApi;

/// danh sách khóa học
Future<ResponseMessage> getCourses() async {
  //return callApi.getApiList<Course>('/ProductsManagement/GetProducts', {'productType': 1});
  var response = await callApi.get('/ProductsManagement/GetProducts', {'productType': 1});
  if (response != null && response.statusCode == 200) {
    List<Course> _data = json.decode(response.body).map<Course>((json) {
      return Course.fromJSON(json);
    }).toList();
    return ResponseMessage.success(result: _data);
  } else {
    return ResponseMessage.error();
  }
}

/// chi tiết khóa học trước khi mua
Future<ResponseMessage> getCoursePreview(int productId) async {
  final response = await callApi.get('/CoursesManagement/GetCoursePreview', {'productId': productId});
  if (response.statusCode == 200) {
    return ResponseMessage.success(result: CoursePreview.fromJSON(json.decode(response.body)));
  } else {
    return ResponseMessage.error();
  }
}

/// mua khóa học
Future<ResponseMessage> buyCourse(int productId, String coupon, DateTime dateJoin) async {
  Map _map = {'productId': productId}; //yyyyMMddHHmmss

  if (dateJoin != null) _map['requestDate'] = '${new intl.DateFormat('yyyy-MM-dd HH:mm:ss').format(dateJoin)}';
  if (coupon != null) _map['promotionCode'] = coupon;

  print("productId $_map");
  return await callApi.postApi('/OrdersManagement/BuyCourse', _map);
}

/// mua khóa học
Future<ResponseMessage> getCoupon(String code) async {
  return await callApi.postApi('/PromotionManagement/CheckPromotionCode', {'promotionCode': code});
}
