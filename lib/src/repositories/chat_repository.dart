import 'dart:convert';

import 'package:global_configuration/global_configuration.dart';
import 'package:hab_app/src/helpers/helper.dart';
import 'package:hab_app/src/models/chat/messageDto.dart';
import 'package:hab_app/src/models/chat/roomDataDto.dart';
import 'package:hab_app/src/models/chat/roomDto.dart';
import 'package:hab_app/src/models/chat/userChatDto.dart';
import 'package:hab_app/src/models/friendUser.dart';
import 'package:hab_app/src/models/response.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:hab_app/src/repositories/callApi.dart' as callApi;
import 'package:hab_app/src/repositories/socket_repository.dart' as socketRepo;
import 'package:hab_app/src/repositories/user_repository.dart' as userRepo;

/// ================ search chat user
Future<PageResult<FriendUser>> getUsersChat(String keyword, int page, int itemInPage) async {
  var ls = await callApi.getApiList<FriendUser>('/Friendship/GetFriends', {
    'SkipCount': itemInPage * page,
    'MaxResultCount': itemInPage,
    'Filter': keyword != null ? keyword : "",
  });
  if ((ls == null || ls.totalCount == 0) && Helper.validPhonumber(keyword) == "" && (keyword) != userRepo.currentUser.value.phoneNumber) {
    var s = await userRepo.searchUser(keyword);
    if (s.success && s.results != null) {
      User u = s.results;
      var f = new FriendUser(
        friendUserId: u.id,
        friendFullName: u.fullName,
        friendUserName: u.phoneNumber,
        friendAvatar: u.profilePicture,
        friendAcountCode: u.accountCode,
      );
      return PageResult<FriendUser>(totalCount: 1, items: [f]);
    }
  }
  return ls;
}

/// chat api ==============================================================================================================================

/// subscribe firebase
Future<ResponseMessage> subscribe(String token) async {
  final response = await callApi.post('${GlobalConfiguration().getString('api_chat')}/chat-gateway/firebase/device-token', {
    'user_id': userRepo.currentUser.value.id,
    'token': token,
  });
  if (response.statusCode == 200) {
    return ResponseMessage.success();
  }
  return ResponseMessage.error();
}

/// un_subscribe firebase
Future<ResponseMessage> unSubscribe(int userId) async {
  final response = await callApi.delete('${GlobalConfiguration().getString('api_chat')}/chat-gateway/firebase/device-token', {
    'user_id': userId,
  });
  if (response.statusCode == 200) {
    return ResponseMessage.success();
  }
  return ResponseMessage.error();
}

/// ===========================================

/// init room firebase
Future<ResponseMessage> findOrCreateRoom(int user_id1, int user_id2) async {
  final response = await callApi.get('${GlobalConfiguration().getString('api_chat')}/chat-gateway/get-room', {
    'user_id1': user_id1,
    'user_id2': user_id2,
  });
  if (response.statusCode == 200) {
    var jsonMap = (json.decode(response.body));
    if (jsonMap['ErrorCode'] != null) return ResponseMessage.error(message: jsonMap['Message']);
    var _type = (jsonMap['type']).toString();
    var _room = RoomDto.fromJSON(jsonMap['room']);
    if (_type == 'create') {
      socketRepo.addUser(_room.id, [user_id1, user_id2]);
    }
    return ResponseMessage.success(result: _room);
  }
  return ResponseMessage.error(message: 'Vui lòng thử lại trong giây lát.');
}

/// getMessages in room
Future<PageResult<MessageDto>> getMessages(int roomId, int page, {String type = ''}) async {
  Map<String, dynamic> _data = {'page': page};
  if (type != null && type.isNotEmpty) _data['type'] = type;
  final response = await callApi.get('${GlobalConfiguration().getString('api_chat')}/chat-gateway/room/$roomId/messages', _data);
  if (response.statusCode == 200) {
    var jsonMap = json.decode(response.body);
    var _totalPages = jsonMap['totalPages'];
    List<MessageDto> _messages = (jsonMap["messages"] == null && jsonMap["messages"].toString().isNotEmpty)
        ? []
        : jsonMap["messages"].map<MessageDto>((js) {
            return MessageDto.fromJSON(js);
          })?.toList();
    if (_messages != null && _messages.length > 0) {
      _messages.sort((a, b) => a.createdAt.compareTo(b.createdAt));
    }
    return PageResult(totalPage: (_totalPages ?? 0), items: _messages);
  }
  return PageResult(totalPage: 0, items: []);
}

/// getMessages in room
Future<PageResult<MessageDto>> getFileMessages(int roomId, int page) async {
  Map<String, dynamic> _data = {'page': page};
  final response = await callApi.get('${GlobalConfiguration().getString('api_chat')}/chat-gateway/room/$roomId/file-messages', _data);
  if (response.statusCode == 200) {
    var jsonMap = json.decode(response.body);
    List<MessageDto> _messages = (jsonMap["messages"] == null && jsonMap["messages"].toString().isNotEmpty)
        ? []
        : jsonMap["messages"].map<MessageDto>((js) {
            return MessageDto.fromJSON(js);
          })?.toList();
    if (_messages != null && _messages.length > 0) {
      _messages.sort((a, b) => a.createdAt.compareTo(b.createdAt));
    }
    var _count = jsonMap['count'];
    return PageResult(totalCount: _count, items: _messages);
  }
  return PageResult(totalCount: 0, items: []);
}

/// getMembers in room
Future<PageResult<UserChatDto>> getMembers(int roomId, int page) async {
  final response = await callApi.get('${GlobalConfiguration().getString('api_chat')}/chat-gateway/room/$roomId/members', {'page': page});
  if (response.statusCode == 200) {
    var jsonMap = json.decode(response.body);
    var _totalPages = jsonMap['totalPages'];
    var _items = (jsonMap["users"] == null && jsonMap["users"].toString().isNotEmpty)
        ? []
        : jsonMap["users"].map<UserChatDto>((js) {
            return UserChatDto.fromJSON(js);
          })?.toList();

    return PageResult(totalCount: (_totalPages ?? 0), items: _items);
  }
  return PageResult(totalCount: 0, items: []);
}

/// getRooms list
Future<PageResult<RoomDataDto>> getRooms(int page) async {
  final response = await callApi.get('${GlobalConfiguration().getString('api_chat')}/chat-gateway/rooms', {
    'user_id': userRepo.currentUser.value.id,
    'page': page,
  });
  if (response.statusCode == 200) {
    var jsonMap = json.decode(response.body);
    var _totalPages = jsonMap['totalPages'];
    var _items = (jsonMap["rooms"] == null && jsonMap["rooms"].toString().isNotEmpty)
        ? []
        : jsonMap["rooms"].map<RoomDataDto>((js) {
            return RoomDataDto(
              room: RoomDto.fromJSON(js['room']),
              lastMessage: MessageDto.fromJSON(js['lastMessage']),
              user1: js['user1'] != null ? UserChatDto.fromJSON(js['user1']) : null,
              user2: js['user2'] != null ? UserChatDto.fromJSON(js['user2']) : null,
            );
          })?.toList();
    return PageResult(totalPage: (_totalPages ?? 0), items: _items);
  }
  return PageResult(totalPage: 0, items: []);
}

/// getRoom info
Future<RoomDataDto> getRoom(int roomId) async {
  final response = await callApi.get('${GlobalConfiguration().getString('api_chat')}/chat-gateway/room/$roomId/info', {});
  if (response.statusCode == 200) {
    var jsonMap = json.decode(response.body);
    return RoomDataDto(
      room: RoomDto.fromJSON(jsonMap['room']),
      lastMessage: MessageDto.fromJSON(jsonMap['lastMessage']),
      media: int.parse(jsonMap['media'] != null ? jsonMap['media'].toString() : "0"),
      members: int.parse(jsonMap['members'] != null ? jsonMap['members'].toString() : "0"),
      user1: jsonMap['user1'] != null ? UserChatDto.fromJSON(jsonMap['user1']) : null,
      user2: jsonMap['user2'] != null ? UserChatDto.fromJSON(jsonMap['user2']) : null,
    );
  }
  return null;
}

/// getRoom info
addFriend(int userId) async {
  return await callApi.postApi('/Friendship/CreateFriendshipRequest', {'userId': userId});
}
