import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:hab_app/src/models/response.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

import '../repositories/callApi.dart' as callApi;

/// -- addExercise  post
Future<List<String>> uploadAssetFiles(List<Asset> images) async {
  var data = <String>[];
  if (images.length > 0) {
    try {
      final response = await callApi.postAssetFiles('/File/UploadFiles', images);
      var rs = ResponseMessage.fromJSON(json.decode(response.body));
      if (rs.success) data = rs.results.map<String>((e) => e.toString()).toList();
    } catch (e) {
      print("uploadFiles error: $e");
      return data;
    }
  }
  log("uploadAssetFiles ${data.toString()}");
  return data;
}

/// ============== up files
Future<List<String>> uploadFiles(List<File> images) async {
  var data = <String>[];
  if (images.length > 0) {
    try {
      final response = await callApi.postFiles('/File/UploadFiles', images);
      var rs = ResponseMessage.fromJSON(json.decode(response.body));
      if (rs.success) data = rs.results.map<String>((e) => e.toString()).toList();
    } catch (e) {
      print("uploadFiles error: $e");
      return data;
    }
  }
  log("uploadAssetFiles ${data.toString()}");
  return data;
}

/// ============== up file
Future<ResponseMessage> uploadFile(File image) async {
  try {
    final response = await callApi.postFile('/File/UploadFile', image);
    return ResponseMessage.fromJSON(json.decode(response.body));
  } catch (e) {
    print("uploadFile error: $e");
    return ResponseMessage.error();
  }
}
