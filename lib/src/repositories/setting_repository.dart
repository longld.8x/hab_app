import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hab_app/src/models/homeSettingsModel.dart';
import 'package:hab_app/src/models/setting.dart';
import 'package:hab_app/src/models/user.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

ValueNotifier<Setting> setting = new ValueNotifier(Setting());
final navigatorKey = GlobalKey<NavigatorState>();
final scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

Future<Setting> initSettings() async {
  Setting _setting;
  try {
    var prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> map = {
      "app_name": "HAB",
      "hotline": "0985566909",
      "mobile_language": "vi",
      "app_version": "",
      "appVersion": "1.1.1",
      "buildNumber": "1",
      "authenType": "",
    };
    try {
      final PackageInfo info = await PackageInfo.fromPlatform();
      map['appVersion'] = info.version;
      map['buildNumber'] = info.buildNumber;
    } catch (e) {
      map['appVersion'] = "1";
      map['buildNumber'] = "1.1.1";
    }

    // try {
    //   var mapSv = await getAppServerSettings();
    //   map.addAll(mapSv);
    // } catch (e) {}

    map['authenType'] = await getAuthenType();
    await prefs.setString('settings', json.encode(map));
    _setting = Setting.fromJSON(map);
    if (prefs.containsKey('language')) {
      _setting.mobileLanguage.value = Locale(prefs.get('language'), '');
    }
    if (prefs.containsKey('isDark')) {
      _setting.brightness.value =
          prefs.getBool('isDark') ?? false ? Brightness.dark : Brightness.light;
    }

    _setting.homeSettings.value = await getHomeSettings();

    setting.value = _setting;
  } catch (e) {
    print('error setting init');
    setting.value = Setting.fromJSON({});
  }
  // print("_setting : ${setting.value.toMap()}");
  setting.notifyListeners();
  return setting.value;
}

//############################################################################################################################################

Future<void> setAuthenType(String authenType) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (authenType != null) {
    await prefs.setString('__authenType', authenType);
  } else {
    await prefs.remove('__authenType');
  }
}

Future<String> getAuthenType() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey('__authenType')) {
    return await prefs.get('__authenType');
  }
  return "";
}

Future<bool> getBiometricTimeout() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var _oldTime = await prefs.get('__authenBiometricTimeout');
  print("_oldTime: $_oldTime");
  if (_oldTime == null) {
    return true;
  }
  DateTime now = DateTime.now();
  DateTime oldTime = DateTime.parse(_oldTime);
  if (now.difference(oldTime) > Duration(seconds: 60)) {
    return true;
  }
  return false;
}

Future<void> setBiometricTimeout(DateTime date) async {
  print("DateTime: $date");
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (date != null) {
    await prefs.setString('__authenBiometricTimeout', date.toString());
  } else {
    await prefs.remove('__authenBiometricTimeout');
  }
}

Future<void> setAuthenUser(AuthenUser authenUser) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (authenUser != null) {
    var oldUser = await getAuthenUser();
    if (oldUser != null && oldUser.phoneNumber != authenUser.phoneNumber) {
      await setAuthenType(null);
    }
    print("__authenUser ${authenUser.toMap()}");
    await prefs.setString('__authenUser', json.encode(authenUser.toMap()));
  } else {
    await prefs.remove('__authenUser');
  }
}

Future<AuthenUser> getAuthenUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey('__authenUser')) {
    String _authenUser = await prefs.get('__authenUser');
    return AuthenUser.fromJSON(json.decode(_authenUser));
  }
  return null;
}

//############################################################################################################################################
/// setHomeSettings

Future<List<HomeSettingsModel>> initHomeSettings() async {
  List<HomeSettingsModel> data = [
    HomeSettingsModel(id: "calo", name: "Thông tin Calo", active: true),
    HomeSettingsModel(id: "follow", name: "Theo dõi", active: true),
    // HomeSettingsModel(id: "follow_eat", name: "Theo dõi ăn uống", active: false),
    // HomeSettingsModel(id: "follow_water", name: "Theo dõi uống nước", active: true),
    // HomeSettingsModel(id: "follow_gym", name: "Theo dõi tập luyện", active: false),
    HomeSettingsModel(id: "work", name: "Công việc của tôi", active: true),
    HomeSettingsModel(id: "my_courses", name: "Lớp học của tôi", active: true),
    HomeSettingsModel(id: "courses", name: "Danh sách khoá học", active: true),
    //HomeSettingsModel(id: "product", name: "Sản phẩm", active: true),
  ];
  return data;
}

Future<List<HomeSettingsModel>> getHomeSettings() async {
  var prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey('homeSettings')) {
    String _str = await prefs.get('__HomeSettings');
    List<HomeSettingsModel> _data =
        json.decode(_str).map<HomeSettingsModel>((json) {
      return HomeSettingsModel.fromJSON(json);
    }).toList();
    return _data;
  } else {
    return initHomeSettings();
  }
}

Future<void> setHomeSettings(List<HomeSettingsModel> data) async {
  try {
    var prefs = await SharedPreferences.getInstance();
    await prefs.setString('__HomeSettings', json.encode(data));
    setting.value.homeSettings.value = data;
    setting.value.homeSettings.notifyListeners();
  } catch (e) {}
}
//############################################################################################################################################

Future<void> setSearchF1Keyword(List<String> data) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (data.length > 1000) {
    data.removeRange(1000, data.length);
  }
  if (data != null) {
    await prefs.setString('__f1_search', json.encode(data));
  } else {
    await prefs.remove('__f1_search');
  }
}

Future<List<String>> getSearchF1Keyword() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey('__f1_search')) {
    var data = await prefs.get('__f1_search');
    return json.decode(data).map<String>((item) => item.toString()).toList();
  }
  return [];
}
