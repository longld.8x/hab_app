package com.hls.hab_app

import android.app.NotificationChannel
import android.app.NotificationManager
import io.flutter.app.FlutterApplication
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin
import io.flutter.plugins.firebasemessaging.FlutterFirebaseMessagingService
import vn.hunghd.flutterdownloader.FlutterDownloaderPlugin
//import miguelruivo.flutter.plugins.Filepicker

class Application : FlutterApplication(), PluginRegistry.PluginRegistrantCallback {
    override fun onCreate() {
        super.onCreate()
        FlutterFirebaseMessagingService.setPluginRegistrant(this)
    }

    override fun registerWith(registry: PluginRegistry) {
        FirebaseMessagingPlugin.registerWith(registry.registrarFor("io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin"))
        FlutterDownloaderPlugin.registerWith(registry.registrarFor("vn.hunghd.flutterdownloader.FlutterDownloaderPlugin"))
        //Filepicker.registerWith(registry.registrarFor("miguelruivo.flutter.plugins.filepicker"))
    }

    private fun createNotificationChannels() {
        val channelHigh = NotificationChannel(
                CHANNEL_HIGH,
                "HAB Notification Channel",
                NotificationManager.IMPORTANCE_HIGH
        )
        channelHigh.description = "HAB Notification Channel"
        val manager = getSystemService(NotificationManager::class.java)
        manager.createNotificationChannel(channelHigh)

    }

    companion object {
        const val CHANNEL_HIGH = "default"
    }
}